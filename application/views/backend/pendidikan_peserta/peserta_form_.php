<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Tambah Peserta</h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                <form class="form-horizontal" action="<?php echo $controller . '/' . $function_add; ?>" method="POST" id="px-pendidikan_peserta-form">
                    <input type="hidden" name="pendidikan_id" id="px-pendidikan_peserta-form-pendidikan_id" value="<?php echo $this->session->userdata('menu_pendidikan')['pendidikan_id']; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Pilih Peserta</label>
                        <div class="col-md-9 col-xs-12">
                        <table class="table datatable table-bordered select_datatable">
                            <thead>
                                <tr>
                                    <th width="1%" class="text-center"><input name="select_all" value="1" type="checkbox"></th>
                                    <th width="6%" class="text-center">No</th>
                                    <th class="text-center">Nama Legkap</th>
                                    <th class="text-center">Pangkat</th>
                                    <th class="text-center">Golongan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<style type="text/css">
    table.dataTable.select_datatable tbody tr,
    table.dataTable thead th:first-child {
      cursor: pointer;
    }
</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_peserta/pendidikan_peserta_form.js"></script>