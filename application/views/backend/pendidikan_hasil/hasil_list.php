<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Hasil Penilaian</b></h3>              
                </div>
                <div class="panel-body">
                    <table class="table datatable table-bordered">
                        <thead>
                          <tr>
                            <th width="6%" class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $no=0;
                          foreach ($elemen_penilaian->tree as $data_row) { $no++; ?>
                            <tr>
                              <td class="text-center"><?php echo $no; ?></td>
                              <td>>> <?php echo $data_row->name?></td>
                              <td class="text-center" width="15%"><a href="pendidikan_hasil/hasil_bagan?skema_penilaian_id=<?php echo $data_row->id ?>&pendidikan_id=<?php echo $this->session->userdata('menu_pendidikan')['pendidikan_id'] ?>" class="btn btn-success btn-sm">Lihat Ranking</a></td>
                            </tr>
                          <?php 
                            if(!empty($data_row->child->tree)){
                              foreach ($data_row->child->tree as $lvl2) {
                                $no++;
                                echo '<tr>
                                  <td class="text-center">'.$no.'</td>
                                  <td><span style="margin-left:20px"> >> '.$lvl2->name.'</span></td>
                                  <td class="text-center"><a href="pendidikan_hasil/hasil_bagan?skema_penilaian_id='.$lvl2->id.'&pendidikan_id='.$this->session->userdata('menu_pendidikan')['pendidikan_id'].'" class="btn btn-success btn-sm">Lihat Ranking</a></td>
                                  </tr>';
                                if(!empty($lvl2->child->tree)){
                                  foreach ($lvl2->child->tree as $lvl3) {
                                    $no++;
                                    echo '<tr>
                                    <td class="text-center">'.$no.'</td>
                                    <td><span style="margin-left:40px"> >> '.$lvl3->name.'</span></td>
                                    <td class="text-center"><a href="pendidikan_hasil/hasil_bagan?skema_penilaian_id='.$lvl3->id.'&pendidikan_id='.$this->session->userdata('menu_pendidikan')['pendidikan_id'].'" class="btn btn-success btn-sm">Lihat Ranking</a></td>
                                    </tr>';
                                    if(!empty($lvl3->child->tree)){
                                      foreach ($lvl3->child->tree as $lvl4) {
                                      $no++;
                                        echo '<tr>
                                          <td class="text-center">'.$no.'</td>
                                          <td><span style="margin-left:60px"> >> '.$lvl4->name.'</span></td>
                                          <td class="text-center"><a href="pendidikan_hasil/hasil_bagan?skema_penilaian_id='.$lvl4->id.'&pendidikan_id='.$this->session->userdata('menu_pendidikan')['pendidikan_id'].'" class="btn btn-success btn-sm">Lihat Ranking</a></td>
                                          </tr>';
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            
                         } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->


<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- END PAGE PLUGINS -->

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_hasil/hasil_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
