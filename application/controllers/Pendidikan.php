<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan', 'pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $now = date('Y-m-d', now());

        $data['pendidikan'] = $this->model_basic->select_all($this->tbl_pendidikan);
        foreach ($data['pendidikan'] as $data_row) {
            $jenis = $this->model_basic->select_where($this->tbl_jenis_pendidikan, 'id', $data_row->jenis_pendidikan_id);
            if($jenis->num_rows() == 1)
                $data_row->jenis_pendidikan = $jenis->row()->name;
            else
                $data_row->jenis_pendidikan = 'Unknown';

            $bagan = $this->model_basic->select_where($this->tbl_bagan_pendidikan, 'id', $data_row->bagan_pendidikan_id);
            if($bagan->num_rows() == 1)
                $data_row->tahun = $bagan->row()->tahun;
            else
                $data_row->tahun = 'Unknown';

            if($bagan->num_rows() == 1)
                $data_row->tahun = $bagan->row()->tahun;
            else
                $data_row->tahun = 'Unknown';

            if(strtotime($now) < strtotime($data_row->date_end) && strtotime($now) > strtotime($data_row->date_start))
                $data_row->status = 1;
            else
                $data_row->status = 0;
        }

        $data['content'] = $this->load->view('backend/pendidikan/pendidikan_list',$data,true);
        $this->load->view('backend/index',$data);
    }

    function pendidikan_detail($id){
        $pendidikan = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $id);
        if($pendidikan->num_rows() > 0){
            $this->session->set_userdata('menu_pendidikan', array('pendidikan_id'=>$id,'bagan_pendidikan_id'=>$pendidikan->row()->bagan_pendidikan_id));
        }else{
            redirect('admin');
        }
        
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan', 'pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);


        $data['pendidikan'] = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $id)->row();
        $jenis_pendidikan = $this->model_basic->select_where($this->tbl_jenis_pendidikan, 'id', $data['pendidikan']->jenis_pendidikan_id);
        if($jenis_pendidikan->num_rows() == 1)
            $data['jenis_pendidikan'] = $jenis_pendidikan->row()->name;
        else
            $data['jenis_pendidikan'] = 'Unknown';

        $data['content'] = $this->load->view('backend/pendidikan/pendidikan_detail',$data,true);
        $this->load->view('backend/index',$data);
    }

    function pendidikan_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan', 'pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $pendidikan = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $id)->row();
            $data['data'] = $pendidikan;
        } else
            $data['data'] = null;

        $data['bagan_pendidikan'] = $this->model_basic->select_all($this->tbl_bagan_pendidikan);
        $data['jenis_pendidikan'] = $this->model_basic->select_all($this->tbl_jenis_pendidikan);
        $data['content'] = $this->load->view('backend/pendidikan/pendidikan_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan', 'pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_pendidikan);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['status'] = 1;
        $insert['date_created'] = date('Y-m-d H:i:s', now());

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_pendidikan, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Pendidikan '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function pendidikan_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan', 'pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_pendidikan);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['status']);
        unset($update['date_created']);

        $do_update = $this->model_basic->update($this->tbl_pendidikan, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Pendidikan '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function pendidikan_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan', 'pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_pendidikan, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Pendidikan '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function get_data_bagan_pendidikan(){
        $id = $this->input->post('id');
        $bagan_pendidikan = $this->model_basic->select_where($this->tbl_bagan_pendidikan, 'jenis_pendidikan_id', $id);
        if ($bagan_pendidikan->num_rows() != 0) {                
            $this->returnJson(array('status' => 'ok', 'data' => $bagan_pendidikan->result()));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'data not found'));
        }
    }
}