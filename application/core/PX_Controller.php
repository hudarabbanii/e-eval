<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PX_Controller extends CI_Controller {
        
    public function __construct() {

        parent::__construct();
        // DEFAULT TIME ZONE
        date_default_timezone_set('Asia/Jakarta');

        // TABLE 
        $this->tbl_prefix = 'ev_';
        $this->tbl_adm_config = $this->tbl_prefix . 'adm_config';
        $this->tbl_master_data = $this->tbl_prefix . 'master_data';
        $this->tbl_menu = $this->tbl_prefix . 'menu';
        $this->tbl_user = $this->tbl_prefix . 'user';
        $this->tbl_user_log = $this->tbl_prefix . 'user_log_history';
        $this->tbl_useraccess = $this->tbl_prefix . 'useraccess';
        $this->tbl_usergroup = $this->tbl_prefix . 'usergroup';

        //added
        $this->tbl_elemen_penilaian = $this->tbl_prefix . 'elemen_penilaian';
        $this->tbl_bidang = $this->tbl_prefix . 'bidang';
        $this->tbl_jabatan = $this->tbl_prefix . 'jabatan';
        $this->tbl_pangkat = $this->tbl_prefix . 'pangkat';
        $this->tbl_jenis_penilai = $this->tbl_prefix . 'jenis_penilai';
        $this->tbl_penilai = $this->tbl_prefix . 'penilai';
        $this->tbl_pendidikan = $this->tbl_prefix . 'pendidikan';
        $this->tbl_jenis_pendidikan = $this->tbl_prefix . 'jenis_pendidikan';
        $this->tbl_bagan = $this->tbl_prefix . 'bagan';
        $this->tbl_bidang_studi = $this->tbl_prefix . 'bidang_studi';
        $this->tbl_jenis_bidang_studi = $this->tbl_prefix . 'jenis_bidang_studi';
        $this->tbl_bagan_pendidikan = $this->tbl_prefix . 'bagan_pendidikan';
        $this->tbl_bagan_detail = $this->tbl_prefix . 'bagan_detail';
        $this->tbl_pendidikan_peserta = $this->tbl_prefix . 'pendidikan_peserta';
        $this->tbl_pendidikan_penilai = $this->tbl_prefix . 'pendidikan_penilai';
        $this->tbl_jadwal = $this->tbl_prefix . 'jadwal';
        $this->tbl_tim = $this->tbl_prefix . 'tim';
        $this->tbl_jenis_tim = $this->tbl_prefix . 'jenis_tim';
        $this->tbl_nilai_peserta = $this->tbl_prefix . 'nilai_peserta';
        $this->tbl_tim_detail = $this->tbl_prefix . 'tim_detail';
        $this->tbl_instansi = $this->tbl_prefix . 'instansi';
        $this->tbl_notifikasi = $this->tbl_prefix . 'notifikasi';
        $this->tbl_jadwal_penilai = $this->tbl_prefix . 'jadwal_penilai';
        $this->tbl_jadwal_penilai_pengganti = $this->tbl_prefix . 'jadwal_penilai_pengganti';
        $this->tbl_judul_produk = $this->tbl_prefix . 'judul_produk';
        $this->tbl_skema_penilaian = $this->tbl_prefix . 'skema_penilaian';
        $this->tbl_tim_sementara = $this->tbl_prefix . 'tim_sementara';
        $this->tbl_nilai_akhir_peserta = $this->tbl_prefix . 'nilai_akhir_peserta';

        // MODELS
        $this->load->model('model_basic');
        $this->load->model('model_menu');
        $this->load->model('model_user');
        $this->load->model('model_useraccess');
        $this->load->model('model_usergroup');
        $this->load->model('model_master');

        //added
        $this->load->model('model_eval');
        $this->load->model('model_setting_penilai');
        $this->load->model('model_pendidikan');
        $this->load->model('model_pendidikan_penilai_form');
        $this->load->model('model_pendidikan_jadwal_list');
        $this->load->model('model_hasil_bagan_list');
        $this->load->model('model_hasil_bagan_detail');
        $this->load->model('model_pembagian_tim');
        $this->load->model('model_pendidikan_request_pengganti');
        $this->load->model('model_pendidikan_pembagian_tim_form');
        $this->load->model('model_elemen_penilaian_list');
        $this->load->model('model_bagan_pendidikan_detail');
        $this->load->model('model_tim_sementara');
        $this->load->model('model_ranking');
        $this->load->model('model_rev');
                
        // sessions
        if ($this->session->userdata('admin') != FALSE)
            $this->session_admin = $this->session->userdata('admin');
        else {
            $this->session_admin = array(
                'penilai_id' => 0,
                'peserta_id' => 0,
                'admin_id' => 0,
                'username' => 'GUEST',
                'password' => ' ',
                'realname' => 'GUEST',
                'email' => 'GUEST@LOCAL.DEV',
                'id_usergroup' => 0,
                'name_usergroup' => 'GUEST',
                'photo' => 'THUMB.png'
            );
        }
    }

    function get_app_settings() {
        $d_row = $this->model_basic->select_all_limit($this->tbl_adm_config, 1)->row();
        $data['app_id'] = $d_row->id;
        $data['app_title'] = $d_row->title;
        $data['app_desc'] = $d_row->desc;
        $data['app_login_logo'] = $d_row->login_logo;
        $data['app_mini_logo'] = $d_row->mini_logo;
        $data['app_single_logo'] = $d_row->single_logo;
        $data['app_mini_logo'] = $d_row->mini_logo;
        $data['app_favicon_logo'] = $d_row->favicon_logo;

        if(isset($this->session_admin['penilai_id']) && $this->session_admin['penilai_id'] != 0){
        $data['penilai_detail'] = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session_admin['penilai_id'])->row();
        }
        switch ($this->session_admin['id_usergroup']) {
            case '3': //penilai
                $notifikasi = $this->model_eval->get_notifikasi_penilai([]);
                $data['notifikasi'] = $notifikasi->result();
                foreach ($data['notifikasi'] as $data_row) {
                    $data_row->tanggal = $this->tanggal_indo($data_row->tanggal);
                }
                $data['notifikasi_rows'] = $notifikasi->num_rows();
            break;
            case '1': //admin
                $notifikasi = $this->model_eval->get_notifikasi_admin([]);
                $data['notifikasi'] = $notifikasi->result();
                foreach ($data['notifikasi'] as $data_row) {
                    $data_row->tanggal = $this->tanggal_indo($data_row->tanggal);
                }
                $data['notifikasi_rows'] = $notifikasi->num_rows();
            break;
            default:
                $data['notifikasi'] = array();
                $data['notifikasi_rows'] = 0;
            break;
        }
        return $data;
    }

    function get_content_url($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        if (curl_errno($ch)) {
            return FALSE;
        } else {
            curl_close($ch);
        }

        if (!is_string($contents) || !strlen($contents)) {
            return FALSE;
        }
        return $contents;
    }

    function makeThumbnails($updir, $img, $width, $height) {
        $thumbnail_width = $width;
        $thumbnail_height = $height;
        $thumb_beforeword = "thumb";
        $arr_image_details = getimagesize("$updir" . "$img"); // pass id to thumb name
        $original_width = $arr_image_details[0];
        $original_height = $arr_image_details[1];
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);
        if ($arr_image_details[2] == 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        }
        if ($arr_image_details[2] == 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        }
        if ($arr_image_details[2] == 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }
        if ($imgt) {
            $old_image = $imgcreatefrom("$updir" . "$img");
            $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
            imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            $imgt($new_image, "$updir" . "$thumb_beforeword" . "$img");
        }
    }

   function get_function($name, $function) {
        $data['function_name'] = $name;
        $data['function'] = $function;
        $data['function_form'] = $function . '_form';
        $data['function_add'] = $function . '_add';
        $data['function_edit'] = $function . '_edit';
        $data['function_delete'] = $function . '_delete';
        $data['function_get'] = $function . '_get';
        $menu_id = $this->model_basic->select_where($this->tbl_menu, 'target', $function)->row();
        if ($menu_id)
            $data['function_id'] = $menu_id->id;
        else
            $data['function_id'] = 0;
        return $data;
    }

    function get_menu() {
        if($this->session->userdata('menu_pendidikan')){
            $menu = $this->model_menu->get_menu_bar_pendidikan($this->session_admin['id_usergroup']);
            $submenu = $this->model_menu->get_sub_menu_pendidikan($this->session_admin['id_usergroup']);
        }else{
            $menu = $this->model_menu->get_menu_bar($this->session_admin['id_usergroup']);
            $submenu = $this->model_menu->get_sub_menu($this->session_admin['id_usergroup']);
        }
        $data = array();
        foreach ($menu as $m) {
            $data[$m->id_menu] = $m;
            $m->submenu = array();
        }
        foreach ($submenu as $sm) {
            $data[$sm->id_parent]->submenu[] = $sm;
        }
        $data['menu'] = $data;

        $data['count_total_penilai'] = $this->model_basic->get_count($this->tbl_penilai);

        return $data;
    }

    function get_all_menu() {
        $menu = $this->model_basic->select_where_order($this->tbl_menu, 'id_parent', '0', 'orders', 'ASC')->result();
        $submenu = $this->model_basic->select_where_order($this->tbl_menu, 'id_parent >', '0', 'orders', 'ASC')->result();
        $data = array();
        foreach ($menu as $m) {
            $data[$m->id] = $m;
            $m->submenu = array();
        }
        foreach ($submenu as $sm) {
            $data[$sm->id_parent]->submenu[] = $sm;
        }
        return $data;
    }

    function check_login() {
        if ($this->session->userdata('admin') == FALSE) {
            redirect('admin');
        }
        else
            return true;
    }

    function check_userakses($menu_id, $function, $user = 'admin') {
        if ($user == 'admin')
            $group_id = $this->session_admin['id_usergroup'];
        if ($user == 'member')
            $group_id = $this->session->userdata['member']['group_id'];
        $access = $this->model_useraccess->get_useraccess($group_id, $menu_id);
        switch ($function) {
            case 1:
                if ($access->act_read == 1)
                    return TRUE;
                else
                    $this->returnJson(array('status' => 'failed', 'msg' => 'Kamu tidak memiliki akses untuk aksi ini..'));
                break;
            case 2:
                if ($access->act_create == 1)
                    return TRUE;
                else
                    $this->returnJson(array('status' => 'failed', 'msg' => 'Kamu tidak memiliki akses untuk aksi ini..'));
                break;
            case 3:
                if ($access->act_update == 1)
                    return TRUE;
                else
                    $this->returnJson(array('status' => 'failed', 'msg' => 'Kamu tidak memiliki akses untuk aksi ini..'));
                break;
            case 4:
                if ($access->act_delete == 1)
                    return TRUE;
                else
                    $this->returnJson(array('status' => 'failed', 'msg' => 'Kamu tidak memiliki akses untuk aksi ini..'));
                break;
        }
    }

    function delete_temp($folder) {
        if ($this->session->userdata($folder) != FALSE) {
            $temp_folder = $this->session->userdata($folder);
            $files = glob(FCPATH . "assets/uploads/temp/" . $temp_folder . "/{,.}*", GLOB_BRACE);
            foreach ($files as $file) {
                if (is_file($file))
                    @unlink($file);
            }
            @rmdir(FCPATH . "assets/uploads/temp/" . $temp_folder);
            $this->session->unset_userdata($folder);
        }
    }

    function delete_folder($folder) {
        $files = glob(FCPATH . "assets/uploads/" . $folder . "/{,.}*", GLOB_BRACE);
        foreach ($files as $file) {
            if (is_file($file))
                @unlink($file);
        }
        @rmdir(FCPATH . "assets/uploads/" . $folder);
    }

     function format_log() {
        $log['id_log_type'];
        $log['id_user'];
        $log['desc'];
        $log['date_created'];
    }

    function save_log_admin($action, $log_description, $admin_id="") {
        if ($admin_id == "")
            $admin_id = $this->session->userdata['admin']['admin_id'];
        
        $data = array('admin_id' => $admin_id,
            'type_id' => $action,
            'log_description' => $log_description,
            'date_created' => date('Y-m-d H:i:s', now()));
        if ($this->model_basic->insert_all($this->tbl_user_log, $data))
            return true;
        else
            return false;
    }

    function save_log($data) {
        if ($this->model_basic->insert_all($this->tbl_logs, $data))
            return true;
        else
            return false;
    }

    function returnJson($msg) {
        echo json_encode($msg);
        exit;
    }

    function indonesian_currency($number) {
        $result = 'Rp. ' . number_format($number, 0, '', '.');
        return $result;
    }

    function dashboard_filter() {
        $group_id = $this->session_admin['id_usergroup'];
        $controller = $this->model_basic->select_where($this->tbl_usergroup, 'id', $group_id)->row();
        $controller = $controller->usergroup_name;
        $controller = strtolower(str_replace(" ", "_", $controller));
        if ($controller=="super_admin") {
        redirect('admin/dashboard');
        }else{
        redirect('admin_'.$controller.'/dashboard');
        }
    }

    function get_submenu($parent)
    {
        $parent_id = $this->model_basic->select_where($this->tbl_menu, 'target', $parent)->row()->id;
        $submenu = $this->model_eval->get_submenu($parent_id)->result();
        return $submenu;
    }

    function tanggal_indo($date){
        $tanggal = date('j', strtotime($date));
        $bulan = date('n', strtotime($date));
        $tahun = date('Y', strtotime($date));
        $bulan_arr = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $bulan_indo = $bulan_arr[$bulan-1];

        return $tanggal.' '.$bulan_indo.' '.$tahun;
    }

    function password_encode($string){
        $password_hash = password_hash($string,PASSWORD_DEFAULT);

        return $password_hash;
    }

    function password_decode($string){
        $password = password_verify($string,password_hash($string,PASSWORD_DEFAULT));
        if($password)
            return $string;
    }

    function password_verify($string,$hash){
        $verify_result = password_verify($string,$hash);
        
        return $verify_result;
    }
    
}

