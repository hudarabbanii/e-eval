<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pendidikan_jadwal_list extends CI_Model {

	var $column = array('j.tanggal', 'p.nama_lengkap', 't.name', 'ep.name', 'bs.name'); //set column field database for order and search
	var $order = array('j.id' => 'desc'); // default order

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		if($this->session_admin['id_usergroup'] == 3){ //penilai
			$penilai_id = $this->model_basic->select_where($this->tbl_user, 'id', $this->session_admin['admin_id'])->row()->penilai_id;
		}
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$this->db->select('j.*, p.nama_lengkap as penilai, p.gelar_depan as gelar_depan, p.gelar_belakang as gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim');
		$this->db->from($this->tbl_jadwal .' j');
		$this->db->join($this->tbl_jadwal_penilai.' jp', 'j.id = jp.jadwal_id', 'left');
		$this->db->join($this->tbl_penilai.' p', 'p.id = jp.penilai_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = j.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->where('j.pendidikan_id', $pendidikan_id);
		if($this->session_admin['id_usergroup'] == 3)//penilai
			$this->db->where('jp.penilai_id', $penilai_id);
		else
			$this->db->where('jp.status', 1);
		$i = 1;
		$where = "";
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		if($this->session_admin['id_usergroup'] == 3){ //penilai
			$penilai_id = $this->model_basic->select_where($this->tbl_user, 'id', $this->session_admin['admin_id'])->row()->penilai_id;
		}
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$this->db->select('j.*, p.nama_lengkap as penilai, p.gelar_depan as gelar_depan, p.gelar_belakang as gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim');
		$this->db->from($this->tbl_jadwal .' j');
		$this->db->join($this->tbl_jadwal_penilai.' jp', 'j.id = jp.jadwal_id', 'left');
		$this->db->join($this->tbl_penilai.' p', 'p.id = jp.penilai_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = j.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->where('j.pendidikan_id', $pendidikan_id);
		if($this->session_admin['id_usergroup'] == 3)//penilai
			$this->db->where('jp.penilai_id', $penilai_id);
		else
			$this->db->where('jp.status', 1);
		$query = $this->db->get();
		return $query->num_rows();
	}	

	public function get_jadwal_detail_id($id){
		if($this->session_admin['id_usergroup'] == 3){ //penilai
			$penilai_id = $this->model_basic->select_where($this->tbl_user, 'id', $this->session_admin['admin_id'])->row()->penilai_id;
		}
		$this->db->select('j.id as jadwal_id, ep.id as elemen_penilaian_id, t.id as tim_id, bp.id as bagan_pendidikan_id, p.id as pendidikan_id, pn.id as penilai_id, bs.id as bidang_studi_id');
		$this->db->from($this->tbl_jadwal .' j');
		$this->db->join($this->tbl_jadwal_penilai.' jp', 'j.id = jp.jadwal_id', 'left');
		$this->db->join($this->tbl_penilai.' pn', 'pn.id = jp.penilai_id', 'left');
		$this->db->join($this->tbl_pendidikan.' p', 'p.id = j.pendidikan_id', 'left');
		$this->db->join($this->tbl_bagan_pendidikan.' bp', 'bp.id = p.bagan_pendidikan_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = j.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->where('j.id', $id);
		if($this->session_admin['id_usergroup'] == 3)//penilai
			$this->db->where('jp.penilai_id', $penilai_id);
			$query = $this->db->get();
		return $query->row();
	}

	public function get_form2_detail($elemen_penilaian_id,$bagan_pendidikan_id){
		if($this->session_admin['id_usergroup'] == 3){ //penilai
			$penilai_id = $this->model_basic->select_where($this->tbl_user, 'id', $this->session_admin['admin_id'])->row()->penilai_id;
		}
		$this->db->select('j.id as jadwal_id, ep.id as elemen_penilaian_id, t.id as tim_id, bp.id as bagan_pendidikan_id, p.id as pendidikan_id, pn.id as penilai_id, bs.id as bidang_studi_id');
		$this->db->from($this->tbl_jadwal .' j');
		$this->db->join($this->tbl_jadwal_penilai.' jp', 'j.id = jp.jadwal_id', 'left');
		$this->db->join($this->tbl_penilai.' pn', 'pn.id = jp.penilai_id', 'left');
		$this->db->join($this->tbl_pendidikan.' p', 'p.id = j.pendidikan_id', 'left');
		$this->db->join($this->tbl_bagan_pendidikan.' bp', 'bp.id = p.bagan_pendidikan_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = j.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->where('j.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('j.bagan_pendidikan_id', $bagan_pendidikan_id);
		if($this->session_admin['id_usergroup'] == 3)//penilai
			$this->db->where('jp.penilai_id', $penilai_id);
			$query = $this->db->get();
		return $query->row();
	}
}

/* End of file model_site_list.php */
/* Location: ./application/models/model_site_list.php */