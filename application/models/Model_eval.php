<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_eval extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function get_data_skema_penilaian($parent=0){
	   $data = array();
	   $this->db->from($this->tbl_skema_penilaian);
	   $this->db->where('parent_id',$parent);
	   $result = $this->db->get();
	 
	   foreach($result->result() as $row)
	   {
	      	$data[] = array(
	            'id'  			=>$row->id,
	            'parent_id'   	=>$row->parent_id,
	            'name'   		=>$row->name,
	            'bobot_nna'   	=>$row->bobot_nna,
	            'bobot_npa'   	=>$row->bobot_npa,
	            'child'  		=>$this->get_data_skema_penilaian($row->id)
	        );
	   }
	   return $data;
	}

	function get_data_elemen_penilaian($parent=0){
	   $data = array();
	   $this->db->from($this->tbl_elemen_penilaian);
	   $this->db->where('parent_id',$parent);
	   $result = $this->db->get();
	 
	   foreach($result->result() as $row)
	   {
	      	$data[] = array(
	            'id'  			=>$row->id,
	            'parent_id'   	=>$row->parent_id,
	            'name'   		=>$row->name,
	            'tipe_bobot' 	=>$row->tipe_bobot,
	            'tipe_kelompok' =>$row->tipe_kelompok,
	            'tipe_akademis' =>$row->tipe_akademis,
	            'is_show' 		=>$row->is_show, //kegiatan
	            'is_taskap' 	=>$row->is_taskap, //taskap
	            'child'  		=>$this->get_data_elemen_penilaian($row->id)
	        );
	   }
	   return $data;
	}

	function get_data_elemen_penilaian_detail($bagan_id, $parent=0){
	   $data = array();
	   $this->db->from($this->tbl_elemen_penilaian);
	   $this->db->where('parent_id',$parent);
	   $result = $this->db->get();
	 
	   foreach($result->result() as $row)
	   {
	   		$bobot = $this->get_bobot($bagan_id, $row->id);
	      	$data[] = array(
	            'id'  			=>$row->id,
	            'parent_id'   	=>$row->parent_id,
	            'name'   		=>$row->name,
	            'is_show' 		=>$row->is_show,
	            'bobot_nna'   	=>($bobot->num_rows() == 0 ? 0 : $bobot->row()->bobot_nna),
	            'bobot_npa'   	=>($bobot->num_rows() == 0 ? 0 : $bobot->row()->bobot_npa),
	            'isform'		=>$this->check_isform($bagan_id, $row->id),
	            'child'  		=>$this->get_data_elemen_penilaian_detail($bagan_id, $row->id)
	        );
	   }
	   return $data;
	}

	function check_isform($bagan_id, $elemen_id){
		$this->db->from($this->tbl_bagan_detail);
		$this->db->where('elemen_penilaian_id', $elemen_id);
		$this->db->where('bagan_pendidikan_id', $bagan_id);
		return $this->db->get()->num_rows();
	}

	function get_bobot($bagan_id, $elemen_id){
		$this->db->select('bobot_nna, bobot_npa');
		$this->db->from($this->tbl_bagan_detail);
		$this->db->where('elemen_penilaian_id', $elemen_id);
		$this->db->where('bagan_pendidikan_id', $bagan_id);
		return $this->db->get();
	}

	function get_submenu($parent_id){
		$this->db->select('a.*');
		$this->db->from($this->tbl_menu.' a');
		$this->db->join($this->tbl_useraccess.' b', 'b.id_menu = a.id', 'right');
		$this->db->where('a.id_parent', $parent_id);
		$this->db->where('b.act_read', 1);
		$this->db->where('b.id_usergroup', $this->session_admin['id_usergroup']);
		$this->db->order_by('a.orders', 'ASC');
		return $this->db->get();
	}

	function get_bagan_detail_elemen(){
		$this->db->select('bd.id as bagan_detail_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('menu_pendidikan')['pendidikan_id']);
		$this->db->order_by('ep.id', 'ASC');
		return $this->db->get();
	}

	function get_elemen_penilaian_bagan($bagan_pendidikan, $parent, $tipe_akademis){
		$this->db->select('ep.*, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, bd.id as id, bd.urutan as urutan');
		$this->db->from($this->tbl_elemen_penilaian.' ep');
		$this->db->join($this->tbl_bagan_detail.' bd', 'bd.elemen_penilaian_id = ep.id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $bagan_pendidikan);
		$this->db->where('ep.parent_id', $parent);
		// $this->db->where('ep.tipe_akademis', $tipe_akademis);
		$this->db->order_by('ep.tipe_akademis', 'ASC');
		$this->db->order_by('ep.id', 'ASC');
		$this->db->order_by('bd.urutan', 'ASC');
		return $this->db->get();
	}

	function get_notifikasi_penilai($where){
		$this->db->select('a.*, b.tanggal');
		$this->db->from($this->tbl_notifikasi.' a');
		$this->db->join($this->tbl_jadwal.' b', 'b.id = a.jadwal_id');
		if ($this->session_admin['id_usergroup'] == 3) {
			$this->db->where('a.penilai_id', $this->session_admin['penilai_id']);
		}
		$this->db->where('a.status', 0);
		$this->db->where($where);
		$this->db->order_by('a.date_created', 'DESC');
		return $this->db->get();
	}

	function get_notifikasi_admin($where){
		$this->db->select('a.*, b.tanggal');
		$this->db->from($this->tbl_notifikasi.' a');
		$this->db->join($this->tbl_jadwal.' b', 'b.id = a.jadwal_id');
		if ($this->session_admin['id_usergroup'] == 1) {
			$this->db->where('a.penilai_id', 0);
		}
		$this->db->where('a.status', 0);
		$this->db->where($where);
		$this->db->order_by('a.date_created', 'DESC');
		return $this->db->get();
	}
}