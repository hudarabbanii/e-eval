$(document).ready(function(){
	$('.datatable').DataTable({
		"scrollX":true,
		"stateSave": true,
	 	"processing": true,
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"iDisplayLength": 50,
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		}
		],
		"columnDefs": [
		{ 
		    "targets": [0,1,2,3,-1],
		    "orderable": false
		}
		],
		"ordering": false 
	});


	$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-form input[name="id"]').val('');
		$('#px-pendidikan_skema_penilaian-skema_penilaian_list-message-form input[name="id"]').val(id);
	});
	
});