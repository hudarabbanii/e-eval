$(document).ready(function(){
	$('.datatable').DataTable({
	 	"processing": true,
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		}
		],
		"columnDefs": [
		{ 
		    "targets": [0,-1],
		    "orderable": false,
		}
		]
	});

	$('#px-pendidikan-pendidikan_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-pendidikan-pendidikan_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-pendidikan-pendidikan_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-pendidikan-pendidikan_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-pendidikan-pendidikan_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-pendidikan-pendidikan_list-message-form input[name="id"]').val('');
		$('#px-pendidikan-pendidikan_list-message-form input[name="id"]').val(id);
	});
});