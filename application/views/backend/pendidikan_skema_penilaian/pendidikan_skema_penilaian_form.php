<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data</h3>        
                    <button onclick="history.go(-1)" class="btn btn-default pull-right">Kembali</button>                      
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit;
else echo $controller . '/' . $function_add; ?>" method="POST" id="px-pendidikan_skema_penilaian-skema_penilaian-form">
                    <input type="hidden" name="id" id="px-pendidikan_skema_penilaian-skema_penilaian-form-id" value="<?php if ($data) echo $data->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_skema_penilaian-skema_penilaian-form-parent">Parent</label>
                            <div class="col-md-9 col-xs-12">
                                <span class="form-control"><?php echo $parent; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_skema_penilaian-skema_penilaian-form-name">Skema Penilaian</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="name" id="px-pendidikan_skema_penilaian-skema_penilaian-form-name" value="<?php if ($data) echo $data->name; ?>" placeholder="Nama Skema Penilaian" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Elemen Penilaian</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="elemen_penilaian_id" id="px-pendidikan_skema_penilaian-skema_penilaian-form-elemen_penilaian_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($elemen_penilaian as $data_row) { ?>
                                    <option value="<?php echo $data_row['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $data_row['id']) echo 'selected'; ?>><?php echo $data_row['name'] ?></option>
                                        <?php 
                                        if($data_row['child']){
                                        foreach($data_row['child'] as $lvl2) { ?>
                                            <option value="<?php echo $lvl2['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $lvl2['id']) echo 'selected'; ?>><?php echo '&nbsp&nbsp>>'.$lvl2['name'] ?></option>
                                            <?php 
                                            if($lvl2['child']){
                                                foreach($lvl2['child'] as $lvl3) { ?>
                                                <option value="<?php echo $lvl3['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $lvl3['id']) echo 'selected'; ?>><?php echo '&nbsp&nbsp&nbsp&nbsp>>>'.$lvl3['name'] ?></option>
                                                <?php 
                                                }
                                            }
                                        } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_skema_penilaian-skema_penilaian-form-bobot_nna">Bobot NNA</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="bobot_nna" id="px-pendidikan_skema_penilaian-skema_penilaian-form-bobot_nna" value="<?php if ($data) echo $data->bobot_nna; ?>" placeholder="Bobot NNA" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_skema_penilaian-skema_penilaian-form-bobot_npa">Bobot NPA</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="bobot_npa" id="px-pendidikan_skema_penilaian-skema_penilaian-form-bobot_npa" value="<?php if ($data) echo $data->bobot_npa; ?>" placeholder="Bobot NPA" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_skema_penilaian-skema_penilaian-form-is_raport">Apakah elemen ini adalah raport?</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="form-control" name="is_raport" id="px-pendidikan_skema_penilaian-skema_penilaian-form-is_raport" value="<?php if ($data) echo $data->is_raport; ?>" required>
                                    <option value="0">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_skema_penilaian-skema_penilaian-form-is_other_raport">Apakah elemen ini akan dimasukkan ke raport lain?</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="form-control" name="is_other_raport" id="px-pendidikan_skema_penilaian-skema_penilaian-form-is_other_raport" value="<?php if ($data) echo $data->is_other_raport; ?>" required>
                                    <option value="0" <?php if ($data) if($data->is_other_raport == 0) echo 'selected'; ?>>Tidak</option>
                                    <?php foreach($skema_penilaian as $data_row) { ?>
                                    <option value="<?php echo $data_row['id'] ?>" <?php if ($data) if($data->is_other_raport == $data_row['id']) echo 'selected'; ?>><?php echo $data_row['name'] ?></option>
                                        <?php 
                                        if($data_row['child']){
                                        foreach($data_row['child'] as $lvl2) { ?>
                                            <option value="<?php echo $lvl2['id'] ?>" <?php if ($data) if($data->is_other_raport == $lvl2['id']) echo 'selected'; ?>><?php echo '&nbsp&nbsp>>'.$lvl2['name'] ?></option>
                                            <?php 
                                            if($lvl2['child']){
                                                foreach($lvl2['child'] as $lvl3) { ?>
                                                <option value="<?php echo $lvl3['id'] ?>" <?php if ($data) if($data->is_other_raport == $lvl3['id']) echo 'selected'; ?>><?php echo '&nbsp&nbsp&nbsp&nbsp>>>'.$lvl3['name'] ?></option>
                                                <?php 
                                                }
                                            }
                                        } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>              
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>
<script type="text/javascript" src="assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_skema_penilaian/pendidikan_skema_penilaian_form.js"></script>