<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_request_pengganti extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_request_pengganti', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $this->pendidikan_request_pengganti_list();
    }

    function pendidikan_request_pengganti_list() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_request_pengganti');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        // $data['request_list'] = $this->model_pendidikan_request_pengganti->select_all($this->session->userdata('menu_pendidikan')['pendidikan_id']);
        // var_dump($data['request_list']);
        // die;

        $data['content'] = $this->load->view('backend/pendidikan_request_pengganti/request_pengganti_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    public function ajax_pendidikan_request_pengganti_list()
    {
        $list = $this->model_pendidikan_request_pengganti->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;

            $row = array();
            $row[] = $no;
            $row[] = $this->tanggal_indo($data_row->tanggal);
            $row[] = $data_row->tim;
            $row[] = $data_row->elemen_penilaian;
            $row[] = $data_row->bidang_studi;
            $row[] = ($data_row->penilai_gelar_depan != "" ? $data_row->penilai_gelar_depan.' ' : '').$data_row->penilai_nama_lengkap.($data_row->penilai_gelar_belakang != "" ? ', '.$data_row->penilai_gelar_belakang : '');
            $row[] = ($data_row->pengganti_gelar_depan != "" ? $data_row->pengganti_gelar_depan.' ' : '').$data_row->pengganti_nama_lengkap.($data_row->pengganti_gelar_belakang != "" ? ', '.$data_row->pengganti_gelar_belakang : '');
            $row[] = $data_row->user_remark;
            if($data_row->status == 0){
            $row[] = '<span id="status_text_0" class="btn btn-xs btn-warning">Belum Diproses</span><span id="status_text_1" class="btn btn-xs btn-success hidden">Sudah Diproses</span>';
            $row[] = '<button id="btn-status" class="btn btn-xs btn-success" data-id="'.$data_row->id.'" data-status="1" data-jadwal="'.$data_row->jadwal_id.'" data-penilai="'.$data_row->penilai_pengganti_id.'" data-admin="'.$this->session_admin['admin_id'].'">Setujui</button>';
            }else{
            $row[] = '<span class="btn btn-xs btn-success">Sudah Diproses</span>';
            $row[] = '<button class="btn btn-xs btn-success" disabled>Setujui</button>';
            }
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_pendidikan_request_pengganti->count_all(),
                        "recordsFiltered" => $this->model_pendidikan_request_pengganti->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function ajax_status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $admin_id = $this->input->post('admin_id');
        $penilai_pengganti_id = $this->input->post('penilai_pengganti_id');
        $jadwal_id = $this->input->post('jadwal_id');

        $do_update = $this->model_basic->update($this->tbl_jadwal_penilai_pengganti, ['status'=>$status, 'admin_id'=>$admin_id], 'id', $id);

        $insert['jadwal_id'] = $jadwal_id;
        $insert['penilai_id'] = $penilai_pengganti_id;
        $insert['status'] = 0;
        $insert['is_pengganti'] = $id;
        $do_insert_jadwal_penilai = $this->model_basic->insert_all($this->tbl_jadwal_penilai, $insert);

        if($do_insert_jadwal_penilai){
            //update notifications
            $notifikasi = $this->model_basic->select_where($this->tbl_notifikasi, 'jadwal_id', $jadwal_id);
            if ($notifikasi->num_rows() != 0){
            switch ($this->session_admin['id_usergroup']) {
                case '1': //penilai
                    $update = array('status' => 1, 'date_read' => date('Y-m-d H:i:s', now()));
                    $where = array('jadwal_id' => $jadwal_id);
                break;
            }
            $this->model_basic->update_where_array($this->tbl_notifikasi, $update, $where);
            }

            $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $jadwal_id);
            if($jadwal->num_rows() > 0){
                $data['id'] = $jadwal->row()->id;
                $data['penilai_id'] = $penilai_pengganti_id;
                $data['pendidikan_id'] = $jadwal->row()->pendidikan_id;
                $data['bidang_studi_id'] = $jadwal->row()->bidang_studi_id;
                $this->insert_notifications_jadwal($data);
                $this->returnJson(array('status' => 'ok', 'status_now'=>$status));
            }
        }else{
            $this->returnJson(array('status' => 'ok', 'status_now'=>$status));
        }
    }

    function insert_notifications_jadwal($data){
        //pendidikan
        $get_pendidikan = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $data['pendidikan_id']);
        if($get_pendidikan->num_rows() == 1)
            $pendidikan = $get_pendidikan->row()->name;
        else
            $pendidikan = 'Unknown';
        //bidangstudi
        $get_bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $data['bidang_studi_id']);
        if($get_bidang_studi->num_rows() == 1)
            $bidang_studi = $get_bidang_studi->row()->name;
        else
            $bidang_studi = 'Unknown';
        
        $url = 'pendidikan_jadwal/pendidikan_jadwal_detail_form/'.$data['id'];

        $insert = array(
            'jadwal_id' => $data['id'],
            'penilai_id' => $data['penilai_id'],
            'pendidikan' => $pendidikan,
            'bidang_studi' => $bidang_studi,
            'url' => $url,
            'date_created' => date('Y-m-d H:i:s', now())
        );
        $this->model_basic->insert_all($this->tbl_notifikasi, $insert);
    }
}