<style type="text/css">
  .alert-top{
      position: fixed;
      top: 5px; 
      left:2%;
      width: 96%;
      z-index: 9999;
  }
</style>
<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Elemen Penilaian</h3>
                    <a class="btn btn-success pull-right btn-add" href="<?php echo $controller . '/' . $function_form; ?>"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-2"><label class="control-label pull-right">Pilih Parent</label></div>
                    <div class="col-md-8">
                      <select class="form-control" id="px-setting_pendidikan-elemen_penilaian_list-dropdown">
                          <option value="all">Semua Elemen Penilaian</option>
                        <?php foreach ($dropdown as $data_row) { ?>
                          <option value="<?php echo $data_row->id ?>" <?php if(isset($choosen)) if($choosen==$data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="panel-body" id="px-setting_pendidikan-elemen_penilaian_list-table">
                    <div class="alert alert-success alert-top" style="display: none"><strong>Success! </strong><span></span></div>
                    <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                    <div class="alert alert-danger alert-top" style="display: none"><strong>Failed! </strong><span></span></div>
                    <table class="table datatable table-bordered" style="width: 100%">
                        <thead>
                          <tr>
                            <th width="3%" class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Form Kegiatan</th>
                            <th class="text-center">TASKAP</th>
                            <th class="text-center">Tipe Akademis</th>
                            <th class="text-center">Tipe Bobot</th>
                            <th width="10%" class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $no=0;
                          foreach ($elemen_penilaian as $data_row) { $no++; ?>
                            <tr>
                              <td class="text-center"><?php echo $no; ?></td>
                              <td>>> <?php echo $data_row['name']?></td>
                              <td class="text-center"><?php echo ($data_row['is_show']==0 ? "<button class='btn btn-warning btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='0' data-id='".$data_row['id']."'><i class='fa fa-times'></i></button>" : "<button class='btn btn-success btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='1' data-id='".$data_row['id']."'><i class='fa fa-check'></i></button>"); ?></td>
                              <td class="text-center"><?php echo ($data_row['is_taskap']==0 ? "<button class='btn btn-warning btn-change_is_taskap btn-xs' data-is_taskap='0' data-id='".$data_row['id']."'><i class='fa fa-times'></i></button>" : "<button class='btn btn-success btn-change_is_taskap btn-xs' data-is_taskap='1' data-id='".$data_row['id']."'><i class='fa fa-check'></i></button>"); ?></td>
                              <td class="text-center"><?php echo ($data_row['tipe_akademis']==0 ? "<button class='btn btn-primary btn-change_akademis btn-xs' data-akademis='0' data-id='".$data_row['id']."'>Akademis</button>" : "<button class='btn btn-warning btn-change_akademis btn-xs' data-akademis='1' data-id='".$data_row['id']."'>Non-Akademis</button>"); ?></td>
                              <td class="text-center"><?php echo ($data_row['tipe_bobot']==0 ? "<button class='btn btn-primary btn-change_bobot btn-xs' data-bobot='0' data-id='".$data_row['id']."'>Persen</button>" : "<button class='btn btn-warning btn-change_bobot btn-xs' data-bobot='1' data-id='".$data_row['id']."'>Desimal</button>"); ?></td>
                              <td class="text-right">
                                  <form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                                      <input type="hidden" name="id" value="<?php echo $data_row['id'] ?>">
                                      <a class="btn btn-xs btn-success" href="<?php echo $controller . '/sub_elemen_penilaian_form/' . $data_row['id']; ?>"><i class="fa fa-plus"></i></a>
                                      <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                      <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="<?php echo $data_row['id'] ?>"><i class="fa fa-trash-o"></i></button>
                                  </form>
                              </td>
                            </tr>
                          <?php 
                            if($data_row['child']){
                              foreach ($data_row['child'] as $lvl2) {
                                $no++;
                                echo '<tr>
                                  <td class="text-center">'.$no.'</td>
                                  <td style="padding-left:30px"><span> >> '.$lvl2['name'].'</span></td>
                                  <td class="text-center">'.($lvl2['is_show']==0 ? "<span class='btn btn-warning btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='0' data-id='".$lvl2['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='1' data-id='".$lvl2['id']."'><i class='fa fa-check'></i></span>").'</td>
                                  <td class="text-center">'.($lvl2['is_taskap']==0 ? "<span class='btn btn-warning btn-change_is_taskap btn-xs' data-is_taskap='0' data-id='".$lvl2['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_is_taskap btn-xs' data-is_taskap='1' data-id='".$lvl2['id']."'><i class='fa fa-check'></i></span>").'</td>
                                  <td class="text-center">'.($lvl2['tipe_akademis']==0 ? "<span class='btn btn-primary btn-change_akademis btn-xs' data-akademis='0' data-id='".$lvl2['id']."'>Akademis</span>" : "<span class='btn btn-warning btn-change_akademis btn-xs' data-akademis='1' data-id='".$lvl2['id']."'>Non-Akademis</span>").'</td>
                                  <td class="text-center">'.($lvl2['tipe_bobot']==0 ? "<button class='btn btn-primary btn-change_bobot btn-xs' data-bobot='0' data-id='".$lvl2['id']."'>Persen</button>" : "<button class='btn btn-warning btn-change_bobot btn-xs' data-bobot='1' data-id='".$lvl2['id']."'>Desimal</button>").'</td>
                                  <td class="text-right">
                                      <form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                                          <input type="hidden" name="id" value="'.$lvl2['id'].'">
                                          <a class="btn btn-xs btn-success" href="'.$controller . '/sub_elemen_penilaian_form/'.$lvl2['id'].'"><i class="fa fa-plus"></i></a>
                                          <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                          <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="'.$lvl2['id'].'"><i class="fa fa-trash-o"></i></button>
                                      </form>
                                  </td>
                                  </tr>';
                                  if($lvl2['child']){
                                  foreach ($lvl2['child'] as $lvl3) {
                                    $no++;
                                    echo '<tr>
                                    <td class="text-center">'.$no.'</td>
                                    <td style="padding-left:60px"><span> >> '.$lvl3['name'].'</span></td>
                                    <td class="text-center">'.($lvl3['is_show']==0 ? "<span class='btn btn-warning btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='0' data-id='".$lvl3['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='1' data-id='".$lvl3['id']."'><i class='fa fa-check'></i></span>").'</td>
                                    <td class="text-center">'.($lvl3['is_taskap']==0 ? "<span class='btn btn-warning btn-change_is_taskap btn-xs' data-is_taskap='0' data-id='".$lvl3['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_is_taskap btn-xs' data-is_taskap='1' data-id='".$lvl3['id']."'><i class='fa fa-check'></i></span>").'</td>
                                    <td class="text-center">'.($lvl3['tipe_akademis']==0 ? "<span class='btn btn-primary btn-change_akademis btn-xs' data-akademis='0' data-id='".$lvl3['id']."'>Akademis</span>" : "<span class='btn btn-warning btn-change_akademis btn-xs' data-akademis='1' data-id='".$lvl3['id']."'>Non-Akademis</span>").'</td>
                                    <td class="text-center">'.($lvl3['tipe_bobot']==0 ? "<button class='btn btn-primary btn-change_bobot btn-xs' data-bobot='0' data-id='".$lvl3['id']."'>Persen</button>" : "<button class='btn btn-warning btn-change_bobot btn-xs' data-bobot='1' data-id='".$lvl3['id']."'>Desimal</button>").'</td>
                                    <td class="text-right">
                                        <form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                                            <input type="hidden" name="id" value="'.$lvl3['id'].'">
                                            <a class="btn btn-xs btn-success" href="'.$controller . '/sub_elemen_penilaian_form/'.$lvl3['id'].'"><i class="fa fa-plus"></i></a>
                                            <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="'.$lvl3['id'].'"><i class="fa fa-trash-o"></i></button>
                                        </form>
                                    </td>
                                    </tr>';
                                    if($lvl3['child']){
                                      foreach ($lvl3['child'] as $lvl4) {
                                      $no++;
                                        echo '<tr>
                                          <td class="text-center">'.$no.'</td>
                                          <td style="padding-left:90px"><span> >> '.$lvl4['name'].'</span></td>
                                          <td class="text-center">'.($lvl4['is_show']==0 ? "<span class='btn btn-warning btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='0' data-id='".$lvl4['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='1' data-id='".$lvl4['id']."'><i class='fa fa-check'></i></span>").'</td>
                                          <td class="text-center">'.($lvl4['is_taskap']==0 ? "<span class='btn btn-warning btn-change_is_taskap btn-xs' data-is_taskap='0' data-id='".$lvl4['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_is_taskap btn-xs' data-is_taskap='1' data-id='".$lvl4['id']."'><i class='fa fa-check'></i></span>").'</td>
                                          <td class="text-center">'.($lvl4['tipe_akademis']==0 ? "<span class='btn btn-primary btn-change_akademis btn-xs' data-akademis='0' data-id='".$lvl4['id']."'>Akademis</span>" : "<span class='btn btn-warning btn-change_akademis btn-xs' data-akademis='1' data-id='".$lvl4['id']."'>Non-Akademis</span>").'</td>
                                          <td class="text-center">'.($lvl4['tipe_bobot']==0 ? "<button class='btn btn-primary btn-change_bobot btn-xs' data-bobot='0' data-id='".$lvl4['id']."'>Persen</button>" : "<button class='btn btn-warning btn-change_bobot btn-xs' data-bobot='1' data-id='".$lvl4['id']."'>Desimal</button>").'</td>
                                          <td class="text-right">
                                              <form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                                                  <input type="hidden" name="id" value="'.$lvl4['id'].'">
                                                  <a class="btn btn-xs btn-success" href="'.$controller . '/sub_elemen_penilaian_form/'.$lvl4['id'].'"><i class="fa fa-plus"></i></a>
                                                  <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                                  <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="'.$lvl4['id'].'"><i class="fa fa-trash-o"></i></button>
                                              </form>
                                          </td>
                                          </tr>';
                                          if($lvl4['child']){
                                          foreach ($lvl4['child'] as $lvl5) {
                                          $no++;
                                            echo '<tr>
                                              <td class="text-center">'.$no.'</td>
                                              <td style="padding-left:120px"><span> >> '.$lvl5['name'].'</span></td>
                                              <td class="text-center">'.($lvl5['is_show']==0 ? "<span class='btn btn-warning btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='0' data-id='".$lvl5['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='1' data-id='".$lvl5['id']."'><i class='fa fa-check'></i></span>").'</td>
                                              <td class="text-center">'.($lvl5['is_taskap']==0 ? "<span class='btn btn-warning btn-change_is_taskap btn-xs' data-is_taskap='0' data-id='".$lvl5['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_is_taskap btn-xs' data-is_taskap='1' data-id='".$lvl5['id']."'><i class='fa fa-check'></i></span>").'</td>
                                              <td class="text-center">'.($lvl5['tipe_akademis']==0 ? "<span class='btn btn-primary btn-change_akademis btn-xs' data-akademis='0' data-id='".$lvl5['id']."'>Akademis</span>" : "<span class='btn btn-warning btn-change_akademis btn-xs' data-akademis='1' data-id='".$lvl5['id']."'>Non-Akademis</span>").'</td>
                                              <td class="text-center">'.($lvl5['tipe_bobot']==0 ? "<button class='btn btn-primary btn-change_bobot btn-xs' data-bobot='0' data-id='".$lvl5['id']."'>Persen</button>" : "<button class='btn btn-warning btn-change_bobot btn-xs' data-bobot='1' data-id='".$lvl5['id']."'>Desimal</button>").'</td>
                                              <td class="text-right">
                                                  <form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                                                      <input type="hidden" name="id" value="'.$lvl5['id'].'">
                                                      <a class="btn btn-xs btn-success" href="'.$controller . '/sub_elemen_penilaian_form/'.$lvl5['id'].'"><i class="fa fa-plus"></i></a>
                                                      <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                                      <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="'.$lvl5['id'].'"><i class="fa fa-trash-o"></i></button>
                                                  </form>
                                              </td>
                                              </tr>';
                                              if($lvl5['child']){
                                              foreach ($lvl5['child'] as $lvl6) {
                                              $no++;
                                                echo '<tr>
                                                  <td class="text-center">'.$no.'</td>
                                                  <td style="padding-left:150px"><span> >> '.$lvl6['name'].'</span></td>
                                                  <td class="text-center">'.($lvl6['is_show']==0 ? "<span class='btn btn-warning btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='0' data-id='".$lvl6['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_mata_kegiatan btn-xs' data-mata_kegiatan='1' data-id='".$lvl6['id']."'><i class='fa fa-check'></i></span>").'</td>
                                                  <td class="text-center">'.($lvl6['is_taskap']==0 ? "<span class='btn btn-warning btn-change_is_taskap btn-xs' data-is_taskap='0' data-id='".$lvl6['id']."'><i class='fa fa-times'></i></span>" : "<span class='btn btn-success btn-change_is_taskap btn-xs' data-is_taskap='1' data-id='".$lvl6['id']."'><i class='fa fa-check'></i></span>").'</td>
                                                  <td class="text-center">'.($lvl6['tipe_akademis']==0 ? "<span class='btn btn-primary btn-change_akademis btn-xs' data-akademis='0' data-id='".$lvl6['id']."'>Akademis</span>" : "<span class='btn btn-warning btn-change_akademis btn-xs' data-akademis='1' data-id='".$lvl6['id']."'>Non-Akademis</span>").'</td>
                                                  <td class="text-center">'.($lvl6['tipe_bobot']==0 ? "<button class='btn btn-primary btn-change_bobot btn-xs' data-bobot='0' data-id='".$lvl6['id']."'>Persen</button>" : "<button class='btn btn-warning btn-change_bobot btn-xs' data-bobot='1' data-id='".$lvl6['id']."'>Desimal</button>").'</td>
                                                  <td class="text-right">
                                                      <form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                                                          <input type="hidden" name="id" value="'.$lvl6['id'].'">
                                                          <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                                          <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="'.$lvl6['id'].'"><i class="fa fa-trash-o"></i></button>
                                                      </form>
                                                  </td>
                                                  </tr>';
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            
                         } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-setting_pendidikan-elemen_penilaian_list-message-box" class="message-box message-box-warning animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/' . $function_delete; ?>" method="post" id="px-setting_pendidikan-elemen_penilaian_list-message-form">
                <input type="hidden" name="id">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin menghapus data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-danger btn-lg pull-right" type="submit">Hapus</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- //akademis -->
<div id="px-setting_pendidikan-change_akademis-message-box" class="message-box message-box-info animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/ajax_ubah_tipe_akademis'; ?>" method="post" id="px-setting_pendidikan-change_akademis-message-form">
                <input type="hidden" name="elemen_penilaian_id">
                <input type="hidden" name="tipe_akademis">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin mengubah data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-primary btn-lg pull-right" type="submit">Ubah</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- //bobot -->
<div id="px-setting_pendidikan-change_bobot-message-box" class="message-box message-box-info animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/ajax_ubah_tipe_bobot'; ?>" method="post" id="px-setting_pendidikan-change_bobot-message-form">
                <input type="hidden" name="elemen_penilaian_id">
                <input type="hidden" name="tipe_bobot">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin mengubah data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-primary btn-lg pull-right" type="submit">Ubah</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- END PAGE PLUGINS -->

<script type="text/javascript" src="assets/backend_assets/page/setting_pendidikan/elemen_penilaian_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
