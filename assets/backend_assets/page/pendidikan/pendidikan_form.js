$(document).ready(function(){
    $("select.select2").select2();

    $('#px-pendidikan-form').validate({
        rules: {
            id: {
                required: true
            },
            name: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-pendidikan-form .alert-warning').removeClass('hidden');
            $('#px-pendidikan-form .alert-success').addClass('hidden');
            $('#px-pendidikan-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-pendidikan-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-pendidikan-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-pendidikan-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    $('#px-pendidikan-form-jenis_pendidikan_id').change(function(){
        var id = $(this).val();
        $.ajax({
            url:'pendidikan/get_data_bagan_pendidikan',
            type:'POST',
            dataType:'json',
            data : {
                'id':id
            },
            success: function(response)
            {
                if(response.status == 'ok')
                {
                    $('#px-pendidikan-form-bagan_pendidikan_id').html("");
                    $('#px-pendidikan-form-bagan_pendidikan_id').append('<option value="">-- Pilih Bagan Pendidikan --</option>');
                    $.each(response.data, function(i, data){
                    {
                        $('#px-pendidikan-form-bagan_pendidikan_id').append('<option value="'+data.id+'">'+data.name+'</option>');
                    }
                    });
                }
                            
                        
            }
        });
    });
});

