<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<!-- <div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div> -->

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Hasil Penilaian Peserta : <?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></h3>
                </div>
                <ul class="nav nav-tabs faq-cat-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">Blok I</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Blok II</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Blok III</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Blok IV</a></li>
                </ul>
                <div class="tab-content faq-cat-content">
                    <div class="tab-pane active in fade" id="tab-1">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h3>NILAI AKADEMIK DAN NILAI NON AKADEMIK PADA BLOK I <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                                    <hr>
                                </div>
                                <div class="col-md-11 col-md-offset-1">
                                    <table class="table table-condensed no_border">
                                        <tr>
                                            <td>Nama Peserta</td>
                                            <td>:</td>
                                            <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td><?php echo $peserta->pangkat; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Tutor</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <table class="table table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th rowspan="3" width="6%" class="text-center" style="vertical-align: middle">NO</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">KETERANGAN</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">RANK</th>
                                        <th colspan="4" class="text-center" style="vertical-align: middle">NILAI</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-center" style="vertical-align: middle">PESERTA</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERTINGGI</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERENDAH</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" style="vertical-align: middle">NILAI</th>
                                        <th class="text-center" style="vertical-align: middle">RATA2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7"><b>NILAI AKADEMIK</b></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>NAPK OFF CAMPUS</td>
                                        <td class="text-center"><?php echo ($rank_npa->offcampus->rank == 0 ? 1 : $rank_npa->offcampus->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->offcampus->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->avg_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->max_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->min_npa,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->offcampus->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->offcampus->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->offcampus->rank_npa[$index]->rank == 0 ? 1 : $data_blok1->offcampus->rank_npa[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->nilai_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->avg_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->max_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->min_npa,3); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>NAPK ON CAMPUS (BLOK I)</td>
                                        <td class="text-center"><?php echo ($rank_npa->blok1->rank == 0 ? 1 : $rank_npa->blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->avg_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->max_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->min_npa,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->blok1->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->blok1->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->blok1->rank_npa[$index]->rank == 0 ? 1 : $data_blok1->blok1->rank_npa[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->avg_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->max_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->min_npa,3); ?></td>
                                        </tr>
                                    <?php 
                                    $index++;
                                    } ?>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>NAA BLOK I</td>
                                        <td class="text-center"><?php echo ($rank_npa->hasil_akhir_blok1->rank == 0 ? 1 : $rank_npa->hasil_akhir_blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->avg_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->max_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->min_npa,3); ?></td>
                                    </tr>


                                    <tr>
                                        <td colspan="7"><b>NILAI NON AKADEMIK</b></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>NNAPK OFF CAMPUS</td>
                                        <td class="text-center"><?php echo ($rank_nna->offcampus->rank == 0 ? 1 : $rank_nna->offcampus->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->offcampus->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->avg_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->max_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->min_nna,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->offcampus->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->offcampus->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->offcampus->rank_nna[$index]->rank == 0 ? 1 : $data_blok1->offcampus->rank_nna[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->nilai_non_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->avg_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->max_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->min_nna,3); ?></td>
                                        </tr>
                                    <?php $index++; } ?>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>NNAPK ON CAMPUS (BLOK I)</td>
                                        <td class="text-center"><?php echo ($rank_nna->blok1->rank == 0 ? 1 : $rank_nna->blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->avg_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->max_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->min_nna,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->blok1->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->blok1->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->blok1->rank_nna[$index]->rank == 0 ? 1 : $data_blok1->blok1->rank_nna[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_non_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->avg_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->max_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->min_nna,3); ?></td>
                                        </tr>
                                    <?php $index++; } ?>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>NNAA BLOK I</td>
                                        <td class="text-center"><?php echo ($rank_nna->hasil_akhir_blok1->rank == 0 ? 1 : $rank_nna->hasil_akhir_blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->nilai_akhir_non_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->avg_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->max_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->min_nna,3); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="well">
                                Catatan:
                                <ol type="a">
                                <li>Klasifikasi "TERBATAS".</li>
                                <li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
                                <li>Mohon tidak digandakan/diperbanyak.</li>
                                <li>Mohon dapatnya dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
                                <li>Lembar asli stempel basah dan tanda tangan Direvdik.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-2">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h3>NILAI AKADEMIK DAN NILAI NON AKADEMIK PADA BLOK I <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                                    <hr>
                                </div>
                                <div class="col-md-11 col-md-offset-1">
                                    <table class="table table-condensed no_border">
                                        <tr>
                                            <td>Nama Peserta</td>
                                            <td>:</td>
                                            <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td><?php echo $peserta->pangkat; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Tutor</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <table class="table table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th rowspan="3" width="6%" class="text-center" style="vertical-align: middle">NO</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">KETERANGAN</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">RANK</th>
                                        <th colspan="4" class="text-center" style="vertical-align: middle">NILAI</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-center" style="vertical-align: middle">PESERTA</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERTINGGI</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERENDAH</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" style="vertical-align: middle">NILAI</th>
                                        <th class="text-center" style="vertical-align: middle">RATA2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7"><b>NILAI AKADEMIK</b></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>NAA (BLOK I)</td>
                                        <td class="text-center"><?php echo ($rank_npa->na_blok1->rank == 0 ? 1 : $rank_npa->na_blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok2->na_blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->avg_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->max_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->min_npa,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->na_blok1->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->na_blok1->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->na_blok1->rank_npa[$index]->rank == 0 ? 1 : $data_blok1->na_blok1->rank_npa[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->na_blok1->nilai_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[$index]->avg_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[$index]->max_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[$index]->min_npa,3); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>NAPK (BLOK II)</td>
                                        <td class="text-center"><?php echo ($rank_npa->blok1->rank == 0 ? 1 : $rank_npa->blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->avg_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->max_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->min_npa,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->blok1->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->blok1->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->blok1->rank_npa[$index]->rank == 0 ? 1 : $data_blok1->blok1->rank_npa[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->avg_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->max_npa,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->min_npa,3); ?></td>
                                        </tr>
                                    <?php 
                                    $index++;
                                    } ?>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>NAA BLOK II</td>
                                        <td class="text-center"><?php echo ($rank_npa->hasil_akhir_blok1->rank == 0 ? 1 : $rank_npa->hasil_akhir_blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->avg_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->max_npa,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->min_npa,3); ?></td>
                                    </tr>


                                    <tr>
                                        <td colspan="7"><b>NILAI NON AKADEMIK</b></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>NNAPK OFF CAMPUS</td>
                                        <td class="text-center"><?php echo ($rank_nna->offcampus->rank == 0 ? 1 : $rank_nna->offcampus->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->offcampus->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->avg_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->max_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[1]->min_nna,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->offcampus->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->offcampus->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->offcampus->rank_nna[$index]->rank == 0 ? 1 : $data_blok1->offcampus->rank_nna[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->nilai_non_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->avg_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->max_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->offcampus->min_max_avg[$index]->min_nna,3); ?></td>
                                        </tr>
                                    <?php $index++; } ?>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>NNAPK ON CAMPUS (BLOK I)</td>
                                        <td class="text-center"><?php echo ($rank_nna->blok1->rank == 0 ? 1 : $rank_nna->blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_akhir_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->avg_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->max_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->min_max_avg[0]->min_nna,3); ?></td>
                                    </tr>
                                    <?php
                                    $index = 0;
                                    foreach ($data_blok1->blok1->skema_penilaian_id as $data_row) { ?>
                                        <tr>
                                            <td class="text-center"></td>
                                            <td>- <?php echo $data_blok1->blok1->skema_penilaian_name[$index] ?></td>
                                            <td class="text-center"><?php echo ($data_blok1->blok1->rank_nna[$index]->rank == 0 ? 1 : $data_blok1->blok1->rank_nna[$index]->rank); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->nilai_non_akademis[$index],3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->avg_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->max_nna,3); ?></td>
                                            <td class="text-center"><?php echo number_format($data_blok1->blok1->min_max_avg[$index]->min_nna,3); ?></td>
                                        </tr>
                                    <?php $index++; } ?>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>NNAA BLOK I</td>
                                        <td class="text-center"><?php echo ($rank_nna->hasil_akhir_blok1->rank == 0 ? 1 : $rank_nna->hasil_akhir_blok1->rank); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->na_blok1->nilai_akhir_non_akademis, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->avg_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->max_nna,3); ?></td>
                                        <td class="text-center"><?php echo number_format($data_blok1->min_max_avg_akhir->min_nna,3); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="well">
                                Catatan:
                                <ol type="a">
                                <li>Klasifikasi "TERBATAS".</li>
                                <li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
                                <li>Mohon tidak digandakan/diperbanyak.</li>
                                <li>Mohon dapatnya dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
                                <li>Lembar asli stempel basah dan tanda tangan Direvdik.</li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-3">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h3>NILAI AKADEMIK DAN NILAI NON AKADEMIK PADA BLOK I <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                                    <hr>
                                </div>
                                <div class="col-md-11 col-md-offset-1">
                                    <table class="table table-condensed no_border">
                                        <tr>
                                            <td>Nama Peserta</td>
                                            <td>:</td>
                                            <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td><?php echo $peserta->pangkat; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Tutor</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <table class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th rowspan="3" width="6%" class="text-center" style="vertical-align: middle">NO</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">KETERANGAN</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">RANK</th>
                                        <th colspan="4" class="text-center" style="vertical-align: middle">NILAI</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-center" style="vertical-align: middle">PESERTA</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERTINGGI</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERENDAH</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" style="vertical-align: middle">NILAI</th>
                                        <th class="text-center" style="vertical-align: middle">RATA2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- <div class="well">
                                Catatan:
                                <ol type="a">
                                <li>Klasifikasi "TERBATAS".</li>
                                <li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
                                <li>Mohon tidak digandakan/diperbanyak.</li>
                                <li>Mohon dapatnya dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
                                <li>Lembar asli stempel basah dan tanda tangan Direvdik.</li>
                                </ol>
                            </div> -->
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-4">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h3>NILAI AKADEMIK DAN NILAI NON AKADEMIK PADA BLOK I <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                                    <hr>
                                </div>
                                <div class="col-md-11 col-md-offset-1">
                                    <table class="table table-condensed no_border">
                                        <tr>
                                            <td>Nama Peserta</td>
                                            <td>:</td>
                                            <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td><?php echo $peserta->pangkat; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Tutor</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <table class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th rowspan="3" width="6%" class="text-center" style="vertical-align: middle">NO</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">KETERANGAN</th>
                                        <th rowspan="3" class="text-center" style="vertical-align: middle">RANK</th>
                                        <th colspan="4" class="text-center" style="vertical-align: middle">NILAI</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="text-center" style="vertical-align: middle">PESERTA</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERTINGGI</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle">TERENDAH</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" style="vertical-align: middle">NILAI</th>
                                        <th class="text-center" style="vertical-align: middle">RATA2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- <div class="well">
                                Catatan:
                                <ol type="a">
                                <li>Klasifikasi "TERBATAS".</li>
                                <li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
                                <li>Mohon tidak digandakan/diperbanyak.</li>
                                <li>Mohon dapatnya dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
                                <li>Lembar asli stempel basah dan tanda tangan Direvdik.</li>
                                </ol>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                                
</div>

<style type="text/css">
    .no_border td{
        border: none !important;
    }
    .panel-heading{
        cursor: pointer;
    }
    .panel-title{
        font-size: 1.2em !important;
    }
</style>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- <script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_detail_form.js"></script> -->
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
