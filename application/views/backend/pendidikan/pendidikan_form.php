<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Bagan Pendidikan</h3>
                    <button onclick="history.go(-1)" class="btn btn-default pull-right">Kembali</button>
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit; else echo $controller . '/' . $function_add; ?>" method="POST" id="px-pendidikan-form">
                    <input type="hidden" name="id" id="px-pendidikan-form-id" value="<?php if ($data) echo $data->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Jenis Pendidikan</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2 dd_jenis" name="jenis_pendidikan_id" id="px-pendidikan-form-jenis_pendidikan_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($jenis_pendidikan as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->jenis_pendidikan_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Bagan Pendidikan</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2 ss_bagan" name="bagan_pendidikan_id" id="px-pendidikan-form-bagan_pendidikan_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($bagan_pendidikan as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->bagan_pendidikan_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Nama Pendidikan</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="name" id="px-pendidikan-form-name" value="<?php if ($data) echo $data->name; ?>" placeholder="Nama Pendidikan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Tanggal Mulai</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control datepicker" name="date_start" id="px-pendidikan_jadwal-form-date_start" value="<?php if($data) echo $data->date_start ?>" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Tanggal Selesai</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control datepicker" name="date_end" id="px-pendidikan_jadwal-form-date_end" value="<?php if($data) echo $data->date_end ?>" required />
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan/pendidikan_form.js"></script>