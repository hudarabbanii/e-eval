<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<!-- <div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div> -->

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <?php
            // var_dump($this->session->userdata('var'));
            ?>
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form <?php echo $elemen_penilaian; ?></h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>PENILAIAN <?php echo strtoupper($elemen_penilaian); ?> <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                            <hr>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <table class="table table-condensed no_border">
                                    <tr>
                                        <td>Bidang Studi</td>
                                        <td>:</td>
                                        <td><?php echo $bidang_studi; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Hari/Tanggal</td>
                                        <td>:</td>
                                        <td><?php echo $jadwal; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed no_border">
                                    <tr>
                                        <td>Penilai</td>
                                        <td>:</td>
                                        <td><?php echo $penilai; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kelompok</td>
                                        <td>:</td>
                                        <td><?php echo $tim; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <p id="expired_remark" class="<?php echo ($is_active == 1 ? 'hidden' : ''); ?>" style="color: red;"><i>*Jadwal sudah melewati batas waktu pengisian nilai, silakan hubungi admin untuk mengaktifkan jadwal.</i></p>
                        <form id="px-pendidikan_jadwal-jadwal_detail-form" method="post" action="<?php echo $controller.'/pendidikan_jadwal_detail_add'; ?>">
                        <table class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="6%" class="text-center" style="vertical-align: middle">No</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle">Nama Peserta</th>
                                    <?php if($title_nna){ ?>
                                    <th colspan="<?php echo count($title_nna)?>" class="text-center">Nilai Non Akademik</th>
                                    <?php } ?>
                                    <?php if($title_npa){ ?>
                                    <th colspan="<?php echo count($title_npa)?>" class="text-center">Nilai Prestasi Akademik</th>
                                    <?php } ?>
                                    <th colspan="2" class="text-center" style="vertical-align: middle">Nilai Akhir</th>
                                </tr>
                                <tr>
                                    <?php
                                    foreach ($title_nna as $title_row) { ?>
                                            <th rowspan="2" width="10%" class="text-center" style="vertical-align: middle"><?php echo $title_row->name.'<br>'.$title_row->bobot.'%'; ?></th>
                                    <?php } ?>
                                    <?php
                                    foreach ($title_npa as $title_row) { ?>
                                            <th rowspan="2" width="10%" class="text-center" style="vertical-align: middle"><?php echo $title_row->name.'<br>'.$title_row->bobot.'%'; ?></th>
                                    <?php } ?>
                                    <th width="8%" class="text-center">Non-akademik</th>
                                    <th width="8%" class="text-center">Akademik</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // print_r($list);
                                // die;
                                $no=1;
                                foreach ($list as $data_row) { ?>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle"><?php echo $no; ?></td>
                                    <td class="text-center" style="vertical-align: middle"><?php echo $data_row->gelar_depan.' '.ucwords(strtolower($data_row->nama_lengkap)).($data_row->gelar_belakang!="" ? ', '.$data_row->gelar_belakang : ''); ?></td>
                                <?php $x=0; 
                                if(isset($data_row->nilai_non_akademis)){
                                foreach($data_row->nilai_non_akademis as $nilai){ ?>
                                    <td class="text-center">
                                        <input type="text" min="85" max="95" step=".001" class="form-control" value="<?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>" name="nilai_<?php echo $data_row->id.'_'.$data_row->elemen_penilaian_id_nna[$x] ?>" <?php echo ($is_active != 1 ? 'readonly' : ''); ?>>
                                    </td>
                                <?php $x++; }
                                } ?>
                                <?php $x =0; 
                                if(isset($data_row->nilai_akademis)){
                                foreach($data_row->nilai_akademis as $nilai){ 
                                    if(isset($data_row->elemen_child[$x]) && $data_row->elemen_child[$x] == 1){ ?>
                                        <td class="text-center">
                                            <input type="text" min="85" max="95" step=".001" class="form-control" value="<?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>" name="nilai_<?php echo $data_row->id.'_'.$data_row->elemen_penilaian_id_npa[$x] ?>" readonly><a href="pendidikan_jadwal/pendidikan_jadwal_detail_form_2?pendidikan_peserta_id=<?php echo $data_row->id ?>&elemen_penilaian_id=<?php echo $data_row->elemen_penilaian_id_npa[$x] ?>" class="btn btn-sm btn-primary btn-block">Isi Nilai</a>
                                        </td>
                                    <?php }else{ ?>
                                    <td class="text-center">
                                        <input type="text" min="85" max="95" step=".001" class="form-control" value="<?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>" name="nilai_<?php echo $data_row->id.'_'.$data_row->elemen_penilaian_id_npa[$x] ?>" <?php echo ($is_active != 1 ? 'readonly' : ''); ?>>
                                    </td>
                                    <?php } ?>
                                <?php $x++; }
                                } ?>
                                    <td class="text-center" style="vertical-align: middle"><b><?php echo (isset($data_row->nilai_akhir_non_akademis) ? number_format($data_row->nilai_akhir_non_akademis, 3, '.', '') : number_format(0, 3, '.', '')); ?></b></td>
                                    <td class="text-center" style="vertical-align: middle"><b><?php echo (isset($data_row->nilai_akhir_akademis) ? number_format($data_row->nilai_akhir_akademis, 3, '.', '') : number_format(0, 3, '.', '')); ?></b></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                        <div class="panel-footer">
                            <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                            <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                            <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                            <button id="save_button" class="btn btn-primary pull-right" <?php echo ($is_active != 1 ? 'disabled' : ''); ?>>Simpan</button>
                            <span id="simpan_tanpa_validasi" class="btn btn-warning pull-right" <?php echo ($is_active != 1 ? 'disabled' : ''); ?>>Simpan Tanpa Validasi</span>
                            <a href="pendidikan_jadwal/print_form/<?php echo $this->session->userdata('jadwal')['jadwal_id']; ?>" class="btn btn-info pull-right"><i class="fa fa-print"></i> Print PDF</a>
                        </div>
                        </form>

                        <!-- CATATAN -->
                        <div class="well">
                            <?php echo $keterangan ?>
                        </div>
                    </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<style type="text/css">
    .no_border td{
        border: none !important;
    }
</style>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script> -->

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_detail_form.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
