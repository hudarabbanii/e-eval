<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap"> 
    <div class="row">
        <div class="col-md-12">
            <?php foreach ($submenu as $sm) { ?>
                <a class="btn btn-primary" href="<?php echo $controller.'/'.$sm->target ?>"><i class="fa <?php echo $sm->icon ?>"></i> <?php echo $sm->name ?></a>
            <?php } ?>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

