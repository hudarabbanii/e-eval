$(document).ready(function(){
    $("select.select2").select2();
    $('#px-pendidikan_pembagian_tim-form').validate({
        rules: {
            id: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-pendidikan_pembagian_tim-form .alert-warning').removeClass('hidden');
            $('#px-pendidikan_pembagian_tim-form .alert-success').addClass('hidden');
            $('#px-pendidikan_pembagian_tim-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-pendidikan_pembagian_tim-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-pendidikan_pembagian_tim-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-pendidikan_pembagian_tim-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
})
