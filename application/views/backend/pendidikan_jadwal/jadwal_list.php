<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Jadwal</h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                    <a class="btn btn-success pull-right btn-add <?php if($this->session_admin['id_usergroup'] == 3) echo 'hidden'; ?>" href="<?php echo $controller . '/' . $function_form; ?>"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                    <input type="hidden" name="pendidikan_id" id="px-pendidikan_jadwal-list-pendidikan_id" value="<?php echo $this->session->userdata('menu_pendidikan')['pendidikan_id']; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <table class="table datatable table-bordered">
                            <thead>
                                <tr>
                                    <th width="6%" class="text-center">No</th>
                                    <th class="text-center">Tanggal</th>
                                    <th class="text-center">Penilai</th>
                                    <th class="text-center">Tim</th>
                                    <th class="text-center">Elemen Penilaian</th>
                                    <th class="text-center">Bidang Studi</th>
                                    <th width="10%" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                
</div>

<!-- MESSAGE BOX -->
<div id="px-pendidikan_jadwal-message-box" class="message-box message-box-warning animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/' . $function_delete; ?>" method="post" id="px-pendidikan_jadwal-message-form">
                <input type="hidden" name="id">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin menghapus data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-danger btn-lg pull-right" type="submit">Hapus</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script> -->

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
