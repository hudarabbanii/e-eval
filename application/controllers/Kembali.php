<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kembali extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $this->session->unset_userdata('menu_pendidikan');

        redirect('pendidikan');
    }
}