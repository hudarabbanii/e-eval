$(document).ready(function(){
var table =	$('.datatable').DataTable({
	 	"stateSave": true,
	 	"serverSide":true,
	 	"processing": true,
	 	"ajax": {
		      "url": "admin_setting_pendidikan/ajax_bagan_pendidikan_detail_list",
		      "type": "POST",
		      "data": function ( data ) {
                	data.bagan_id = $('#bagan_pendidikan_id').val(),
                	data.elemen_penilaian_id = $('#elemen_penilaian_id').val()
            	}
		  },
		"bLengthChange": false,
		"searching": false,
		"paging": false,
		"columnDefs": [
		{ 
		    "targets": [0,1,2,3,4,-1],
		    "orderable": false,
		},
		{ 
		    "targets": [0,2,3,-1],
		    "className": 'text-center',
		}
		]
	});


	$('#px-setting_pendidikan-bagan_detail-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-setting_pendidikan-bagan_detail-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-setting_pendidikan-bagan_detail-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-setting_pendidikan-bagan_detail-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-setting_pendidikan-bagan_detail-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-setting_pendidikan-bagan_detail-message-form input[name="id"]').val('');
		$('#px-setting_pendidikan-bagan_detail-message-form input[name="id"]').val(id);
	});

	$('body').delegate('.btn-change','click',function(){
        var bagan_pendidikan_id = $('#bagan_pendidikan_id').val();
        var elemen_penilaian_id = $(this).attr('data-target-id');
        var status = $(this).attr('data-status');
        $('#px-setting_pendidikan-bagan_pendidikan_detail .alert-warning').show();
        $.ajax({
                url : 'admin_setting_pendidikan/bagan_pendidikan_detail_change_status',
                type : 'POST',
                dataType : 'json',
                data : {status:status,
                        bagan_pendidikan_id:bagan_pendidikan_id,
                        elemen_penilaian_id:elemen_penilaian_id,
                        },
                success : function(response){
                    if(response.status == 'ok'){
        				$('#px-setting_pendidikan-bagan_pendidikan_detail .alert-warning').hide();
                    	$('#px-setting_pendidikan-bagan_pendidikan_detail .alert-success').show().children('span').text(response.msg);
						setTimeout(function() {
						        $('#px-setting_pendidikan-bagan_pendidikan_detail .alert-success').hide("slow");
						    }, 1000);
                        table.ajax.reload();
                    }
                    else{
                    	$('#px-setting_pendidikan-bagan_pendidikan_detail .alert-warning').hide();
                    	$('#px-setting_pendidikan-bagan_pendidikan_detail .alert-danger').show().children('span').text(response.msg);
						setTimeout(function() {
						        $('#px-setting_pendidikan-bagan_pendidikan_detail .alert-danger').hide("slow");
						    }, 1000); 
                	}
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
    });

    $('body').delegate('#px-setting_pendidikan-elemen_penilaian_list-dropdown','change',function(){
		var elemen_penilaian_id = $('#px-setting_pendidikan-elemen_penilaian_list-dropdown').val();
    	$("#elemen_penilaian_id").val(elemen_penilaian_id);
        table.ajax.reload();
	});
})