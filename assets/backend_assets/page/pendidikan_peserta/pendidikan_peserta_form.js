$(document).ready(function(){
    $("select.select2").select2();
    $('#px-pendidikan_peserta-form').validate({
        rules: {
            id: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-pendidikan_peserta-form .alert-warning').removeClass('hidden');
            $('#px-pendidikan_peserta-form .alert-success').addClass('hidden');
            $('#px-pendidikan_peserta-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-pendidikan_peserta-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-pendidikan_peserta-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-pendidikan_peserta-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
})
