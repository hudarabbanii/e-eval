<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_pembagian_tim extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_pembagian_tim', 'controller_name' => 'Admin Pembagian Tim', 'controller_id' => 0);
    }
    
    public function index(){
    }

    function tim_off_campus(){
        $this->session->unset_userdata('tim_blok');
        $elemen_penilaian_id = 1;
        $this->session->set_userdata('tim_blok', array('id'=>1, 'name'=>'Off Campus', 'menu'=>'off_campus'));
        $this->tim_list($elemen_penilaian_id);
    }
    function tim_blok_satu(){
        $this->session->unset_userdata('tim_blok');
        $elemen_penilaian_id = 1;
        $this->session->set_userdata('tim_blok', array('id'=>3, 'name'=>'Blok I', 'menu'=>'blok_satu'));
        $this->tim_list($elemen_penilaian_id);
    }
    function tim_blok_dua(){
        $this->session->unset_userdata('tim_blok');
        $elemen_penilaian_id = 1;
        $this->session->set_userdata('tim_blok', array('id'=>4, 'name'=>'Blok II', 'menu'=>'blok_dua'));
        $this->tim_list($elemen_penilaian_id);
    }
    function tim_blok_tiga(){
        $this->session->unset_userdata('tim_blok');
        $elemen_penilaian_id = 1;
        $this->session->set_userdata('tim_blok', array('id'=>5, 'name'=>'Blok III', 'menu'=>'blok_tiga'));
        $this->tim_list($elemen_penilaian_id);
    }
    function tim_blok_empat(){
        $this->session->unset_userdata('tim_blok');
        $elemen_penilaian_id = 1;
        $this->session->set_userdata('tim_blok', array('id'=>6, 'name'=>'Blok IV', 'menu'=>'blok_empat'));
        $this->tim_list($elemen_penilaian_id);
    }
    function tim_taskap(){
        $this->session->unset_userdata('tim_blok');
        $elemen_penilaian_id = 1;
        $this->session->set_userdata('tim_blok', array('id'=>350, 'name'=>'Taskap', 'menu'=>'taskap'));
        $this->tim_list($elemen_penilaian_id);
    }

    function tim_list() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $elemen_penilaian_id = $this->session->userdata('tim_blok')['id'];
        $pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $data['tim'] = $this->model_basic->select_where_array($this->tbl_tim, array('pendidikan_id'=>$pendidikan_id, 'elemen_penilaian_id'=>$elemen_penilaian_id))->result();
        foreach ($data['tim'] as $data_row) {
            $jenis_tim = $this->model_basic->select_where($this->tbl_jenis_tim, 'id', $data_row->jenis_tim_id);
            if($jenis_tim->num_rows() == 1)
                $data_row->jenis_tim = $jenis_tim->row()->name;
            else
                $data_row->jenis_tim = 'Unknown';
        }
        $data['content'] = $this->load->view('backend/pendidikan_pembagian_tim/tim_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function tim_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $id)->row();
            $data['data'] = $tim;
        } else
            $data['data'] = null;

        $data['jenis_tim'] = $this->model_basic->select_all($this->tbl_jenis_tim);
        $data['content'] = $this->load->view('backend/pendidikan_pembagian_tim/tim_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function tim_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_tim);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['pendidikan_id'] = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $insert['elemen_penilaian_id'] = $this->session->userdata('tim_blok')['id'];
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        unset($insert['date_modified']);

        if ($this->input->post('name') && $this->input->post('jenis_tim_id')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_tim, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert Kelompok '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function tim_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_tim);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['pendidikan_id'] = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $update['elemen_penilaian_id'] = $this->session->userdata('tim_blok')['id'];
        $update['date_created'] = date('Y-m-d H:i:s', now());
        unset($update['date_modified']);

        if ($this->input->post('name') && $this->input->post('jenis_tim_id')) {
            $do_update = $this->model_basic->update($this->tbl_tim, $update, 'id', $update['id']);
            if ($do_update) {
                $this->save_log_admin(ACT_UPDATE, 'Update Kelompok '.$update['name']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function tim_detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['peserta_tim'] = $this->model_pembagian_tim->get_peserta_tim($id)->result();
        $data['tim_info'] = new stdclass();
        $data['tim_info']->tim_id = $id;
        $data['tim_info']->nama_blok = $this->session->userdata('tim_blok')['name'];
        $data['tim_info']->nama_tim = $this->model_basic->select_where($this->tbl_tim, 'id', $id)->row()->name; 

        $data['content'] = $this->load->view('backend/pendidikan_pembagian_tim/tim_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    // function penilai_delete() {
    //     $data = $this->get_app_settings();
    //     $data += $this->controller_attr;
    //     $data += $this->get_function('Pembagian Tim', 'pendidikan_penilai');
    //     $data += $this->get_menu();
    //     $this->check_userakses($data['function_id'], ACT_DELETE);
    //     $id = $this->input->post('id');
    //     $deleted_data = $this->model_basic->select_where($this->tbl_tim, 'id', $id)->row();
    //     $do_delete = $this->model_basic->delete($this->tbl_tim, 'id', $id);
    //     if ($do_delete) {
    //         $this->save_log_admin(ACT_DELETE, 'Delete Pembagian Tim '.$deleted_data->name);
    //         $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
    //     } else {
    //         $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
    //     }
    // }

    // TAMBAH PESERTA TIM
    function peserta_tim_form($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $data['tim_id'] = $id;

        $data['content'] = $this->load->view('backend/pendidikan_pembagian_tim/peserta_tim_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    public function ajax_pendidikan_pembagian_tim_form()
    {
        $list = $this->model_pendidikan_pembagian_tim_form->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;

            $row = array();
            $row[] = $data_row->id;
            $row[] = $no;
            $row[] = $data_row->nama_lengkap;
            $row[] = $data_row->pangkat;
            $row[] = $data_row->instansi_negara;
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_pendidikan_pembagian_tim_form->count_all(),
                        "recordsFiltered" => $this->model_pendidikan_pembagian_tim_form->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function peserta_tim_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $tim_id = $this->input->post('tim_id');
        $peserta_add = $this->input->post('id');

        foreach ($peserta_add as $id) {
            $insert = array();
            $insert['tim_id'] = $tim_id;
            $insert['pendidikan_peserta_id'] = $id;
            $insert['date_created'] = date('Y-m-d H:i:s', now());

            $do_insert = $this->model_basic->insert_all($this->tbl_tim_detail, $insert);
        }
        if($do_insert)
            $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/tim_detail/'.$tim_id));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
    }

    function peserta_tim_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pembagian Tim', 'tim_'.$this->session->userdata('tim_blok')['menu']);
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);

        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $id)->row();
        $do_delete = $this->model_basic->delete_where_array($this->tbl_tim_detail, ['pendidikan_peserta_id'=>$id, 'tim_id'=>$this->session->userdata('tim_blok')['id']]);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Peserta Tim '.$deleted_data->nama_lengkap.' dari Tim '.$this->session->userdata('tim_blok')['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/tim_detail/'.$this->session->userdata('tim_blok')['id']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }
}