<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_hasil extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_hasil', 'controller_name' => 'Admin Hasil', 'controller_id' => 0);
    }
    
    public function index(){            
        $this->hasil();
    }

    function hasil() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil', 'admin_hasil');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['peserta'] = $this->model_basic->select_all($this->tbl_pendidikan_peserta);

        $data['content'] = $this->load->view('backend/admin_hasil/hasil_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function hasil_detail($id){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil', 'admin_hasil');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['peserta'] = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $id)->row();

        $data['content'] = $this->load->view('backend/admin_hasil/hasil_detail', $data, true);
        $this->load->view('backend/index', $data);   
    }
}