<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pendidikan_request_pengganti extends CI_Model {
	var $column = array('b.tanggal', 't.name', 'ep.name', 'bs.name', 'd.nama_lengkap', 'e.nama_lengkap', 'a.user_remark','',''); //set column field database for order and search
	var $order = array('a.id' => 'desc'); // default order

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{	
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$this->db->select("a.*, b.tanggal as tanggal, c.name as pendidikan_name, d.gelar_depan as penilai_gelar_depan, d.nama_lengkap as penilai_nama_lengkap, d.gelar_belakang as penilai_gelar_belakang, e.gelar_depan as pengganti_gelar_depan, e.nama_lengkap as pengganti_nama_lengkap, e.gelar_belakang as pengganti_gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim", FALSE);
		$this->db->from($this->tbl_jadwal_penilai_pengganti.' a');
		$this->db->join($this->tbl_jadwal.' b', 'b.id = a.jadwal_id', 'left');
		$this->db->join($this->tbl_pendidikan.' c', 'c.id = b.pendidikan_id', 'left');
		$this->db->join($this->tbl_penilai.' d', 'd.id = a.penilai_id', 'left');
		$this->db->join($this->tbl_penilai.' e', 'e.id = a.penilai_pengganti_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = b.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = b.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = b.tim_id', 'left');
		$this->db->where('b.pendidikan_id', $pendidikan_id);
		$this->db->order_by('b.date_created', 'DESC');
		$i = 1;
		$where = "";
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$this->db->select("a.*, b.tanggal as tanggal, c.name as pendidikan_name, d.gelar_depan as penilai_gelar_depan, d.nama_lengkap as penilai_nama_lengkap, d.gelar_belakang as penilai_gelar_belakang, e.gelar_depan as pengganti_gelar_depan, e.nama_lengkap as pengganti_nama_lengkap, e.gelar_belakang as pengganti_gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim");
		$this->db->from($this->tbl_jadwal_penilai_pengganti.' a');
		$this->db->join($this->tbl_jadwal.' b', 'b.id = a.jadwal_id', 'left');
		$this->db->join($this->tbl_pendidikan.' c', 'c.id = b.pendidikan_id', 'left');
		$this->db->join($this->tbl_penilai.' d', 'd.id = a.penilai_id', 'left');
		$this->db->join($this->tbl_penilai.' e', 'e.id = a.penilai_pengganti_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = b.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = b.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = b.tim_id', 'left');
		$this->db->where('b.pendidikan_id', $pendidikan_id);
		$this->db->order_by('b.date_created', 'DESC');
		$query = $this->db->get();
		return $query->num_rows();
	}
}