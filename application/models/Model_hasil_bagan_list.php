<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_hasil_bagan_list extends CI_Model {

	var $column = array('j.tanggal', 'p.nama_lengkap', 't.name', 'ep.name', 'bs.name'); //set column field database for order and search
	var $order = array('j.id' => 'desc'); // default order

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$elemen_penilaian_id = $this->session->userdata('var')['elemen_penilaian_id'];
		$this->db->select('j.*, p.nama_lengkap as penilai, p.gelar_depan as gelar_depan, p.gelar_belakang as gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim');
		$this->db->from($this->tbl_jadwal .' j');
		$this->db->join($this->tbl_penilai.' p', 'p.id = j.penilai_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = j.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->where('j.pendidikan_id', $pendidikan_id);
		$this->db->where('j.elemen_penilaian_id', $elemen_penilaian_id);
		$i = 1;
		$where = "";
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$elemen_penilaian_id = $this->session->userdata('var')['elemen_penilaian_id'];
		$this->db->select('j.*, p.nama_lengkap as penilai, p.gelar_depan as gelar_depan, p.gelar_belakang as gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim');
		$this->db->from($this->tbl_jadwal .' j');
		$this->db->join($this->tbl_penilai.' p', 'p.id = j.penilai_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = j.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->where('j.pendidikan_id', $pendidikan_id);
		$this->db->where('j.elemen_penilaian_id', $elemen_penilaian_id);
		$query = $this->db->get();
		return $query->num_rows();
	}	

}

/* End of file model_site_list.php */
/* Location: ./application/models/model_site_list.php */