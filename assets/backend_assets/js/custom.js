  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

    var loadFileLightbox = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output_lightbox');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  var loadFileMobile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output_mobile');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };