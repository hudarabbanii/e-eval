function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}
//e.o. datatables function$(document).ready(function(){

//datatables function
var save_method; //for save method string
var table;

// Array holding selected row IDs
var rows_selected = [];

table = $('.datatable').DataTable({
  // "stateSave": true,
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "bLengthChange": false,
  "dom": 'lBfrtip',
  "lengthMenu": [
      [ 10, 25, 50, -1 ],
      [ '10 rows', '25 rows', '50 rows', 'Show all' ]
    ],
  "buttons": [
  {
    extend: 'pageLength'
  }],

  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "pendidikan_pembagian_tim/ajax_pendidikan_pembagian_tim_form",
      "type": "POST"
  },

  //Set column definition initialisation properties.
  "columnDefs": [
  { 
    'targets': [0,1],
    'searchable': false,
    'orderable': false,
  },
  {
    "className": "text-center",
    "targets": [ 0,1,2,3,-1 ], //add class
  },
  {
   'targets': 0,
   'searchable': false,
   'orderable': false,
   'width': '1%',
   'className': 'dt-body-center',
   'render': function (data, type, full, meta){
             // return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '" >';
             return '<input type="checkbox">';
             // return data;
         }
  },
  {
    'targets': 0,
    'checkboxes': {
       'selectRow': true
    }
  },
  {'select': {
     'style': 'multi'
    }
  }
  ],
  'order': [[2, 'asc']],
  'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];
         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
         }
      }
});

// Handle click on checkbox
$('.datatable tbody').on('click', 'input[type="checkbox"]', function(e){
  var $row = $(this).closest('tr');

  // Get row data
  var data = table.row($row).data();

  // Get row ID
  var rowId = data[0];

  // Determine whether row ID is in the list of selected row IDs
  var index = $.inArray(rowId, rows_selected);

  // If checkbox is checked and row ID is not in list of selected row IDs
  if(this.checked && index === -1){
     rows_selected.push(rowId);

  // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
  } else if (!this.checked && index !== -1){
     rows_selected.splice(index, 1);
  }

  if(this.checked){
     $row.addClass('selected');
  } else {
     $row.removeClass('selected');
  }

  // Update state of "Select all" control
  updateDataTableSelectAllCtrl(table);

  // Prevent click event from propagating to parent
  e.stopPropagation();
});

// Handle click on table cells with checkboxes
$('.datatable').on('click', 'tbody td, thead th:first-child', function(e){
  $(this).parent().find('input[type="checkbox"]').trigger('click');
});

// Handle click on "Select all" control
$('thead input[name="select_all"]', table.table().container()).on('click', function(e){
  if(this.checked){
     $('.datatable tbody input[type="checkbox"]:not(:checked)').trigger('click');
  } else {
     $('.datatable tbody input[type="checkbox"]:checked').trigger('click');
  }

  // Prevent click event from propagating to parent
  e.stopPropagation();
});

// Handle table draw event
table.on('draw', function(){
  // Update state of "Select all" control
  updateDataTableSelectAllCtrl(table);
});

$('#px-pendidikan_pembagian_tim-form').on('submit', function(e){
    var form = this;

    // Iterate over all selected checkboxes
    $.each(rows_selected, function(index, rowId){
       // Create a hidden element 
       $(form).append(
           $('<input>')
              .attr('type', 'hidden')
              .attr('name', 'id[]')
              .val(rowId)
       );
    });

    e.preventDefault();
    var target = $(form).attr('action');
    $('#px-pendidikan_pembagian_tim-form .alert-warning').removeClass('hidden');
    $('#px-pendidikan_pembagian_tim-form .alert-success').addClass('hidden');
    $('#px-pendidikan_pembagian_tim-form .alert-danger').addClass('hidden');
    $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        success : function(response){
            $('#px-pendidikan_pembagian_tim-form .alert-warning').addClass('hidden');
            if(response.status == 'ok'){
                $('#px-pendidikan_pembagian_tim-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                window.location.href = response.redirect;
            }
            else
                $('#px-pendidikan_pembagian_tim-form .alert-danger').removeClass('hidden').children('span').text(response.msg); 
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        }
    });
 });
