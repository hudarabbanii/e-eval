$(document).ready(function(){

	$('.datatable').DataTable({
		"stateSave": true,
	 	"processing": true,
	 	"serverSide": true,
	 	"ajax": {
		      "url": "admin_setting_pendidikan/ajax_elemen_penilaian_list",
		      "type": "POST"
		  },
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"iDisplayLength": 50,
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		},
		{
			extend: 'print',
			autoPrint: true,
			exportOptions: {
                columns: [0,1]
            },
                customize: function ( win ) {
					$(win.document.body).find( 'table' )
						.addClass( 'compact' )
						.css( 'font-size', 'inherit' );
						
					$(win.document.body)
						.css( 'font-size', '10pt' )
						.prepend(
							'<div style=" text-align:center; border-bottom:1px solid #ccc; margin-bottom:20px;"><h3>BIDANG</h3></div>'
						);
				}
		},
		{
			extend: 'excel',
			exportOptions: {
                columns: [0,1]
            }
		}
		],
		"columnDefs": [
		{ 
		    "targets": [0,-1],
		    "orderable": false,
		}
		]
	});


	$('#px-setting_pendidikan-elemen_penilaian_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-setting_pendidikan-elemen_penilaian_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-setting_pendidikan-elemen_penilaian_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-setting_pendidikan-elemen_penilaian_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-setting_pendidikan-elemen_penilaian_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-setting_pendidikan-elemen_penilaian_list-message-form input[name="id"]').val('');
		$('#px-setting_pendidikan-elemen_penilaian_list-message-form input[name="id"]').val(id);
	});

	//change akademis
	$('#px-setting_pendidikan-change_akademis-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    if(response.status == 'ok'){
						$('#px-setting_pendidikan-change_akademis-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_akademis-message-box').removeClass('open');
						window.location.href = response.redirect;
                    }
                    else
						$('#px-setting_pendidikan-change_akademis-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_akademis-message-box').removeClass('open');
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
		}
	});

	$('body').delegate('.btn-change_akademis','click',function(){
		$('#px-setting_pendidikan-change_akademis-message-box').addClass('open');
		var elemen_penilaian_id = $(this).attr('data-id');
		var tipe_akademis = $(this).attr('data-akademis');
		$('#px-setting_pendidikan-change_akademis-message-form input[name="elemen_penilaian_id"]').val('');
		$('#px-setting_pendidikan-change_akademis-message-form input[name="elemen_penilaian_id"]').val(elemen_penilaian_id);
		$('#px-setting_pendidikan-change_akademis-message-form input[name="tipe_akademis"]').val('');
		$('#px-setting_pendidikan-change_akademis-message-form input[name="tipe_akademis"]').val(tipe_akademis);
	});

	//change bobot
	$('#px-setting_pendidikan-change_bobot-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    if(response.status == 'ok'){
						$('#px-setting_pendidikan-change_bobot-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_bobot-message-box').removeClass('open');
						window.location.href = response.redirect;
                    }
                    else
						$('#px-setting_pendidikan-change_bobot-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_bobot-message-box').removeClass('open');
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
		}
	});

	$('body').delegate('.btn-change_bobot','click',function(){
		$('#px-setting_pendidikan-change_bobot-message-box').addClass('open');
		var elemen_penilaian_id = $(this).attr('data-id');
		var tipe_bobot = $(this).attr('data-bobot');
		$('#px-setting_pendidikan-change_bobot-message-form input[name="elemen_penilaian_id"]').val('');
		$('#px-setting_pendidikan-change_bobot-message-form input[name="elemen_penilaian_id"]').val(elemen_penilaian_id);
		$('#px-setting_pendidikan-change_bobot-message-form input[name="tipe_bobot"]').val('');
		$('#px-setting_pendidikan-change_bobot-message-form input[name="tipe_bobot"]').val(tipe_bobot);
	});
})