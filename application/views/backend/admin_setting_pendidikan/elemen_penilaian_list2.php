<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Bidang</h3>
                    <a class="btn btn-success pull-right btn-add" href="<?php echo $controller . '/' . $function_form; ?>"><i class="fa fa-plus"></i> Tambah</a>
                    <a class="btn btn-default pull-right btn-add" href="<?php echo 'admin_download' . '/export_bidang'; ?>"><i class="fa fa-download"></i> Download Excel</a>
                    <a class="btn btn-default pull-right btn-add" href="<?php echo 'admin_upload' . '/import_elemen_penilaian_form'; ?>"><i class="fa fa-upload"></i> Upload Excel</a>                  
                </div>
                <div class="panel-body">
                    <div class="alert alert-success hidden"><strong>Berhasil! </strong><span></span></div>
                    <div class="alert alert-warning hidden"><strong>Sedang diproses.. </strong><span>Mohon Tunggu...</span></div>
                    <div class="alert alert-danger hidden"><strong>Gagal! </strong><span></span></div>
                    <table class="table datatable table-bordered">
                        <thead>
                          <tr>
                            <th width="6%" class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Tipe Akademis</th>
                            <th class="text-center">Tipe Bobot</th>
                            <th width="18%" class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-setting_pendidikan-elemen_penilaian_list-message-box" class="message-box message-box-warning animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/' . $function_delete; ?>" method="post" id="px-setting_pendidikan-elemen_penilaian_list-message-form">
                <input type="hidden" name="id">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin menghapus data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-danger btn-lg pull-right" type="submit">Hapus</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- //akademis -->
<div id="px-setting_pendidikan-change_akademis-message-box" class="message-box message-box-info animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/ajax_ubah_tipe_akademis'; ?>" method="post" id="px-setting_pendidikan-change_akademis-message-form">
                <input type="hidden" name="elemen_penilaian_id">
                <input type="hidden" name="tipe_akademis">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin mengubah data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-primary btn-lg pull-right" type="submit">Ubah</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- //bobot -->
<div id="px-setting_pendidikan-change_bobot-message-box" class="message-box message-box-info animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/ajax_ubah_tipe_bobot'; ?>" method="post" id="px-setting_pendidikan-change_bobot-message-form">
                <input type="hidden" name="elemen_penilaian_id">
                <input type="hidden" name="tipe_bobot">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin mengubah data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-primary btn-lg pull-right" type="submit">Ubah</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- END PAGE PLUGINS -->

<script type="text/javascript" src="assets/backend_assets/page/setting_pendidikan/elemen_penilaian_list2.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
