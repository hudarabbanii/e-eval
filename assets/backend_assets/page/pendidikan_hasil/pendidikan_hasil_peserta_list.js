$(document).ready(function(){
	$('.datatable').DataTable({
	 	"processing": true,
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		}
		],
		"columnDefs": [
		{ 
		    "targets": [0,-1],
		    "orderable": false,
		}
		]
	});
});