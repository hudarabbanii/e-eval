<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Bidang Studi</h3>
                    <a class="btn btn-success pull-right btn-add" href="<?php echo $controller . '/' . $function_form; ?>"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                <div class="panel-body" id="px-setting_pendidikan-bidang_studi-list">
                    <div class="alert alert-success" style="display: none"><strong>Perubahan berhasil! </strong><span></span></div>
                    <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                    <div class="alert alert-danger" style="display: none"><strong>Failed! </strong><span></span></div>
                    <table class="table datatable table-bordered" style="width: 100%">
                        <thead>
                          <tr>
                            <th width="6%" class="text-center">No</th>
                            <th class="text-center">Jenis Bidang Studi</th>
                            <th class="text-center">Bidang Studi</th>
                            <th class="text-center">Peserta Asing</th>
                            <th width="15%" class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $no=1;
                          foreach ($bidang_studi as $data_row) { ?>
                          <tr>
                            <td class="text-center"><?php echo $no; ?></td>
                            <td class="text-center"><?php echo $data_row->jenis_bs; ?></td>
                            <td class="text-center"><?php echo $data_row->name; ?></td>
                            <td class="text-center">
                              <?php if($data_row->status_asing == 0){ ?>
                              <button id="btn-status_asing" class="btn btn-xs btn-success" data-id="<?php echo $data_row->id ?>" data-status="0"><span id="status" class="fa fa-check"></span></button>
                              <?php }else{ ?>
                              <button id="btn-status_asing" class="btn btn-xs btn-warning" data-id="<?php echo $data_row->id ?>" data-status="1"><span id="status" class="fa fa-times"></span></button>
                              <?php } ?>
                            </td>
                            <td class="text-center">
                              <form action="admin_setting_pendidikan/bidang_studi_form" method="post">
                                  <input type="hidden" name="id" value="<?php echo $data_row->id ?>">
                                  <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                  <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="<?php echo $data_row->id ?>"><i class="fa fa-trash-o"></i></button>
                              </form>
                          </td>
                          </tr>
                          <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-setting_pendidikan-bidang_studi_list-message-box" class="message-box message-box-warning animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/' . $function_delete; ?>" method="post" id="px-setting_pendidikan-bidang_studi_list-message-form">
                <input type="hidden" name="id">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin menghapus data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-danger btn-lg pull-right" type="submit">Hapus</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
 
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- END PAGE PLUGINS -->

<script type="text/javascript" src="assets/backend_assets/page/setting_pendidikan/bidang_studi_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

