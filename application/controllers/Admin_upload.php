<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_upload extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_upload', 'controller_name' => 'Admin Upload', 'controller_id' => 0);
    }

    function import_bidang_form() {
        $this->controller_attr = array('controller' => 'admin_setting_penilai', 'controller_name' => 'Admin Upload', 'controller_id' => 0);
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Upload Bidang', 'bidang');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $data['content'] = $this->load->view('backend/admin_upload/upload_bidang_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function import_bidang() {
        set_time_limit(0);
        $file = $this->input->post('files');
        $this->load->library('excel');
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow();
        
        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':B' . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
            //  Insert row data array into your database of choice here
            $data_object = new stdClass();
            $data_object->id = $rowData[0][0];
            $data_object->bidang = $rowData[0][1];

            $this->db->trans_start();
            if($data_object->id != '')
            {
                $data_update = array(
                    'name' => $data_object->bidang);
                $this->model_basic->update($this->tbl_bidang, $data_update, 'id', $data_object->id);
            }
            else
            {
                $data_insert = array(
                    'name' => $data_object->bidang);
                $this->model_basic->insert_all($this->tbl_bidang, $data_insert);
            }
            $this->db->trans_complete();
        }
        if($this->db->trans_status() != FALSE)
        {
            $this->delete_temp('temp_folder');
            $this->returnJson(array('status' => 'ok', 'redirect' => 'admin_setting_penilai/bidang?import=success', 'msg' => 'Import Success'));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Failed when Importing data, try Again'));
    }

    function import_penilai_form() {
        $this->controller_attr = array('controller' => 'admin_setting_penilai', 'controller_name' => 'Admin Upload', 'controller_id' => 0);
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Upload Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $data['content'] = $this->load->view('backend/admin_upload/upload_penilai_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function import_penilai() {
        set_time_limit(0);
        $file = $this->input->post('files');
        $this->load->library('excel');
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow();
        
        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':K' . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
            //  Insert row data array into your database of choice here
            $data_object = new stdClass();
            $data_object->id = $rowData[0][0];
            $data_object->gelar_depan = $rowData[0][1];
            $data_object->gelar_belakang = $rowData[0][2];
            $data_object->nama_lengkap = $rowData[0][3];
            $data_object->telp = $rowData[0][4];
            $data_object->ruangan = $rowData[0][5];
            $data_object->pangkat_id = $rowData[0][6];
            $data_object->jabatan_id = $rowData[0][7];
            $data_object->bidang_id = $rowData[0][8];
            $data_object->jenis_penilai_id = $rowData[0][9];
            $data_object->photo = $rowData[0][10];

            $this->db->trans_start();
            if($data_object->id != '')
            {
                $data_update = array(
                    'date_modified' => date('Y-m-d H:i:s', now()),
                    'gelar_depan' => $data_object->gelar_depan,
                    'gelar_belakang' => $data_object->gelar_belakang,
                    'nama_lengkap' => $data_object->nama_lengkap,
                    'telp' => $data_object->telp,
                    'ruangan' => $data_object->ruangan,
                    'pangkat_id' => $data_object->pangkat_id,
                    'jabatan_id' => $data_object->jabatan_id,
                    'bidang_id' => $data_object->bidang_id,
                    'jenis_penilai_id' => $data_object->jenis_penilai_id,
                    'photo' => $data_object->photo);
                $this->model_basic->update($this->tbl_penilai, $data_update, 'id', $data_object->id);

                $data_update_login = array(
                    'username' => 'p'.$data_object->id,
                    'password' => $this->password_encode($data_object->password),
                    'id_usergroup' => 3,
                    'penilai_id' => $data_object->id);
                $this->model_basic->update($this->tbl_user, $data_update_login, 'penilai_id', $data_object->id);
            }
            else
            {
                $data_insert = array(
                    'date_created' => date('Y-m-d H:i:s', now()),
                    'gelar_depan' => $data_object->gelar_depan,
                    'gelar_belakang' => $data_object->gelar_belakang,
                    'nama_lengkap' => $data_object->nama_lengkap,
                    'telp' => $data_object->telp,
                    'ruangan' => $data_object->ruangan,
                    'pangkat_id' => $data_object->pangkat_id,
                    'jabatan_id' => $data_object->jabatan_id,
                    'bidang_id' => $data_object->bidang_id,
                    'jenis_penilai_id' => $data_object->jenis_penilai_id,
                    'photo' => $data_object->photo);
                $do_insert = $this->model_basic->insert_all($this->tbl_penilai, $data_insert);

                $data_insert_login = array(
                    'username' => 'p'.$do_insert->id,
                    'password' => $this->password_encode('password'),
                    'id_usergroup' => 3,
                    'penilai_id' => $do_insert->id);
                $this->model_basic->insert_all($this->tbl_user, $data_insert_login);
            }
            $this->db->trans_complete();
        }
        if($this->db->trans_status() != FALSE)
        {
            $this->delete_temp('temp_folder');
            $this->returnJson(array('status' => 'ok', 'redirect' => 'admin_setting_penilai/penilai?import=success', 'msg' => 'Import Success'));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Failed when Importing data, try Again'));
    }

}
