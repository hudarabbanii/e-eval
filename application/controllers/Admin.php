<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends PX_Controller {

	public function __construct() {
		parent::__construct();
		// $data_row_ctr = $this->model_basic->select_where($this->table_menu,'target','admin')->row();
		$this->controller_attr = array('controller' => 'admin','controller_name' => 'Admin','controller_id' => 0);
	}

	public function index() {
		$data = $this->controller_attr;
		if($this->session->userdata('admin') != FALSE){
			redirect('admin/dashboard');
		}
		else
			redirect('admin/login');
	}

	function dashboard() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin','admin');
		$data += $this->get_menu();
		if($this->session->userdata('admin') != FALSE){
			$data['content'] = $this->load->view('backend/admin/dashboard',$data,true);
			$this->load->view('backend/index',$data);
		}
		else
			redirect('admin');
	}

	function login() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Login','admin_login');
		$data += $this->get_menu();
		$this->load->view('backend/admin/login',$data);
	}

	function do_login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user_data = $this->model_basic->select_where($this->tbl_user,'username',$username)->row();
		if($user_data){
			if(md5($password) === $user_data->password){
				if($user_data->penilai_id != 0){
					$penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $user_data->penilai_id)->row();
					
					$user_group = $this->model_basic->select_where($this->tbl_usergroup,'id',$user_data->id_usergroup)->row()->usergroup_name;
					$data_user = array(
						'penilai_id'=> $user_data->penilai_id,
						'peserta_id'=> $user_data->peserta_id,
						'admin_id' => $user_data->id,
						'username' => $user_data->username,
						'password' => $password,
						'realname' => ($penilai->gelar_depan != "" ? $penilai->gelar_depan.' ' : '').$penilai->nama_lengkap.($penilai->gelar_belakang != "" ? ', '.$penilai->gelar_belakang : ''),
						'email' => $user_data->email,
						'id_usergroup' => $user_data->id_usergroup,
						'name_usergroup' => $user_group,
						'photo' => $user_data->photo
						);
					$this->session->set_userdata('admin',$data_user);
				}else{
					$user_group = $this->model_basic->select_where($this->tbl_usergroup,'id',$user_data->id_usergroup)->row()->usergroup_name;
					$data_user = array(
						'penilai_id'=> $user_data->penilai_id,
						'peserta_id'=> $user_data->peserta_id,
						'admin_id' => $user_data->id,
						'username' => $user_data->username,
						'password' => $password,
						'realname' => $user_data->realname,
						'email' => $user_data->email,
						'id_usergroup' => $user_data->id_usergroup,
						'name_usergroup' => $user_group,
						'photo' => $user_data->photo
						);
					$this->session->set_userdata('admin',$data_user);
				}

				$this->returnJson(array('status' => 'ok','msg' => 'Login berhasil, Anda akan segera diarahkan ke halaman utama..','redirect' => 'admin/dashboard'));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Login gagal, password salah..'));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Login gagal, username tidak terdaftar.'));
	}

	function do_logout() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Logout','admin_logout');
		$data += $this->get_menu();
		if($this->session->userdata('admin') != FALSE){
			$this->session->unset_userdata('admin');
			$this->session->unset_userdata('menu_pendidikan');
			$this->session->unset_userdata('var');
			$this->session->unset_userdata('jadwal');
			redirect('admin');
		}
		else
			redirect('admin');
	}

	function php_info(){
		echo phpinfo();
	}

	public function multi_dropdown()
	{
		$data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang', 'bidang');
        $data += $this->get_menu();
	   // $data['elemen'] = $this->model_eval->get_data();
	   $elemen = $this->model_eval->get_data();

		// $row = new stdClass;
	   // foreach ($elemen as $d_row) {
		  //  $row['name'] = $d_row->name;
		  //  $row['child'] = $d_row->child;
	   // }
	   $data['elemen'] = $elemen;
	   $data['content'] = $this->load->view('backend/admin/list', $data, true);
        $this->load->view('backend/index', $data);
	}

	function get_rekursif($pendidikan = 4, $peserta = 5, $parent = 29){
		$data = array();
		$result = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'parent_id', $parent);

		if($result->num_rows() == 0){
			$nilai = $this->model_basic->select_where_array($this->tbl_nilai_peserta, array('pendidikan_id'=>$pendidikan, 'peserta_id'=>$peserta));
		}

		foreach($result->result() as $row)
		{
		  	$data[] = array(
		        'id'  			=>$row->id,
		        'parent_id'   	=>$row->parent_id,
		        'name'   		=>$row->name,
		        'child'  		=>$this->get_rekursif(2,5, $row->id),
		        'nilai'			=>$this->get_nilai()
		    );
		}
		return $data;
	}

	function get_data_elemen_penilaian_detail($bagan_id, $parent=0){
	   $data = array();
	   $this->db->from($this->tbl_elemen_penilaian);
	   $this->db->where('parent_id',$parent);
	   $result = $this->db->get();
	 
	   foreach($result->result() as $row)
	   {
	   		$bobot = $this->get_bobot($bagan_id, $row->id);
	      	$data[] = array(
	            'id'  			=>$row->id,
	            'parent_id'   	=>$row->parent_id,
	            'name'   		=>$row->name,
	            'bobot'   		=>($bobot->num_rows() == 0 ? 0 : $bobot->row()->bobot),
	            'isform'		=>$this->check_isform($bagan_id, $row->id),
	            'child'  		=>$this->get_data_elemen_penilaian_detail($bagan_id, $row->id)
	        );
	   }
	   return $data;
	}

	function update_user_data(){
		$penilai = $this->model_basic->select_all($this->tbl_penilai);

		foreach ($penilai as $data_row) {
			$user = $this->model_basic->select_where($this->tbl_user, 'penilai_id', $data_row->id);
			// echo $data_row->nama_lengkap.'<br>';
			$array_update[$data_row->id] = array(
				'penilai_id' => $data_row->id,
				'realname' => $data_row->nama_lengkap,
				'photo' => '56edc2c076ca0-original.jpg'
			);
		}
        $this->db->trans_begin();
		$do_update = $this->model_basic->update_batch($this->tbl_user, 'penilai_id', $array_update);
		if ($this->db->trans_status() !== FALSE) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        } 
	}

	function update_all_penilai_password(){
		$penilai = $this->model_basic->select_where($this->tbl_user, 'id_usergroup', 3)->result();

		foreach ($penilai as $data_row) {
			$user = $this->model_basic->select_where($this->tbl_user, 'penilai_id', $data_row->id);
			// echo $data_row->nama_lengkap.'<br>';
			$array_update[$data_row->id] = array(
				'id' => $data_row->id,
				'password' => md5('password')
			);
		}
        $this->db->trans_begin();
		$do_update = $this->model_basic->update_batch($this->tbl_user, 'id', $array_update);
		if ($this->db->trans_status() !== FALSE) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        } 
	}
}
