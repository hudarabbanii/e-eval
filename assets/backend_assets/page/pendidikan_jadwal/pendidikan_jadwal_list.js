
function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}
//datatables function
var save_method; //for save method string
var table;

table = $('.datatable').DataTable({
  "stateSave": true,
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "bLengthChange": false,
  "dom": 'lBfrtip',
  "lengthMenu": [
      [ 10, 25, 50, -1 ],
      [ '10 rows', '25 rows', '50 rows', 'Show all' ]
    ],
  "buttons": [
  {
    extend: 'pageLength'
  }],
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "pendidikan_jadwal/ajax_pendidikan_jadwal_list",
      "type": "POST"/*,
      "data": function ( data ) {
                data.status = $('.btn-status').val();
            }*/
  },
  //Set column definition initialisation properties.
  "order": [[ 1, "desc" ]],
  "columnDefs": [
  { 
    "targets": [0], //last column
    "orderable": false, //set not orderable
  },
  {
    "className": "text-center",
    "targets": [ 0,1,2,3,4,5,-1 ], //add class
  }
  ]

});

$(document).ready(function(){
  $('#px-pendidikan_jadwal-message-form').validate({
    ignore: [],
    rules: {                                            
      id: {
        required: true
      }
    },
    submitHandler: function(form) {
      var target = $(form).attr('action');
      $('#px-pendidikan_jadwal-message-form .msg-status').text('Deleting data');
      $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        success : function(response){
          if(response.status == 'ok'){
            $('#px-pendidikan_jadwal-message-form .msg-status').text('Delete Success...');
            window.location.href = response.redirect;
          }
          else
            $('#px-pendidikan_jadwal-message-form .msg-status').text('Delete Failed');
        },
        error : function(jqXHR, textStatus, errorThrown) {
          alert(textStatus, errorThrown);
        }
      });
    }
  });
  $('body').delegate('.btn-delete','click',function(){
    $('#px-pendidikan_jadwal-message-box').addClass('open');
    var id = $(this).attr('data-target-id');
    $('#px-pendidikan_jadwal-message-form input[name="id"]').val('');
    $('#px-pendidikan_jadwal-message-form input[name="id"]').val(id);
  });
})