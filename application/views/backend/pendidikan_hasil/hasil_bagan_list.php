<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Ranking <?php echo $skema_penilaian->name ?></h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <table class="table datatable table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="6%" class="text-center">No</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">NPA</th>
                                    <th class="text-center">NNA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                  $no=1;
                                  foreach ($ranking as $data_row) { ?>
                                  <tr>
                                    <td class="text-center"><?php echo $no; ?></td>
                                    <td class="text-center"><?php echo $data_row->peserta_name; ?></td>
                                    <td class="text-center"><?php echo $data_row->npa; ?></td>
                                    <td class="text-center"><?php echo $data_row->nna; ?></td>
                                  </tr>
                                  <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script> -->

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_hasil/hasil_bagan_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
