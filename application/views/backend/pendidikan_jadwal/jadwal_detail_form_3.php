<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<!-- <div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div> -->

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <?php
            // var_dump($this->session->userdata('var'));
            ?>
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form</h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>PENILAIAN <?php echo strtoupper($elemen_penilaian); ?> - <?php echo strtoupper($elemen_penilaian_parent); ?><br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                            <hr>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <table class="table table-condensed no_border">
                                    <tr>
                                        <td>Judul</td>
                                        <td>:</td>
                                        <td><input id="judul_produk" class="form-control input-sm" type="text" name="judul" value=""></td>
                                    </tr>
                                    <tr>
                                        <td>Kelompok</td>
                                        <td>:</td>
                                        <td><?php echo $tim; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <p id="expired_remark" class="<?php echo ($is_active == 1 ? 'hidden' : ''); ?>" style="color: red;"><i>*Jadwal sudah melewati batas waktu pengisian nilai, silakan hubungi admin untuk mengaktifkan jadwal.</i></p>
                        <form id="px-pendidikan_jadwal-jadwal_detail-form" method="post" action="<?php echo $controller.'/pendidikan_jadwal_detail_add_3'; ?>">

                        <?php foreach ($list->tree as $parent_row) { ?>
                            <h4 style="margin-left:10px"><?php echo $parent_row->name; ?></h4>
                            <table class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th width="6%" class="text-center" style="vertical-align: middle">No</th>
                                        <th class="text-center" style="vertical-align: middle">Elemen yang Dinilai</th>
                                        <th class="text-center" style="vertical-align: middle">Bobot</th>
                                        <th class="text-center" style="vertical-align: middle">Nilai</th>
                                        <th class="text-center" style="vertical-align: middle">Nilai Bobot</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no=1;
                                    foreach ($parent_row->child->tree as $data_row) { ?>
                                    <tr>
                                        <?php if(isset($data_row->child) && !empty($data_row->child)){ ?>
                                            <td class="text-center" style="vertical-align: middle"><?php echo $no; ?></td>
                                            <td class="text-center" style="vertical-align: middle"><?php echo $data_row->name; ?></td>
                                            <td class="text-center" style="vertical-align: middle"><?php echo $data_row->bobot; ?></td>
                                            <td class="text-center">
                                                <input type="text" step=".001" class="form-control" value="<?php echo ($data_row->nilai != null ? number_format($data_row->nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>" name="nilai_<?php echo $pendidikan_peserta->id ?>_<?php echo $data_row->id ?>">
                                            </td>
                                            <td class="text-center" style="vertical-align: middle"><?php echo ($data_row->nilai_bobot != null ? number_format($data_row->nilai_bobot, 3, ".", "") : number_format(0, 3, ".", "")); ?></td>
                                            <?php }else{ ?>
                                            <td class="text-center" style="vertical-align: middle"><?php echo $no; ?></td>
                                            <td class="text-center" style="vertical-align: middle"><?php echo $data_row->name; ?></td>
                                            <td class="text-center" style="vertical-align: middle"><?php echo $data_row->bobot; ?></td>
                                            <td class="text-center">
                                                <input type="text" step=".001" class="form-control" value="<?php echo ($data_row->nilai != null ? number_format($data_row->nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>" name="nilai_<?php echo $pendidikan_peserta->id ?>_<?php echo $data_row->id ?>">
                                            </td>
                                            <td class="text-center" style="vertical-align: middle"><?php echo ($data_row->nilai_bobot != null ? number_format($data_row->nilai_bobot, 3, ".", "") : number_format(0, 3, ".", "")); ?></td>
                                            <?php } ?>
                                        
                                    </tr>
                                    <?php $no++; } ?>
                                    <tr>
                                        <th class="text-center" colspan="4">Nilai Akhir</th>
                                        <td class="text-center" style="vertical-align: middle"><b><?php echo number_format($parent_row->child->nilai_akademis, 3, '.', ''); ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php } ?>
                        <div class="panel-footer">
                            <input type="hidden" name="pendidikan_peserta_id" value="<?php echo $pendidikan_peserta->id ?>">
                            <input type="hidden" name="elemen_penilaian_id" value="<?php echo $elemen_penilaian_id ?>">
                            <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                            <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                            <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                            <button class="btn btn-primary pull-right" <?php echo ($is_active != 1 ? 'disabled' : ''); ?>>Simpan</button>
                            <a href="pendidikan_jadwal/print_form3?elemen_penilaian_id=<?php echo $elemen_penilaian_id; ?>&bagan_pendidikan_id=<?php echo $bagan_pendidikan_id; ?>&pendidikan_peserta_id=<?php echo $pendidikan_peserta_id; ?>" class="btn btn-info pull-right"><i class="fa fa-print"></i> Print PDF</a>
                        </div>
                        </form>

                        <!-- CATATAN -->
                        <div class="well">
                            <?php echo $keterangan ?>
                        </div>
                    </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<style type="text/css">
    .no_border td{
        border: none !important;
    }
</style>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script> -->

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_detail_form_3.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
