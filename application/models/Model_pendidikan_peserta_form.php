<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pendidikan_peserta_form extends CI_Model {

	var $column = array('a.id', 'a.nama_lengkap','c.name', 'c.golongan'); //set column field database for order and search
	var $order = array('a.nama_lengkap' => 'asc'); // default order

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$this->db->select('peserta_id');
		$this->db->from($this->tbl_pendidikan_peserta);
		$this->db->where('pendidikan_id', $pendidikan_id);
		$ignore_data = $this->db->get()->result();
		// var_dump($ignore_data);
		foreach ($ignore_data as $row) {
			$ignore[] = $row->peserta_id;
		}

		$this->db->select('a.*, b.name as jabatan, c.name as pangkat, c.golongan as golongan');
		$this->db->from($this->tbl_peserta.' a');
		$this->db->join($this->tbl_jabatan.' b', 'b.id = a.jabatan_id', 'left');
		$this->db->join($this->tbl_pangkat.' c', 'c.id = a.pangkat_id', 'left');
		if(!empty($ignore))
			$this->db->where_not_in('a.id', $ignore);
		$i = 1;
		$where = "";
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tbl_peserta);
		return $this->db->count_all_results();
	}	

}

/* End of file model_site_list.php */
/* Location: ./application/models/model_site_list.php */