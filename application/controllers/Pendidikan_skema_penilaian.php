<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class pendidikan_skema_penilaian extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_skema_penilaian', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $this->pendidikan_skema_penilaian();
    }

    function pendidikan_skema_penilaian() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan Skema Penilaian', 'pendidikan_skema_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        if($this->input->get('id')){
            $skema_penilaian = $this->model_eval->get_data_skema_penilaian($this->input->get('id'));
            $data['choosen'] = $this->input->get('id');
        }else{
            $skema_penilaian = $this->model_eval->get_data_skema_penilaian();
        }

        $data['skema_penilaian'] = $skema_penilaian;

        $dropdown = $this->model_basic->select_where_in($this->tbl_skema_penilaian, 'id', [1,2,3,4,5,6,7])->result();
        $data['dropdown'] = $dropdown;

        // echo "<pre>";
        // print_r($data['skema_penilaian']);
        // echo "</pre>";
        // die;

        $data['content'] = $this->load->view('backend/pendidikan_skema_penilaian/pendidikan_skema_penilaian_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_skema_penilaian_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Skema Penilaian', 'pendidikan_skema_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $skema_penilaian = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $id)->row();
            $parent = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $skema_penilaian->parent_id);
            if($parent->num_rows() > 0)
                $parent = $parent->row()->name;
            else
                $parent = '-';
            $data['parent'] = $parent;
            $data['data'] = $skema_penilaian;
        } else {
            $data['parent'] = '-';
            $data['data'] = null;
        }
        $data['elemen_penilaian'] = $this->model_eval->get_data_elemen_penilaian_detail($this->session->userdata('menu_pendidikan')['pendidikan_id'],0);
        $data['skema_penilaian'] = $this->model_eval->get_data_skema_penilaian($this->session->userdata('menu_pendidikan')['pendidikan_id'],0);
        $data['content'] = $this->load->view('backend/pendidikan_skema_penilaian/pendidikan_skema_penilaian_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_sub_skema_penilaian_form($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Skema Penilaian', 'pendidikan_skema_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        if ($id) {
            $skema_penilaian = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $id)->row();
            $data['data_add'] = $skema_penilaian;
            $data['parent'] = $skema_penilaian->name;
            $data['data'] = null; //new data
        } else {
            $data['parent'] = 'Parent';
            $data['data_add'] = null;
            $data['data'] = null;
        }
        $data['elemen_penilaian'] = $this->model_eval->get_data_elemen_penilaian_detail($this->session->userdata('menu_pendidikan')['pendidikan_id'],0);
        $data['skema_penilaian'] = $this->model_eval->get_data_skema_penilaian($this->session->userdata('menu_pendidikan')['pendidikan_id'],0);
        $data['content'] = $this->load->view('backend/pendidikan_skema_penilaian/pendidikan_sub_skema_penilaian_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_skema_penilaian_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Skema Penilaian', 'pendidikan_skema_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        if ($this->input->post('name')) {

            $id = $this->input->post('id');
            if($id)
                $insert['parent_id'] = $id;
            else
                $insert['parent_id'] = 0;
            $insert['bobot_nna'] = $this->input->post('bobot_nna');
            $insert['bobot_npa'] = $this->input->post('bobot_npa');
            $insert['name'] = $this->input->post('name');
            $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
            $insert['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];
            $do_insert = $this->model_basic->insert_all($this->tbl_skema_penilaian, $insert);

            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert Skema Penilaian '.$insert['name']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function pendidikan_skema_penilaian_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Skema Penilaian', 'pendidikan_skema_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_skema_penilaian);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['parent_id']);
        unset($update['pendidikan_id']);
        unset($update['bagan_pendidikan_id']);

        if ($this->input->post('name')) {
            $do_update = $this->model_basic->update($this->tbl_skema_penilaian, $update, 'id', $update['id']);
            if ($do_update) {
                $this->save_log_admin(ACT_UPDATE, 'Update Skema Penilaian '.$update['name']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function pendidikan_skema_penilaian_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Skema Penilaian', 'pendidikan_skema_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_skema_penilaian, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Skema Penilaian '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

}