<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_setting_penilai extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_setting_penilai', 'controller_name' => 'Admin Setting Penilai', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Setting Penilai', 'admin_setting_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['submenu'] = $this->get_submenu($data['controller']);

        $data['content'] = $this->load->view('backend/admin_setting_penilai/index',$data,true);
        $this->load->view('backend/index',$data);
    }

    function bidang() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang', 'bidang');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['bidang'] = $this->model_basic->select_all($this->tbl_bidang);

        $data['content'] = $this->load->view('backend/admin_setting_penilai/bidang_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bidang_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang', 'bidang');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $bidang = $this->model_basic->select_where($this->tbl_bidang, 'id', $id)->row();
            $data['data'] = $bidang;
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('backend/admin_setting_penilai/bidang_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bidang_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang', 'bidang');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_bidang);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_bidang, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Bidang '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function bidang_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang', 'bidang');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_bidang);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_bidang, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Bidang '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function bidang_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang', 'bidang');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_bidang, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_bidang, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Bidang '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function jabatan() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jabatan', 'jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['jabatan'] = $this->model_basic->select_all($this->tbl_jabatan);

        $data['content'] = $this->load->view('backend/admin_setting_penilai/jabatan_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jabatan_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jabatan', 'jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $jabatan = $this->model_basic->select_where($this->tbl_jabatan, 'id', $id)->row();
            $data['data'] = $jabatan;
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('backend/admin_setting_penilai/jabatan_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jabatan_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jabatan', 'jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_jabatan);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_jabatan, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Jabatan '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function jabatan_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jabatan', 'jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_jabatan);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_jabatan, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Jabatan '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function jabatan_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jabatan', 'jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_jabatan, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_jabatan, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Jabatan '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function pangkat() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pangkat', 'pangkat');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['pangkat'] = $this->model_basic->select_all($this->tbl_pangkat);

        $data['content'] = $this->load->view('backend/admin_setting_penilai/pangkat_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pangkat_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pangkat', 'pangkat');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $pangkat = $this->model_basic->select_where($this->tbl_pangkat, 'id', $id)->row();
            $data['data'] = $pangkat;
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('backend/admin_setting_penilai/pangkat_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pangkat_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pangkat', 'pangkat');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_pangkat);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['is_show'] = 0;

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_pangkat, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Pangkat '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function pangkat_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pangkat', 'pangkat');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_pangkat);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_pangkat, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Pangkat '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function pangkat_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pangkat', 'pangkat');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_pangkat, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_pangkat, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Pangkat '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function jenis_penilai() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Penilai', 'jenis_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['jenis_penilai'] = $this->model_basic->select_all($this->tbl_jenis_penilai);

        $data['content'] = $this->load->view('backend/admin_setting_penilai/jenis_penilai_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jenis_penilai_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Penilai', 'jenis_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $jenis_penilai = $this->model_basic->select_where($this->tbl_jenis_penilai, 'id', $id)->row();
            $data['data'] = $jenis_penilai;
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('backend/admin_setting_penilai/jenis_penilai_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jenis_penilai_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Penilai', 'jenis_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_jenis_penilai);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_jenis_penilai, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Jenis Penilai '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function jenis_penilai_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Penilai', 'jenis_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_jenis_penilai);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_jenis_penilai, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Jenis Penilai '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function jenis_penilai_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Penilai', 'jenis_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_jenis_penilai, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_jenis_penilai, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Jenis Penilai '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function penilai() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['penilai'] = $this->model_setting_penilai->get_penilai_all();

        $data['content'] = $this->load->view('backend/admin_setting_penilai/penilai_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function penilai_detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $penilai_detail = $this->model_setting_penilai->get_penilai_detail($id);
        if ($penilai_detail->num_rows() == 1)
            $data['penilai'] = $penilai_detail->row();
        else
            redirect('admin_setting_penilai/penilai');

        $data['content'] = $this->load->view('backend/admin_setting_penilai/penilai_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function penilai_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $id)->row();
            $data['data'] = $penilai;
        } else
            $data['data'] = null;

        $data['pangkat'] = $this->model_basic->select_all($this->tbl_pangkat);
        $data['jabatan'] = $this->model_basic->select_all($this->tbl_jabatan);
        $data['bidang'] = $this->model_basic->select_all($this->tbl_bidang);
        $data['jenis_penilai'] = $this->model_basic->select_all($this->tbl_jenis_penilai);
        $data['content'] = $this->load->view('backend/admin_setting_penilai/penilai_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function penilai_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_penilai);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        unset($insert['date_modified']);
        unset($insert['photo']);

        if ($this->input->post('nama_lengkap')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_penilai, $insert);
            if ($do_insert) {
                //insert login
                $data_insert_login = array(
                    'username' => 'p'.$do_insert->id,
                    'password' => $this->password_encode('password'),
                    'id_usergroup' => 3,
                    'penilai_id' => $do_insert->id);
                $this->model_basic->insert_all($this->tbl_user, $data_insert_login);

                $this->save_log_admin(ACT_CREATE, 'Insert New Penilai '.$insert['nama_lengkap']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function penilai_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_penilai);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        unset($update['date_created']);
        unset($update['photo']);

        $do_update = $this->model_basic->update($this->tbl_penilai, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Penilai '.$update['nama_lengkap']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function penilai_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_penilai, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_penilai, 'id', $id);
        $do_delete_login = $this->model_basic->delete($this->tbl_user, 'penilai_id', $id);
        if ($do_delete && $do_delete_login) {
            $this->save_log_admin(ACT_DELETE, 'Delete Penilai '.$deleted_data->nama_lengkap);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function export_datatable(){
        $list = $this->model_basic->select_all($this->tbl_bidang);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $data_row->name;

            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_basic->get_count($this->tbl_bidang),
                        "recordsFiltered" => $this->model_basic->get_count($this->tbl_bidang),
                        "data" => $data
                );
        echo json_encode($output);
    }

}