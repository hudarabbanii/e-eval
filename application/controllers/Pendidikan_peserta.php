<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_peserta extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_peserta', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index(){
        $this->pendidikan_peserta_list();
    }

    function pendidikan_peserta_list() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Peserta', 'pendidikan_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $data['peserta'] = $this->model_pendidikan->select_peserta_pendidikan($pendidikan_id);
        foreach ($data['peserta'] as $data_row) {
            if($data_row->tutor_penilai_id != 0){
                $tutor = $this->model_basic->select_where($this->tbl_penilai, 'id', $data_row->tutor_penilai_id);
                if($tutor->num_rows() == 1)
                    $data_row->tutor_name = $tutor->row()->gelar_depan.' '.$tutor->row()->nama_lengkap.($tutor->row()->gelar_belakang!="" ? ', '.$tutor->row()->gelar_belakang : '');
                else
                    $data_row->tutor_name = 'Data tidak ditemukan.';
            }else{
                $data_row->tutor_name = 'Data tidak ditemukan.';
            }
        }

        $data['content'] = $this->load->view('backend/pendidikan_peserta/peserta_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function ajax_status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $do_update = $this->model_basic->update($this->tbl_pendidikan_peserta, ['status'=>$status_change], 'id', $id);

        $this->returnJson(array('status' => 'ok', 'status_now'=>$status_change));
    }

    function pendidikan_peserta_detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Peserta', 'pendidikan_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $peserta_detail = $this->model_basic->select_where($this->tbl_peserta, 'id', $id);
        if ($peserta_detail->num_rows() == 1)
            $data['peserta'] = $peserta_detail->row();
        else
            redirect('pendidikan_peserta/peserta');

        $data['content'] = $this->load->view('backend/pendidikan_peserta/peserta_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    // function pendidikan_peserta_delete() {
    //     $data = $this->get_app_settings();
    //     $data += $this->controller_attr;
    //     $data += $this->get_function('Peserta', 'pendidikan_peserta');
    //     $data += $this->get_menu();
    //     $this->check_userakses($data['function_id'], ACT_DELETE);
    //     $id = $this->input->post('id');
    //     $deleted_data = $this->model_basic->select_where($this->tbl_peserta, 'id', $id)->row();
    //     $do_delete = $this->model_basic->delete($this->tbl_peserta, 'id', $id);
    //     if ($do_delete) {
    //         $this->save_log_admin(ACT_DELETE, 'Delete Peserta '.$deleted_data->name);
    //         $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
    //     } else {
    //         $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
    //     }
    // }

    // TAMBAH PESERTA
    // function pendidikan_peserta_form() {
    //     $data = $this->get_app_settings();
    //     $data += $this->controller_attr;
    //     $data += $this->get_function('Peserta', 'pendidikan_peserta');
    //     $data += $this->get_menu();
    //     $this->check_userakses($data['function_id'], ACT_CREATE);

    //     $data['content'] = $this->load->view('backend/pendidikan_peserta/peserta_form', $data, true);
    //     $this->load->view('backend/index', $data);
    // }

    // public function ajax_pendidikan_peserta_form()
    // {
    //     $list = $this->model_pendidikan_peserta_form->get_datatables();
    //     //die(print_r($this->db->last_query()));
    //     $data = array();
    //     $no = $_POST['start'];
    //     foreach ($list as $data_row) {
    //         //render data
    //         $no++;

    //         $row = array();
    //         $row[] = $data_row->id;
    //         $row[] = $no;
    //         $row[] = $data_row->nama_lengkap;
    //         $row[] = $data_row->pangkat;
    //         $row[] = $data_row->golongan;
        
    //         $data[] = $row;
    //     }

    //     $output = array(
    //                     "draw" => $_POST['draw'],
    //                     "recordsTotal" => $this->model_pendidikan_peserta_form->count_all(),
    //                     "recordsFiltered" => $this->model_pendidikan_peserta_form->count_filtered(),
    //                     "data" => $data,
    //             );
    //     //output to json format
    //     echo json_encode($output);
    // }

    // function pendidikan_peserta_add() {
    //     $data = $this->get_app_settings();
    //     $data += $this->controller_attr;
    //     $data += $this->get_function('Peserta', 'pendidikan_peserta');
    //     $data += $this->get_menu();
    //     $this->check_userakses($data['function_id'], ACT_CREATE);

    //     $pendidikan_id = $this->input->post('pendidikan_id');
    //     $peserta_add = $this->input->post('id');

    //     foreach ($peserta_add as $id) {
    //         $insert = array();
    //         $insert['pendidikan_id'] = $pendidikan_id;
    //         $insert['peserta_id'] = $id;

    //         $do_insert = $this->model_basic->insert_all($this->tbl_pendidikan_peserta, $insert);
    //     }
    //     if($do_insert)
    //         $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller']));
    //     else
    //         $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
    // }

    function pendidikan_peserta_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan Peserta', 'pendidikan_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $pendidikan_peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $id)->row();
            $data['data'] = $pendidikan_peserta;
        } else
            $data['data'] = null;

        $data['pangkat'] = $this->model_basic->select_all($this->tbl_pangkat);
        $data['instansi'] = $this->model_basic->select_all($this->tbl_instansi);
        $data['tutor'] = $this->model_basic->select_all($this->tbl_penilai);
        foreach ($data['tutor'] as $data_row) {
            $data_row->tutor_name = $data_row->gelar_depan.' '.$data_row->nama_lengkap.($data_row->gelar_belakang!="" ? ', '.$data_row->gelar_belakang : '');
        }
        $data['content'] = $this->load->view('backend/pendidikan_peserta/peserta_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_peserta_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan Peserta', 'pendidikan_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_pendidikan_peserta);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['pendidikan_id'] = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        unset($insert['date_modified']);

        if ($this->input->post('nama_lengkap')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_pendidikan_peserta, $insert);
            if ($do_insert) {
                //create login
                $insert_login['username'] = 'peserta'.$do_insert->id;
                $insert_login['password'] = $this->password_encode('peserta');
                $insert_login['id_usergroup'] = 2;
                $insert_login['peserta_id'] = $do_insert->id;
                $do_insert_login = $this->model_basic->insert_all($this->tbl_user, $insert_login);
                if($do_insert_login){
                    $this->save_log_admin(ACT_CREATE, 'Insert New Pendidikan Peserta '.$insert['nama_lengkap']);
                    $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
                }else{
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed to create user login'));
                }
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function pendidikan_peserta_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan Peserta', 'pendidikan_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_pendidikan_peserta);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['date_created'] = date('Y-m-d H:i:s', now());
        unset($update['date_created']);
        unset($update['pendidikan_id']);
        unset($update['status']);

        $do_update = $this->model_basic->update($this->tbl_pendidikan_peserta, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Pendidikan Peserta '.$update['nama_lengkap']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function pendidikan_peserta_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pendidikan Peserta', 'pendidikan_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_pendidikan_peserta, 'id', $id);
        $do_delete_user = $this->model_basic->delete($this->tbl_user, 'peserta_id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Pendidikan Peserta '.$deleted_data->nama_lengkap);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    //API
    function consume_api_insert_peserta() {
        $table = $this->input->post('table');
        $select = $this->input->post('select');
        $where = $this->input->post('where');
        $limit = $this->input->post('limit');
        $angkatan = $this->input->post('angkatan');
        // $table = 'peserta';
        // $select = 'gelar_depan, gelar_belakang, nama, NIP, jabatan, pangkat, instansi_negara';
        // $where = "angkatan = '7'";
        // $limit = '10000';
        $where = "angkatan='".$angkatan."'";
        $post_data = ['table' => $table, 'select' => $select, 'where' => $where, 'limit' => $limit];

        $data = $this->akses_api($post_data);
        $result_replace = str_replace("=", ":", $data);
        $result= json_decode($result_replace, true);
        // var_dump($result); 
        $no = 1;
        $this->db->trans_start();
        foreach ($result as $data_row) {
            $insert_array = array();
            foreach ($data_row as $field) {
                $insert_array[] = $field;
            }
            $insert['pendidikan_id'] = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
            $insert['gelar_depan'] = $insert_array[0];
            $insert['gelar_belakang'] = $insert_array[1];
            $insert['nama_lengkap'] = $insert_array[2];
            $insert['nip'] = $insert_array[3];
            $insert['jabatan'] = $insert_array[4];
            $insert['pangkat'] = $insert_array[5];
            $insert['instansi_negara'] = $insert_array[6];
            $insert['date_created'] = date('Y-m-d H:i:s', now());

            $check = $this->model_basic->select_where_like($this->tbl_pendidikan_peserta, 'pendidikan_id', $insert['pendidikan_id'], 'nama_lengkap', $insert['nama_lengkap']);
            if($check->num_rows() == 0){
                $do_insert = $this->model_basic->insert_all($this->tbl_pendidikan_peserta, $insert);
                $this->save_log_admin(ACT_DELETE, 'Insert Pendidikan Peserta '.$insert['nama_lengkap']);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Insert failed'));
            $this->db->trans_rollback();
        } else {
            $this->returnJson(array('status' => 'ok', 'msg' => 'Insert Success', 'redirect' => 'pendidikan_peserta'));
        }
        // exit();
    }

    function akses_api($data) {
        $first_url = "http://peserta.lemhannas.go.id/index.php/Api/select";
        $url = $first_url;
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'uniquekey: 31f1d09548802021288cf4f07e5b9b0b', 'Content-type: multipart/form-data'
        ));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return json_encode(['status' => 'error', 'msg' => $info]);
        }
        curl_close($curl);
        return $curl_response;
    }

    function pendidikan_peserta_order_edit(){
        $item = $this->input->post('item');
        if(count($item) > 0){
            $orders = 1;
            $error = 0;
            foreach ($item as $peserta_id) {
                $update['orders'] = $orders;
                if(!$this->model_basic->update($this->tbl_pendidikan_peserta,$update,'id',$peserta_id))
                    $error++;
                $orders++;
            }
            if($error < 1)
                $this->returnJson(array('status' => 'ok','msg' => 'Update success'));
            else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Please check the form'));
    }
}