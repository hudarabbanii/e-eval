$('.datatable').DataTable({
 	"processing": true,
	"bLengthChange": false,
	"dom": 'lBfrtip',
	"lengthMenu": [
			[ 10, 25, 50, -1 ],
			[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
	"buttons": [
	{
		extend: 'pageLength'
	},
	{
		extend: 'print',
		autoPrint: true,
		exportOptions: {
            columns: [0,1]
        },
            customize: function ( win ) {
				$(win.document.body).find( 'table' )
					.addClass( 'compact' )
					.css( 'font-size', 'inherit' );
					
				$(win.document.body)
					.css( 'font-size', '10pt' )
					.prepend(
						'<div style=" text-align:center; border-bottom:1px solid #ccc; margin-bottom:20px;"><h3>JENIS PENILAI</h3></div>'
					);
			}
	},
	{
		extend: 'excel',
		exportOptions: {
            columns: [0,1]
        }
	}
	],
	"columnDefs": [
	{ 
	    "targets": [0,-1],
	    "orderable": false,
	}
	]
});

$(document).ready(function(){
	$('#px-setting_penilai-jenis_penilai_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-setting_penilai-jenis_penilai_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-setting_penilai-jenis_penilai_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-setting_penilai-jenis_penilai_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-setting_penilai-jenis_penilai_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-setting_penilai-jenis_penilai_list-message-form input[name="id"]').val('');
		$('#px-setting_penilai-jenis_penilai_list-message-form input[name="id"]').val(id);
	});
})