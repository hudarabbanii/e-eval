<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Ubah Tim Sementara</h3>
                    <button onclick="history.go(-1)" class="btn btn-default pull-right">Kembali</button>
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit; else echo $controller . '/' . $function_add; ?>" method="POST" id="px-pendidikan_ubah_tim_sementara-form">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="col-md-6">
                            <h4>Jadwal awal:</h4>
                            <div class="form-group">
                                <label class="col-md-2 col-xs-12 control-label">Jadwal</label>
                                <div class="col-md-9 col-xs-12">
                                    <select class="select2" name="jadwal_id" id="px-pendidikan_ubah_tim_sementara-form-jadwal_id" required>
                                        <option value="">Pilih</option>
                                        <?php foreach($jadwal as $data_row) { ?>
                                        <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->jadwal_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->tanggal.' | '.$data_row->elemen_penilaian.' ('.$data_row->bidang_studi.') - '.$data_row->tim ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-xs-12 control-label">Peserta</label>
                                <div class="col-md-9 col-xs-12">
                                    <select class="select2" name="pendidikan_peserta_id" id="px-pendidikan_ubah_tim_sementara-form-pendidikan_peserta_id" required>
                                        <option value="">Pilih</option>
                                        <?php foreach($peserta as $data_row) { ?>
                                        <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->pendidikan_peserta_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->nama_lengkap ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Jadwal pengganti:</h4>
                            <div class="form-group">
                                <label class="col-md-2 col-xs-12 control-label">Jadwal</label>
                                <div class="col-md-9 col-xs-12">
                                    <select class="select2" name="jadwal_pengganti_id" id="px-pendidikan_ubah_tim_sementara-form-jadwal_pengganti_id" required>
                                        <option value="">Pilih</option>
                                        <?php foreach($jadwal as $data_row) { ?>
                                        <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->jadwal_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->tanggal.' | '.$data_row->elemen_penilaian.' ('.$data_row->bidang_studi.') - '.$data_row->tim ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-xs-12 control-label">Catatan</label>
                                <div class="col-md-9 col-xs-12">
                                    <textarea class="form-control" type="text" name="remark" id="px-pendidikan_ubah_tim_sementara-form-catatan" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="margin-top: 10px;">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_ubah_tim_sementara/tim_form.js"></script>