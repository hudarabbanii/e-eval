<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_setting_penilai extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function get_penilai_all(){
		$this->db->select('a.*, b.name as jabatan, c.name as pangkat, c.golongan as golongan, d.name as bidang, e.name as jenis_penilai, f.username as username');
		$this->db->from($this->tbl_penilai.' a');
		$this->db->join($this->tbl_jabatan.' b', 'b.id = a.jabatan_id', 'left');
		$this->db->join($this->tbl_pangkat.' c', 'c.id = a.pangkat_id', 'left');
		$this->db->join($this->tbl_bidang.' d', 'd.id = a.bidang_id', 'left');
		$this->db->join($this->tbl_jenis_penilai.' e', 'e.id = a.jenis_penilai_id', 'left');
		$this->db->join($this->tbl_user.' f', 'f.penilai_id = a.id', 'left');
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get()->result();
	}

	function get_penilai_detail($id){
		$this->db->select('a.*, b.name as jabatan, c.name as pangkat, c.golongan as golongan, d.name as bidang, e.name as jenis_penilai, f.username as username, f.password as password');
		$this->db->from($this->tbl_penilai.' a');
		$this->db->join($this->tbl_jabatan.' b', 'b.id = a.jabatan_id', 'left');
		$this->db->join($this->tbl_pangkat.' c', 'c.id = a.pangkat_id', 'left');
		$this->db->join($this->tbl_bidang.' d', 'd.id = a.bidang_id', 'left');
		$this->db->join($this->tbl_jenis_penilai.' e', 'e.id = a.jenis_penilai_id', 'left');
		$this->db->join($this->tbl_user.' f', 'f.penilai_id = a.id', 'left');
		$this->db->where('a.id', $id);
		return $this->db->get();
	}
}