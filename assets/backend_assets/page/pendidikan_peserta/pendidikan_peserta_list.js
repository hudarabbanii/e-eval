$(document).ready(function(){
	$('.datatable').DataTable({
	 	"processing": true,
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		},
		{
			extend: 'print',
			autoPrint: true,
			exportOptions: {
                columns: [0,1,2,3,4]
            },
                customize: function ( win ) {
					$(win.document.body).find( 'table' )
						.addClass( 'compact' )
						.css( 'font-size', 'inherit' );
						
					$(win.document.body)
						.css( 'font-size', '10pt' )
						.prepend(
							'<div style=" text-align:center; border-bottom:1px solid #ccc; margin-bottom:20px;"><h3>BIDANG</h3></div>'
						);
				}
		},
		{
			extend: 'excel',
			exportOptions: {
                columns: [0,1,2,3,4]
            }
		},
		// {
		// 	text: 'Adv Search',
		// 	action: function ( e, dt, node, config ) {
		// 		$('#advsearch').show();
		// 	}
		// }
		],
		"columnDefs": [
		{ 
		    "targets": [0,-1],
		    "orderable": false,
		}
		]
	});


	$('#px-pendidikan_peserta-peserta_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-pendidikan_peserta-peserta_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-pendidikan_peserta-peserta_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-pendidikan_peserta-peserta_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-pendidikan_peserta-peserta_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-pendidikan_peserta-peserta_list-message-form input[name="id"]').val('');
		$('#px-pendidikan_peserta-peserta_list-message-form input[name="id"]').val(id);
	});

	//syncronize
	$('#px-pendidikan_peserta-get_api-message-form').validate({
		ignore: [],
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-pendidikan_peserta-get_api-message-box').removeClass('open');
			$('#px-pendidikan_peserta .alert-warning').removeClass('hidden');
            $('#px-pendidikan_peserta .alert-success').addClass('hidden');
            $('#px-pendidikan_peserta .alert-danger').addClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
				$('#px-pendidikan_peserta .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-pendidikan_peserta .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-pendidikan_peserta .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-api','click',function(){
		$('#px-pendidikan_peserta-get_api-message-box').addClass('open');
		var table = $('#table').val();
		var select = $('#select').val();
		var where = $('#where').val();
		var limit = $('#limit').val();
		$('#px-pendidikan_peserta-get_api-message-form input[name="table"]').val('');
		$('#px-pendidikan_peserta-get_api-message-form input[name="table"]').val(table);
		$('#px-pendidikan_peserta-get_api-message-form input[name="select"]').val('');
		$('#px-pendidikan_peserta-get_api-message-form input[name="select"]').val(select);
		$('#px-pendidikan_peserta-get_api-message-form input[name="where"]').val('');
		$('#px-pendidikan_peserta-get_api-message-form input[name="where"]').val(where);
		$('#px-pendidikan_peserta-get_api-message-form input[name="limit"]').val('');
		$('#px-pendidikan_peserta-get_api-message-form input[name="limit"]').val(limit);
	});

	$('body').delegate('#btn-status','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-status');
		$.ajax({
			url : 'pendidikan_peserta/ajax_status',
			type : 'POST',
			dataType : 'json',
			data : {id:id,
					status:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-pendidikan_peserta .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-pendidikan_peserta .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 1){
						$('#btn-status[data-id="'+id+'"]').removeClass('btn-warning');
						$('#btn-status[data-id="'+id+'"] span').removeClass('fa-times');
						$('#btn-status[data-id="'+id+'"]').attr('data-status','1');
						$('#btn-status[data-id="'+id+'"]').addClass('btn-success');
						$('#btn-status[data-id="'+id+'"] span').addClass('fa-check');
					}else{
						$('#btn-status[data-id="'+id+'"]').removeClass('btn-success');
						$('#btn-status[data-id="'+id+'"][data-id="'+id+'"] span').removeClass('fa-check');
						$('#btn-status[data-id="'+id+'"]').attr('data-status','0');
						$('#btn-status[data-id="'+id+'"]').addClass('btn-warning');
						$('#btn-status[data-id="'+id+'"] span').addClass('fa-times');
					}
				}
				else{
					$('#px-pendidikan_peserta .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-pendidikan_peserta .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});
})