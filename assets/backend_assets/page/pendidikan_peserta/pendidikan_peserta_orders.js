$(document).ready(function(){
	$('.urutan-save').click(function(){
		var data = $('.panel-dragable .list-group').sortable('serialize');
		$('#px-pendidikan_peserta-orders-dragable-footer .alert-warning').removeClass('hidden');
        $('#px-pendidikan_peserta-orders-dragable-footer .alert-success').addClass('hidden');
        $('#px-pendidikan_peserta-orders-dragable-footer .alert-danger').addClass('hidden');
		$.ajax({
			type: 'POST',
			dataType: 'json',
			data : data,
			url : 'pendidikan_peserta/pendidikan_peserta_order_edit',
			success: function(){
                $('#px-pendidikan_peserta-orders-dragable-footer .alert-warning').addClass('hidden');
            	$('#px-pendidikan_peserta-orders-dragable-footer .alert-success').removeClass('hidden');
                setTimeout(function(){
					window.location.href = 'pendidikan_peserta';
                }, 1000);
			},
			error : function(jqXHR, textStatus, errorThrown) {
                $('#px-pendidikan_peserta-orders-dragable-footer .alert-danger').removeClass('hidden');	
            }
		});
	});
	$('.panel-dragable .list-group').sortable({
		axis: 'y'
	});
})