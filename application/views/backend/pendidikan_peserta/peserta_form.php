<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Penilai</h3>                              
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit; else echo $controller . '/' . $function_add; ?>" method="POST" id="px-pendidikan_peserta-form">
                    <input type="hidden" name="id" id="px-pendidikan_peserta-form-id" value="<?php if ($data) echo $data->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-gelar_depan">Gelar Depan</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="gelar_depan" id="px-pendidikan_peserta-form-gelar_depan" value="<?php if ($data) echo $data->gelar_depan; ?>" placeholder="Gelar Depan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-nama_lengkap">Nama Lengkap</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="nama_lengkap" id="px-pendidikan_peserta-form-nama_lengkap" value="<?php if ($data) echo $data->nama_lengkap; ?>" placeholder="Nama Lengkap" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-gelar_belakang">Gelar Belakang</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="gelar_belakang" id="px-pendidikan_peserta-form-gelar_belakang" value="<?php if ($data) echo $data->gelar_belakang; ?>" placeholder="Gelar Belakang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-nip">NIP</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="nip" id="px-pendidikan_peserta-form-nip" value="<?php if ($data) echo $data->nip; ?>" placeholder="NIP">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-nrp">NRP</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="nrp" id="px-pendidikan_peserta-form-nrp" value="<?php if ($data) echo $data->nrp; ?>" placeholder="NRP">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-jabatan">Jabatan</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="jabatan" id="px-pendidikan_peserta-form-jabatan" value="<?php if ($data) echo $data->jabatan; ?>" placeholder="Jabatan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-pangkat">Pangkat</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="pangkat" id="px-pendidikan_peserta-form-pangkat" value="<?php if ($data) echo $data->pangkat; ?>" placeholder="Pangkat">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendidikan_peserta-form-instansi_negara">Instansi Negara</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="instansi_negara" id="px-pendidikan_peserta-form-instansi_negara" value="<?php if ($data) echo $data->instansi_negara; ?>" placeholder="Instansi Negara">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Tutor</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="tutor_penilai_id" id="px-pendidikan-form-tutor_penilai_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($tutor as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->tutor_penilai_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->tutor_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Pangkat</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="pangkat_id" id="px-pendidikan_peserta-form-pangkat_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($pangkat as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->pangkat_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Instansi</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="instansi_id" id="px-pendidikan_peserta-form-instansi_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($instansi as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->instansi_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> -->
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_peserta/pendidikan_peserta_form.js"></script>