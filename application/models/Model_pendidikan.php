<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pendidikan extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function select_penilai_pendidikan($pendidikan_id){
		$this->db->select('b.*, c.name as jabatan, d.name as pangkat, d.golongan as golongan, e.name as bidang, f.name as jenis_penilai');
		$this->db->from($this->tbl_pendidikan_penilai.' a');
		$this->db->join($this->tbl_penilai.' b', 'b.id = a.penilai_id', 'left');
		$this->db->join($this->tbl_jabatan.' c', 'c.id = b.jabatan_id', 'left');
		$this->db->join($this->tbl_pangkat.' d', 'd.id = b.pangkat_id', 'left');
		$this->db->join($this->tbl_bidang.' e', 'e.id = b.bidang_id', 'left');
		$this->db->join($this->tbl_jenis_penilai.' f', 'f.id = b.jenis_penilai_id', 'left');
		$this->db->where('a.pendidikan_id', $pendidikan_id);
		$this->db->order_by('b.nama_lengkap', 'ASC');
		return $this->db->get()->result();
	}

	function select_peserta_pendidikan($pendidikan_id){
		$this->db->select('a.*');
		$this->db->from($this->tbl_pendidikan_peserta.' a');
		$this->db->where('a.pendidikan_id', $pendidikan_id);
		$this->db->order_by('a.orders', 'ASC');
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get()->result();
	}

	function select_peserta_pendidikan_tim($pendidikan_id, $elemen_penilaian_id){
		$this->db->select('a.*, c.*');
		$this->db->from($this->tbl_tim_detail.' a');
		$this->db->join($this->tbl_tim.' b', 'b.id = a.tim_id', 'left');
		$this->db->join($this->tbl_pendidikan_peserta.' c', 'c.id = a.pendidikan_peserta_id', 'left');
		$this->db->where('b.pendidikan_id', $pendidikan_id);
		$this->db->where('b.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get()->result();
	}
}