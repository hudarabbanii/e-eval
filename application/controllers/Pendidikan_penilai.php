<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_penilai extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_penilai', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index(){
        $this->penilai();
    }

    function penilai() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'pendidikan_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $data['penilai'] = $this->model_pendidikan->select_penilai_pendidikan($pendidikan_id);

        $data['content'] = $this->load->view('backend/pendidikan_penilai/penilai_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function penilai_detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'pendidikan_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $penilai_detail = $this->model_basic->select_where($this->tbl_penilai, 'id', $id);
        if ($penilai_detail->num_rows() == 1)
            $data['penilai'] = $penilai_detail->row();
        else
            redirect('pendidikan_penilai/penilai');

        $data['content'] = $this->load->view('backend/pendidikan_penilai/penilai_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function penilai_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'pendidikan_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);

        $pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $id = $this->input->post('id');

        $deleted_data = $this->model_basic->select_where_array($this->tbl_pendidikan_penilai, ['pendidikan_id'=>$pendidikan_id, 'penilai_id'=>$id])->row();
        $do_delete = $this->model_basic->delete_where_array($this->tbl_pendidikan_penilai, ['pendidikan_id'=>$pendidikan_id, 'penilai_id'=>$id]);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Pendidikan Penilai Penilai ID:'.$deleted_data->penilai_id.' - Pendidikan ID: '.$pendidikan_id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    // TAMBAH PENILAI
    function pendidikan_penilai_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'pendidikan_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $data['content'] = $this->load->view('backend/pendidikan_penilai/penilai_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    public function ajax_pendidikan_penilai_form()
    {
        $list = $this->model_pendidikan_penilai_form->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;

            $row = array();
            $row[] = $data_row->id;
            $row[] = $no;
            $row[] = $data_row->jenis_penilai;
            $row[] = $data_row->nama_lengkap;
            $row[] = $data_row->pangkat;
            $row[] = $data_row->golongan;
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_pendidikan_penilai_form->count_all(),
                        "recordsFiltered" => $this->model_pendidikan_penilai_form->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function pendidikan_penilai_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Penilai', 'pendidikan_penilai');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $pendidikan_id = $this->input->post('pendidikan_id');
        $penilai_add = $this->input->post('id');

        foreach ($penilai_add as $id) {
            $insert = array();
            $insert['pendidikan_id'] = $pendidikan_id;
            $insert['penilai_id'] = $id;
            // $insert['jabatan'] = $id;

            $do_insert = $this->model_basic->insert_all($this->tbl_pendidikan_penilai, $insert);
        }
        if($do_insert)
            $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller']));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
    }
}