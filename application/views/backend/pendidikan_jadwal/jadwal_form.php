<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Jadwal</h3>                              
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit; else echo $controller . '/' . $function_add; ?>" method="POST" id="px-pendidikan_jadwal-jadwal-form">
                    <input type="hidden" name="id" id="px-pendidikan_jadwal-jadwal-form-id" value="<?php if ($data) echo $data->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Tanggal</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control datepicker" name="tanggal" id="px-pendidikan_jadwal-form-tanggal" value="<?php if($data) echo $data->tanggal ?>" required />
                            </div>
                        </div>
                        <?php if($data_penilai) { ?>
                        <div id="form-wrapper">
                        <?php $count=1; foreach ($data_penilai as $field_row) { ?>
                            <div class="form-group">
                                <label class="col-md-2 col-xs-12 control-label">Penilai</label>
                                <div class="col-md-7 col-xs-12 penilai_count">
                                    <select class="select2" name="penilai_id[]" id="px-pendidikan_jadwal-form-penilai_id" required>
                                        <option value="">Pilih</option>
                                        <?php foreach($penilai as $data_row) { ?>
                                        <option value="<?php echo $data_row->id ?>" <?php if ($data) if($field_row->penilai_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->penilai ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-2 col-xs-6">
                                    <?php if($count==1){ ?>
                                    <a href="javascript:void(0);" class="add_button btn btn-success btn-sm" title="Tambah Penilai" ><span class="fa fa-plus" aria-hidden="true"></span></a>
                                    <?php }else{ ?>
                                    <a href="javascript:void(0);" class="remove_button btn btn-warning btn-sm" title="Hapus Penilai" ><span class="fa fa-minus" aria-hidden="true"></span></a>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php $count++; } ?>
                        </div>
                        <?php }else{ ?>
                            <div id="form-wrapper">
                                <div class="form-group">
                                    <label class="col-md-2 col-xs-12 control-label">Penilai</label>
                                    <div class="col-md-7 col-xs-12">
                                        <select class="select2" name="penilai_id[]" id="px-pendidikan_jadwal-form-penilai_id" required>
                                            <option value="">Pilih</option>
                                            <?php foreach($penilai as $data_row) { ?>
                                            <option value="<?php echo $data_row->id ?>"><?php echo $data_row->penilai ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                    <a href="javascript:void(0);" class="add_button btn btn-success btn-sm" title="Tambah Penilai" ><span class="fa fa-plus" aria-hidden="true"></span></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Elemen Penilaian</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="elemen_penilaian_id" id="px-pendidikan_jadwal-form-elemen_penilaian_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($elemen_penilaian as $data_row) { ?>
                                    <option value="<?php echo $data_row['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $data_row['id']) echo 'selected'; if($data_row['is_show']==0) echo 'disabled'; ?>><?php echo $data_row['name'] ?></option>
                                        <?php 
                                        if($data_row['child']){
                                        foreach($data_row['child'] as $lvl2) { ?>
                                            <option value="<?php echo $lvl2['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $lvl2['id']) echo 'selected'; if($lvl2['is_show']==0) echo 'disabled'; ?>><?php echo '&nbsp&nbsp>>'.$lvl2['name'] ?></option>
                                            <?php 
                                            if($lvl2['child']){
                                                foreach($lvl2['child'] as $lvl3) { ?>
                                                <option value="<?php echo $lvl3['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $lvl3['id']) echo 'selected'; if($lvl3['is_show']==0) echo 'disabled'; ?>><?php echo '&nbsp&nbsp&nbsp&nbsp>>>'.$lvl3['name'] ?></option>
                                                <?php 
                                                    if($lvl3['child']){
                                                    foreach($lvl3['child'] as $lvl4) { ?>
                                                    <option value="<?php echo $lvl4['id'] ?>" <?php if ($data) if($data->elemen_penilaian_id == $lvl4['id']) echo 'selected'; if($lvl4['is_show']==0) echo 'disabled'; ?>><?php echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp>>>'.$lvl4['name'] ?></option>
                                                    <?php 
                                                    }
                                                }
                                                ?>
                                                <?php 
                                                }
                                            }
                                        } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Bidang Studi</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="bidang_studi_id" id="px-pendidikan_jadwal-form-bidang_studi_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($bidang_studi as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->bidang_studi_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Nama Kelompok</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="tim_id" id="px-pendidikan_jadwal-form-tim_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($tim as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->tim_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Aktivasi Manual</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="manual_activation" id="px-pendidikan_jadwal-form-manual_activation">
                                    <option value="0" <?php if ($data) if($data->manual_activation == 0) echo 'selected'; ?>>Nonaktif</option>
                                    <option value="1" <?php if ($data) if($data->manual_activation == 1) echo 'selected'; ?>>Aktif</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Catatan</label>
                            <div class="col-md-9 col-xs-12">
                                <textarea class="form-control" id="px-pendidikan_jadwal-form-remark" name="remark" placeholder="*Wajib diisi bila melakukan aktivasi manual"><?php if ($data) echo $data->remark; ?></textarea>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>
<style type="text/css">#form-wrapper{margin-bottom: 15px;}</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_form.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    //Penilai Input
    var maxField = 3; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('#form-wrapper'); //Input field wrapper
    var fieldHTML = '<div class="form-group">'+
                    '<label class="col-md-2 col-xs-12 control-label">Penilai</label>'+
                    '<div class="col-md-7 col-xs-12">'+
                        '<select class="select2" name="penilai_id[]" id="px-pendidikan_jadwal-form-penilai_id" required>'+
                            '<option value="">Pilih</option>'+
                            '<?php foreach($penilai as $data_row) { ?>'+
                            '<option value="<?php echo $data_row->id ?>"><?php echo $data_row->penilai ?></option>'+
                            '<?php } ?>'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-md-2 col-xs-6">'+
                    '<a href="javascript:void(0);" class="remove_button btn btn-warning btn-sm" title="Hapus Penilai" ><span class="fa fa-minus" aria-hidden="true"></span></a>'+
                    '</div></div>';
                    //New input field html 
    var x = $('.penilai_count').length; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
            $("select.select2").select2();
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>