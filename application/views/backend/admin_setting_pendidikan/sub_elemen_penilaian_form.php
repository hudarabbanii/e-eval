<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data</h3>        
                    <button onclick="history.go(-1)" class="btn btn-default pull-right">Kembali</button>                      
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit;
else echo $controller . '/' . $function_add; ?>" method="POST" id="px-setting_pendidikan-elemen_penilaian-form">
                    <input type="hidden" name="id" id="px-setting_pendidikan-elemen_penilaian-form-id" value="<?php if ($data_add) echo $data_add->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-setting_pendidikan-elemen_penilaian-form-parent">Parent</label>
                            <div class="col-md-9 col-xs-12">
                                <span class="form-control"><?php echo $parent ?></span>
                            </div>
                        </div>
                        <div id="form-wrapper" class="form-group">
                            <div class="form-group">
                                <label class="col-md-2 col-xs-12 control-label" for="#px-setting_pendidikan-elemen_penilaian-form-name">Elemen Penilaian</label>
                                <div class="col-md-7 col-xs-12">
                                    <input type="text" class="form-control" name="name[]" id="px-setting_pendidikan-elemen_penilaian-form-name" value="" placeholder="Nama Elemen Penilaian" required>
                                </div>
                                <div class="col-md-2 col-xs-6">
                                    <a href="javascript:void(0);" class="add_button btn btn-success btn-sm" title="Tambah Elemen" ><span class="fa fa-plus" aria-hidden="true"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>              
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/setting_pendidikan/elemen_penilaian_form.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    //Penilai Input
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('#form-wrapper'); //Input field wrapper
    var fieldHTML = '<div class="form-group">'+
                    '<label class="col-md-2 col-xs-12 control-label">Elemen Penilaian</label>'+
                    '<div class="col-md-7 col-xs-12">'+
                        '<input type="text" class="form-control" name="name[]" id="px-setting_pendidikan-elemen_penilaian-form-name" placeholder="Nama Elemen Penilaian" required>'+
                    '</div>'+
                    '<div class="col-md-2 col-xs-6">'+
                    '<a href="javascript:void(0);" class="remove_button btn btn-warning btn-sm" title="Hapus Elemen Penilaian" ><span class="fa fa-minus" aria-hidden="true"></span></a>'+
                    '</div></div>';
                    //New input field html 
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>