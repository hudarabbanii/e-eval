<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_setting_pendidikan extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_setting_pendidikan', 'controller_name' => 'Admin Setting Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Setting Pendidikan', 'admin_setting_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['submenu'] = $this->get_submenu($data['controller']);

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/index',$data,true);
        $this->load->view('backend/index',$data);
    }

    function jenis_pendidikan() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Pendidikan', 'jenis_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['jenis_pendidikan'] = $this->model_basic->select_all($this->tbl_jenis_pendidikan);

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/jenis_pendidikan_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jenis_pendidikan_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Pendidikan', 'jenis_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $jenis_pendidikan = $this->model_basic->select_where($this->tbl_jenis_pendidikan, 'id', $id)->row();
            $data['data'] = $jenis_pendidikan;
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/jenis_pendidikan_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jenis_pendidikan_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Pendidikan', 'jenis_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_jenis_pendidikan);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_jenis_pendidikan, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Jenis Pendidikan '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function jenis_pendidikan_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Pendidikan', 'jenis_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_jenis_pendidikan);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_jenis_pendidikan, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Jenis Pendidikan '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function jenis_pendidikan_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Pendidikan', 'jenis_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_jenis_pendidikan, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_jenis_pendidikan, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Jenis Pendidikan '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function jenis_bidang_studi() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Bidang Studi', 'jenis_bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['jenis_bidang_studi'] = $this->model_basic->select_all($this->tbl_jenis_bidang_studi);

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/jenis_bidang_studi_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jenis_bidang_studi_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Bidang Studi', 'jenis_bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $jenis_bidang_studi = $this->model_basic->select_where($this->tbl_jenis_bidang_studi, 'id', $id)->row();
            $data['data'] = $jenis_bidang_studi;
        } else
            $data['data'] = null;

        $data['jenis_bidang_studi'] = $this->model_basic->select_all($this->tbl_jenis_bidang_studi);
        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/jenis_bidang_studi_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function jenis_bidang_studi_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Bidang Studi', 'jenis_bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_jenis_bidang_studi);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_jenis_bidang_studi, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Jenis Bidang Studi '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function jenis_bidang_studi_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Bidang Studi', 'jenis_bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_jenis_bidang_studi);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_jenis_bidang_studi, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Jenis Bidang Studi '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function jenis_bidang_studi_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jenis Bidang Studi', 'jenis_bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_jenis_bidang_studi, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_jenis_bidang_studi, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Jenis Bidang Studi '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function bidang_studi() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang Studi', 'bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['bidang_studi'] = $this->model_basic->select_all($this->tbl_bidang_studi);
        foreach ($data['bidang_studi'] as $data_row) {
            $jenis_bs = $this->model_basic->select_where($this->tbl_jenis_bidang_studi, 'id', $data_row->jenis_bidang_studi_id);
            if ($jenis_bs->num_rows() == 1)
                $data_row->jenis_bs = $jenis_bs->row()->name;
            else
                $data_row->jenis_bs = 'Unknown';
        }

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bidang_studi_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function ajax_status_asing(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $do_update = $this->model_basic->update($this->tbl_bidang_studi, ['status_asing'=>$status_change], 'id', $id);

        $this->returnJson(array('status' => 'ok', 'status_now'=>$status_change));
    }

    function bidang_studi_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang Studi', 'bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $id)->row();
            $data['data'] = $bidang_studi;
        } else
            $data['data'] = null;

        $data['jenis_bidang_studi'] = $this->model_basic->select_all($this->tbl_jenis_bidang_studi);
        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bidang_studi_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bidang_studi_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang Studi', 'bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_bidang_studi);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_bidang_studi, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Bidang Studi '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function bidang_studi_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang Studi', 'bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_bidang_studi);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_bidang_studi, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Bidang Studi '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function bidang_studi_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bidang Studi', 'bidang_studi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_bidang_studi, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Bidang Studi '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function elemen_penilaian() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        if($this->input->get('id')){
            $elemen_penilaian = $this->model_eval->get_data_elemen_penilaian($this->input->get('id'));
            $data['choosen'] = $this->input->get('id');
        }else{
            $elemen_penilaian = $this->model_eval->get_data_elemen_penilaian();
        }

        $data['elemen_penilaian'] = $elemen_penilaian;

        $dropdown = $this->model_basic->select_where_in($this->tbl_elemen_penilaian, 'id', [1,2,3,4,5,6,7])->result();
        $data['dropdown'] = $dropdown;

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/elemen_penilaian_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function ajax_elemen_penilaian_list(){
        $list = $this->model_elemen_penilaian_list->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $data_row->name;
            $row[] = $data_row->tipe_akademis;
            $row[] = $data_row->tipe_bobot;
            //add html for action
            $row[] = '<form action="admin_setting_pendidikan/elemen_penilaian_form" method="post">
                          <input type="hidden" name="id" value="'.$data_row->id.'">
                          <a class="btn btn-sm btn-success" href="admin_setting_pendidikan/sub_elemen_penilaian_form/'.$data_row->id.'"><i class="fa fa-plus"></i> Tambah Sub</a>
                          <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah"><i class="fa fa-edit"></i></button>
                          <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
                      </form>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_elemen_penilaian_list->count_all(),
                        "recordsFiltered" => $this->model_elemen_penilaian_list->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function elemen_penilaian_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $id)->row();
            $parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->parent_id)->row()->name;
            $data['parent'] = $parent;
            $data['data'] = $elemen_penilaian;
        } else {
            $data['parent'] = '-';
            $data['data'] = null;
        }

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/elemen_penilaian_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function sub_elemen_penilaian_form($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        if ($id) {
            $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $id)->row();
            $data['data_add'] = $elemen_penilaian;
            $data['parent'] = $elemen_penilaian->name;
            $data['data'] = null; //new data
        } else {
            $data['parent'] = 'Parent';
            $data['data_add'] = null;
            $data['data'] = null;
        }
        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/sub_elemen_penilaian_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function elemen_penilaian_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        if ($this->input->post('name')) {

            $id = $this->input->post('id');
            if($id)
                $insert['parent_id'] = $id;
            else
                $insert['parent_id'] = 0;
            $insert['tipe_bobot'] = 0;
            $insert['tipe_akademis'] = 0;

            foreach ($this->input->post('name') as $value) {
                $insert['name'] = $value;
                $do_insert = $this->model_basic->insert_all($this->tbl_elemen_penilaian, $insert);
            }

            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert Elemen Penilaian '.$insert['name']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function elemen_penilaian_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_elemen_penilaian);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['parent_id']);
        unset($update['tipe_bobot']);
        unset($update['tipe_akademis']);

        if ($this->input->post('name')) {
            $do_update = $this->model_basic->update($this->tbl_elemen_penilaian, $update, 'id', $update['id']);
            if ($do_update) {
                $this->save_log_admin(ACT_UPDATE, 'Update Elemen Penilaian '.$update['name']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function ajax_ubah_tipe_akademis() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $detail = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->input->post('elemen_penilaian_id'))->row();
        $status = $this->input->post('tipe_akademis');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $update['id'] = $this->input->post('elemen_penilaian_id');
        $update['tipe_akademis'] = $status_change;

        $do_update = $this->model_basic->update($this->tbl_elemen_penilaian, $update, 'id', $update['id']);
        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Elemen Penilaian '.$detail->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function'], 'status_now' => $status_change));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function ajax_ubah_tipe_bobot() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $detail = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->input->post('elemen_penilaian_id'))->row();
        $status = $this->input->post('tipe_bobot');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $update['id'] = $this->input->post('elemen_penilaian_id');
        $update['tipe_bobot'] = $status_change;

        $do_update = $this->model_basic->update($this->tbl_elemen_penilaian, $update, 'id', $update['id']);
        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Elemen Penilaian '.$detail->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function'], 'status_now' => $status_change));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function ajax_ubah_tipe_kelompok() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $detail = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->input->post('elemen_penilaian_id'))->row();
        $status = $this->input->post('tipe_kelompok');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $update['id'] = $this->input->post('elemen_penilaian_id');
        $update['tipe_kelompok'] = $status_change;

        $do_update = $this->model_basic->update($this->tbl_elemen_penilaian, $update, 'id', $update['id']);
        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Elemen Penilaian '.$detail->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function'], 'status_now' => $status_change));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function ajax_ubah_mata_kegiatan() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $detail = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->input->post('elemen_penilaian_id'))->row();
        $status = $this->input->post('mata_kegiatan');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $update['id'] = $this->input->post('elemen_penilaian_id');
        $update['is_show'] = $status_change;

        $do_update = $this->model_basic->update($this->tbl_elemen_penilaian, $update, 'id', $update['id']);
        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Elemen Penilaian '.$detail->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function'], 'status_now' => $status_change));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function ajax_ubah_is_taskap() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $detail = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->input->post('elemen_penilaian_id'))->row();
        $status = $this->input->post('is_taskap');
        if($status == 0)
            $status_change = 1;
        else
            $status_change = 0;

        $update['id'] = $this->input->post('elemen_penilaian_id');
        $update['is_taskap'] = $status_change;

        $do_update = $this->model_basic->update($this->tbl_elemen_penilaian, $update, 'id', $update['id']);
        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Elemen Penilaian '.$detail->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function'], 'status_now' => $status_change));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function elemen_penilaian_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Elemen Penilaian', 'elemen_penilaian');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_elemen_penilaian, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Elemen Penilaian '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function bagan_pendidikan() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['bagan_pendidikan'] = $this->model_basic->select_all($this->tbl_bagan_pendidikan);
        foreach ($data['bagan_pendidikan'] as $data_row) {
            $jenis_pendidikan = $this->model_basic->select_where($this->tbl_jenis_pendidikan, 'id', $data_row->jenis_pendidikan_id);
            if($jenis_pendidikan->num_rows() == 1)
                $data_row->jenis_pendidikan = $jenis_pendidikan->row()->name;
            else
                $data_row->jenis_pendidikan = 'Unknown';
        }

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bagan_pendidikan_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bagan_pendidikan_detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        if($this->input->get('elemen_penilaian_id')){
            // $elemen_penilaian = $this->model_eval->get_data_elemen_penilaian_detail($id,$this->input->get('elemen_penilaian_id'));
            $data['choosen'] = $this->input->get('elemen_penilaian_id');
        }else{
            // $elemen_penilaian = $this->model_eval->get_data_elemen_penilaian_detail($id, 0);
        }

        // $data['elemen_penilaian'] = $elemen_penilaian;
        $data['bagan'] = $this->model_basic->select_where($this->tbl_bagan_pendidikan, 'id', $id)->row();
        $data['bagan_pendidikan_id'] = $id;

        $dropdown = $this->model_basic->select_where_in($this->tbl_elemen_penilaian, 'id', [1,2,3,4,5,6,7])->result();
        $data['dropdown'] = $dropdown;

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bagan_pendidikan_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function ajax_bagan_pendidikan_detail_list(){
        $bagan_pendidikan_id = $this->input->post('bagan_id');
        $list = $this->model_bagan_pendidikan_detail->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '>>'.$data_row['name'];
            $row[] = $data_row['bobot_nna'];
            $row[] = $data_row['bobot_npa'];
            if($data_row['is_show'] == 1){
                $row[] = '
                    <a href="admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form?elemen_penilaian='.$data_row['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-info">Keterangan Form</a>
                    <a href="admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
            }else{
                $row[] = '<a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
            }
            if($data_row['isform'] == 0)
                $row[] = '<button class="btn btn-danger btn-xs btn-change" type="button" data-status="1" data-target-id='.$data_row['id'].' value="Unchecked"><i class="fa fa-times"></i></button>';
            else
                $row[] = '<button class="btn btn-success btn-xs btn-change" type="button" data-original-title="Click to Uncheck" data-status="0" data-target-id='.$data_row['id'].' value="Checked"><i class="fa fa-check"></i></button>';
            $data[] = $row;

            if($data_row['child']){
                foreach ($data_row['child'] as $data_row2) {
                    $no++;
                    $row = array();
                    $row[] = $no;
                    $row[] = '<span style="margin-left:30px"> >> '.$data_row2['name'].'</span>';
                    $row[] = $data_row2['bobot_nna'];
                    $row[] = $data_row2['bobot_npa'];
                    if($data_row2['is_show'] == 1){
                        $row[] = '
                            <a href="admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form?elemen_penilaian='.$data_row2['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-info">Keterangan Form</a>
                            <a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row2['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row2['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                    }else{
                        $row[] = '<a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row2['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row2['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                    }
                    if($data_row2['isform'] == 0)
                        $row[] = '<button class="btn btn-danger btn-xs btn-change" type="button" data-status="1" data-target-id='.$data_row2['id'].' value="Unchecked"><i class="fa fa-times"></i></button>';
                    else
                        $row[] = '<button class="btn btn-success btn-xs btn-change" type="button" data-original-title="Click to Uncheck" data-status="0" data-target-id='.$data_row2['id'].' value="Checked"><i class="fa fa-check"></i></button>';
                    $data[] = $row;

                    if($data_row2['child']){
                        foreach ($data_row2['child'] as $data_row3) {
                            $no++;
                            $row = array();
                            $row[] = $no;
                            $row[] = '<span style="margin-left:60px"> >> '.$data_row3['name'].'</span>';
                            $row[] = $data_row3['bobot_nna'];
                            $row[] = $data_row3['bobot_npa'];
                            if($data_row3['is_show'] == 1){
                                $row[] = '
                                    <a href="admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form?elemen_penilaian='.$data_row3['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-info">Keterangan Form</a>
                                    <a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row3['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row3['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                            }else{
                                $row[] = '<a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row3['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row3['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                            }
                            if($data_row3['isform'] == 0)
                                $row[] = '<button class="btn btn-danger btn-xs btn-change" type="button" data-status="1" data-target-id='.$data_row3['id'].' value="Unchecked"><i class="fa fa-times"></i></button>';
                            else
                                $row[] = '<button class="btn btn-success btn-xs btn-change" type="button" data-original-title="Click to Uncheck" data-status="0" data-target-id='.$data_row3['id'].' value="Checked"><i class="fa fa-check"></i></button>';
                            $data[] = $row;

                            if($data_row3['child']){
                                foreach ($data_row3['child'] as $data_row4) {
                                    $no++;
                                    $row = array();
                                    $row[] = $no;
                                    $row[] = '<span style="margin-left:90px"> >> '.$data_row4['name'].'</span>';
                                    $row[] = $data_row4['bobot_nna'];
                                    $row[] = $data_row4['bobot_npa'];
                                    if($data_row4['is_show'] == 1){
                                        $row[] = '
                                            <a href="admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form?elemen_penilaian='.$data_row4['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-info">Keterangan Form</a>
                                            <a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row4['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row4['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                                    }else{
                                        $row[] = '<a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row4['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row4['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                                    }
                                    if($data_row4['isform'] == 0)
                                        $row[] = '<button class="btn btn-danger btn-xs btn-change" type="button" data-status="1" data-target-id='.$data_row4['id'].' value="Unchecked"><i class="fa fa-times"></i></button>';
                                    else
                                        $row[] = '<button class="btn btn-success btn-xs btn-change" type="button" data-original-title="Click to Uncheck" data-status="0" data-target-id='.$data_row4['id'].' value="Checked"><i class="fa fa-check"></i></button>';
                                    $data[] = $row;

                                    if($data_row4['child']){
                                        foreach ($data_row4['child'] as $data_row5) {
                                            $no++;
                                            $row = array();
                                            $row[] = $no;
                                            $row[] = '<span style="margin-left:120px"> >> '.$data_row5['name'].'</span>';
                                            $row[] = $data_row5['bobot_nna'];
                                            $row[] = $data_row5['bobot_npa'];
                                            if($data_row5['is_show'] == 1){
                                                $row[] = '
                                                    <a href="admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form?elemen_penilaian='.$data_row5['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-info">Keterangan Form</a>
                                                    <a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row5['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row5['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                                            }else{
                                                $row[] = '<a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row5['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row5['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                                            }
                                            if($data_row5['isform'] == 0)
                                                $row[] = '<button class="btn btn-danger btn-xs btn-change" type="button" data-status="1" data-target-id='.$data_row5['id'].' value="Unchecked"><i class="fa fa-times"></i></button>';
                                            else
                                                $row[] = '<button class="btn btn-success btn-xs btn-change" type="button" data-original-title="Click to Uncheck" data-status="0" data-target-id='.$data_row5['id'].' value="Checked"><i class="fa fa-check"></i></button>';
                                            $data[] = $row;

                                            if($data_row5['child']){
                                                foreach ($data_row5['child'] as $data_row6) {
                                                    $no++;
                                                    $row = array();
                                                    $row[] = $no;
                                                    $row[] = '<span style="margin-left:150px"> >> '.$data_row6['name'].'</span>';
                                                    $row[] = $data_row6['bobot_nna'];
                                                    $row[] = $data_row6['bobot_npa'];
                                                    if($data_row5['is_show'] == 1){
                                                        $row[] = '
                                                            <a href="admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form?elemen_penilaian='.$data_row6['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-info">Keterangan Form</a>
                                                            <a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row6['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row6['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                                                    }else{
                                                        $row[] = '<a href=admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$data_row6['id'].'&bagan_pendidikan='.$bagan_pendidikan_id.'" class="btn btn-xs btn-warning '.($data_row6['isform'] == 0 ? 'disabled' : '').'">Bobot / Urutan</a>';
                                                    }
                                                    if($data_row6['isform'] == 0)
                                                        $row[] = '<button class="btn btn-danger btn-xs btn-change" type="button" data-status="1" data-target-id='.$data_row6['id'].' value="Unchecked"><i class="fa fa-times"></i></button>';
                                                    else
                                                        $row[] = '<button class="btn btn-success btn-xs btn-change" type="button" data-original-title="Click to Uncheck" data-status="0" data-target-id='.$data_row6['id'].' value="Checked"><i class="fa fa-check"></i></button>';
                                                    $data[] = $row;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $no,
                        "recordsFiltered" => $no,
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function bagan_pendidikan_detail_keterangan_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $elemen_penilaian = $this->input->get('elemen_penilaian');
        $bagan_pendidikan = $this->input->get('bagan_pendidikan');
        if ($elemen_penilaian && $bagan_pendidikan) {
            $data['data'] = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $elemen_penilaian, 'bagan_pendidikan_id' => $bagan_pendidikan])->row();
            $content = new domDocument;
            libxml_use_internal_errors(true);
            if($data['data']->keterangan_form == NULL)
                $content->loadHTML('-');
            else
                $content->loadHTML($data['data']->keterangan_form);
            libxml_use_internal_errors(false);
            $content->preserveWhiteSpace = false;
            $images = $content->getElementsByTagName('img');
            if ($images) {
                foreach ($images as $image) {
                    $data['data']->image[] = $image->getAttribute('src');
                }
            }
        }
        else
            $data['data'] = null;
        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bagan_pendidikan_detail_keterangan_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bagan_pendidikan_detail_keterangan_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        
        $bagan_pendidikan_id = $this->input->post('bagan_pendidikan_id');

        $update['id'] = $this->input->post('id');
        $update['keterangan_form'] = $this->input->post('keterangan_form');
        $do_update = $this->model_basic->update($this->tbl_bagan_detail, $update, 'id', $update['id']);

        if ($do_update)
        {
            $this->returnJson(array('status' => 'ok', 'msg' => 'Update berhasil', 'redirect' => $data['controller'] . '/bagan_pendidikan_detail/' . $bagan_pendidikan_id));
        }
        else{
            $this->returnJson(array('status' => 'error', 'msg' => 'Gagal mengupdate data'));
        }
    }

    function bagan_pendidikan_detail_bobot_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $bagan_pendidikan_id = $this->input->get('bagan_pendidikan');
        $elemen_penilaian_id = $this->input->get('elemen_penilaian');

        $elemen_penilaian_row = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id)->row();
        $elemen_penilaian = $this->model_eval->get_elemen_penilaian_bagan($bagan_pendidikan_id, $elemen_penilaian_row->parent_id, $elemen_penilaian_row->tipe_akademis)->result();
        $data['data'] = $elemen_penilaian;
        $data['bagan_pendidikan_id'] = $bagan_pendidikan_id;
        $data['elemen_penilaian_id'] = $elemen_penilaian_id;
        $data['tipe_bobot'] = $elemen_penilaian_row->tipe_bobot;

        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bagan_pendidikan_detail_bobot_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bagan_pendidikan_detail_bobot_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        
        $bagan_pendidikan_id = $this->input->post('bagan_pendidikan_id');
        $elemen_penilaian_id = $this->input->post('elemen_penilaian_id');

        $elemen_penilaian_row = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id)->row();
        // var_dump($elemen_penilaian_row);
        $bagan_detail = $this->model_eval->get_elemen_penilaian_bagan($bagan_pendidikan_id, $elemen_penilaian_row->parent_id, $elemen_penilaian_row->tipe_akademis)->result();
        foreach ($bagan_detail as $bd) { 
            $update['bobot_nna'] = $this->input->post('bobot_nna_'.$bd->id);
            $update['bobot_npa'] = $this->input->post('bobot_npa_'.$bd->id);
            $update['urutan'] = $this->input->post('urutan_'.$bd->id);
            $do_update = $this->model_basic->update($this->tbl_bagan_detail, $update, 'id', $bd->id);
        }

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Bagan Detail Bobot '.$bagan_pendidikan_id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/bagan_pendidikan_detail_bobot_form?elemen_penilaian='.$elemen_penilaian_id.'&bagan_pendidikan='.$bagan_pendidikan_id));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function bagan_pendidikan_detail_change_status() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $elemen_penilaian_id = $this->input->post('elemen_penilaian_id');
        $bagan_pendidikan_id = $this->input->post('bagan_pendidikan_id');
        $status = $this->input->post('status');

        if ($status == 1) {
            $status_now = 0;
            $insert = array(
                'elemen_penilaian_id' => $elemen_penilaian_id,
                'bagan_pendidikan_id' => $bagan_pendidikan_id
            );
            $do_insert = $this->model_basic->insert_all($this->tbl_bagan_detail, $insert);
        }elseif($status == 0){
            $status_now = 1;
            $do_delete = $this->model_basic->delete_where_array($this->tbl_bagan_detail, array('elemen_penilaian_id'=>$elemen_penilaian_id, 'bagan_pendidikan_id'=>$bagan_pendidikan_id));
            $do_insert = true;
        }

        if ($do_insert || $do_delete) {
            $this->save_log_admin(ACT_UPDATE, 'Update Bagan Detail - elemen_penilaian =  '.$elemen_penilaian_id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/bagan_pendidikan_detail/'.$bagan_pendidikan_id, 'status_now' => $status_now));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function bagan_pendidikan_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $bagan_pendidikan = $this->model_basic->select_where($this->tbl_bagan_pendidikan, 'id', $id)->row();
            $data['data'] = $bagan_pendidikan;
        } else
            $data['data'] = null;

        $data['jenis_pendidikan'] = $this->model_basic->select_all($this->tbl_jenis_pendidikan);
        $data['content'] = $this->load->view('backend/admin_setting_pendidikan/bagan_pendidikan_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function bagan_pendidikan_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_bagan_pendidikan);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_bagan_pendidikan, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Bagan Pendidikan '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function bagan_pendidikan_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_bagan_pendidikan);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_bagan_pendidikan, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Bagan Pendidikan '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function bagan_pendidikan_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Bagan Pendidikan', 'bagan_pendidikan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_bagan_pendidikan, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_bagan_pendidikan, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Bagan Pendidikan '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

}