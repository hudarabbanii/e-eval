$(document).ready(function(){
    $("select.select2").select2();
    $('#px-pendidikan_ubah_tim_sementara-form').validate({
        rules: {
            id: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-pendidikan_ubah_tim_sementara-form .alert-warning').removeClass('hidden');
            $('#px-pendidikan_ubah_tim_sementara-form .alert-success').addClass('hidden');
            $('#px-pendidikan_ubah_tim_sementara-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-pendidikan_ubah_tim_sementara-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-pendidikan_ubah_tim_sementara-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-pendidikan_ubah_tim_sementara-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });

    $('#px-pendidikan_ubah_tim_sementara-form-jadwal_id').change(function(){
        $.ajax({
            url: 'Pendidikan_ubah_tim_sementara/get_data_peserta',
            type: 'POST',
            dataType: 'json',
            data: {
                jadwal_id: $(this).val()
            },
            success: function(result){
                $('#px-pendidikan_ubah_tim_sementara-form-pendidikan_peserta_id').html('');
                $('#px-pendidikan_ubah_tim_sementara-form-pendidikan_peserta_id').append('<option value="">Pilih</option>');
                $.each(result.data, function( index , value ) {
                    $('#px-pendidikan_ubah_tim_sementara-form-pendidikan_peserta_id').append('<option value="'+value.id+'">'+value.nama_lengkap+'</option>');
                });
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            }
        });
    });
})
