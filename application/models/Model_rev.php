<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_rev extends CI_Model {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function get_data_elemen_penilaian_detail($pendidikan_id, $bagan_id, $parent=0, $pendidikan_peserta_id){
		$data = array();
		$npa_saved = 0;
		$nna_saved = 0;
		$npa = 0;
		$nna = 0;
		$this->db->select('ep.id as id, ep.name, ep.parent_id, ep.is_show, ep.tipe_akademis, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa');
		$this->db->from($this->tbl_elemen_penilaian.' ep');
		$this->db->join($this->tbl_bagan_detail.' bd', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id',$parent);
		$this->db->where('bd.bagan_pendidikan_id',$bagan_id);
		$this->db->order_by('urutan','ASC');
		$this->db->order_by('id','ASC');
		$result = $this->db->get();

		foreach($result->result() as $row)
		{
			$npa = $this->get_nilai_bobot_akademis($pendidikan_id,$bagan_id,$pendidikan_peserta_id,$row->id);
			$nna = $this->get_nilai_bobot_non_akademis($pendidikan_id,$bagan_id,$pendidikan_peserta_id,$row->id);

			//nilai
			$nilai_akademis = (is_null($npa) ? 0 : $npa->nilai);
			$nilai_non_akademis = (is_null($nna) ? 0 : $nna->nilai);
			//nilai_bobot
			$nilai_bobot_akademis = (is_null($npa) ? 0 : $npa->nilai_bobot);
			$nilai_bobot_non_akademis = (is_null($nna) ? 0 : $nna->nilai_bobot);

			$child = array(
				'id'  				=>$row->id,
				'parent_id'   		=>$row->parent_id,
				'name'   			=>$row->name,
				'is_show' 			=>$row->is_show,
				'bobot' 			=>($row->tipe_akademis == 0 ? $row->bobot_npa : $row->bobot_nna),
				'nilai'				=>($row->tipe_akademis == 0 ? $nilai_akademis : $nilai_non_akademis),
				'nilai_bobot'		=>($row->tipe_akademis == 0 ? $nilai_bobot_akademis : $nilai_bobot_non_akademis),
				'child'  			=>$this->get_data_elemen_penilaian_detail($pendidikan_id,$bagan_id,$row->id,$pendidikan_peserta_id)
			);
			$data['tree'][] = (object) $child;
			$npa_saved = $npa_saved+$nilai_bobot_akademis;
			$data['nilai_akademis'] = $npa_saved;
			$nna_saved = $nna_saved+$nilai_bobot_non_akademis;
			$data['nilai_non_akademis'] = $nna_saved;
			$data['tipe_akademis'] = $row->tipe_akademis;
		}
		return (Object) $data;
	}

	function get_data_elemen_penilaian_detail_one_level($pendidikan_id, $bagan_id, $parent=0, $pendidikan_peserta_id){
		$data = array();
		$npa_saved = 0;
		$nna_saved = 0;
		$npa = 0;
		$nna = 0;
		$this->db->select('ep.id as id, ep.name, ep.parent_id, ep.is_show, ep.tipe_akademis, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa');
		$this->db->from($this->tbl_elemen_penilaian.' ep');
		$this->db->join($this->tbl_bagan_detail.' bd', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id',$parent);
		$this->db->where('bd.bagan_pendidikan_id',$bagan_id);
		$this->db->order_by('urutan','ASC');
		$this->db->order_by('id','ASC');
		$result = $this->db->get();

		foreach($result->result() as $row)
		{
			$npa = $this->get_nilai_bobot_akademis($pendidikan_id,$bagan_id,$pendidikan_peserta_id,$row->id);
			$nna = $this->get_nilai_bobot_non_akademis($pendidikan_id,$bagan_id,$pendidikan_peserta_id,$row->id);

			//nilai
			$nilai_akademis = (is_null($npa) ? 0 : $npa->nilai);
			$nilai_non_akademis = (is_null($nna) ? 0 : $nna->nilai);
			//nilai_bobot
			$nilai_bobot_akademis = (is_null($npa) ? 0 : $npa->nilai_bobot);
			$nilai_bobot_non_akademis = (is_null($nna) ? 0 : $nna->nilai_bobot);

			$child = array(
				'id'  				=>$row->id,
				'parent_id'   		=>$row->parent_id,
				'name'   			=>$row->name,
				'is_show' 			=>$row->is_show,
				'tipe_akademis' 	=>$row->tipe_akademis,
				'bobot' 			=>($row->tipe_akademis == 0 ? $row->bobot_npa : $row->bobot_nna),
				'nilai'				=>($row->tipe_akademis == 0 ? $nilai_akademis : $nilai_non_akademis),
				'nilai_bobot'		=>($row->tipe_akademis == 0 ? $nilai_bobot_akademis : $nilai_bobot_non_akademis)
			);
			$data['tree'][] = (object) $child;
			$npa_saved = $npa_saved+$nilai_bobot_akademis;
			$data['nilai_akademis'] = $npa_saved;
			$nna_saved = $nna_saved+$nilai_bobot_non_akademis;
			$data['nilai_non_akademis'] = $nna_saved;
			$data['tipe_akademis'] = $row->tipe_akademis;
		}
		return (Object) $data;
	}

	function get_nilai_bobot_akademis($pendidikan_id,$bagan_pendidikan_id,$pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('nilai, bobot_npa, (nilai*bobot_npa)/(IF(tipe_bobot=0, 100, 10)) as nilai_bobot, a.content as keterangan', FALSE);
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $pendidikan_id);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $bagan_pendidikan_id);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('c.tipe_akademis', 0);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row();
		}
		else{
			return $query->row();
		}
	}

	function get_nilai_bobot_non_akademis($pendidikan_id,$bagan_pendidikan_id,$pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('nna as nilai, bobot_nna, (nna*bobot_nna)/(IF(tipe_bobot=0, 100, 10)) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $pendidikan_id);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $bagan_pendidikan_id);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('c.tipe_akademis', 1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row();
		}
		else{
			return $query->row();
		}
	}

	function get_skema_penilaian_id_by_ep($pendidikan_id, $pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('b.id as skema_penilaian_id');
		$this->db->from($this->tbl_nilai_akhir_peserta.' a');
		$this->db->join($this->tbl_skema_penilaian.' b', 'b.id = a.skema_penilaian_id AND b.pendidikan_id=a.pendidikan_id', 'left');
		$this->db->where('a.pendidikan_id', $pendidikan_id);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('b.elemen_penilaian_ids', $elemen_penilaian_id);
		return $this->db->get();
	}

	//===================================================================================================================//

	function get_data_skema_penilaian($pendidikan_id, $bagan_id, $parent=0){
		$data = array();
		$this->db->select('id, elemen_penilaian_id, name, parent_id, bobot_nna, bobot_npa');
		$this->db->from($this->tbl_skema_penilaian.' sp');
		$this->db->where('parent_id',$parent);
		$this->db->where('bagan_pendidikan_id',$bagan_id);
		$this->db->order_by('id','ASC');
		$result = $this->db->get();

		foreach($result->result() as $row)
		{
			$child = array(
				'id'  					=>$row->id,
				'parent_id'   			=>$row->parent_id,
				'elemen_penilaian_id'  	=>$row->elemen_penilaian_id,
				'name'   				=>$row->name,
				'child'					=>$this->get_data_skema_penilaian($pendidikan_id,$bagan_id,$row->id)
			);
			$data['tree'][] = (object) $child;
		}
		return (Object) $data;
	}

	function get_data_skema_penilaian_detail_one_level($pendidikan_id, $bagan_id, $parent=0, $pendidikan_peserta_id){
		//get data parent
		$this->db->select('id as id, name, parent_id, bobot_nna, bobot_npa');
		$this->db->from($this->tbl_skema_penilaian.' sp');
		$this->db->where('id',$parent);
		$this->db->where('bagan_pendidikan_id',$bagan_id);
		$result_parent = $this->db->get();
		// end get data parent
		$data = array();
		$npa_saved = 0;
		$nna_saved = 0;
		$this->db->select('id as id, name, parent_id, bobot_nna, bobot_npa');
		$this->db->from($this->tbl_skema_penilaian.' sp');
		$this->db->where('parent_id',$parent);
		$this->db->where('bagan_pendidikan_id',$bagan_id);
		$this->db->order_by('id','ASC');
		$result = $this->db->get();

		foreach($result->result() as $row)
		{
			$nilai = $this->get_nilai_bobot_skema($pendidikan_id,$bagan_id,$pendidikan_peserta_id,$row->id);

			//nilai
			$nilai_akademis = (is_null($nilai) ? 0 : $nilai->npa);
			$nilai_non_akademis = (is_null($nilai) ? 0 : $nilai->nna);
			//nilai_bobot
			$nilai_bobot_akademis = (is_null($nilai) ? 0 : $nilai->nilai_bobot_npa);
			$nilai_bobot_non_akademis = (is_null($nilai) ? 0 : $nilai->nilai_bobot_nna);

			$child = array(
				'id'  				=>$row->id,
				'parent_id'   		=>$row->parent_id,
				'name'   			=>$row->name,
				'bobot_npa' 		=>$row->bobot_npa,
				'bobot_nna' 		=>$row->bobot_nna,
				'npa'				=>$nilai_akademis,
				'nna'				=>$nilai_non_akademis,
				'nilai_bobot_npa'	=>$nilai_bobot_akademis,
				'nilai_bobot_nna'	=>$nilai_bobot_non_akademis
			);
			$data['tree'][] = (object) $child;
			$npa_saved = $npa_saved+$nilai_bobot_akademis;
			$data['nilai_akademis'] = $npa_saved;
			$nna_saved = $nna_saved+$nilai_bobot_non_akademis;
			$data['nilai_non_akademis'] = $nna_saved;
			$data['name'] = $result_parent->row()->name;
		}
		return (Object) $data;
	}

	function get_data_skema_penilaian_detail_self($pendidikan_id, $bagan_id, $parent=0, $pendidikan_peserta_id){
		//get data parent
		$this->db->select('id as id, name, parent_id, bobot_nna, bobot_npa');
		$this->db->from($this->tbl_skema_penilaian.' sp');
		$this->db->where('id',$parent);
		$this->db->where('bagan_pendidikan_id',$bagan_id);
		$result_parent = $this->db->get()->row();
		// end get data parent
		$data = array();
		$npa_saved = 0;
		$nna_saved = 0;
		$nilai = $this->get_nilai_bobot_skema($pendidikan_id,$bagan_id,$pendidikan_peserta_id,$result_parent->id);

		//nilai
		$nilai_akademis = (is_null($nilai) ? 0 : $nilai->npa);
		$nilai_non_akademis = (is_null($nilai) ? 0 : $nilai->nna);
		//nilai_bobot
		$nilai_bobot_akademis = (is_null($nilai) ? 0 : $nilai->nilai_bobot_npa);
		$nilai_bobot_non_akademis = (is_null($nilai) ? 0 : $nilai->nilai_bobot_nna);

		$self = array(
			'id'  				=>$result_parent->id,
			'parent_id'   		=>$result_parent->parent_id,
			'name'   			=>$result_parent->name,
			'bobot_npa' 		=>$result_parent->bobot_npa,
			'bobot_nna' 		=>$result_parent->bobot_nna,
			'npa'				=>$nilai_akademis,
			'nna'				=>$nilai_non_akademis,
			'nilai_bobot_npa'	=>$nilai_bobot_akademis,
			'nilai_bobot_nna'	=>$nilai_bobot_non_akademis
		);
		$data['self'][] = (object) $self;
		$npa_saved = $npa_saved+$nilai_bobot_akademis;
		$data['nilai_akademis'] = $npa_saved;
		$nna_saved = $nna_saved+$nilai_bobot_non_akademis;
		$data['nilai_non_akademis'] = $nna_saved;
		$data['name'] = $result_parent->name;
		
		return (Object) $data;
	}

	function get_nilai_bobot_skema($pendidikan_id,$bagan_pendidikan_id,$pendidikan_peserta_id, $skema_penilaian_id){
		$this->db->select('npa, nna, bobot_npa, bobot_nna, (npa*bobot_npa)/100 as nilai_bobot_npa, (nna*bobot_nna)/100 as nilai_bobot_nna', FALSE);
		$this->db->from($this->tbl_nilai_akhir_peserta.' a');
		$this->db->join($this->tbl_skema_penilaian.' b', 'b.id = a.skema_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $pendidikan_id);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('b.bagan_pendidikan_id', $bagan_pendidikan_id);
		$this->db->where('a.skema_penilaian_id', $skema_penilaian_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row();
		}
		else{
			return $query->row();
		}
	}

}

/* End of file model_site_list.php */
/* Location: ./application/models/model_site_list.php */