<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Hasil <?php echo $elemen_penilaian; ?></h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>PENILAIAN <?php echo strtoupper($elemen_penilaian); ?> <br> PESERTA PPRA/ PPSA LEMHANNAS RI </h3>
                            <hr>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <table class="table table-condensed no_border">
                                    <tr>
                                        <td>Bidang Studi</td>
                                        <td>:</td>
                                        <td><?php echo $bidang_studi; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Hari/Tanggal</td>
                                        <td>:</td>
                                        <td><?php echo $jadwal; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed no_border">
                                    <tr>
                                        <td>Penilai</td>
                                        <td>:</td>
                                        <td><?php echo $penilai; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kelompok</td>
                                        <td>:</td>
                                        <td><?php echo $tim; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <table class="table datatable table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th rowspan="2" width="6%" class="text-center" style="vertical-align: middle">No</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle">Nama Peserta</th>
                                    <?php
                                    foreach ($title as $title_row) { 
                                        if($title_row->child == null){ ?>
                                            <th rowspan="2" width="10%" class="text-center" style="vertical-align: middle"><?php echo $title_row->name.'<br>'.$title_row->bobot.'%'; ?></th>
                                        <?php } ?>
                                    <?php } ?>
                                    <th colspan="2" class="text-center">Kualitas Tanggapan</th>
                                    <th colspan="2" class="text-center">Nilai Akhir</th>
                                    <!-- <th <?php echo (count($title_row->child)!=0 ? 'colspan="2"' : ''); ?> class="text-center" style="vertical-align: middle">Nilai Akhir</th> -->
                                </tr>
                                <tr>
                                    <?php foreach ($title as $title_row) {
                                            if($title_row->child != null){
                                                foreach ($title_row->child as $child) { ?>
                                                    <th width="12%" class="text-center"><?php echo $child->name.'<br>'.$child->bobot.'%'; ?></th>
                                            <?php } } ?>
                                        <?php } ?>
                                    <th width="8%" class="text-center">Non-akademis</th>
                                    <th width="8%" class="text-center">Akademis</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($list as $data_row) { ?>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle"><?php echo $no; ?></td>
                                    <td class="text-center" style="vertical-align: middle"><?php echo $data_row->gelar_depan.' '.ucwords(strtolower($data_row->nama_lengkap)).($data_row->gelar_belakang!="" ? ', '.$data_row->gelar_belakang : ''); ?></td>
                                <?php $x=0; foreach($data_row->nilai_non_akademis as $nilai){ ?>
                                    <td class="text-center">
                                        <?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>
                                    </td>
                                <?php $x++; } ?>
                                <?php foreach($data_row->nilai_akademis as $nilai){ ?>
                                    <td class="text-center">
                                        <?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>
                                    </td>
                                <?php $x++; } ?>
                                    <td class="text-center" style="vertical-align: middle"><b><?php echo number_format($data_row->nilai_akhir_non_akademis, 3, '.', ''); ?></b></td>
                                    <td class="text-center" style="vertical-align: middle"><b><?php echo number_format($data_row->nilai_akhir_akademis, 3, '.', ''); ?></b></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<style type="text/css">
    .no_border td{
        border: none !important;
    }
</style>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script> -->

<!-- <script type="text/javascript" src="assets/backend_assets/page/pendidikan_hasil/hasil_bagan_list.js"></script> -->
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
