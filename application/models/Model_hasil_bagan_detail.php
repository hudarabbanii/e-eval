<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_hasil_bagan_detail extends CI_Model {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function get_peserta_from_pendidikan_peserta($bidang_studi_status=0){
		$this->db->select('a.*');
		$this->db->from($this->tbl_pendidikan_peserta.' a');
		$this->db->join($this->tbl_tim_detail.' b', 'b.pendidikan_peserta_id = a.id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		if($bidang_studi_status==1)
			$this->db->where('a.status', 0);
		$this->db->where('b.tim_id', $this->session->userdata('jadwal')['tim_id']);
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get();
	}

	function get_peserta_pindahan($jadwal_pengganti){
		$this->db->select('a.*');
		$this->db->from($this->tbl_pendidikan_peserta.' a');
		$this->db->join($this->tbl_tim_sementara.' b', 'b.pendidikan_peserta_id = a.id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('b.jadwal_pengganti_id', $this->session->userdata('jadwal')['jadwal_id']);
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get();
	}

	function get_peserta_pindahan_asal($jadwal_asal){
		$this->db->select('a.*');
		$this->db->from($this->tbl_pendidikan_peserta.' a');
		$this->db->join($this->tbl_tim_sementara.' b', 'b.pendidikan_peserta_id = a.id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('b.jadwal_id', $this->session->userdata('jadwal')['jadwal_id']);
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get();
	}

	function cek_peserta_pindahan_asal($jadwal_asal,$pendidikan_peserta_id){
		$this->db->select('a.*');
		$this->db->from($this->tbl_pendidikan_peserta.' a');
		$this->db->join($this->tbl_tim_sementara.' b', 'b.pendidikan_peserta_id = a.id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('b.jadwal_id', $this->session->userdata('jadwal')['jadwal_id']);
		$this->db->where('b.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->order_by('a.nama_lengkap', 'ASC');
		return $this->db->get();
	}

	function cek_peserta_pindahan($pendidikan_peserta_id,$jadwal_id){
		$this->db->select('a.*');
		$this->db->from($this->tbl_tim_sementara.' a');
		$this->db->where('a.jadwal_id', $this->session->userdata('jadwal')['jadwal_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$result = $this->db->get();
		if($result->num_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function get_nilai_akademis($pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('nilai, bobot_npa, ((nilai*bobot_npa)/100) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		// $this->db->where('a.bagan_detail_id', $bagan_detail_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		// $this->db->where('a.penilai_id', $this->session->userdata('jadwal')['penilai_id']);
		// $this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		$this->db->where('c.tipe_akademis', 0);
		// $this->db->order_by('b.id', 'ASC');
		return $this->db->get();
	}

	function get_nilai_akademis_taskap($pendidikan_peserta_id, $elemen_penilaian_id, $penilai_id){
		$this->db->select('nilai, bobot_npa, ((nilai*bobot_npa)/100) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		// $this->db->where('a.bagan_detail_id', $bagan_detail_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		$this->db->where('a.penilai_id', $penilai_id);
		// $this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		$this->db->where('c.tipe_akademis', 0);
		// $this->db->order_by('b.id', 'ASC');
		return $this->db->get();
	}

	function get_nilai_non_akademis($pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('nna, bobot_nna, ((nna*bobot_nna)/100) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		// $this->db->where('a.bagan_detail_id', $bagan_detail_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		// $this->db->where('a.penilai_id', $this->session->userdata('jadwal')['penilai_id']);
		// $this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		$this->db->where('c.tipe_akademis', 1);
		// $this->db->order_by('b.id', 'ASC');
		return $this->db->get();
	}

	function get_nilai_non_akademis_taskap($pendidikan_peserta_id, $elemen_penilaian_id, $penilai_id){
		$this->db->select('nna, bobot_nna, ((nna*bobot_nna)/100) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		// $this->db->where('a.bagan_detail_id', $bagan_detail_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		$this->db->where('a.penilai_id', $penilai_id);
		// $this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		$this->db->where('c.tipe_akademis', 1);
		// $this->db->order_by('b.id', 'ASC');
		return $this->db->get();
	}

	function get_title(){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, ep.tipe_akademis as tipe_akademis, ep.tipe_bobot as tipe_bobot');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id', $this->session->userdata('var')['elemen_penilaian_id']);
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}
	function get_title_by_epid($elemen_penilaian_id){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, ep.tipe_akademis as tipe_akademis, ep.tipe_bobot as tipe_bobot');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id', $elemen_penilaian_id);
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	function get_title_non_akademis(){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot, ep.tipe_akademis as tipe_akademis, ep.is_taskap as is_taskap');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('ep.parent_id', $this->session->userdata('var')['elemen_penilaian_id']);
		$this->db->where('ep.tipe_akademis', 1);
		$this->db->order_by('bd.urutan', 'ASC');
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	function get_title_akademis(){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_npa as bobot, ep.tipe_akademis as tipe_akademis, ep.is_taskap as is_taskap');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('ep.parent_id', $this->session->userdata('var')['elemen_penilaian_id']);
		$this->db->where('ep.tipe_akademis', 0);
		$this->db->order_by('bd.urutan', 'ASC');
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	function get_bagan($elemen_penilaian_id,$tipe_akademis){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, ep.tipe_akademis as tipe_akademis');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('ep.parent_id', $elemen_penilaian_id);
		$this->db->where('ep.tipe_akademis', $tipe_akademis);
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	// private function _get_datatables_query()
	// {
	// 	$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
	// 	$bagan_detail_id = $this->input->post('bagan_detail_id');
	// 	$this->db->select('j.*, p.nama_lengkap as penilai, p.gelar_depan as gelar_depan, p.gelar_belakang as gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim');
	// 	$this->db->from($this->tbl_jadwal .' j');
	// 	$this->db->join($this->tbl_penilai.' p', 'p.id = j.penilai_id', 'left');
	// 	$this->db->join($this->tbl_bagan_detail.' bd', 'bd.id = j.bagan_detail_id', 'left');
	// 	$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
	// 	$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
	// 	$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
	// 	$this->db->where('j.pendidikan_id', $pendidikan_id);
	// 	$this->db->where('j.bagan_detail_id', $bagan_detail_id);
	// 	$i = 1;
	// 	$where = "";
	// 	foreach ($this->column as $item) 
	// 	{
	// 		if($_POST['search']['value'])
	// 		{
	// 			if ($i==1) {
	// 				$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
	// 			}else{
	// 				$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
	// 			}

	// 			if ($i == (count($this->column) - 1)) {
	// 				$where .= ")";
	// 				$this->db->where($where);
	// 			}
	// 		}
				
	// 		$this->column[$i] = $item;
	// 		$i++;
	// 	}
		
	// 	if(isset($_POST['order']))
	// 	{
	// 		$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	// 	} 
	// 	else if(isset($this->order))
	// 	{
	// 		$order = $this->order;
	// 		$this->db->order_by(key($order), $order[key($order)]);
	// 	}
	// }

	// function get_datatables()
	// {
	// 	$this->_get_datatables_query();
	// 	if($_POST['length'] != -1)
	// 	$this->db->limit($_POST['length'], $_POST['start']);
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }

	// function count_filtered()
	// {
	// 	$this->_get_datatables_query();
	// 	$query = $this->db->get();
	// 	return $query->num_rows();
	// }

	// public function count_all()
	// {
	// 	$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
	// 	$bagan_detail_id = $this->input->post('bagan_detail_id');
	// 	$this->db->select('j.*, p.nama_lengkap as penilai, p.gelar_depan as gelar_depan, p.gelar_belakang as gelar_belakang, ep.name as elemen_penilaian, bs.name as bidang_studi, t.name as tim');
	// 	$this->db->from($this->tbl_jadwal .' j');
	// 	$this->db->join($this->tbl_penilai.' p', 'p.id = j.penilai_id', 'left');
	// 	$this->db->join($this->tbl_bagan_detail.' bd', 'bd.id = j.bagan_detail_id', 'left');
	// 	$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
	// 	$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
	// 	$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
	// 	$this->db->where('j.pendidikan_id', $pendidikan_id);
	// 	$this->db->where('j.bagan_detail_id', $bagan_detail_id);
	// 	$query = $this->db->get();
	// 	return $query->num_rows();
	// }	

	function bagan_detail_child($elemen_penilaian_id){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, ep.tipe_akademis as tipe_akademis');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id', $elemen_penilaian_id);
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	function get_data_elemen_penilaian_detail($bagan_id, $parent=0, $pendidikan_peserta_id, $penilai_id=0){
		$data = array();
		$npa_saved = 0;
		$nna_saved = 0;
		$npa = 0;
		$nna = 0;
		$this->db->select('ep.id as id, ep.name, ep.parent_id, ep.is_show, ep.tipe_akademis, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa');
		$this->db->from($this->tbl_elemen_penilaian.' ep');
		$this->db->join($this->tbl_bagan_detail.' bd', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id',$parent);
		$this->db->where('bd.bagan_pendidikan_id',$bagan_id);
		$this->db->order_by('urutan','ASC');
		$this->db->order_by('id','ASC');
		$result = $this->db->get();

		foreach($result->result() as $row)
		{
			$npa = $this->get_nilai_bobot_akademis($pendidikan_peserta_id,$row->id,$penilai_id);
			$nna = $this->get_nilai_bobot_non_akademis($pendidikan_peserta_id,$row->id,$penilai_id);

			//nilai
			$nilai_akademis = (is_null($npa) ? 0 : $npa->nilai);
			$nilai_non_akademis = (is_null($nna) ? 0 : $nna->nilai);
			//nilai_bobot
			$nilai_bobot_akademis = (is_null($npa) ? 0 : $npa->nilai_bobot);
			$nilai_bobot_non_akademis = (is_null($nna) ? 0 : $nna->nilai_bobot);

			$child = array(
				'id'  				=>$row->id,
				'parent_id'   		=>$row->parent_id,
				'name'   			=>$row->name,
				'is_show' 			=>$row->is_show,
				'bobot' 			=>($row->tipe_akademis == 0 ? $row->bobot_npa : $row->bobot_nna),
				'nilai'				=>($row->tipe_akademis == 0 ? $nilai_akademis : $nilai_non_akademis),
				'nilai_bobot'		=>($row->tipe_akademis == 0 ? $nilai_bobot_akademis : $nilai_bobot_non_akademis),
				'child'  			=>$this->get_data_elemen_penilaian_detail($bagan_id,$row->id,$pendidikan_peserta_id,$penilai_id)
			);
			$data['tree'][] = (object) $child;
			$npa_saved = $npa_saved+$nilai_bobot_akademis;
			$data['nilai_akademis'] = $npa_saved;
			$nna_saved = $nna_saved+$nilai_bobot_non_akademis;
			$data['nilai_non_akademis'] = $nna_saved;
			$data['tipe_akademis'] = $row->tipe_akademis;
		}
		return (Object) $data;
	}

	function check_child($bagan_id, $elemen_id){
		$this->db->select('ep.id, ep.name, ep.parent_id,ep.is_show,bd.bobot');
		$this->db->from($this->tbl_elemen_penilaian.' ep');
		$this->db->join($this->tbl_bagan_detail.' bd', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('ep.parent_id',$elemen_id);
		$this->db->where('bd.bagan_pendidikan_id',$bagan_id);
		if($this->db->get()->num_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	function check_isform($bagan_id, $elemen_id){
		$this->db->from($this->tbl_bagan_detail);
		$this->db->where('elemen_penilaian_id', $elemen_id);
		$this->db->where('bagan_pendidikan_id', $bagan_id);
		return $this->db->get()->num_rows();
	}

	function get_bobot($bagan_id, $elemen_id){
		$this->db->select('bobot_nna as bobot_nna, bobot.npa as bobot_npa');
		$this->db->from($this->tbl_bagan_detail);
		$this->db->where('elemen_penilaian_id', $elemen_id);
		$this->db->where('bagan_pendidikan_id', $bagan_id);
		return $this->db->get();
	}

	function get_nilai_esai($pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('nilai, bobot_npa, (nilai*bobot_npa)/10 as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		$this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->nilai_bobot;
		}
		else{
			return 0;
		}
	}

	function get_nilai_bobot_akademis($pendidikan_peserta_id, $elemen_penilaian_id, $penilai_id=0){
		$this->db->select('nilai, bobot_npa, (nilai*bobot_npa)/(IF(tipe_bobot=0, 100, 10)) as nilai_bobot, a.content as keterangan', FALSE);
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		$this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		if($penilai_id > 0){
			$this->db->where('a.penilai_id', $penilai_id);
		}
		$this->db->where('c.tipe_akademis', 0);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row();
		}
		else{
			return $query->row();
		}
	}

	function get_nilai_bobot_non_akademis($pendidikan_peserta_id, $elemen_penilaian_id, $penilai_id=0){
		$this->db->select('nilai, bobot_nna, (nilai*bobot_nna)/(IF(tipe_bobot=0, 100, 10)) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		$this->db->where('a.bidang_studi_id', $this->session->userdata('jadwal')['bidang_studi_id']);
		$this->db->where('a.tim_id', $this->session->userdata('jadwal')['tim_id']);
		if($penilai_id > 0){
			$this->db->where('a.penilai_id', $penilai_id);
		}
		$this->db->where('c.tipe_akademis', 1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row();
		}
		else{
			return $query->row();
		}
	}

	function get_hasil_title(){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, ep.tipe_akademis as tipe_akademis, ep.name as elemen_penilaian');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('ep.parent_id', $this->session->userdata('var')['elemen_penilaian_id']);
		$this->db->order_by('bd.urutan', 'ASC');
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	function get_hasil_title_by_ep($ep){
		$this->db->select('bd.id as id, bd.bagan_pendidikan_id as bagan_pendidikan_id, bd.elemen_penilaian_id as elemen_penilaian_id, ep.name as name, bd.bobot_nna as bobot_nna, bd.bobot_npa as bobot_npa, ep.tipe_akademis as tipe_akademis, ep.name as elemen_penilaian');
		$this->db->from($this->tbl_bagan_detail.' bd');
		$this->db->join($this->tbl_elemen_penilaian.' ep', 'ep.id = bd.elemen_penilaian_id', 'left');
		$this->db->where('bd.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('ep.parent_id', $ep);
		$this->db->order_by('bd.urutan', 'ASC');
		$this->db->order_by('bd.id', 'ASC');
		return $this->db->get();
	}

	function get_hasil_nilai_akademis($pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('c.name as elemen_penilaian, nilai, bobot_npa, ((nilai*bobot_npa)/100) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		return $this->db->get();
	}

	function get_hasil_nilai_non_akademis($pendidikan_peserta_id, $elemen_penilaian_id){
		$this->db->select('c.name as elemen_penilaian, nna, bobot_nna, ((nna*bobot_nna)/100) as nilai_bobot, a.content as keterangan');
		$this->db->from($this->tbl_nilai_peserta.' a');
		$this->db->join($this->tbl_bagan_detail.' b', 'b.bagan_pendidikan_id = a.bagan_pendidikan_id AND b.elemen_penilaian_id=a.elemen_penilaian_id', 'left');
		$this->db->join($this->tbl_elemen_penilaian.' c', 'c.id = a.elemen_penilaian_id', 'left');
		$this->db->where('a.pendidikan_id', $this->session->userdata('var')['pendidikan_id']);
		$this->db->where('a.pendidikan_peserta_id', $pendidikan_peserta_id);
		$this->db->where('a.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('a.elemen_penilaian_id', $elemen_penilaian_id);
		return $this->db->get();
	}

	function get_hasil_akhir_title($elemen_penilaian_id){
		$this->db->select('sp.id as id, sp.bagan_pendidikan_id as bagan_pendidikan_id, sp.elemen_penilaian_id as elemen_penilaian_id, sp.name as name, sp.bobot_nna as bobot_nna, sp.bobot_npa as bobot_npa');
		$this->db->from($this->tbl_skema_penilaian.' sp');
		$this->db->where('sp.bagan_pendidikan_id', $this->session->userdata('var')['bagan_pendidikan_id']);
		$this->db->where('sp.parent_id', $elemen_penilaian_id);
		$this->db->order_by('sp.id', 'ASC');
		return $this->db->get();
	}

	function get_hasil_nilai_all_bs($pendidikan_peserta_id, $elemen_penilaian_id){
		$query = $this->db->query("
			SELECT np.elemen_penilaian_id, sum(nilai)/count(nilai) as npa, sum(nna)/count(nna) as nna, 
			bobot_nna, bobot_npa
			FROM ev_nilai_peserta np
			LEFT JOIN ev_bagan_detail bd ON bd.bagan_pendidikan_id=np.bagan_pendidikan_id AND bd.elemen_penilaian_id=np.elemen_penilaian_id
			WHERE np.pendidikan_peserta_id=".$pendidikan_peserta_id." AND np.elemen_penilaian_id=".$elemen_penilaian_id."
			GROUP BY np.pendidikan_peserta_id");

		return $query;
	}

	function get_hasil_nilai_one_bs($pendidikan_peserta_id, $elemen_penilaian_id, $bidang_studi_id){
		$query = $this->db->query("
			SELECT np.elemen_penilaian_id, sum(nilai)/count(nilai) as npa, sum(nna)/count(nna) as nna, 
			bobot_nna, bobot_npa
			FROM ev_nilai_peserta np
			LEFT JOIN ev_bagan_detail bd ON bd.bagan_pendidikan_id=np.bagan_pendidikan_id AND bd.elemen_penilaian_id=np.elemen_penilaian_id
			WHERE np.pendidikan_peserta_id=".$pendidikan_peserta_id." AND np.elemen_penilaian_id=".$elemen_penilaian_id." AND np.bidang_studi_id=".$bidang_studi_id."
			GROUP BY np.pendidikan_peserta_id");

		return $query;
	}

}

/* End of file model_site_list.php */
/* Location: ./application/models/model_site_list.php */