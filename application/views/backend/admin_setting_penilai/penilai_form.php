<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Penilai</h3>
                    <button onclick="history.go(-1)" class="btn btn-default pull-right">Kembali</button>
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit; else echo $controller . '/' . $function_add; ?>" method="POST" id="px-setting_penilai-penilai-form">
                    <input type="hidden" name="id" id="px-setting_penilai-penilai-form-id" value="<?php if ($data) echo $data->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Gelar Depan</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="gelar_depan" id="px-setting_penilai-penilai-form-gelar_depan" value="<?php if ($data) echo $data->gelar_depan; ?>" placeholder="Gelar Depan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Gelar Belakang</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="gelar_belakang" id="px-setting_penilai-penilai-form-gelar_belakang" value="<?php if ($data) echo $data->gelar_belakang; ?>" placeholder="Gelar Belakang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Nama</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="nama_lengkap" id="px-setting_penilai-penilai-form-nama_lengkap" value="<?php if ($data) echo $data->nama_lengkap; ?>" placeholder="Nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Telepon</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="telp" id="px-setting_penilai-penilai-form-telp" value="<?php if ($data) echo $data->telp; ?>" placeholder="Telepon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Ruangan</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="ruangan" id="px-setting_penilai-penilai-form-ruangan" value="<?php if ($data) echo $data->ruangan; ?>" placeholder="Ruangan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Pangkat</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="pangkat_id" id="px-pendidikan-form-pangkat_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($pangkat as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->pangkat_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Jabatan</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="jabatan_id" id="px-pendidikan-form-jabatan_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($jabatan as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->jabatan_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Bidang</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="bidang_id" id="px-pendidikan-form-bidang_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($bidang as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->bidang_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Jenis Penilai</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="select2" name="jenis_penilai_id" id="px-pendidikan-form-jenis_penilai_id" required>
                                    <option value="">Pilih</option>
                                    <?php foreach($jenis_penilai as $data_row) { ?>
                                    <option value="<?php echo $data_row->id ?>" <?php if ($data) if($data->jenis_penilai_id == $data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/setting_penilai/penilai_form.js"></script>