<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Hasil Penilaian Peserta : <?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></h3>
                    <button class="btn btn-default pull-right" onclick="history.go(-1)">Kembali</button>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3><?php echo strtoupper($detail->name) ?> <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                            <hr>
                        </div>
                        <div class="col-md-11 col-md-offset-1">
                            <table class="table table-condensed no_border">
                                <tr>
                                    <td>Nama Peserta</td>
                                    <td>:</td>
                                    <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
                                </tr>
                                <tr>
                                    <td>Pangkat / Golongan</td>
                                    <td>:</td>
                                    <td><?php echo $peserta->pangkat; ?></td>
                                </tr>
                                <tr>
                                    <td>Nama Tutor</td>
                                    <td>:</td>
                                    <td><?php echo $peserta->tutor_name; ?></td>
                                </tr>
                                <tr>
                                    <td>Pangkat / Golongan</td>
                                    <td>:</td>
                                    <td><?php echo $peserta->tutor_pangkat; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <table class="table table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th rowspan="3" width="6%" class="text-center" style="vertical-align: middle">NO</th>
                                <th rowspan="3" class="text-center" style="vertical-align: middle">KETERANGAN</th>
                                <th rowspan="3" class="text-center" style="vertical-align: middle">RANK</th>
                                <th colspan="4" class="text-center" style="vertical-align: middle">NILAI</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-center" style="vertical-align: middle">PESERTA</th>
                                <th rowspan="2" class="text-center" style="vertical-align: middle">TERTINGGI</th>
                                <th rowspan="2" class="text-center" style="vertical-align: middle">TERENDAH</th>
                            </tr>
                            <tr>
                                <th class="text-center" style="vertical-align: middle">NILAI</th>
                                <th class="text-center" style="vertical-align: middle">RATA2</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7"><b>NILAI AKADEMIK</b></td>
                            </tr>
                            <!-- <pre> -->
                            <!-- <?php print_r($raport->data_akademik); ?> -->
                            <!-- </pre> -->

                            <?php $no = 1; 
                            foreach ($raport->data_akademik as $rda) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $no ?></td>
                                    <td>NAPK <?php echo strtoupper($rda->parent->skema_penilaian_name) ?></td>
                                    <td class="text-center"><?php echo ($rda->parent->rank_npa == 0 ? 1 : $rda->parent->rank_npa); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->nilai_akademis, 3); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->min_max_avg->avg_npa, 3); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->min_max_avg->max_npa, 3); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->min_max_avg->min_npa, 3); ?></td>
                                </tr>
                                <?php foreach ($rda->child as $rda_child) { ?>
                                    <tr>
                                        <td class="text-center"></td>
                                        <td>- <?php echo strtoupper($rda_child['skema_penilaian_name']) ?></td>
                                        <td class="text-center"><?php echo ($rda_child['rank_npa'] == 0 ? 1 : $rda_child['rank_npa']); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['nilai_akademis'], 3); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['min_max_avg']->avg_npa, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['min_max_avg']->max_npa, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['min_max_avg']->min_npa, 3); ?></td>
                                    </tr>
                                <?php } ?>
                            <?php $no++; } ?>

                            <tr>
                                <td colspan="7"><b>NILAI NON AKADEMIK</b></td>
                            </tr>
                            <?php $no = 1; 
                            foreach ($raport->data_non_akademik as $rda) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $no ?></td>
                                    <td>NNAPK <?php echo strtoupper($rda->parent->skema_penilaian_name) ?></td>
                                    <td class="text-center"><?php echo ($rda->parent->rank_nna == 0 ? 1 : $rda->parent->rank_nna); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->nilai_non_akademis, 3); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->min_max_avg->avg_nna, 3); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->min_max_avg->max_nna, 3); ?></td>
                                    <td class="text-center"><?php echo number_format($rda->parent->min_max_avg->min_nna, 3); ?></td>
                                </tr>
                                <?php foreach ($rda->child as $rda_child) { ?>
                                    <tr>
                                        <td class="text-center"></td>
                                        <td>- <?php echo strtoupper($rda_child['skema_penilaian_name']) ?></td>
                                        <td class="text-center"><?php echo ($rda_child['rank_nna'] == 0 ? 1 : $rda_child['rank_nna']); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['nilai_non_akademis'], 3); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['min_max_avg']->avg_nna, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['min_max_avg']->max_nna, 3); ?></td>
                                        <td class="text-center"><?php echo number_format($rda_child['min_max_avg']->min_nna, 3); ?></td>
                                    </tr>
                                <?php } ?>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    <div class="well">
                        Catatan:
                        <ol type="a">
                        <li>Klasifikasi "TERBATAS".</li>
                        <li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
                        <li>Mohon tidak digandakan/diperbanyak.</li>
                        <li>Mohon dapatnya dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
                        <li>Lembar asli stempel basah dan tanda tangan Direvdik.</li>
                        </ol>
                    </div>
                    <a href="pendidikan_hasil/print_raport_blok/<?php echo $peserta_id.'/'.$skema_penilaian_id ?>" class="btn btn-info pull-right"><i class="fa fa-print"></i> Print PDF</a>
                </div>
            </div>
        </div>
    </div>                                
</div>

<style type="text/css">
    .no_border td{
        border: none !important;
    }
    .panel-title{
        font-size: 1.2em !important;
    }
</style>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- <script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_detail_form.js"></script> -->
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
