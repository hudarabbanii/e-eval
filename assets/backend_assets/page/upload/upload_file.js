$(document).ready(function(){
	$('#px-admin_upload-import_data-form').validate({
		ignore: [],
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-admin_upload-import_data-form .alert-warning').removeClass('hidden');
			$('#px-admin_upload-import_data-form .alert-success').addClass('hidden');
			$('#px-admin_upload-import_data-form .alert-danger').addClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-admin_upload-import_data-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-admin_upload-import_data-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-admin_upload-import_data-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$("#file-upload").fileupload({
		dataType: 'text',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(xls|xlsx)$/i,
		maxFileSize: 50000000 // 50 MB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('Image file must be xls/xlsx with less than 50MB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			var target = $('#target-file').val();
			$('#px-admin_upload-import_data-fileupload-'+target+'-progress').text(progress+'%');
			$('#px-admin_upload-import_data-fileupload-'+target+'-upload-button').attr('disabled',true);
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file').val();
			$('#px-admin_upload-import_data-fileupload-'+target+'-upload-button').removeAttr('disabled');
			$('#preview_'+target+' a').attr('href',data.result);
			$('#preview_'+target+' img').attr('src',data.result);
			$('[name="'+target+'"]').val(data.result);
                        $('#filename-view').html(data.result);
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed.');
			$('#px-admin_upload-import_data-fileupload-upload-button').removeAttr('disabled');
		}).on('fileuploadalways', function() {
	});
	$('.btn-upload').click(function(){
		var target = $(this).attr('data-target');
		var old_temp = $('[name="'+target+'"]').val();
		$('#file-upload #target-file').val(target);
		$('#file-upload #old-file').val(old_temp);
	});
});