<!DOCTYPE html>
<html lang = "en-US">
 <head>
 <meta charset = "UTF-8">
 <title>Raport Peserta Pendidikan</title>

	<style type="text/css">
		body{
			padding:10px;
			font-family: "Calibri";
			font-size: 0.8em;
		}
		th {
		    white-space: nowrap;
		}
		hr{
			display: block;
		    margin-top: 0.5em;
		    margin-bottom: 0.5em;
		    margin-left: auto;
		    margin-right: auto;
		    border-style: inset;
		    border-width: 1px;
		}
    	.page{
    		width: 100%;
		    display: table;
		    vertical-align: middle;
		    border: 0px;
    	}
    	.page-border{
    		width: 99%;
		    display: table;
		    vertical-align: middle;
		    border: 1px solid;
    	}
    	.centered {
		    vertical-align: middle;
		    display: table-cell;
		    margin: 0px;
		    padding: 0px; 
		}
		.content{
			width: 100%;
		    margin: auto;
		    border: 0px;
		}
		.content h1{
			margin: 150px auto;
		}
		.img-header{
			max-height:80px;
		}
		.center{
			text-align: center;
		}
		.text-right{
			text-align: right;
		}
		.text-left{
			text-align: left;
		}
		.alignleft {
			float: left;
		}
		.alignright {
			float: right;
		}
		/*table*/
		.tr{
			display: table-row;
		}
		.td{
			display: table-cell;
		}
		.table-default{
			border: none;
		}
		.table-border{
			border: 1px solid black;
    		border-collapse: collapse;
    		padding: 5px;
		}
		.table-border_padd2{
			border: 1px solid black;
    		border-collapse: collapse;
    		padding: 2px 5px 2px 5px;
		}
		/*margin*/
		.mrl20{
			margin-right: 20px; 
			margin-left: 20px; 
		}
		.ml40{
			margin-left: 40px;
		}
		.mr40{
			margin-right: 40px;
		}
		.padd50{
			padding: 50px;
		}
		.padd10{
			padding: 10px;
		}
		.padd5{
			padding: 5px;
		}
		/*font-size*/
		.font07{
			font-size: 0.7em;
		}
		.font13{
			font-size: 1.3em;
		}
		/*col*/
		.col3{
			width: 33.3333%;
		}

		/*table setting*/
		.table{
		  display: table;
		  width:100%;
		  border-collapse:collapse;
		}
		 .table-row{
		  display: table-row;
		}
	    .table-cell, .table-head{
	      display: table-cell;
	      padding:1em;
	      border:#f0f0f0 1px solid;
	    }
	    .table-head{
	        font-weight:bold;
	    }
	</style>
</head>
<body>

<header>
</header>
	<h3 align="center"><?php echo strtoupper($detail->name) ?></h3>
	<h3 align="center">PESERTA PPRA LVII TAHUN 2018</h3>
	<br>
	<table class="table table-default">
        <tr>
            <td>Nama Peserta</td>
            <td>:</td>
            <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
        </tr>
        <tr>
            <td>Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $peserta->pangkat; ?></td>
        </tr>
        <tr>
            <td>Nama Tutor</td>
            <td>:</td>
            <td><?php echo $peserta->tutor_name; ?></td>
        </tr>
        <tr>
            <td>Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $peserta->tutor_pangkat; ?></td>
        </tr>
    </table>
    <br>
    <br>
<table class="table-border_padd2" width="100%">
	<tr class="table-border_padd2">
        <th rowspan="3" class="table-border_padd2" style="vertical-align: middle">NO</th>
        <th rowspan="3" class="table-border_padd2" style="vertical-align: middle">KETERANGAN</th>
        <th rowspan="3" class="table-border_padd2" style="vertical-align: middle">RANK</th>
        <th colspan="4" class="table-border_padd2" style="vertical-align: middle">NILAI</th>
    </tr>
    <tr>
        <th colspan="2" class="table-border_padd2" style="vertical-align: middle">PESERTA</th>
        <th rowspan="2" class="table-border_padd2" style="vertical-align: middle">TERTINGGI</th>
        <th rowspan="2" class="table-border_padd2" style="vertical-align: middle">TERENDAH</th>
    </tr>
    <tr>
        <th class="table-border_padd2" style="vertical-align: middle">NILAI</th>
        <th class="table-border_padd2" style="vertical-align: middle">RATA2</th>
    </tr>
    <tr>
        <td class="table-border_padd2" colspan="7"><b>NILAI AKADEMIK</b></td>
    </tr>
    <?php $no = 1; 
    foreach ($raport->data_akademik as $rda) { ?>
        <tr>
            <td class="table-border_padd2 center"><?php echo $no ?></td>
            <td class="table-border_padd2">NAPK <?php echo strtoupper($rda->parent->skema_penilaian_name) ?></td>
            <td class="table-border_padd2 center"><?php echo ($rda->parent->rank_npa == 0 ? 1 : $rda->parent->rank_npa); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->nilai_akademis, 3); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->min_max_avg->avg_npa, 3); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->min_max_avg->max_npa, 3); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->min_max_avg->avg_nna, 3); ?></td>
        </tr>
        <?php foreach ($rda->child as $rda_child) { ?>
            <tr>
                <td class="table-border_padd2 center"></td>
                <td class="table-border_padd2">- <?php echo strtoupper($rda_child['skema_penilaian_name']) ?></td>
                <td class="table-border_padd2 center"><?php echo ($rda_child['rank_npa'] == 0 ? 1 : $rda_child['rank_npa']); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['nilai_akademis'], 3); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['min_max_avg']->avg_npa, 3); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['min_max_avg']->max_npa, 3); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['min_max_avg']->avg_nna, 3); ?></td>
            </tr>
        <?php } ?>
    <?php $no++; } ?>

    <tr>
        <td class="table-border_padd2" colspan="7"><b>NILAI NON AKADEMIK</b></td>
    </tr>
    <?php $no = 1; 
    foreach ($raport->data_non_akademik as $rda) { ?>
        <tr>
            <td class="table-border_padd2 center"><?php echo $no ?></td>
            <td class="table-border_padd2">NNAPK <?php echo strtoupper($rda->parent->skema_penilaian_name) ?></td>
            <td class="table-border_padd2 center"><?php echo ($rda->parent->rank_nna == 0 ? 1 : $rda->parent->rank_nna); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->nilai_non_akademis, 3); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->min_max_avg->avg_npa, 3); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->min_max_avg->max_npa, 3); ?></td>
            <td class="table-border_padd2 center"><?php echo number_format($rda->parent->min_max_avg->avg_nna, 3); ?></td>
        </tr>
        <?php foreach ($rda->child as $rda_child) { ?>
            <tr>
                <td class="table-border_padd2 center"></td>
                <td class="table-border_padd2">- <?php echo strtoupper($rda_child['skema_penilaian_name']) ?></td>
                <td class="table-border_padd2 center"><?php echo ($rda_child['rank_nna'] == 0 ? 1 : $rda_child['rank_nna']); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['nilai_non_akademis'], 3); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['min_max_avg']->avg_npa, 3); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['min_max_avg']->max_npa, 3); ?></td>
                <td class="table-border_padd2 center"><?php echo number_format($rda_child['min_max_avg']->avg_nna, 3); ?></td>
            </tr>
        <?php } ?>
    <?php $no++; } ?>
</table>
<br>
<div class="padd10">
	<div style="float: left; width: 50%; margin-left: 10px">
	Catatan:<br>
	<ol type="a">
		<li>Klasifikasi "TERBATAS".</li>
		<li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
		<li>Mohon untuk tidak digandakan/diperbanyak.</li>
		<li>Mohon dapat dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
		<li>Lembar asli stempel basah dn tanda tangan Direvdik.</li>
	</ol>
    </div>
    <div style="float: right; width: 30%;">
    <b>Direktur Evaluasi Pendidikan</b><br><br><br><br><br>
	I Wayan Suarjaya S.Sos., M.Tr. (Han)<br>
	Laksamana Pertama TNI
    </div>
    <div style="clear: both; margin: 0pt; padding: 0pt; "></div>
</div>

</body>
</html>

