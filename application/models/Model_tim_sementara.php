<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_tim_sementara extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function get_all(){
		$this->db->select('ts.*, j.tanggal as tanggal, jp.tanggal as tanggal_pengganti, bs.name as bidang _studi, t.name as tim, tp.name as tim_pengganti, ppes.nama_lengkap as nama_peserta');
		$this->db->from($this->tbl_tim_sementara.' ts');
		$this->db->join($this->tbl_jadwal.' j', 'j.id = ts.jadwal_id', 'left');
		$this->db->join($this->tbl_jadwal.' jp', 'jp.id = ts.jadwal_pengganti_id', 'left');
		$this->db->join($this->tbl_tim.' t', 't.id = j.tim_id', 'left');
		$this->db->join($this->tbl_tim.' tp', 'tp.id = jp.tim_id', 'left');
		$this->db->join($this->tbl_bidang_studi.' bs', 'bs.id = j.bidang_studi_id', 'left');
		$this->db->join($this->tbl_pendidikan_peserta.' ppes', 'ppes.id = ts.pendidikan_peserta_id', 'left');
		$this->db->order_by('ts.date_created', 'DESC');
		return $this->db->get()->result();
	}

	function get_peserta_by_tim($tim_id){
		$this->db->select('ppes.*');
		$this->db->from($this->tbl_tim_detail.' td');
		$this->db->join($this->tbl_pendidikan_peserta.' ppes', 'ppes.id = td.pendidikan_peserta_id', 'left');
		$this->db->where('td.tim_id', $tim_id);
		$this->db->order_by('ppes.nama_lengkap', 'ASC');
		return $this->db->get()->result();
	}
}