<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_ubah_tim_sementara extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_ubah_tim_sementara', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index(){
        $this->pendidikan_ubah_tim_sementara();
    }

    function pendidikan_ubah_tim_sementara() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Ubah Tim Sementara', 'pendidikan_ubah_tim_sementara');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['tim_sementara'] = $this->model_tim_sementara->get_all();

        $data['content'] = $this->load->view('backend/pendidikan_ubah_tim_sementara/tim_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_ubah_tim_sementara_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Ubah Tim Sementara', 'pendidikan_ubah_tim_sementara');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $id = $this->input->post('id');
        if ($id) {
            $bidang = $this->model_basic->select_where($this->tbl_bidang, 'id', $id)->row();
            $data['data'] = $bidang;
        } else
            $data['data'] = null;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'pendidikan_id', $this->session->userdata('menu_pendidikan')['pendidikan_id']);
        $data['jadwal'] = $jadwal->result();
        if($jadwal->num_rows() > 0){
            foreach ($data['jadwal'] as $data_row) {
                $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $data_row->elemen_penilaian_id);
                if($elemen_penilaian->num_rows() > 0)
                    $data_row->elemen_penilaian = $elemen_penilaian->row()->name;
                else
                    $data_row->elemen_penilaian = 'Data tidak ditemukan';

                $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $data_row->bidang_studi_id);
                if($bidang_studi->num_rows() > 0)
                    $data_row->bidang_studi = $bidang_studi->row()->name;
                else
                    $data_row->bidang_studi = 'Data tidak ditemukan';

                $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $data_row->tim_id);
                if($tim->num_rows() > 0)
                    $data_row->tim = $tim->row()->name;
                else
                    $data_row->tim = 'Data tidak ditemukan';
            }
        }
        $data['content'] = $this->load->view('backend/pendidikan_ubah_tim_sementara/tim_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function get_data_peserta(){
        $jadwal_id = $this->input->post('jadwal_id');
        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $jadwal_id)->row();

        $peserta = $this->model_tim_sementara->get_peserta_by_tim($jadwal->tim_id);

        $this->returnJson(array('status' => 'ok', 'data' => $peserta));
    }

    function pendidikan_ubah_tim_sementara_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Ubah Tim Sementara', 'pendidikan_ubah_tim_sementara');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_tim_sementara);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['date_created'] = date('Y-m-d H:i:s', now());

        if ($this->input->post('jadwal_id') && $this->input->post('jadwal_pengganti_id') && $this->input->post('pendidikan_peserta_id') && $this->input->post('remark')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_tim_sementara, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Tambah data tim sementara '.$insert['pendidikan_peserta_id']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }
}