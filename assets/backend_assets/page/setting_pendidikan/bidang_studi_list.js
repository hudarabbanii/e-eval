$(document).ready(function(){

	$('.datatable').DataTable({
		"scrollX":true,
	 	"processing": true,
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		},
		{
			extend: 'print',
			autoPrint: true,
			exportOptions: {
                columns: [0,1]
            },
                customize: function ( win ) {
					$(win.document.body).find( 'table' )
						.addClass( 'compact' )
						.css( 'font-size', 'inherit' );
						
					$(win.document.body)
						.css( 'font-size', '10pt' )
						.prepend(
							'<div style=" text-align:center; border-bottom:1px solid #ccc; margin-bottom:20px;"><h3>BIDANG</h3></div>'
						);
				}
		},
		{
			extend: 'excel',
			exportOptions: {
                columns: [0,1]
            }
		}
		],
		"columnDefs": [
		{ 
		    "targets": [0,-1],
		    "orderable": false,
		}
		]
	});


	$('#px-setting_pendidikan-bidang_studi_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-setting_pendidikan-bidang_studi_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-setting_pendidikan-bidang_studi_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-setting_pendidikan-bidang_studi_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-setting_pendidikan-bidang_studi_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-setting_pendidikan-bidang_studi_list-message-form input[name="id"]').val('');
		$('#px-setting_pendidikan-bidang_studi_list-message-form input[name="id"]').val(id);
	});

	$('body').delegate('#btn-status_asing','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-status');
		$.ajax({
			url : 'admin_setting_pendidikan/ajax_status_asing',
			type : 'POST',
			dataType : 'json',
			data : {id:id,
					status:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-setting_pendidikan-bidang_studi-list .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-bidang_studi-list .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 0){
						$('#btn-status_asing[data-id="'+id+'"]').removeClass('btn-warning');
						$('#btn-status_asing[data-id="'+id+'"] span').removeClass('fa-times');
						$('#btn-status_asing[data-id="'+id+'"]').attr('data-status','0');
						$('#btn-status_asing[data-id="'+id+'"]').addClass('btn-success');
						$('#btn-status_asing[data-id="'+id+'"] span').addClass('fa-check');
					}else{
						$('#btn-status_asing[data-id="'+id+'"]').removeClass('btn-success');
						$('#btn-status_asing[data-id="'+id+'"][data-id="'+id+'"] span').removeClass('fa-check');
						$('#btn-status_asing[data-id="'+id+'"]').attr('data-status','1');
						$('#btn-status_asing[data-id="'+id+'"]').addClass('btn-warning');
						$('#btn-status_asing[data-id="'+id+'"] span').addClass('fa-times');
					}
				}
				else{
					$('#px-setting_pendidikan-bidang_studi-list .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-bidang_studi-list .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});
})