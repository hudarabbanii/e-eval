<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Raport</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success hidden"><strong>Berhasil! </strong><span></span></div>
                    <div class="alert alert-warning hidden"><strong>Sedang diproses.. </strong><span>Mohon Tunggu...</span></div>
                    <div class="alert alert-danger hidden"><strong>Gagal! </strong><span></span></div>
                    <table class="table datatable table-bordered">
                        <thead>
                          <tr>
                            <th width="6%" class="text-center">No</th>
                            <th class="text-center">Raport</th>
                            <th width="15%" class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $no=1;
                          foreach ($skema_penilaian as $data_row) { ?>
                          <tr>
                            <td class="text-center"><?php echo $no; ?></td>
                            <td class="text-center"><?php echo $data_row->name; ?></td>
                            <td class="text-center">
                              <a href="pendidikan_hasil/hasil_peserta_detail_blok/<?php echo $peserta->id ?>/<?php echo $data_row->id ?>" class="btn btn-primary btn-sm">Lihat Raport</a>
                          </td>
                          </tr>
                          <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_hasil/pendidikan_hasil_peserta_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
