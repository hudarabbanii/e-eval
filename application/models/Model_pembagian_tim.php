<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pembagian_tim extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function get_peserta_tim($tim_id){
		$this->db->select('a.id as id, b.name as tim, b.pendidikan_id as pendidikan_id, b.elemen_penilaian_id as elemen_penilaian_id, c.gelar_depan as gelar_depan, c.gelar_belakang as gelar_belakang, c.nama_lengkap as nama_lengkap, c.pangkat as pangkat, c.instansi_negara as instansi, c.id as peserta_id');
		$this->db->from($this->tbl_tim_detail.' a');
		$this->db->join($this->tbl_tim.' b', 'b.id = a.tim_id', 'left');
		$this->db->join($this->tbl_pendidikan_peserta.' c', 'c.id = a.pendidikan_peserta_id', 'left');
		$this->db->where('b.id', $tim_id);
		$this->db->order_by('c.nama_lengkap', 'ASC');
		return $this->db->get();
	}

	function get_data_tim_sementara(){
		$this->db->select('a.id as id, b.nama_lengkap as nama_peserta, c.jadwal_ as pendidikan_id, b.elemen_penilaian_id as elemen_penilaian_id, c.gelar_depan as gelar_depan, c.gelar_belakang as gelar_belakang, c.nama_lengkap as nama_lengkap, c.pangkat as pangkat, c.instansi_negara as instansi, c.id as peserta_id');
		$this->db->from($this->tbl_tim_sementara.' a');
		$this->db->join($this->tbl_pendidikan_peserta.' b', 'b.id = a.pendidikan_peserta_id', 'left');
		$this->db->join($this->tbl_jadwal.' c', 'c.id = a.jadwal_id', 'left');
		$this->db->join($this->tbl_jadwal.' d', 'd.id = a.jadwal_pengganti_id', 'left');
		$this->db->order_by('a.date_created', 'DESC');
		return $this->db->get();
	}
}