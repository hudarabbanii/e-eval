<style type="text/css">
  .alert-top{
      position: fixed;
      top: 5px; 
      left:2%;
      width: 96%;
      z-index: 9999;
  }
</style><!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Bagan Detail <b><?php echo $bagan->name ?></b></h3>              
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-2"><label class="control-label pull-right">Pilih Parent</label></div>
                    <div class="col-md-8">
                      <select class="form-control" id="px-setting_pendidikan-elemen_penilaian_list-dropdown" name="px-setting_pendidikan-elemen_penilaian_list-dropdown">
                          <option value="all">Semua Elemen Penilaian</option>
                        <?php foreach ($dropdown as $data_row) { ?>
                          <option value="<?php echo $data_row->id ?>" <?php if(isset($choosen)) if($choosen==$data_row->id) echo 'selected'; ?>><?php echo $data_row->name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="panel-body" id="px-setting_pendidikan-bagan_pendidikan_detail">
                    <div class="alert alert-success alert-top" style="display: none"><strong>Success! </strong><span></span></div>
                    <div class="alert alert-warning alert-top" style="display: none"><strong>Processing! </strong><span>Please wait...</span></div>
                    <div class="alert alert-danger alert-top" style="display: none"><strong>Failed! </strong><span></span></div>
                    <input type="hidden" id="bagan_pendidikan_id" name="bagan_pendidikan_id" value="<?php echo $bagan_pendidikan_id; ?>">
                    <input type="hidden" id="elemen_penilaian_id" name="elemen_penilaian_id" value="0">
                    <table class="table datatable table-bordered">
                        <thead>
                          <tr>
                            <th width="6%" class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Bobot NNA</th>
                            <th class="text-center">Bobot NPA</th>
                            <th width="12%" class="text-center">Ubah</th>
                            <th width="8%" class="text-center">Is Form</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>
<!-- END PAGE PLUGINS -->

<script type="text/javascript" src="assets/backend_assets/page/setting_pendidikan/bagan_detail.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
