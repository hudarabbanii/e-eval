<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_bagan_pendidikan_detail extends CI_Model {

	var $column = array('id', 'name', 'is_show', 'tipe_akademis', 'tipe_bobot'); //set column field database for order and search
	var $order = array(); // default order

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function get_data_elemen_penilaian_detail($bagan_id, $parent=0){
	   $data = array();
	   $this->db->from($this->tbl_elemen_penilaian);
	   $this->db->where('parent_id',$parent);
	   $result = $this->db->get();
	 
	   foreach($result->result() as $row)
	   {
	   		$bobot = $this->get_bobot($bagan_id, $row->id);
	      	$data[] = array(
	            'id'  			=>$row->id,
	            'parent_id'   	=>$row->parent_id,
	            'name'   		=>$row->name,
	            'is_show' 		=>$row->is_show,
	            'bobot_nna'   	=>($bobot->num_rows() == 0 ? 0 : $bobot->row()->bobot_nna),
	            'bobot_npa'   	=>($bobot->num_rows() == 0 ? 0 : $bobot->row()->bobot_npa),
	            'isform'		=>$this->check_isform($bagan_id, $row->id),
	            'child'  		=>$this->get_data_elemen_penilaian_detail($bagan_id, $row->id)
	        );
	   }
	   return $data;
	}

	function check_isform($bagan_id, $elemen_id){
		$this->db->from($this->tbl_bagan_detail);
		$this->db->where('elemen_penilaian_id', $elemen_id);
		$this->db->where('bagan_pendidikan_id', $bagan_id);
		return $this->db->get()->num_rows();
	}

	function get_bobot($bagan_id, $elemen_id){
		$this->db->select('bobot_nna, bobot_npa');
		$this->db->from($this->tbl_bagan_detail);
		$this->db->where('elemen_penilaian_id', $elemen_id);
		$this->db->where('bagan_pendidikan_id', $bagan_id);
		return $this->db->get();
	}

	private function _get_datatables_query()
	{
		$bagan_id = $this->input->post('bagan_id');
		if($this->input->post('elemen_penilaian_id'))
			$elemen_penilaian_id = $this->input->post('elemen_penilaian_id');
		else
			$elemen_penilaian_id = 0;
		return $this->get_data_elemen_penilaian_detail($bagan_id,$elemen_penilaian_id);

		// $this->db->from($this->tbl_elemen_penilaian);
	 //   	$this->db->where('parent_id',0);

		// $i = 1;
		// $where = "";
		// foreach ($this->column as $item) 
		// {
		// 	if($_POST['search']['value'])
		// 	{
		// 		if ($i==1) {
		// 			$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
		// 		}else{
		// 			$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
		// 		}

		// 		if ($i == (count($this->column) - 1)) {
		// 			$where .= ")";
		// 			$this->db->where($where);
		// 		}
		// 	}
				
		// 	$this->column[$i] = $item;
		// 	$i++;
		// }
		
		// if(isset($_POST['order']))
		// {
		// 	$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		// } 
		// else if(isset($this->order))
		// {
		// 	$order = $this->order;
		// 	$this->db->order_by(key($order), $order[key($order)]);
		// }
	}

	function get_datatables()
	{
		$result = $this->_get_datatables_query();
		// if($_POST['length'] != -1)
		// $this->db->limit($_POST['length'], $_POST['start']);
		// $query = $this->db->get();
		// return $query->result();
		return $result;
	}

	// function count_filtered()
	// {
	// 	$this->_get_datatables_query();
	// 	$query = $this->db->get();
	// 	return $query->num_rows();
	// }

	// public function count_all()
	// {
	// 	$this->db->from($this->tbl_elemen_penilaian);
	//    	$this->db->where('parent_id',0);
	// 	$query = $this->db->get();
	// 	return $query->num_rows();
	// }	
}

/* End of file model_bagan_pendidikan_detail.php */
/* Location: ./application/models/model_bagan_pendidikan_detail.php */