<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Elemen Penilaian</h3>
                    <div class="pull-right"><a href="<?php echo $controller . '/bagan_pendidikan_detail/'.$bagan_pendidikan_id; ?>" class="btn btn-default btn-md">Kembali</a></div>
                </div>
                <form action="<?php echo $controller . '/bagan_pendidikan_detail_bobot_edit'; ?>" method="POST" id="px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form">
                <input type="hidden" name="bagan_pendidikan_id" id="px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bagan_pendidikan_id" value="<?php echo $bagan_pendidikan_id; ?>">
                <input type="hidden" name="elemen_penilaian_id" id="px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-elemen_penilaian_id" value="<?php echo $elemen_penilaian_id; ?>">
                <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="6%" class="text-center">No</th>
                                <th class="text-center">Elemen Penilaian</th>
                                <th class="text-center">Tipe</th>
                                <th width="10%" class="text-center">Bobot NNA</th>
                                <th width="10%" class="text-center">Bobot NPA</th>
                                <th width="10%" class="text-center">Urutan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($data as $d_row) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $no; ?></td>
                                    <td class="text-center"><?php echo $d_row->name; ?></td>
                                    <td class="text-center"><?php if($d_row->tipe_akademis == 0){ echo 'Akademis'; }else{ echo 'Non Akademis';} ?></td>
                                    <td class="text-center">
                                        <input type="number" class="form-control" name="<?php echo 'bobot_nna_'.$d_row->id; ?>" id="px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_nna" value="<?php echo ($d_row->tipe_akademis == 0 && $d_row->is_show == 0 ? 0 : $d_row->bobot_nna); ?>" min="0" max="100" <?php echo ($d_row->tipe_akademis == 0 && $d_row->is_show == 0 ? 'readonly' : ''); ?>>
                                    </td>
                                    <td class="text-center">
                                        <input type="number" class="form-control" name="<?php echo 'bobot_npa_'.$d_row->id; ?>" id="px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_npa" value="<?php echo ($d_row->tipe_akademis == 1  && $d_row->is_show == 0 ? 0 : $d_row->bobot_npa); ?>" min="0" max="100" <?php echo ($d_row->tipe_akademis == 1  && $d_row->is_show == 0 ? 'readonly' : ''); ?>>
                                    </td>
                                    <td class="text-center">
                                        <input type="number" class="form-control" name="<?php echo 'urutan_'.$d_row->id; ?>" id="px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-urutan" value="<?php echo $d_row->urutan;  ?>" min="0">
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                    <div class="panel-footer">
                        <div class="row">
                            <label class="col-md-4 col-xs-12 control-label">Total Bobot NNA (Harus sama dengan <?php echo ($tipe_bobot == 0 ? '100%' : '10') ?>)</label>
                            <div class="col-md-5 col-xs-12">
                            <input style="background-color: #FFF;color:#555555;width: 100px" type="text" class="form-control form-horizontal" name="total_bobot_nna" id="total_bobot_nna" value="" readonly>
                            </div>
                            <input type="hidden" name="must100" id="must100" value="<?php echo($tipe_bobot == 0 ? 100 : 10) ?>">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                        <div class="row">
                            <label class="col-md-4 col-xs-12 control-label">Total Bobot NPA (Harus sama dengan <?php echo ($tipe_bobot == 0 ? '100%' : '10') ?>)</label>
                            <div class="col-md-5 col-xs-12">
                            <input style="background-color: #FFF;color:#555555;width: 100px" type="text" class="form-control form-horizontal" name="total_bobot_npa" id="total_bobot_npa" value="" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
<script type="text/javascript" src="assets/backend_assets/page/setting_pendidikan/bagan_pendidikan_detail_bobot_form.js"></script>