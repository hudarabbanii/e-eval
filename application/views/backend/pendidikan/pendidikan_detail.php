<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Detail Pendidikan</h3>         
                </div>
                <div class="panel-body">
                  <div class="col-md-6 col-xs-12">
                    <table class="table table-bordered table-striped">
                      <tr>
                        <td>Tahun</td>
                        <!-- <td>:</td> -->
                        <td><?php echo date('Y', strtotime($pendidikan->date_start)); ?></td>
                      </tr>
                      <tr>
                        <td>Jenis Pendidikan</td>
                        <!-- <td>:</td> -->
                        <td><?php echo $jenis_pendidikan; ?></td>
                      </tr>
                      <tr>
                        <td>Nama Pendidikan</td>
                        <!-- <td>:</td> -->
                        <td><?php echo $pendidikan->name; ?></td>
                      </tr>
                      <tr>
                        <td>Tanggal Kegiatan</td>
                        <!-- <td>:</td> -->
                        <td><?php echo $pendidikan->date_start.' s/d '.$pendidikan->date_end; ?></td>
                      </tr>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->


<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>