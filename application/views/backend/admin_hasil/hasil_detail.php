<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>

<!-- <div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div> -->

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <?php
            // var_dump($this->session->userdata('var'));
            ?>
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Hasil Penilaian</h3>
                    <button onclick="history.go(-1)" class="btn btn-default btn-md pull-right">Kembali</button>
                </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>NILAI AKADEMIK DAN NILAI NON AKADEMIK PADA BLOK I <br> PESERTA PPRA/PPSA LEMHANNAS RI </h3>
                            <hr>
                            </div>
                            <div class="col-md-11 col-md-offset-1">
                                <table class="table table-condensed no_border">
                                    <tr>
                                        <td>Nama Peserta</td>
                                        <td>:</td>
                                        <td><?php echo ($peserta->gelar_depan != "" ? $peserta->gelar_depan.' ' : '').$peserta->nama_lengkap.($peserta->gelar_belakang != "" ? ', '.$peserta->gelar_belakang : ''); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pangkat / Golongan</td>
                                        <td>:</td>
                                        <td><?php echo $peserta->pangkat; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Tutor</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Pangkat / Golongan</td>
                                        <td>:</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                                <tr>
                                    <th rowspan="3" width="6%" class="text-center" style="vertical-align: middle">NO</th>
                                    <th rowspan="3" class="text-center" style="vertical-align: middle">KETERANGAN</th>
                                    <th rowspan="3" class="text-center" style="vertical-align: middle">RANK</th>
                                    <th colspan="4" class="text-center" style="vertical-align: middle">NILAI</th>
                                </tr>
                                <tr>
                                    <th colspan="2" class="text-center" style="vertical-align: middle">PESERTA</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle">TERTINGGI</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle">TERENDAH</th>
                                </tr>
                                <tr>
                                    <th class="text-center" style="vertical-align: middle">NILAI</th>
                                    <th class="text-center" style="vertical-align: middle">RATA2</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>

                        <!-- CATATAN -->
                        <!-- <div class="well">
                            Catatan:
                            <ol type="a">
                            <li>Klasifikasi "TERBATAS".</li>
                            <li>Hanya untuk tutor pembimbing peserta yang bersangkutan.</li>
                            <li>Mohon tidak digandakan/diperbanyak.</li>
                            <li>Mohon dapatnya dipergunakan sebagaimana mestinya untuk keperluan pembimbingan peserta.</li>
                            <li>Lembar asli stempel basah dan tanda tangan Direvdik.</li>
                            </ol>
                        </div> -->
                    </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<style type="text/css">
    .no_border td{
        border: none !important;
    }
</style>

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<!-- <link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/> -->
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script> -->
<!-- <script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script> -->

<!-- <script type="text/javascript" src="assets/backend_assets/page/pendidikan_jadwal/pendidikan_jadwal_detail_form.js"></script> -->
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>
