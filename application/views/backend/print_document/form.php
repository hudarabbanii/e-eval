<!DOCTYPE html>
<html lang = "en-US">
 <head>
 <meta charset = "UTF-8">
 <title>Form Penilaian</title>

	<style type="text/css">
		body{
			padding:10px;
			font-family: "Calibri";
			font-size: 0.8em;
		}
		th {
		    white-space: nowrap;
		}
		hr{
			display: block;
		    margin-top: 0.5em;
		    margin-bottom: 0.5em;
		    margin-left: auto;
		    margin-right: auto;
		    border-style: inset;
		    border-width: 1px;
		}
    	.page{
    		width: 100%;
		    display: table;
		    vertical-align: middle;
		    border: 0px;
    	}
    	.page-border{
    		width: 99%;
		    display: table;
		    vertical-align: middle;
		    border: 1px solid;
    	}
    	.centered {
		    vertical-align: middle;
		    display: table-cell;
		    margin: 0px;
		    padding: 0px; 
		}
		.content{
			width: 100%;
		    margin: auto;
		    border: 0px;
		}
		.content h1{
			margin: 150px auto;
		}
		.img-header{
			max-height:80px;
		}
		.center{
			text-align: center;
		}
		.alignleft {
			float: left;
		}
		.alignright {
			float: right;
		}
		/*table*/
		.tr{
			display: table-row;
		}
		.td{
			display: table-cell;
		}
		.table-default{
			border: none;
		}
		.table-border{
			border: 1px solid black;
    		border-collapse: collapse;
    		padding: 5px;
		}
		.table-border_padd2{
			border: 1px solid black;
    		border-collapse: collapse;
    		padding: 2px 5px 2px 5px;
		}
		/*margin*/
		.mrl20{
			margin-right: 20px; 
			margin-left: 20px; 
		}
		.ml40{
			margin-left: 40px;
		}
		.mr40{
			margin-right: 40px;
		}
		.padd50{
			padding: 50px;
		}
		.padd10{
			padding: 10px;
		}
		.padd5{
			padding: 5px;
		}
		/*font-size*/
		.font07{
			font-size: 0.7em;
		}
		.font13{
			font-size: 1.3em;
		}
		/*col*/
		.col3{
			width: 33.3333%;
		}

		/*table setting*/
		.table{
		  display: table;
		  width:100%;
		  border-collapse:collapse;
		}
		 .table-row{
		  display: table-row;
		}
	    .table-cell, .table-head{
	      display: table-cell;
	      padding:1em;
	      border:#f0f0f0 1px solid;
	    }
	    .table-head{
	        font-weight:bold;
	    }
	</style>
</head>
<body>

<header>
	<img src="assets/backend_assets/img/lemhannas.jpg" class="alignleft img-header">
	<div style="clear: both;"></div>
	<hr>
</header>
	<h3 align="center">PENILAIAN <?php echo $elemen_penilaian ?></h3>
	<h3 align="center"><u>PESERTA PPRA LVII LEMHANNAS RI</u></h3>
	<br>
	<table class="table table-default">
        <tr>
            <td>Bidang Studi</td>
            <td>:</td>
            <td><?php echo $bidang_studi; ?></td>

            <td>Penilai</td>
            <td>:</td>
            <td><?php echo $penilai; ?></td>
        </tr>
        <tr>
            <td>Hari/Tanggal</td>
            <td>:</td>
            <td><?php echo $jadwal; ?></td>

            <td>Kelompok</td>
            <td>:</td>
            <td><?php echo $tim; ?></td>
        </tr>
    </table>
    <br>
    <br>
<table class="table-border_padd2" width="100%">
	<tr class="table-border_padd2">
        <th rowspan="2" class="table-border_padd2" style="vertical-align: middle">No</th>
        <th rowspan="2" class="table-border_padd2" style="vertical-align: middle">Nama Peserta</th>
        <th colspan="<?php echo count($title_nna)?>" class="table-border_padd2">Nilai Non Akademik</th>
        <th colspan="<?php echo count($title_npa)?>" class="table-border_padd2">Nilai Prestasi Akademik</th>
        <th colspan="2" class="table-border_padd2" style="vertical-align: middle">Nilai Akhir</th>
    </tr>
    <tr>
    	<?php
    	foreach ($title_nna as $title_row) { ?>
    		<th class="table-border_padd2" style="vertical-align: middle"><?php echo $title_row->name.'<br>'.$title_row->bobot.'%'; ?></th>
    	<?php } ?>
    	<?php
    	foreach ($title_npa as $title_row) { ?>
    		<th class="table-border_padd2" style="vertical-align: middle"><?php echo $title_row->name.'<br>'.$title_row->bobot.'%'; ?></th>
    	<?php } ?>
    	<th class="table-border_padd2">Non-akademik</th>
    	<th class="table-border_padd2">Akademik</th>
    </tr>
	<?php 
        $no=1;
        foreach ($list as $data_row) { ?>
        <tr class="table-border_padd2">
            <td class="table-border_padd2" style="vertical-align: middle"><?php echo $no; ?></td>
            <td class="table-border_padd2" style="vertical-align: middle"><?php echo $data_row->gelar_depan.' '.ucwords(strtolower($data_row->nama_lengkap)).($data_row->gelar_belakang!="" ? ', '.$data_row->gelar_belakang : ''); ?></td>
        <?php $x=0; foreach($data_row->nilai_non_akademis as $nilai){ ?>
            <td class="table-border_padd2">
                <?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>
            </td>
        <?php $x++; } ?>
        <?php foreach($data_row->nilai_akademis as $nilai){ ?>
            <td class="table-border_padd2">
                <?php echo ($nilai != null ? number_format($nilai, 3, ".", "") : number_format(0, 3, ".", "")); ?>
            </td>
        <?php $x++; } ?>
            <td class="table-border_padd2" style="vertical-align: middle"><b><?php echo number_format($data_row->nilai_akhir_non_akademis, 3, '.', ''); ?></b></td>
            <td class="table-border_padd2" style="vertical-align: middle"><b><?php echo number_format($data_row->nilai_akhir_akademis, 3, '.', ''); ?></b></td>
        </tr>
        <?php $no++; } ?>
</table>
<br>
<div class="padd10">
	<div style="float: left; width: 50%; margin-left: 10px">
		<?php echo $keterangan ?>
    </div>
    <div style="float: right; width: 30%;">
    Jakarta, <?php echo $tanggal_cetak ?><br>
    Pemegang Materi<br><br><br><br><br>
	___________________<br>
	(<?php echo $penilai ?>)
    </div>
    <div style="clear: both; margin: 0pt; padding: 0pt; "></div>
    <hr>
    <div style="float: left; width: 50%; margin-left: 10px">
    </div>
    <div style="float: right; width: 30%;">
    Deputi Pendidikan<br>
	Pimpinan Tingkat Nasional,
    <br><br><br><br><br>
	Karsiyanto, S.E.<br>
	Mayor Jenderal TNI<br>
    </div>
</div>

</body>
</html>

