<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_download extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_download', 'controller_name' => 'Admin Download', 'controller_id' => 0);
    }

    function export_bidang() {
        $bidang = $this->model_basic->select_all($this->tbl_bidang);
        //initialize
        $sheet_name = 'Data';
        $file_name = 'Eval - Bidang';
        //load library
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle($sheet_name)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        //table head
        $fields = array(
            'RowID',
            'Bidang');
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setBold(true);
            // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }
        $row = 2;
        foreach ($bidang as $data) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $data->id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $data->name);
            $row++;
            $no++;
        }
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $filename = $file_name . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_penilai() {
        $penilai = $this->model_basic->select_all($this->tbl_penilai);
        //initialize
        $sheet_name = 'Data';
        $file_name = 'Eval - Penilai';
        //load library
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle($sheet_name)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        //table head
        $fields = array(
            'RowID',
            'Gelar Depan',
            'Gelar Belakang',
            'Nama',
            'Telp.',
            'Ruangan',
            'pangkat_id',
            'jabatan_id',
            'bidang_id',
            'jenis_penilai_id',
            'Photo');
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setBold(true);
            // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }
        $row = 2;
        foreach ($penilai as $data) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $data->id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $data->gelar_depan);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $data->gelar_belakang);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $data->nama_lengkap);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $data->telp);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $data->ruangan);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $data->pangkat_id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $data->jabatan_id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $data->bidang_id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $data->jenis_penilai_id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $data->photo);
            $row++;
            $no++;
        }
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $filename = $file_name . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
}