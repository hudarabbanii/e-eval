$(document).ready(function(){
    var total_nna = 0;
    $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form').find("input[id=px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_nna]").each(function() {
        var row = $(this).val();
        total_nna += parseFloat(row);
    });
    $("#total_bobot_nna").val(total_nna);

    var total_npa = 0;
    $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form').find("input[id=px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_npa]").each(function() {
        var row = $(this).val();
        total_npa += parseFloat(row);
    });
    $("#total_bobot_npa").val(total_npa);

    //if input change
    $("#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form").on("change", "input[id='px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_nna']", function () {
        var total_nna = 0;
        $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form').find("input[id=px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_nna]").each(function() {
            var row = $(this).val();
            total_nna += parseFloat(row);
        });
            $("#total_bobot_nna").val(total_nna);
    });
    $("#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form").on("change", "input[id='px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_npa']", function () {
        var total_npa = 0;
        $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form').find("input[id=px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form-bobot_npa]").each(function() {
            var row = $(this).val();
            total_npa += parseFloat(row);
        });
            $("#total_bobot_npa").val(total_npa);
    });

    jQuery.validator.addMethod("bobot_rules", function (value, element,options) {
        if (this.optional(element) || value == $(options).val() || value == 0) {
            //elems.removeClass('error');
            return true;
        } else {
            //elems.addClass('error');
        }
    }, 'Total bobot harus sesuai dengan Tipe Bobot (100 atau 10)');

    $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form').validate({
        rules: {
            id: {
                required: true
            },
            total_bobot_nna: {
                bobot_rules: '#must100'
            },
            total_bobot_npa: {
                bobot_rules: '#must100'
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form .alert-warning').removeClass('hidden');
            $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form .alert-success').addClass('hidden');
            $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-bagan_pendidikan-bagan_pendidikan_detail_bobot-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
})
