$(document).ready(function(){

	$('.datatable').DataTable({
		"scrollX":true,
		"stateSave": true,
	 	"processing": true,
		"bLengthChange": false,
		"dom": 'lBfrtip',
		"iDisplayLength": 50,
		"lengthMenu": [
				[ 10, 25, 50, -1 ],
				[ '10 rows', '25 rows', '50 rows', 'Show all' ]
			],
		"buttons": [
		{
			extend: 'pageLength'
		},
		{
			extend: 'print',
			autoPrint: true,
			exportOptions: {
                columns: [0,1]
            },
                customize: function ( win ) {
					$(win.document.body).find( 'table' )
						.addClass( 'compact' )
						.css( 'font-size', 'inherit' );
						
					$(win.document.body)
						.css( 'font-size', '10pt' )
						.prepend(
							'<div style=" text-align:center; border-bottom:1px solid #ccc; margin-bottom:20px;"><h3>BIDANG</h3></div>'
						);
				}
		},
		{
			extend: 'excel',
			exportOptions: {
                columns: [0,1]
            }
		}
		],
		"columnDefs": [
		{ 
		    "targets": [0,1,2,3,4,5,-1],
		    "orderable": false
		}
		],
		"ordering": false 
	});


	$('#px-setting_pendidikan-elemen_penilaian_list-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-setting_pendidikan-elemen_penilaian_list-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-setting_pendidikan-elemen_penilaian_list-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-setting_pendidikan-elemen_penilaian_list-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-setting_pendidikan-elemen_penilaian_list-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-setting_pendidikan-elemen_penilaian_list-message-form input[name="id"]').val('');
		$('#px-setting_pendidikan-elemen_penilaian_list-message-form input[name="id"]').val(id);
	});

	//change akademis
	$('#px-setting_pendidikan-change_akademis-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    if(response.status == 'ok'){
						$('#px-setting_pendidikan-change_akademis-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_akademis-message-box').removeClass('open');
						window.location.href = response.redirect;
                    }
                    else
						$('#px-setting_pendidikan-change_akademis-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_akademis-message-box').removeClass('open');
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
		}
	});

	// $('body').delegate('.btn-change_akademis','click',function(){
	// 	$('#px-setting_pendidikan-change_akademis-message-box').addClass('open');
	// 	var elemen_penilaian_id = $(this).attr('data-id');
	// 	var tipe_akademis = $(this).attr('data-akademis');
	// 	$('#px-setting_pendidikan-change_akademis-message-form input[name="elemen_penilaian_id"]').val('');
	// 	$('#px-setting_pendidikan-change_akademis-message-form input[name="elemen_penilaian_id"]').val(elemen_penilaian_id);
	// 	$('#px-setting_pendidikan-change_akademis-message-form input[name="tipe_akademis"]').val('');
	// 	$('#px-setting_pendidikan-change_akademis-message-form input[name="tipe_akademis"]').val(tipe_akademis);
	// });

	$('body').delegate('.btn-change_akademis','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-akademis');
		$.ajax({
			url : 'admin_setting_pendidikan/ajax_ubah_tipe_akademis',
			type : 'POST',
			dataType : 'json',
			data : {elemen_penilaian_id:id,
					tipe_akademis:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 0){
						$('.btn-change_akademis[data-id="'+id+'"]').removeClass('btn-warning');
						$('.btn-change_akademis[data-id="'+id+'"]').attr('data-akademis','0');
						$('.btn-change_akademis[data-id="'+id+'"]').addClass('btn-primary');
						$('.btn-change_akademis[data-id="'+id+'"]').text('Akademis');
					}else{
						$('.btn-change_akademis[data-id="'+id+'"]').removeClass('btn-primary');
						$('.btn-change_akademis[data-id="'+id+'"]').attr('data-akademis','1');
						$('.btn-change_akademis[data-id="'+id+'"]').addClass('btn-warning');
						$('.btn-change_akademis[data-id="'+id+'"]').text('Non-Akademis');
					}
				}
				else{
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});

	//change bobot
	$('#px-setting_pendidikan-change_bobot-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    if(response.status == 'ok'){
						$('#px-setting_pendidikan-change_bobot-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_bobot-message-box').removeClass('open');
						window.location.href = response.redirect;
                    }
                    else{
						$('#px-setting_pendidikan-change_bobot-message-form .msg-status').text(response.msg);
						$('#px-setting_pendidikan-change_bobot-message-box').removeClass('open');
					}
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
		}
	});

	// $('body').delegate('.btn-change_bobot','click',function(){
	// 	$('#px-setting_pendidikan-change_bobot-message-box').addClass('open');
	// 	var elemen_penilaian_id = $(this).attr('data-id');
	// 	var tipe_bobot = $(this).attr('data-bobot');
	// 	$('#px-setting_pendidikan-change_bobot-message-form input[name="elemen_penilaian_id"]').val('');
	// 	$('#px-setting_pendidikan-change_bobot-message-form input[name="elemen_penilaian_id"]').val(elemen_penilaian_id);
	// 	$('#px-setting_pendidikan-change_bobot-message-form input[name="tipe_bobot"]').val('');
	// 	$('#px-setting_pendidikan-change_bobot-message-form input[name="tipe_bobot"]').val(tipe_bobot);
	// });

	$('body').delegate('.btn-change_bobot','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-bobot');
		$.ajax({
			url : 'admin_setting_pendidikan/ajax_ubah_tipe_bobot',
			type : 'POST',
			dataType : 'json',
			data : {elemen_penilaian_id:id,
					tipe_bobot:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 0){
						$('.btn-change_bobot[data-id="'+id+'"]').removeClass('btn-warning');
						$('.btn-change_bobot[data-id="'+id+'"]').attr('data-bobot','0');
						$('.btn-change_bobot[data-id="'+id+'"]').addClass('btn-primary');
						$('.btn-change_bobot[data-id="'+id+'"]').text('Persen');
					}else{
						$('.btn-change_bobot[data-id="'+id+'"]').removeClass('btn-primary');
						$('.btn-change_bobot[data-id="'+id+'"]').attr('data-bobot','1');
						$('.btn-change_bobot[data-id="'+id+'"]').addClass('btn-warning');
						$('.btn-change_bobot[data-id="'+id+'"]').text('Desimal');
					}
				}
				else{
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});

	$('body').delegate('.btn-change_kelompok','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-kelompok');
		$.ajax({
			url : 'admin_setting_pendidikan/ajax_ubah_tipe_kelompok',
			type : 'POST',
			dataType : 'json',
			data : {elemen_penilaian_id:id,
					tipe_kelompok:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 0){
						$('.btn-change_kelompok[data-id="'+id+'"]').removeClass('btn-warning');
						$('.btn-change_kelompok[data-id="'+id+'"]').attr('data-kelompok','0');
						$('.btn-change_kelompok[data-id="'+id+'"]').addClass('btn-primary');
						$('.btn-change_kelompok[data-id="'+id+'"]').text('Personal');
					}else{
						$('.btn-change_kelompok[data-id="'+id+'"]').removeClass('btn-primary');
						$('.btn-change_kelompok[data-id="'+id+'"]').attr('data-kelompok','1');
						$('.btn-change_kelompok[data-id="'+id+'"]').addClass('btn-warning');
						$('.btn-change_kelompok[data-id="'+id+'"]').text('Kelompok');
					}
				}
				else{
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});

	$('body').delegate('.btn-change_mata_kegiatan','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-mata_kegiatan');
		$.ajax({
			url : 'admin_setting_pendidikan/ajax_ubah_mata_kegiatan',
			type : 'POST',
			dataType : 'json',
			data : {elemen_penilaian_id:id,
					mata_kegiatan:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 0){
						$('.btn-change_mata_kegiatan[data-id="'+id+'"]').removeClass('btn-success');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"]').attr('data-mata_kegiatan','0');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"]').addClass('btn-warning');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"] i').removeClass('fa-check');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"] i').addClass('fa-times');
					}else{
						$('.btn-change_mata_kegiatan[data-id="'+id+'"]').removeClass('btn-warning');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"]').attr('data-mata_kegiatan','1');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"]').addClass('btn-success');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"] i').removeClass('fa-times');
						$('.btn-change_mata_kegiatan[data-id="'+id+'"] i').addClass('fa-check');
					}
				}
				else{
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});

	$('body').delegate('.btn-change_is_taskap','click',function(){
		var id = $(this).attr('data-id');
		var status = $(this).attr('data-is_taskap');
		$.ajax({
			url : 'admin_setting_pendidikan/ajax_ubah_is_taskap',
			type : 'POST',
			dataType : 'json',
			data : {elemen_penilaian_id:id,
					is_taskap:status},
			success : function(response){
				if(response.status == 'ok'){
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-success').hide("slow");
					    }, 1000);
					if(response.status_now == 0){
						$('.btn-change_is_taskap[data-id="'+id+'"]').removeClass('btn-success');
						$('.btn-change_is_taskap[data-id="'+id+'"]').attr('data-is_taskap','0');
						$('.btn-change_is_taskap[data-id="'+id+'"]').addClass('btn-warning');
						$('.btn-change_is_taskap[data-id="'+id+'"] i').removeClass('fa-check');
						$('.btn-change_is_taskap[data-id="'+id+'"] i').addClass('fa-times');
					}else{
						$('.btn-change_is_taskap[data-id="'+id+'"]').removeClass('btn-warning');
						$('.btn-change_is_taskap[data-id="'+id+'"]').attr('data-is_taskap','1');
						$('.btn-change_is_taskap[data-id="'+id+'"]').addClass('btn-success');
						$('.btn-change_is_taskap[data-id="'+id+'"] i').removeClass('fa-times');
						$('.btn-change_is_taskap[data-id="'+id+'"] i').addClass('fa-check');
					}
				}
				else{
					$('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').show().children('span').text(response.msg);
					setTimeout(function() {
					        $('#px-setting_pendidikan-elemen_penilaian_list-table .alert-danger').hide("slow");
					    }, 1000);
					}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});

	$('body').delegate('#px-setting_pendidikan-elemen_penilaian_list-dropdown','change',function(){
		var id = $('#px-setting_pendidikan-elemen_penilaian_list-dropdown').val();
		if (id) {
		  if(id==0)
		  	window.location = 'admin_setting_pendidikan/elemen_penilaian';
		  else
		  	window.location = 'admin_setting_pendidikan/elemen_penilaian?id='+id;
		}
		return false;
	});
	
})