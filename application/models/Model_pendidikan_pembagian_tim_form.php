<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pendidikan_pembagian_tim_form extends CI_Model {

	var $column = array('id', 'nama_lengkap', 'pangkat','instansi_negara'); //set column field database for order and search
	var $order = array('nama_lengkap' => 'asc'); // default order

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$elemen_penilaian_id = $this->session->userdata('tim_blok')['id'];
		$this->db->select('td.pendidikan_peserta_id as id');
		$this->db->from($this->tbl_tim_detail.' td');
		$this->db->join($this->tbl_tim.' t', 't.id = td.tim_id', 'left');
		$this->db->where('t.pendidikan_id', $pendidikan_id);
		$this->db->where('t.elemen_penilaian_id', $elemen_penilaian_id);
		$ignore_data = $this->db->get()->result();
		foreach ($ignore_data as $row) {
			$ignore[] = $row->id;
		}

		$this->db->select('*');
		$this->db->from($this->tbl_pendidikan_peserta);
		$this->db->where('pendidikan_id', $pendidikan_id);
		if(!empty($ignore))
			$this->db->where_not_in('id', $ignore);

		$i = 1;
		$where = "";
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
		$elemen_penilaian_id = $this->session->userdata('tim_blok')['id'];
		$this->db->select('td.pendidikan_peserta_id as id');
		$this->db->from($this->tbl_tim_detail.' td');
		$this->db->join($this->tbl_tim.' t', 't.id = td.tim_id', 'left');
		$this->db->where('t.pendidikan_id', $pendidikan_id);
		$this->db->where('t.elemen_penilaian_id', $elemen_penilaian_id);
		$ignore_data = $this->db->get()->result();
		foreach ($ignore_data as $row) {
			$ignore[] = $row->id;
		}

		$this->db->select('*');
		$this->db->from($this->tbl_pendidikan_peserta);
		$this->db->where('pendidikan_id', $pendidikan_id);
		if(!empty($ignore))
			$this->db->where_not_in('id', $ignore);

		return $this->db->count_all_results();
	}	

}

/* End of file model_site_list.php */
/* Location: ./application/models/model_site_list.php */