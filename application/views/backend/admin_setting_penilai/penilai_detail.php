<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?> Detail</li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Penilai : <?php echo $penilai->gelar_depan.' '.$penilai->nama_lengkap.($penilai->gelar_belakang!="" ? ', '.$penilai->gelar_belakang : ''); ?></h3>
                    <button onclick="history.go(-1)" class="btn btn-default pull-right">Kembali</button>
                </div>
                <div class="tab-content faq-cat-content">
                    <!-- TAB DATA SITE -->
                    <div class="tab-pane active in fade" id="tab-1">
                        <form class="form-horizontal">
                            <div class="panel-body">
                                <div class="col-md-10 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Gelar Depan</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->gelar_depan ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Gelar Belakang</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->gelar_belakang ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Nama Lengkap</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->nama_lengkap ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Telepon</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->telp ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Ruangan</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->ruangan ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Pangkat</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->pangkat ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Jabatan</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->jabatan ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Bidang</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->bidang ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label">Jenis Penilai</label>
                                        <div class="col-md-9 col-xs-12">
                                            <span class="form-control"><?php echo $penilai->jenis_penilai ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>

<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<!-- <script type="text/javascript" src="assets/backend_assets/js/settings.js"></script> -->

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>