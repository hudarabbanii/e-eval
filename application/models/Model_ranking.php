<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_ranking extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	function get_rank_skema_npa($skema_penilaian_id,$pendidikan_peserta_id){
		$query = $this->db->query("
			SELECT pendidikan_peserta_id, skema_penilaian_id, npa, FIND_IN_SET( npa, (    
			SELECT GROUP_CONCAT( npa
			ORDER BY npa DESC ) 
			FROM ev_nilai_akhir_peserta WHERE skema_penilaian_id = ".$skema_penilaian_id.")
			) AS rank
			FROM ev_nilai_akhir_peserta
			WHERE pendidikan_peserta_id = ".$pendidikan_peserta_id." AND skema_penilaian_id = ".$skema_penilaian_id);

		return $query->row();
	}

	function get_rank_skema_nna($skema_penilaian_id,$pendidikan_peserta_id){
		$query = $this->db->query("
			SELECT pendidikan_peserta_id, skema_penilaian_id, nna, FIND_IN_SET( nna, (    
			SELECT GROUP_CONCAT( nna
			ORDER BY nna DESC ) 
			FROM ev_nilai_akhir_peserta WHERE skema_penilaian_id = ".$skema_penilaian_id.")
			) AS rank
			FROM ev_nilai_akhir_peserta
			WHERE pendidikan_peserta_id = ".$pendidikan_peserta_id." AND skema_penilaian_id = ".$skema_penilaian_id);

		return $query->row();
	}

	function get_min_max_avg($skema_penilaian_id,$pendidikan_id){
		$query = $this->db->query(
			"select max(npa) as max_npa, min(npa) as min_npa, avg(npa) as avg_npa, max(nna) as max_nna, min(nna) as min_nna, avg(nna) as avg_nna
			from (select a.npa,a.nna from ev_nilai_akhir_peserta a
			where a.skema_penilaian_id=".$skema_penilaian_id." AND a.pendidikan_id=".$pendidikan_id.") as table_nilai");
		return $query->row();
	}

	function get_min_max_avg_elemen_penilaian($elemen_penilaian_id,$pendidikan_id){
		$query = $this->db->query(
			"select max(npa) as max_npa, min(npa) as min_npa, avg(npa) as avg_npa, max(nna) as max_nna, min(nna) as min_nna, avg(nna) as avg_nna
			from (SELECT np.elemen_penilaian_id, sum(nilai)/count(nilai) as npa, sum(nna)/count(nna) as nna, 
			bobot_nna, bobot_npa
			FROM ev_nilai_peserta np
			LEFT JOIN ev_bagan_detail bd ON bd.bagan_pendidikan_id=np.bagan_pendidikan_id AND bd.elemen_penilaian_id=np.elemen_penilaian_id
			WHERE np.pendidikan_id=".$pendidikan_id." AND np.elemen_penilaian_id=".$elemen_penilaian_id."
			GROUP BY np.pendidikan_peserta_id) as table_nilai");
		return $query->row();
	}

	function get_min_max_avg_skema_penilaian($skema_penilaian_id,$pendidikan_id){
		$query = $this->db->query(
			"select max(npa) as max_npa, min(npa) as min_npa, avg(npa) as avg_npa, max(nna) as max_nna, min(nna) as min_nna, avg(nna) as avg_nna
			from ev_nilai_akhir_peserta nap
			WHERE nap.pendidikan_id=".$pendidikan_id." AND nap.skema_penilaian_id=".$skema_penilaian_id);
		return $query->row();
	}
}