<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_jadwal extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_jadwal', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $this->pendidikan_jadwal();
    }

    function pendidikan_jadwal() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['content'] = $this->load->view('backend/pendidikan_jadwal/jadwal_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function ajax_pendidikan_jadwal_list(){
        $list = $this->model_pendidikan_jadwal_list->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {

            //render data
            $no++;

            $row = array();
            $row[] = $no;
            $row[] = $this->tanggal_indo($data_row->tanggal);
            //get multiple penilai
            $get_jadwal_penilai = $this->model_basic->select_where($this->tbl_jadwal_penilai, 'jadwal_id', $data_row->id);
            if($get_jadwal_penilai->num_rows() > 1){
                foreach ($get_jadwal_penilai->result() as $jp_row) {
                    $penilai_row = $this->model_basic->select_where($this->tbl_penilai, 'id', $jp_row->penilai_id)->row();
                    $x[] = ($penilai_row->gelar_depan != "" ? $penilai_row->gelar_depan.' ' : '').$penilai_row->nama_lengkap.($penilai_row->gelar_belakang != "" ? ', '.$penilai_row->gelar_belakang : '');
                }
                $penilai = implode(',', $x);
            }else{
                $penilai = ($data_row->gelar_depan != "" ? $data_row->gelar_depan.' ' : '').$data_row->penilai.($data_row->gelar_belakang != "" ? ', '.$data_row->gelar_belakang : '');
            }
            $row[] = $penilai;
            $row[] = $data_row->tim;
            $row[] = $data_row->elemen_penilaian;
            $row[] = $data_row->bidang_studi;
            if($this->session_admin['id_usergroup'] == 3){//penilai
            $row[] = '<a href="pendidikan_jadwal/pendidikan_jadwal_detail_form/'.$data_row->id.'" class="btn btn-success btn-xs" data-original-title="Detail" data-placement="top" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                    <a href="pendidikan_jadwal/request_pengganti_form/'.$data_row->id.'" class="btn btn-info btn-xs btn-pengganti" data-original-title="Request Pengganti" data-placement="top" data-toggle="tooltip"><i class="fa fa-exchange"></i></a>';
            }else{
            $row[] = '<form action="pendidikan_jadwal/pendidikan_jadwal_form" method="post">
                        <input type="hidden" name="id" value="'.$data_row->id.'">
                        <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
                    </form>';
            }
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_pendidikan_jadwal_list->count_all(),
                        "recordsFiltered" => $this->model_pendidikan_jadwal_list->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function pendidikan_jadwal_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $id)->row();
            $penilai = $this->model_basic->select_where($this->tbl_jadwal_penilai, 'jadwal_id', $id)->result();
            foreach ($penilai as $data_row) {
                $detail_penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $data_row->penilai_id)->row();
                $data_row->penilai = ($detail_penilai->gelar_depan != "" ? $detail_penilai->gelar_depan.' ' : '').$detail_penilai->nama_lengkap.($detail_penilai->gelar_belakang != "" ? ', '.$detail_penilai->gelar_belakang : '');
            }
            $data['data'] = $jadwal;
            $data['data_penilai'] = $penilai;
        } else{
            $data['data'] = null;
            $data['data_penilai'] = null;
        }

        $data['penilai'] = $this->model_pendidikan->select_penilai_pendidikan($this->session->userdata('menu_pendidikan')['pendidikan_id']);
        foreach ($data['penilai'] as $data_row) {
            $data_row->penilai = ($data_row->gelar_depan != "" ? $data_row->gelar_depan.' ' : '').$data_row->nama_lengkap.($data_row->gelar_belakang != "" ? ', '.$data_row->gelar_belakang : '');
        }
        $data['bidang_studi'] = $this->model_basic->select_all_order($this->tbl_bidang_studi, 'name', 'ASC');
        $data['tim'] = $this->model_basic->select_where_order($this->tbl_tim, 'pendidikan_id', $this->session->userdata('menu_pendidikan')['pendidikan_id'], 'name', 'ASC')->result();
        $data['elemen_penilaian'] = $this->model_eval->get_data_elemen_penilaian_detail($this->session->userdata('menu_pendidikan')['pendidikan_id'],0);
        $data['content'] = $this->load->view('backend/pendidikan_jadwal/jadwal_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function request_pengganti_form($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        if ($id) {
            $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $id)->row();
            $data['data'] = $jadwal;
        } else{
            $data['data'] = null;
        }

        $data['penilai'] = $this->model_pendidikan->select_penilai_pendidikan($this->session->userdata('menu_pendidikan')['pendidikan_id']);
        foreach ($data['penilai'] as $data_row) {
            $data_row->penilai = ($data_row->gelar_depan != "" ? $data_row->gelar_depan.' ' : '').$data_row->nama_lengkap.($data_row->gelar_belakang != "" ? ', '.$data_row->gelar_belakang : '');
        }

        $data['content'] = $this->load->view('backend/pendidikan_jadwal/jadwal_form_pengganti', $data, true);
        $this->load->view('backend/index', $data);
    }

    function request_pengganti_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $insert['jadwal_id'] = $this->input->post('id');
        $insert['penilai_id'] = $this->session_admin['penilai_id'];
        $insert['penilai_pengganti_id'] = $this->input->post('penilai_pengganti_id');
        $insert['user_remark'] = $this->input->post('user_remark');
        $insert['admin_id'] = 0;
        $insert['status'] = 0;
        $insert['date_created'] = date('Y-m-d H:i:s', now());

        if ($this->input->post('penilai_pengganti_id') && $this->input->post('user_remark')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_jadwal_penilai_pengganti, $insert);
            if ($do_insert) {
                $this->insert_notifications_jadwal_admin($insert);
                $this->save_log_admin(ACT_CREATE, 'Insert New Penilai Pengganti, Jadwal = '.$insert['jadwal_id']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function insert_notifications_jadwal_admin($data){
        $jadwal_detail_id = $this->model_pendidikan_jadwal_list->get_jadwal_detail_id($data['jadwal_id']);
        //pendidikan
        $get_pendidikan = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $jadwal_detail_id->pendidikan_id);
        if($get_pendidikan->num_rows() == 1)
            $pendidikan = $get_pendidikan->row()->name;
        else
            $pendidikan = 'Unknown';
        //bidangstudi
        $get_bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $jadwal_detail_id->bidang_studi_id);
        if($get_bidang_studi->num_rows() == 1)
            $bidang_studi = $get_bidang_studi->row()->name;
        else
            $bidang_studi = 'Unknown';
        
        $url = 'pendidikan_request_pengganti';

        $insert = array(
            'jadwal_id' => $data['jadwal_id'],
            'penilai_id' => 0,
            'pendidikan' => $pendidikan,
            'bidang_studi' => $bidang_studi,
            'url' => $url,
            'date_created' => date('Y-m-d H:i:s', now())
        );
        $this->model_basic->insert_all($this->tbl_notifikasi, $insert);
    }

    function pendidikan_jadwal_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $penilai_id_post = $this->input->post('penilai_id');
        $penilai_id = array_unique($penilai_id_post);
        $count = count($penilai_id);

        $elemen_penilaian_id = $this->input->post('elemen_penilaian_id');
        $bagan_pendidikan_id = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $this->session->userdata('menu_pendidikan')['pendidikan_id'])->row()->bagan_pendidikan_id;
        $bagan_detail = $this->model_basic->select_where_array($this->tbl_bagan_detail, array('bagan_pendidikan_id'=>$bagan_pendidikan_id, 'elemen_penilaian_id'=>$elemen_penilaian_id));
        if($bagan_detail->num_rows() == 0)
            $this->returnJson(array('status' => 'error', 'msg' => 'Elemen Penilaian tidak ditemukan di Bagan'));

        $table_field = $this->db->list_fields($this->tbl_jadwal);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['bagan_pendidikan_id'] = $bagan_pendidikan_id;
        $insert['elemen_penilaian_id'] = $elemen_penilaian_id;
        $insert['pendidikan_id'] = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $insert['is_active'] = 1;
        $insert['manual_activation'] = 0;
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        $insert['date_modified'] = date('Y-m-d H:i:s', now());

        if ($this->input->post('tanggal') && $this->input->post('elemen_penilaian_id') && $this->input->post('penilai_id') && $this->input->post('bidang_studi_id') && $this->input->post('tim_id')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_jadwal, $insert);
            if ($do_insert) {
            $insert['id'] = $this->db->insert_id();
                $_count = 1;
                foreach ($penilai_id as $val) {
                    $insert_penilai = array();
                    $insert_penilai['penilai_id'] = $val;
                    $insert_penilai['jadwal_id'] = $do_insert->id;
                    if($_count == 1)
                        $insert_penilai['status'] = 1;
                    else
                        $insert_penilai['status'] = 0;

                    $do_insert_penilai = $this->model_basic->insert_all($this->tbl_jadwal_penilai, $insert_penilai);
                    $_count++;
                    $insert['penilai_id'] = $val;
                    $this->insert_notifications_jadwal($insert);
                }

                $this->save_log_admin(ACT_CREATE, 'Insert New Jadwal, Elemen Penilaian = '.$insert['elemen_penilaian_id']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function insert_notifications_jadwal($data){
        //pendidikan
        $get_pendidikan = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $data['pendidikan_id']);
        if($get_pendidikan->num_rows() == 1)
            $pendidikan = $get_pendidikan->row()->name;
        else
            $pendidikan = 'Unknown';
        //bidangstudi
        $get_bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $data['bidang_studi_id']);
        if($get_bidang_studi->num_rows() == 1)
            $bidang_studi = $get_bidang_studi->row()->name;
        else
            $bidang_studi = 'Unknown';
        
        $url = 'pendidikan_jadwal/pendidikan_jadwal_detail_form/'.$data['id'];

        $insert = array(
            'jadwal_id' => $data['id'],
            'penilai_id' => $data['penilai_id'],
            'pendidikan' => $pendidikan,
            'bidang_studi' => $bidang_studi,
            'url' => $url,
            'date_created' => date('Y-m-d H:i:s', now())
        );
        $this->model_basic->insert_all($this->tbl_notifikasi, $insert);
    }

    function pendidikan_jadwal_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $penilai_id = $this->input->post('penilai_id');
        $count = count($this->input->post('penilai_id'));

        $table_field = $this->db->list_fields($this->tbl_jadwal);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        unset($update['pendidikan_id']);
        unset($update['bagan_pendidikan_id']);
        unset($update['is_active']);
        unset($update['date_created']);


        $do_update = $this->model_basic->update($this->tbl_jadwal, $update, 'id', $update['id']);

        if ($do_update) {
            $get_jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $update['id'])->row();
            $update_notif['id'] = $get_jadwal->id;
            $update_notif['pendidikan_id'] = $get_jadwal->pendidikan_id;
            $update_notif['bidang_studi_id'] = $get_jadwal->bidang_studi_id;
            //delete penilai
            $do_delete = $this->model_basic->delete($this->tbl_jadwal_penilai, 'jadwal_id', $update['id']);
            //add new penilai
            $_count = 1;
            foreach ($penilai_id as $val) {
                $insert_penilai = array();
                $insert_penilai['penilai_id'] = $val;
                $insert_penilai['jadwal_id'] = $get_jadwal->id;
                if($_count == 1)
                    $insert_penilai['status'] = 1;
                else
                    $insert_penilai['status'] = 0;

                $do_insert_penilai = $this->model_basic->insert_all($this->tbl_jadwal_penilai, $insert_penilai);
                
                $update_notif['penilai_id'] = $val;                
                $this->insert_notifications_jadwal($update_notif);
                $_count++;
            }
            $this->save_log_admin(ACT_UPDATE, 'Update Jadwal '.$update['id']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function pendidikan_jadwal_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_jadwal, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_jadwal, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Jadwal '.$deleted_data->id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }

    function pendidikan_jadwal_detail_form($id){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        //update notifications
        $notifikasi = $this->model_basic->select_where($this->tbl_notifikasi, 'jadwal_id', $id);
        if ($notifikasi->num_rows() != 0){
        switch ($this->session_admin['id_usergroup']) {
            case '3': //penilai
                $update = array('status' => 1, 'date_read' => date('Y-m-d H:i:s', now()));
                $where = array('jadwal_id' => $id);
            break;
        }
        $this->model_basic->update_where_array($this->tbl_notifikasi, $update, $where);
        }

        $jadwal_detail_id = $this->model_pendidikan_jadwal_list->get_jadwal_detail_id($id);

        //add var to session
        $var = array(
        'pendidikan_id'         => $jadwal_detail_id->pendidikan_id,
        'bagan_pendidikan_id'   => $jadwal_detail_id->bagan_pendidikan_id,
        'elemen_penilaian_id'   => $jadwal_detail_id->elemen_penilaian_id,
        );
        $this->session->set_userdata('var', $var);

        //add var to session
        $jadwal = array(
        'jadwal_id'         => $jadwal_detail_id->jadwal_id,
        'penilai_id'        => $jadwal_detail_id->penilai_id,
        'tim_id'            => $jadwal_detail_id->tim_id,
        'bidang_studi_id'   => $jadwal_detail_id->bidang_studi_id
        );
        $this->session->set_userdata('jadwal', $jadwal);

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->session->userdata('var')['elemen_penilaian_id']);
        if($elemen_penilaian->num_rows() == 1)
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
        else
            $data['elemen_penilaian'] = 'Unknown';

        $get_elemen_penilaian_nna = $this->model_hasil_bagan_detail->get_title_non_akademis();
        $get_elemen_penilaian_npa = $this->model_hasil_bagan_detail->get_title_akademis();
        $npa = $get_elemen_penilaian_npa->result();
        foreach ($npa as $data_row) {
            $check_child = $this->model_hasil_bagan_detail->bagan_detail_child($data_row->elemen_penilaian_id);
            if($check_child->num_rows() == 0){
                $data_row->child = null;
            }else{
                $data_row->child = $check_child->result();
            }
        }
        $data['title_nna'] = $get_elemen_penilaian_nna->result();
        $data['title_npa'] = $npa;
        
        $bidang_studi_status = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id'])->row()->status_asing;
        // get peserta list
        $peserta = $this->model_hasil_bagan_detail->get_peserta_from_pendidikan_peserta($bidang_studi_status)->result();
        $peserta_pindahan = $this->model_hasil_bagan_detail->get_peserta_pindahan($this->session->userdata('jadwal')['jadwal_id']);
        if($peserta_pindahan->num_rows() > 0){
            foreach ($peserta_pindahan->result() as $ppindahan_row) {
                array_push($peserta, $ppindahan_row);
            }
        }
        // echo '<pre>';
        // print_r($cek_peserta_pindahan);
        // echo '</pre>';
        // die;
        $index = 0;
        foreach ($peserta as $peserta_row) {
            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            //get data non akademis
            foreach($get_elemen_penilaian_nna->result() as $elemen_row){
                
                $peserta_row->elemen_penilaian_id_nna[] = $elemen_row->elemen_penilaian_id;
                //if taskap
                if($elemen_row->is_taskap == 1){
                    $nilai_non_akademis = $this->model_hasil_bagan_detail->get_nilai_non_akademis_taskap($peserta_row->id, $elemen_row->elemen_penilaian_id, $this->session_admin['penilai_id']);
                }else{
                    $nilai_non_akademis = $this->model_hasil_bagan_detail->get_nilai_non_akademis($peserta_row->id, $elemen_row->elemen_penilaian_id);
                }
                if($nilai_non_akademis->num_rows() == 0){
                        $peserta_row->nilai_non_akademis[] = 0;
                        $peserta_row->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + 0;
                }else{
                        $peserta_row->nilai_non_akademis[] = $nilai_non_akademis->row()->nna;
                        $peserta_row->keterangan[] = $nilai_non_akademis->row()->keterangan;
                        $peserta_row->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + $nilai_non_akademis->row()->nilai_bobot;
                        $nilai_akhir_non_akademis = $peserta_row->nilai_akhir_non_akademis;
                }
            }
            //get data akademis
            foreach($get_elemen_penilaian_npa->result() as $elemen_row){
                
                $peserta_row->elemen_penilaian_id_npa[] = $elemen_row->elemen_penilaian_id;

                //if taskap
                if($elemen_row->is_taskap == 1){
                    $nilai_akademis = $this->model_hasil_bagan_detail->get_nilai_akademis_taskap($peserta_row->id, $elemen_row->elemen_penilaian_id, $this->session_admin['penilai_id']);
                }else{
                    $nilai_akademis = $this->model_hasil_bagan_detail->get_nilai_akademis($peserta_row->id, $elemen_row->elemen_penilaian_id);
                }
                if($nilai_akademis->num_rows() == 0){
                        $peserta_row->nilai_akademis[] = 0;
                        $peserta_row->nilai_akhir_akademis = $nilai_akhir_akademis + 0;
                }else{
                        $peserta_row->nilai_akademis[] = $nilai_akademis->row()->nilai;
                        $peserta_row->keterangan[] = $nilai_akademis->row()->keterangan;
                        $peserta_row->nilai_akhir_akademis = $nilai_akhir_akademis + $nilai_akademis->row()->nilai_bobot;
                        $nilai_akhir_akademis = $peserta_row->nilai_akhir_akademis;
                }
                if(isset($elemen_row->child)){
                    $peserta_row->elemen_child[] = 1;
                }else{
                    $peserta_row->elemen_child[] = 0;
                }
            }

            $cek_peserta_pindahan = $this->model_hasil_bagan_detail->cek_peserta_pindahan($peserta_row->id, $this->session->userdata('jadwal')['jadwal_id']);
            if($cek_peserta_pindahan == TRUE){
                $hapus_peserta_index[] = $index;
            }
        }
        //hapus peserta pindah jadwal
        if(!empty($hapus_peserta_index)){
            foreach ($hapus_peserta_index as $index_hapus) {
                unset($peserta[$index_hapus]);
            }
        }

        $data['list'] = $peserta;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);

        //is_active / expired
        if(strtotime($jadwal->row()->tanggal.'+2 days') < strtotime(date('Y-m-d', now()))){
            $do_update_jadwal_is_active = $this->model_basic->update($this->tbl_jadwal, ['is_active' => 0], 'id', $jadwal->row()->id);
            //cek manual activation
            if($jadwal->row()->manual_activation == 1)
                $data['is_active'] = 1;
            else
                $data['is_active'] = 0;
        }else{
            $data['is_active'] = 1;
        }

        //get header
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $this->tanggal_indo($jadwal->row()->tanggal);
        else
            $data['jadwal'] = 'Unknown';        

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $this->session->userdata('jadwal')['tim_id']);
        if($tim->num_rows() == 1)
            $data['tim'] = $tim->row()->name;
        else
            $data['tim'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';

        $keterangan = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $this->session->userdata('var')['elemen_penilaian_id'], 'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id']]);
        if($keterangan->num_rows() == 1)
            $data['keterangan'] = $keterangan->row()->keterangan_form;
        else
            $data['keterangan'] = '-';
        
        $data['content'] = $this->load->view('backend/pendidikan_jadwal/jadwal_detail_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_jadwal_detail_form_2(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $pendidikan_peserta_id = $this->input->get('pendidikan_peserta_id');
        $elemen_penilaian_id = $this->input->get('elemen_penilaian_id');

        $data['pendidikan_peserta_id'] = $pendidikan_peserta_id;
        $data['elemen_penilaian_id'] = $elemen_penilaian_id;
        $data['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];

        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_data_elemen_penilaian_detail($this->session->userdata('var')['bagan_pendidikan_id'], $elemen_penilaian_id, $pendidikan_peserta_id,$this->session_admin['penilai_id']);

        // print_r($get_elemen_penilaian);
        // die;

        $data['list'] = $get_elemen_penilaian;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);

        //is_active / expired
        if(strtotime($jadwal->row()->tanggal.'+2 days') < strtotime(date('Y-m-d', now()))){
            $do_update_jadwal_is_active = $this->model_basic->update($this->tbl_jadwal, ['is_active' => 0], 'id', $jadwal->row()->id);
            //cek manual activation
            if($jadwal->row()->manual_activation == 1)
                $data['is_active'] = 1;
            else
                $data['is_active'] = 0;
        }else{
            $data['is_active'] = 1;
        }

        //get header
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $this->tanggal_indo($jadwal->row()->tanggal);
        else
            $data['jadwal'] = 'Unknown';        

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id);
        if($elemen_penilaian->num_rows() == 1){
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
            $data['elemen_penilaian_id'] = $elemen_penilaian->row()->id;

            $elemen_penilaian_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->row()->parent_id);
            $data['elemen_penilaian_parent'] = $elemen_penilaian_parent->row()->name;
        }else
            $data['elemen_penilaian'] = 'Unknown';

        $pendidikan_peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $pendidikan_peserta_id);
        if($pendidikan_peserta->num_rows() == 1)
            $data['pendidikan_peserta'] = $pendidikan_peserta->row();
        else
            $data['pendidikan_peserta'] = 'Unknown';

        $judul_produk = $this->model_basic->select_where_array($this->tbl_judul_produk, ['elemen_penilaian_id'=>$elemen_penilaian_id, 'pendidikan_peserta_id'=>$pendidikan_peserta_id]);
        if($judul_produk->num_rows() > 1)
            $data['judul_produk'] = $judul_produk->row()->judul;
        else
            $data['judul_produk'] = '';

        $keterangan = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $this->session->userdata('var')['elemen_penilaian_id'], 'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id']]);
        if($keterangan->num_rows() == 1)
            $data['keterangan'] = $keterangan->row()->keterangan_form;
        else
            $data['keterangan'] = '-';
        
        $data['content'] = $this->load->view('backend/pendidikan_jadwal/jadwal_detail_form_2', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_jadwal_detail_form_3(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $pendidikan_peserta_id = $this->input->get('pendidikan_peserta_id');
        $elemen_penilaian_id = $this->input->get('elemen_penilaian_id');
        
        $data['pendidikan_peserta_id'] = $pendidikan_peserta_id;
        $data['elemen_penilaian_id'] = $elemen_penilaian_id;
        $data['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id);
        if($elemen_penilaian->num_rows() == 1){
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
            $data['elemen_penilaian_id'] = $elemen_penilaian->row()->id;

            $elemen_penilaian_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->row()->parent_id);
            $data['elemen_penilaian_parent'] = $elemen_penilaian_parent->row()->name;
        }else
            $data['elemen_penilaian'] = 'Unknown';

        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_data_elemen_penilaian_detail($this->session->userdata('var')['bagan_pendidikan_id'], $elemen_penilaian_id, $pendidikan_peserta_id,$this->session_admin['penilai_id']);

        $data['list'] = $get_elemen_penilaian;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);

        //is_active / expired
        if(strtotime($jadwal->row()->tanggal.'+2 days') < strtotime(date('Y-m-d', now()))){
            $do_update_jadwal_is_active = $this->model_basic->update($this->tbl_jadwal, ['is_active' => 0], 'id', $jadwal->row()->id);
            //cek manual activation
            if($jadwal->row()->manual_activation == 1)
                $data['is_active'] = 1;
            else
                $data['is_active'] = 0;
        }else{
            $data['is_active'] = 1;
        }

        //get header
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $this->tanggal_indo($jadwal->row()->tanggal);
        else
            $data['jadwal'] = 'Unknown';        

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id);
        if($elemen_penilaian->num_rows() == 1){
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
            $data['elemen_penilaian_id'] = $elemen_penilaian->row()->id;

            $elemen_penilaian_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->row()->parent_id);
            $data['elemen_penilaian_parent'] = $elemen_penilaian_parent->row()->name;
        }else
            $data['elemen_penilaian'] = 'Unknown';

        $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $this->session->userdata('jadwal')['tim_id']);
        if($tim->num_rows() == 1)
            $data['tim'] = $tim->row()->name;
        else
            $data['tim'] = 'Unknown';

        $pendidikan_peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $pendidikan_peserta_id);
        if($pendidikan_peserta->num_rows() == 1)
            $data['pendidikan_peserta'] = $pendidikan_peserta->row();
        else
            $data['pendidikan_peserta'] = 'Unknown';

        $keterangan = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $this->session->userdata('var')['elemen_penilaian_id'], 'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id']]);
        if($keterangan->num_rows() == 1)
            $data['keterangan'] = $keterangan->row()->keterangan_form;
        else
            $data['keterangan'] = '-';
        
        $data['content'] = $this->load->view('backend/pendidikan_jadwal/jadwal_detail_form_3', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pendidikan_jadwal_detail_add() { //form 1
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $bidang_studi_status = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id'])->row()->status_asing;

        $this->db->trans_begin();
        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_title();
        $peserta = $this->model_hasil_bagan_detail->get_peserta_from_pendidikan_peserta($bidang_studi_status)->result();
        $peserta_pindahan = $this->model_hasil_bagan_detail->get_peserta_pindahan($this->session->userdata('jadwal')['jadwal_id']);
        if($peserta_pindahan->num_rows() > 0){
            foreach ($peserta_pindahan->result() as $ppindahan_row) {
                array_push($peserta, $ppindahan_row);
            }
        }
        foreach ($peserta as $peserta_row) {
            $check_peserta_pindahan = $this->model_hasil_bagan_detail->get_peserta_pindahan_asal($this->session->userdata('jadwal')['jadwal_id'],$peserta_row->id);
            if($check_peserta_pindahan->num_rows() > 0){
                continue;
            }
            $nna = 0;
            $npa = 0;
            foreach($get_elemen_penilaian->result() as $elemen_row){
                $nilai = $this->add_edit_nilai($peserta_row->id, $elemen_row->elemen_penilaian_id, $elemen_row->tipe_akademis);
                $nna = $nna + $nilai->nna * $elemen_row->bobot_nna / 100;
                $npa = $npa + $nilai->npa * $elemen_row->bobot_npa / 100;
            }
            //fill nilai self
            $this->add_edit_nilai_parent($peserta_row->id, $this->session->userdata('var')['elemen_penilaian_id'], $nna, $npa);
            //fill nilai parent
            $elemen = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->session->userdata('var')['elemen_penilaian_id'])->row();
            $elemen_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen->parent_id)->row();
            $data_nilai_parent = $this->model_rev->get_data_elemen_penilaian_detail_one_level($this->session->userdata('var')['pendidikan_id'],$this->session->userdata('var')['bagan_pendidikan_id'],$elemen_parent->id,$peserta_row->id);
            if($elemen_parent->id != 1 && $elemen_parent->id != 2 && $elemen_parent->id != 3 && $elemen_parent->id != 4 && $elemen_parent->id != 5 && $elemen_parent->id != 6 && $elemen_parent->id != 7){
                $this->add_edit_nilai_parent($peserta_row->id, $elemen_parent->id, $data_nilai_parent->nilai_non_akademis, $data_nilai_parent->nilai_akademis);
                //is there parent?
                $elemen_parent_2 = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_parent->id)->row();
                $elemen_parent_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_parent_2->parent_id)->row();
                $data_nilai_parent = $this->model_rev->get_data_elemen_penilaian_detail_one_level($this->session->userdata('var')['pendidikan_id'],$this->session->userdata('var')['bagan_pendidikan_id'],$elemen_parent_parent->id,$peserta_row->id);
                if($elemen_parent_parent->id != 1 && $elemen_parent_parent->id != 2 && $elemen_parent_parent->id != 3 && $elemen_parent_parent->id != 4 && $elemen_parent_parent->id != 5 && $elemen_parent_parent->id != 6 && $elemen_parent_parent->id != 7){
                    $this->add_edit_nilai_parent($peserta_row->id, $elemen_parent_parent->id, $data_nilai_parent->nilai_non_akademis, $data_nilai_parent->nilai_akademis);
                }else{
                    //input to skema (nilai akumulasi all bs)
                    $nilai_all_bs = $this->model_hasil_bagan_detail->get_hasil_nilai_all_bs($peserta_row->id, $elemen_parent_2->id);
                    //input and get skema parent
                    $skema_penilaian_parent_id = $this->add_edit_nilai_skema_by_ep($peserta_row->id, $elemen_parent_2->id, $nilai_all_bs->row()->nna, $nilai_all_bs->row()->npa);
                    $skema_parent = $this->model_rev->get_data_skema_penilaian_detail_one_level($this->session->userdata('var')['pendidikan_id'], $this->session->userdata('var')['bagan_pendidikan_id'], $skema_penilaian_parent_id, $peserta_row->id);
                    $this->add_edit_nilai_skema_by_sp($peserta_row->id, $skema_penilaian_parent_id, $skema_parent->nilai_non_akademis, $skema_parent->nilai_akademis);
                }
            }else{
                //input to skema (nilai akumulasi all bs)
                $nilai_all_bs = $this->model_hasil_bagan_detail->get_hasil_nilai_all_bs($peserta_row->id, $elemen->id);
                //input and get skema parent
                $skema_penilaian_parent_id = $this->add_edit_nilai_skema_by_ep($peserta_row->id, $elemen_parent->id, $nilai_all_bs->row()->nna, $nilai_all_bs->row()->npa);
                if($skema_penilaian_parent_id != 0){
                    $skema_parent = $this->model_rev->get_data_skema_penilaian_detail_one_level($this->session->userdata('var')['pendidikan_id'], $this->session->userdata('var')['bagan_pendidikan_id'], $skema_penilaian_parent_id, $peserta_row->id);
                    $this->add_edit_nilai_skema_by_sp($peserta_row->id, $skema_penilaian_parent_id, $skema_parent->nilai_non_akademis, $skema_parent->nilai_akademis);
                }
            }
        }

        if ($this->db->trans_status() != FALSE) {
            $this->db->trans_commit();
            $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/pendidikan_jadwal_detail_form/'.$this->session->userdata('jadwal')['jadwal_id']));
        }else{
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Gagal menyimpan data'));
        }
    }

    function pendidikan_jadwal_detail_add_2() { //form 2
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $pendidikan_peserta_id = $this->input->post('pendidikan_peserta_id');
        $elemen_penilaian_id = $this->input->post('elemen_penilaian_id');

        //insert judul
        $check_exist = $this->model_basic->select_where_array($this->tbl_judul_produk, ['pendidikan_peserta_id'=>$pendidikan_peserta_id,'elemen_penilaian_id'=>$elemen_penilaian_id]);
        if($check_exist->num_rows() > 0){
            $update_data = array(
                'judul' => $this->input->post('judul'),
                'date_modified' => date('Y-m-d H:i:s', now())
            );
            $do_update_judul = $this->model_basic->update($this->tbl_judul_produk, $update_data, 'id', $check_exist->row()->id);
        }else{
            $insert_data = array(
                'pendidikan_peserta_id' => $pendidikan_peserta_id,
                'elemen_penilaian_id' => $elemen_penilaian_id,
                'judul' => $this->input->post('judul'),
                'date_created' => date('Y-m-d H:i:s', now()),
                'date_modified' => '0000-00-00 00:00:00'
            );
            $do_insert_judul = $this->model_basic->insert_all($this->tbl_judul_produk, $insert_data);
        }

        $bidang_studi_status = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id'])->row()->status_asing;

        // insert
        $this->db->trans_begin();
        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_title_by_epid($elemen_penilaian_id);
        // $peserta = $this->model_hasil_bagan_detail->get_peserta_from_pendidikan_peserta($bidang_studi_status)->result();
        // $peserta = array()
        // foreach ($peserta as $peserta_row) {
            $nna = 0;
            $npa = 0;
            foreach($get_elemen_penilaian->result() as $elemen_row){
                $nilai = $this->add_edit_nilai($pendidikan_peserta_id, $elemen_row->elemen_penilaian_id, $elemen_row->tipe_akademis);
                if($elemen_row->tipe_bobot == 0)
                    $pembagi = 100;
                else
                    $pembagi = 10;
                $nna = $nna + $nilai->nna * $elemen_row->bobot_nna / $pembagi;
                $npa = $npa + $nilai->npa * $elemen_row->bobot_npa / $pembagi;
            }
            $this->add_edit_nilai_parent($pendidikan_peserta_id, $elemen_penilaian_id, $nna, $npa);
        // }

        if ($this->db->trans_status() != FALSE) {
            $this->db->trans_commit();
            $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/pendidikan_jadwal_detail_form/'.$this->session->userdata('jadwal')['jadwal_id']));
        }else{
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Gagal menyimpan data'));
        }
    }

    function pendidikan_jadwal_detail_add_3() { //form 3
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Jadwal', 'pendidikan_jadwal');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $pendidikan_peserta_id = $this->input->post('pendidikan_peserta_id');
        $elemen_penilaian_id = $this->input->post('elemen_penilaian_id');

        //insert judul
        $insert_data = array(
            'pendidikan_peserta_id' => $pendidikan_peserta_id,
            'elemen_penilaian_id' => $elemen_penilaian_id,
            'judul' => $this->input->post('judul'),
            'date_created' => date('Y-m-d H:i:s', now()),
            'date_modified' => '0000-00-00 00:00:00'
        );
        // $do_insert_judul = $this->model_basic->insert_all($this->tbl_judul_produk, $insert_data);

        $bidang_studi_status = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id'])->row()->status_asing;
        // insert
        $this->db->trans_begin();
        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_title_by_epid($elemen_penilaian_id);
        // print_r($get_elemen_penilaian->result());
        // die;
        // $peserta = $this->model_hasil_bagan_detail->get_peserta_from_pendidikan_peserta($bidang_studi_status)->result();
        // $peserta = array()
        // foreach ($peserta as $peserta_row) {
            $nna = 0;
            $npa = 0;
            foreach($get_elemen_penilaian->result() as $elemen_row){
                $get_elemen_penilaian2 = $this->model_hasil_bagan_detail->get_title_by_epid($elemen_row->elemen_penilaian_id);
                foreach($get_elemen_penilaian2->result() as $elemen_row2){
                    $nilai = $this->add_edit_nilai($pendidikan_peserta_id, $elemen_row2->elemen_penilaian_id, $elemen_row2->tipe_akademis);
                    if($elemen_row2->tipe_bobot == 0)
                        $pembagi = 100;
                    else
                        $pembagi = 10;
                    $nna = $nna + $nilai->nna * $elemen_row2->bobot_nna / $pembagi;
                    $npa = $npa + $nilai->npa * $elemen_row2->bobot_npa / $pembagi;
                }
            $this->add_edit_nilai_parent($pendidikan_peserta_id, $elemen_row->elemen_penilaian_id, $nna, $npa);
            }
        // }

        if ($this->db->trans_status() != FALSE) {
            $this->db->trans_commit();
            $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/pendidikan_jadwal_detail_form_2?pendidikan_peserta_id='.$pendidikan_peserta_id.'&elemen_penilaian_id='.$elemen_penilaian_id));
        }else{
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Gagal menyimpan data'));
        }
    }

    function add_edit_nilai($peserta_id, $elemen_penilaian_id, $tipe_akademis){
        $taskap_full = 0;
        $check_taskap = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id)->row();
        if($check_taskap->is_taskap == 1){
            $check = $this->model_basic->select_where_array($this->tbl_nilai_peserta, 
            array(
                'pendidikan_id' => $this->session->userdata('var')['pendidikan_id'],
                'penilai_id' => $this->session_admin['penilai_id'],
                'pendidikan_peserta_id' => $peserta_id,
                'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id'],
                'elemen_penilaian_id' => $elemen_penilaian_id
            ));
            if($check->num_rows() >=3){
                $taskap_full = 1;
            }
            if($check->num_rows() != 0 && $taskap_full == 1){
                //update data
                if($tipe_akademis == 0){
                    $update['nilai'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                    $update['nna'] = 0;
                }else{
                    $update['nilai'] = 0;
                    $update['nna'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                }
                $update['penilai_id'] = $this->session_admin['penilai_id'];
                $update['date_modified'] = date('Y-m-d H:i:s', now());

                $do_update = $this->model_basic->update($this->tbl_nilai_peserta, $update, 'id', $check->row()->id);

                $nilai = new stdClass;
                $nilai->nna = $update['nna'];
                $nilai->npa = $update['nilai'];
            }else{
                //insert data
                $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
                $insert['pendidikan_peserta_id'] = $peserta_id;
                $insert['bidang_studi_id'] = $this->session->userdata('jadwal')['bidang_studi_id'];
                $insert['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];
                $insert['elemen_penilaian_id'] = $elemen_penilaian_id;
                $insert['tim_id'] =  $this->session->userdata('jadwal')['tim_id'];
                if($tipe_akademis == 0){
                    $insert['nilai'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                    $insert['nna'] = 0;
                }else{
                    $insert['nilai'] = 0;
                    $insert['nna'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                }
                $insert['date_created'] = date('Y-m-d H:i:s', now());
                $insert['date_modified'] = '0000-00-00 00:00:00';
                $insert['penilai_id'] = $this->session_admin['penilai_id'];

                $do_insert = $this->model_basic->insert_all($this->tbl_nilai_peserta, $insert);
            
                $nilai = new stdClass;
                $nilai->nna = $insert['nna'];
                $nilai->npa = $insert['nilai'];
            }
        }else{
            $check = $this->model_basic->select_where_array($this->tbl_nilai_peserta, 
                array(
                    'pendidikan_id' => $this->session->userdata('var')['pendidikan_id'],
                    'pendidikan_peserta_id' => $peserta_id,
                    'bidang_studi_id' => $this->session->userdata('jadwal')['bidang_studi_id'],
                    'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id'],
                    'elemen_penilaian_id' => $elemen_penilaian_id
            ));
            if($check->num_rows() != 0){
                //update data
                if($tipe_akademis == 0){
                    $update['nilai'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                    $update['nna'] = 0;
                }else{
                    $update['nilai'] = 0;
                    $update['nna'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                }
                $update['penilai_id'] = $this->session_admin['penilai_id'];
                $update['date_modified'] = date('Y-m-d H:i:s', now());

                $do_update = $this->model_basic->update($this->tbl_nilai_peserta, $update, 'id', $check->row()->id);

                $nilai = new stdClass;
                $nilai->nna = $update['nna'];
                $nilai->npa = $update['nilai'];
            }else{
                //insert data
                $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
                $insert['pendidikan_peserta_id'] = $peserta_id;
                $insert['bidang_studi_id'] = $this->session->userdata('jadwal')['bidang_studi_id'];
                $insert['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];
                $insert['elemen_penilaian_id'] = $elemen_penilaian_id;
                $insert['tim_id'] =  $this->session->userdata('jadwal')['tim_id'];
                if($tipe_akademis == 0){
                    $insert['nilai'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                    $insert['nna'] = 0;
                }else{
                    $insert['nilai'] = 0;
                    $insert['nna'] = $this->input->post('nilai_'.$peserta_id.'_'.$elemen_penilaian_id);
                }
                $insert['date_created'] = date('Y-m-d H:i:s', now());
                $insert['date_modified'] = '0000-00-00 00:00:00';
                $insert['penilai_id'] = $this->session_admin['penilai_id'];

                $do_insert = $this->model_basic->insert_all($this->tbl_nilai_peserta, $insert);
            
                $nilai = new stdClass;
                $nilai->nna = $insert['nna'];
                $nilai->npa = $insert['nilai'];
            }
        }
        return $nilai;
    }

    function add_edit_nilai_parent($peserta_id, $elemen_penilaian_id, $nna, $npa){
        $taskap_full = 0;
        $check_taskap = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id)->row();
        if($check_taskap->is_taskap == 1){
            $check = $this->model_basic->select_where_array($this->tbl_nilai_peserta, 
            array(
                'pendidikan_id' => $this->session->userdata('var')['pendidikan_id'],
                'penilai_id' => $this->session_admin['penilai_id'],
                'pendidikan_peserta_id' => $peserta_id,
                'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id'],
                'elemen_penilaian_id' => $elemen_penilaian_id
            ));
            if($check->num_rows() >=3){
                $taskap_full = 1;
            }
            if($check->num_rows() != 0 && $taskap_full == 1){
                //update data
                $update['nilai'] = $npa;
                $update['nna'] = $nna;
                $update['penilai_id'] = $this->session_admin['penilai_id'];
                $update['date_modified'] = date('Y-m-d H:i:s', now());

                $do_update = $this->model_basic->update($this->tbl_nilai_peserta, $update, 'id', $check->row()->id);
            }else{
                //insert data
                $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
                $insert['pendidikan_peserta_id'] = $peserta_id;
                $insert['bidang_studi_id'] = $this->session->userdata('jadwal')['bidang_studi_id'];
                $insert['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];
                $insert['elemen_penilaian_id'] = $elemen_penilaian_id;
                $insert['tim_id'] =  $this->session->userdata('jadwal')['tim_id'];
                $insert['nilai'] = $npa;
                $insert['nna'] = $nna;
                $insert['date_created'] = date('Y-m-d H:i:s', now());
                $insert['date_modified'] = date('Y-m-d H:i:s', now());
                $insert['penilai_id'] = $this->session_admin['penilai_id'];

                $do_insert = $this->model_basic->insert_all($this->tbl_nilai_peserta, $insert);
            }
        }else{
            $check = $this->model_basic->select_where_array($this->tbl_nilai_peserta, 
            array(
                'pendidikan_id' => $this->session->userdata('var')['pendidikan_id'],
                'pendidikan_peserta_id' => $peserta_id,
                'bidang_studi_id' => $this->session->userdata('jadwal')['bidang_studi_id'],
                'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id'],
                'elemen_penilaian_id' => $elemen_penilaian_id
            ));
            if($check->num_rows() != 0){
                //update data
                $update['nilai'] = $npa;
                $update['nna'] = $nna;
                $update['penilai_id'] = $this->session_admin['penilai_id'];
                $update['date_modified'] = date('Y-m-d H:i:s', now());

                $do_update = $this->model_basic->update($this->tbl_nilai_peserta, $update, 'id', $check->row()->id);
            }else{
                //insert data
                $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
                $insert['pendidikan_peserta_id'] = $peserta_id;
                $insert['bidang_studi_id'] = $this->session->userdata('jadwal')['bidang_studi_id'];
                $insert['bagan_pendidikan_id'] = $this->session->userdata('var')['bagan_pendidikan_id'];
                $insert['elemen_penilaian_id'] = $elemen_penilaian_id;
                $insert['tim_id'] =  $this->session->userdata('jadwal')['tim_id'];
                $insert['nilai'] = $npa;
                $insert['nna'] = $nna;
                $insert['date_created'] = date('Y-m-d H:i:s', now());
                $insert['date_modified'] = date('Y-m-d H:i:s', now());
                $insert['penilai_id'] = $this->session_admin['penilai_id'];

                $do_insert = $this->model_basic->insert_all($this->tbl_nilai_peserta, $insert);
            }
        }
    }

    function add_edit_nilai_skema_by_ep($peserta_id, $elemen_penilaian_id, $nna, $npa){
        $skema_penilaian = $this->model_basic->select_where($this->tbl_skema_penilaian, 'elemen_penilaian_id', $elemen_penilaian_id)->row();
        $skema_penilaian_id = $skema_penilaian->id;
        $check = $this->model_basic->select_where_array($this->tbl_nilai_akhir_peserta, 
            array(
                'pendidikan_id'=>$this->session->userdata('var')['pendidikan_id'],
                'pendidikan_peserta_id' => $peserta_id,
                'skema_penilaian_id' => $skema_penilaian_id
        ));
        $this->db->trans_begin();
        if($check->num_rows() != 0){
            //update data
            $update['npa'] = $npa;
            $update['nna'] = $nna;
            $update['date_modified'] = date('Y-m-d H:i:s', now());

            $do_update = $this->model_basic->update($this->tbl_nilai_akhir_peserta, $update, 'id', $check->row()->id);
        }else{
            //insert data
            $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
            $insert['pendidikan_peserta_id'] = $peserta_id;
            $insert['skema_penilaian_id'] = $skema_penilaian_id;
            $insert['npa'] = $npa;
            $insert['nna'] = $nna;
            $insert['date_created'] = date('Y-m-d H:i:s', now());
            $insert['date_modified'] = date('Y-m-d H:i:s', now());

            $do_insert = $this->model_basic->insert_all($this->tbl_nilai_akhir_peserta, $insert);
        }

        $skema_penilaian_parent_id = $skema_penilaian->parent_id;
        if ($this->db->trans_status() != FALSE) {
            $this->db->trans_commit();
            return $skema_penilaian_parent_id;
        }else{
            $this->db->trans_rollback();
        }
    }

    function add_edit_nilai_skema_by_sp($peserta_id, $skema_penilaian_id, $nna, $npa){
        $check = $this->model_basic->select_where_array($this->tbl_nilai_akhir_peserta, 
            array(
                'pendidikan_id'=>$this->session->userdata('var')['pendidikan_id'],
                'pendidikan_peserta_id' => $peserta_id,
                'skema_penilaian_id' => $skema_penilaian_id
        ));
        $this->db->trans_begin();
        if($check->num_rows() != 0){
            //update data
            $update['npa'] = $npa;
            $update['nna'] = $nna;
            $update['date_modified'] = date('Y-m-d H:i:s', now());

            $do_update = $this->model_basic->update($this->tbl_nilai_akhir_peserta, $update, 'id', $check->row()->id);
        }else{
            //insert data
            $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
            $insert['pendidikan_peserta_id'] = $peserta_id;
            $insert['skema_penilaian_id'] = $skema_penilaian_id;
            $insert['npa'] = $npa;
            $insert['nna'] = $nna;
            $insert['date_created'] = date('Y-m-d H:i:s', now());
            $insert['date_modified'] = date('Y-m-d H:i:s', now());

            $do_insert = $this->model_basic->insert_all($this->tbl_nilai_akhir_peserta, $insert);
        }

        if ($this->db->trans_status() != FALSE) {
            $this->db->trans_commit();
        }else{
            $this->db->trans_rollback();
        }
    }

    function print_form($id){

        $jadwal_detail_id = $this->model_pendidikan_jadwal_list->get_jadwal_detail_id($id);

        //add var to session
        $var = array(
        'pendidikan_id'         => $jadwal_detail_id->pendidikan_id,
        'bagan_pendidikan_id'   => $jadwal_detail_id->bagan_pendidikan_id,
        'elemen_penilaian_id'   => $jadwal_detail_id->elemen_penilaian_id,
        );
        $this->session->set_userdata('var', $var);

        //add var to session
        $jadwal = array(
        'jadwal_id'         => $jadwal_detail_id->jadwal_id,
        'penilai_id'        => $jadwal_detail_id->penilai_id,
        'tim_id'            => $jadwal_detail_id->tim_id,
        'bidang_studi_id'   => $jadwal_detail_id->bidang_studi_id
        );
        $this->session->set_userdata('jadwal', $jadwal);

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->session->userdata('var')['elemen_penilaian_id']);
        if($elemen_penilaian->num_rows() == 1)
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
        else
            $data['elemen_penilaian'] = 'Unknown';

        $get_elemen_penilaian_nna = $this->model_hasil_bagan_detail->get_title_non_akademis();
        $get_elemen_penilaian_npa = $this->model_hasil_bagan_detail->get_title_akademis();
        $npa = $get_elemen_penilaian_npa->result();
        foreach ($npa as $data_row) {
            $check_child = $this->model_hasil_bagan_detail->bagan_detail_child($data_row->elemen_penilaian_id);
            if($check_child->num_rows() == 0){
                $data_row->child = null;
            }else{
                $data_row->child = $check_child->result();
            }
        }
        $data['title_nna'] = $get_elemen_penilaian_nna->result();
        $data['title_npa'] = $npa;
        
        $bidang_studi_status = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id'])->row()->status_asing;
        // get peserta list
        $peserta = $this->model_hasil_bagan_detail->get_peserta_from_pendidikan_peserta($bidang_studi_status)->result();
        $peserta_pindahan = $this->model_hasil_bagan_detail->get_peserta_pindahan($this->session->userdata('jadwal')['jadwal_id']);
        if($peserta_pindahan->num_rows() > 0){
            foreach ($peserta_pindahan->result() as $ppindahan_row) {
                array_push($peserta, $ppindahan_row);
            }
        }
        // echo '<pre>';
        // print_r($cek_peserta_pindahan);
        // echo '</pre>';
        // die;
        $index = 0;
        foreach ($peserta as $peserta_row) {
            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            //get data non akademis
            foreach($get_elemen_penilaian_nna->result() as $elemen_row){
                
                $peserta_row->elemen_penilaian_id_nna[] = $elemen_row->elemen_penilaian_id;
                //if taskap
                if($elemen_row->is_taskap == 1){
                    $nilai_non_akademis = $this->model_hasil_bagan_detail->get_nilai_non_akademis_taskap($peserta_row->id, $elemen_row->elemen_penilaian_id, $this->session_admin['penilai_id']);
                }else{
                    $nilai_non_akademis = $this->model_hasil_bagan_detail->get_nilai_non_akademis($peserta_row->id, $elemen_row->elemen_penilaian_id);
                }
                if($nilai_non_akademis->num_rows() == 0){
                        $peserta_row->nilai_non_akademis[] = 0;
                        $peserta_row->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + 0;
                }else{
                        $peserta_row->nilai_non_akademis[] = $nilai_non_akademis->row()->nna;
                        $peserta_row->keterangan[] = $nilai_non_akademis->row()->keterangan;
                        $peserta_row->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + $nilai_non_akademis->row()->nilai_bobot;
                        $nilai_akhir_non_akademis = $peserta_row->nilai_akhir_non_akademis;
                }
            }
            //get data akademis
            foreach($get_elemen_penilaian_npa->result() as $elemen_row){
                
                $peserta_row->elemen_penilaian_id_npa[] = $elemen_row->elemen_penilaian_id;

                $nilai_akademis = $this->model_hasil_bagan_detail->get_nilai_akademis($peserta_row->id, $elemen_row->elemen_penilaian_id);
                if($nilai_akademis->num_rows() == 0){
                        $peserta_row->nilai_akademis[] = 0;
                        $peserta_row->nilai_akhir_akademis = $nilai_akhir_akademis + 0;
                }else{
                        $peserta_row->nilai_akademis[] = $nilai_akademis->row()->nilai;
                        $peserta_row->keterangan[] = $nilai_akademis->row()->keterangan;
                        $peserta_row->nilai_akhir_akademis = $nilai_akhir_akademis + $nilai_akademis->row()->nilai_bobot;
                        $nilai_akhir_akademis = $peserta_row->nilai_akhir_akademis;
                }
                if(isset($elemen_row->child)){
                    $peserta_row->elemen_child[] = 1;
                }else{
                    $peserta_row->elemen_child[] = 0;
                }
            }

            $cek_peserta_pindahan = $this->model_hasil_bagan_detail->cek_peserta_pindahan($peserta_row->id, $this->session->userdata('jadwal')['jadwal_id']);
            if($cek_peserta_pindahan == TRUE){
                $hapus_peserta_index[] = $index;
            }
        }
        //hapus peserta pindah jadwal
        if(!empty($hapus_peserta_index)){
            foreach ($hapus_peserta_index as $index_hapus) {
                unset($peserta[$index_hapus]);
            }
        }

        $data['list'] = $peserta;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);

        //is_active / expired
        if(strtotime($jadwal->row()->tanggal.'+2 days') < strtotime(date('Y-m-d', now()))){
            $do_update_jadwal_is_active = $this->model_basic->update($this->tbl_jadwal, ['is_active' => 0], 'id', $jadwal->row()->id);
            //cek manual activation
            if($jadwal->row()->manual_activation == 1)
                $data['is_active'] = 1;
            else
                $data['is_active'] = 0;
        }else{
            $data['is_active'] = 1;
        }

        //get header
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $this->tanggal_indo($jadwal->row()->tanggal);
        else
            $data['jadwal'] = 'Unknown';        

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $this->session->userdata('jadwal')['tim_id']);
        if($tim->num_rows() == 1)
            $data['tim'] = $tim->row()->name;
        else
            $data['tim'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';

        $keterangan = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $this->session->userdata('var')['elemen_penilaian_id'], 'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id']]);
        if($keterangan->num_rows() == 1)
            $data['keterangan'] = $keterangan->row()->keterangan_form;
        else
            $data['keterangan'] = '-';

        $data['tanggal_cetak'] = $this->tanggal_indo(date('Y-m-d'));


        $html=$this->load->view('backend/print_document/form', $data, true);

        ob_start();
        $pdfFilePath = "Form ".$data['elemen_penilaian'].".pdf";
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        // print_r($html); exit;
    }

    function print_form2(){

        $parent_elemen_penilaian_id = $this->session->userdata('var')['elemen_penilaian_id'];
        $elemen_penilaian_id = $this->input->get('elemen_penilaian_id');
        $bagan_pendidikan_id = $this->input->get('bagan_pendidikan_id');
        $pendidikan_peserta_id = $this->input->get('pendidikan_peserta_id');

        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_data_elemen_penilaian_detail($bagan_pendidikan_id, $elemen_penilaian_id, $pendidikan_peserta_id);

        $data['list'] = $get_elemen_penilaian;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);

        //get header
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $this->tanggal_indo($jadwal->row()->tanggal);
        else
            $data['jadwal'] = 'Unknown';        

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id);
        if($elemen_penilaian->num_rows() == 1){
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
            $data['elemen_penilaian_id'] = $elemen_penilaian->row()->id;

            $elemen_penilaian_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->row()->parent_id);
            $data['elemen_penilaian_parent'] = $elemen_penilaian_parent->row()->name;
        }else
            $data['elemen_penilaian'] = 'Unknown';

        $pendidikan_peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $pendidikan_peserta_id);
        if($pendidikan_peserta->num_rows() == 1)
            $data['pendidikan_peserta'] = $pendidikan_peserta->row();
        else
            $data['pendidikan_peserta'] = 'Unknown';

        $judul_produk = $this->model_basic->select_where_array($this->tbl_judul_produk, ['elemen_penilaian_id'=>$elemen_penilaian_id, 'pendidikan_peserta_id'=>$pendidikan_peserta_id]);
        if($judul_produk->num_rows() > 1)
            $data['judul_produk'] = $judul_produk->row()->judul;
        else
            $data['judul_produk'] = '';

        $keterangan = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $this->session->userdata('var')['elemen_penilaian_id'], 'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id']]);
        if($keterangan->num_rows() == 1)
            $data['keterangan'] = $keterangan->row()->keterangan_form;
        else
            $data['keterangan'] = '-';

        $data['tanggal_cetak'] = $this->tanggal_indo(date('Y-m-d'));

        // echo '<pre>';
        // echo print_r($data);
        // echo '</pre>';
        // die;
        $html=$this->load->view('backend/print_document/form2', $data, true);

        ob_start();
        $pdfFilePath = "Form ".$data['elemen_penilaian'].".pdf";
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        // print_r($html); exit;
    }

    function print_form3(){

        $parent_elemen_penilaian_id = $this->session->userdata('var')['elemen_penilaian_id'];
        $elemen_penilaian_id = $this->input->get('elemen_penilaian_id');
        $bagan_pendidikan_id = $this->input->get('bagan_pendidikan_id');
        $pendidikan_peserta_id = $this->input->get('pendidikan_peserta_id');

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id);
        if($elemen_penilaian->num_rows() == 1){
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
            $data['elemen_penilaian_id'] = $elemen_penilaian->row()->id;

            $elemen_penilaian_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->row()->parent_id);
            $data['elemen_penilaian_parent'] = $elemen_penilaian_parent->row()->name;
        }else
            $data['elemen_penilaian'] = 'Unknown';

        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_data_elemen_penilaian_detail($this->session->userdata('var')['bagan_pendidikan_id'], $elemen_penilaian_id, $pendidikan_peserta_id);

        $data['list'] = $get_elemen_penilaian;

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);

        //is_active / expired
        if(strtotime($jadwal->row()->tanggal.'+2 days') < strtotime(date('Y-m-d', now()))){
            $do_update_jadwal_is_active = $this->model_basic->update($this->tbl_jadwal, ['is_active' => 0], 'id', $jadwal->row()->id);
            //cek manual activation
            if($jadwal->row()->manual_activation == 1)
                $data['is_active'] = 1;
            else
                $data['is_active'] = 0;
        }else{
            $data['is_active'] = 1;
        }

        //get header
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $this->tanggal_indo($jadwal->row()->tanggal);
        else
            $data['jadwal'] = 'Unknown';        

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id);
        if($elemen_penilaian->num_rows() == 1){
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
            $data['elemen_penilaian_id'] = $elemen_penilaian->row()->id;

            $elemen_penilaian_parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian->row()->parent_id);
            $data['elemen_penilaian_parent'] = $elemen_penilaian_parent->row()->name;
        }else
            $data['elemen_penilaian'] = 'Unknown';

        $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $this->session->userdata('jadwal')['tim_id']);
        if($tim->num_rows() == 1)
            $data['tim'] = $tim->row()->name;
        else
            $data['tim'] = 'Unknown';

        $pendidikan_peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $pendidikan_peserta_id);
        if($pendidikan_peserta->num_rows() == 1)
            $data['pendidikan_peserta'] = $pendidikan_peserta->row();
        else
            $data['pendidikan_peserta'] = 'Unknown';

        $keterangan = $this->model_basic->select_where_array($this->tbl_bagan_detail, ['elemen_penilaian_id' => $this->session->userdata('var')['elemen_penilaian_id'], 'bagan_pendidikan_id' => $this->session->userdata('var')['bagan_pendidikan_id']]);
        if($keterangan->num_rows() == 1)
            $data['keterangan'] = $keterangan->row()->keterangan_form;
        else
            $data['keterangan'] = '-';

        $data['tanggal_cetak'] = $this->tanggal_indo(date('Y-m-d'));

        // echo '<pre>';
        // echo print_r($data);
        // echo '</pre>';
        // die;
        $html=$this->load->view('backend/print_document/form3', $data, true);

        ob_start();
        $pdfFilePath = "Form ".$data['elemen_penilaian'].".pdf";
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        // print_r($html); exit;
    }

    function fill_nilai_ep_by_ep_bs($elemen_penilaian_id,$pendidikan_peserta_id,$bidang_studi_id){

        $pendidikan_id = $this->session->userdata('var')['pendidikan_id'];
        $bagan_pendidikan_id = $this->session->userdata('var')['bagan_pendidikan_id'];
        $jadwal_id = $this->session->userdata('jadwal')['jadwal_id'];
        $tim_id = $this->session->userdata('jadwal')['tim_id'];
        $bidang_studi_id = $this->session->userdata('jadwal')['bidang_studi_id'];

        // $child = $this->model_rev->get_data_elemen_penilaian_detail_by_bs($pendidikan_id,$bagan_pendidikan_id,$elemen_penilaian_id,$pendidikan_peserta_id,$bidang_studi_id);

        // //input_nilai
        // $nna = $child->nilai_akademis;
        // $npa = $child->nilai_non_akademis;
        // $do_input = $this->add_edit_nilai_single($pendidikan_id,$bagan_pendidikan_id,$bidang_studi_id,$jadwal_id,$tim_id,$pendidikan_peserta_id, $elemen_penilaian_id, $nna, $npa);
        $nilai_all_bs = $this->model_hasil_bagan_detail->get_hasil_nilai_one_bs($pendidikan_peserta_id,$elemen_penilaian_id,$bidang_studi_id);

        $nilai = new stdClass;
        $nilai_akhir_akademis = 0;
        $nilai_akhir_non_akademis = 0;

        if($nilai_all_bs->num_rows() == 0){
            $nilai->nilai_non_akademis[] = 0;
            $nilai->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + 0;

            $nilai->nilai_akademis[] = 0;
            $nilai->nilai_akhir_akademis = $nilai_akhir_akademis + 0;
        }else{
            $nilai->nilai_non_akademis[] = $nilai_all_bs->row()->nna;
            $nilai->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + ($nilai_all_bs->row()->bobot_nna*$nilai_all_bs->row()->nna/100);
            $nilai_akhir_non_akademis = $nilai->nilai_akhir_non_akademis;

            $nilai->nilai_akademis[] = $nilai_all_bs->row()->npa;
            $nilai->nilai_akhir_akademis = $nilai_akhir_akademis + ($nilai_all_bs->row()->bobot_npa*$nilai_all_bs->row()->npa/100);
            $nilai_akhir_akademis = $nilai->nilai_akhir_akademis;
        }

        echo "<pre>";
        print_r($nilai);
        echo "</pre>";
        die;
    }

    // function fill_nilai_ep_and_parent_by_ep($pendidikan_id=1,$bagan_pendidikan_id=1,$elemen_penilaian_id=107,$pendidikan_peserta_id=1){
    function fill_nilai_ep_and_parent_by_ep($elemen_penilaian_id,$pendidikan_peserta_id){

        $pendidikan_id = $this->session->userdata('var')['pendidikan_id'];
        $bagan_pendidikan_id = $this->session->userdata('var')['bagan_pendidikan_id'];
        $jadwal_id = $this->session->userdata('jadwal')['jadwal_id'];
        $tim_id = $this->session->userdata('jadwal')['tim_id'];
        $bidang_studi_id = $this->session->userdata('jadwal')['bidang_studi_id'];

        $child = $this->model_rev->get_data_elemen_penilaian_detail($pendidikan_id,$bagan_pendidikan_id,$elemen_penilaian_id,$pendidikan_peserta_id);

        echo '<pre>';
        print_r($child);
        echo '</pre>';

        //input_nilai
        $nna = $child->nilai_akademis;
        $npa = $child->nilai_non_akademis;
        $do_input = $this->add_edit_nilai_single($pendidikan_id,$bagan_pendidikan_id,$bidang_studi_id,$jadwal_id,$tim_id,$pendidikan_peserta_id, $elemen_penilaian_id, $nna, $npa);

        //parent
        $parent_id = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $elemen_penilaian_id)->row()->parent_id;
        $parent = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $parent_id)->row();
        if($parent->is_show == 1){
            $parent_data = $this->model_rev->get_data_elemen_penilaian_detail_one_level($pendidikan_id,$bagan_pendidikan_id,$parent->id,$pendidikan_peserta_id);  
        }
    }

    function add_edit_nilai_single($pendidikan_id,$bagan_pendidikan_id,$bidang_studi_id,$jadwal_id,$tim_id,$peserta_id, $elemen_penilaian_id, $nna, $npa){
        $check = $this->model_basic->select_where_array($this->tbl_nilai_peserta, 
            array(
                'pendidikan_id' => $pendidikan_id,
                'pendidikan_peserta_id' => $peserta_id,
                'bidang_studi_id' => $bidang_studi_id,
                'bagan_pendidikan_id' => $bagan_pendidikan_id,
                'elemen_penilaian_id' => $elemen_penilaian_id
        ));
        if($check->num_rows() != 0){
            //update data
            $update['nilai'] = $npa;
            $update['nna'] = $nna;
            $update['penilai_id'] = $this->session_admin['penilai_id'];
            $update['date_modified'] = date('Y-m-d H:i:s', now());

            $do_update = $this->model_basic->update($this->tbl_nilai_peserta, $update, 'id', $check->row()->id);
        }else{
            //insert data
            $insert['pendidikan_id'] = $pendidikan_id;
            $insert['pendidikan_peserta_id'] = $peserta_id;
            $insert['bidang_studi_id'] = $bidang_studi_id;
            $insert['bagan_pendidikan_id'] = $bagan_pendidikan_id;
            $insert['elemen_penilaian_id'] = $elemen_penilaian_id;
            $insert['tim_id'] =  $tim_id;
            $insert['nilai'] = $npa;
            $insert['nna'] = $nna;
            $insert['date_created'] = date('Y-m-d H:i:s', now());
            $insert['date_modified'] = date('Y-m-d H:i:s', now());
            $insert['penilai_id'] = $this->session_admin['penilai_id'];

            $do_insert = $this->model_basic->insert_all($this->tbl_nilai_peserta, $insert);
        }
    }

}