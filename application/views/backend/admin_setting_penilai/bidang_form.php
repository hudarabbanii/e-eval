<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active">Form <?php echo $function_name; ?></li>
</ul>

<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>

<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Bidang</h3>                              
                </div>
                <form class="form-horizontal" action="<?php if ($data) echo $controller . '/' . $function_edit; else echo $controller . '/' . $function_add; ?>" method="POST" id="px-setting_penilai-bidang-form">
                    <input type="hidden" name="id" id="px-setting_penilai-bidang-form-id" value="<?php if ($data) echo $data->id; ?>">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-setting_penilai-bidang-form-name">Nama Bidang</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="name" id="px-setting_penilai-bidang-form-name" value="<?php if ($data) echo $data->name; ?>" placeholder="Nama Bidang">
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">Simpan</button>
                        </div>
                    </div>
                </form>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>

<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/select2.js"></script>
<link href="assets/backend_assets/css/select2.css" rel="stylesheet">
<style type="text/css">select{cursor: pointer;}</style>

<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/setting_penilai/bidang_form.js"></script>