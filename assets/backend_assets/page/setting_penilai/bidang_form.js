$(document).ready(function(){
    // $("select.select2").select2();

    $('#px-setting_penilai-bidang-form').validate({
        rules: {
            id: {
                required: true
            },
            name: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-setting_penilai-bidang-form .alert-warning').removeClass('hidden');
            $('#px-setting_penilai-bidang-form .alert-success').addClass('hidden');
            $('#px-setting_penilai-bidang-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-setting_penilai-bidang-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-setting_penilai-bidang-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-setting_penilai-bidang-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
})
