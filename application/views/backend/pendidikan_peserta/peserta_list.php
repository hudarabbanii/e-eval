<style type="text/css">
  .panel-body {
      padding: 0;
  }
</style>
<!--START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Peserta</h3>
                    <a class="btn btn-success pull-right btn-add" href="<?php echo $controller . '/' . $function_form; ?>"><i class="fa fa-plus"></i> Tambah</a>                
                      <input type="hidden" id="table" name="table" value="peserta">
                      <input type="hidden" id="select" name="select" value="gelar_depan,gelar_belakang,nama,NIP,jabatan,pangkat,instansi_negara">
                      <input type="hidden" id="where" name="where" value="angkatan='7'">
                      <input type="hidden" id="limit" name="limit" value="1000">
                      <button class="btn-api btn btn-warning pull-right" <?php echo (count($peserta) > 0 ? 'disabled' : ''); ?>><i class="fa fa-refresh"></i> Ambil dari E-PESERTA</button>
                      <button type="button" class="btn btn-info pull-right btn-peserta-order" data-toggle="modal" data-target="#modal_orders" onclick="$('.panel-dragable .list-group').sortable('refresh');"><i class="fa fa-exchange"></i> Ubah Urutan</button>
                </div>
                <div class="panel-body" id="px-pendidikan_peserta">
                    <div class="alert alert-success hidden"><strong>Berhasil! </strong><span></span></div>
                    <div class="alert alert-warning hidden"><strong>Sedang diproses.. </strong><span>Mohon Tunggu...</span></div>
                    <div class="alert alert-danger hidden"><strong>Gagal! </strong><span></span></div>
                    <table class="table datatable table-bordered">
                        <thead>
                          <tr>
                            <th class="text-center">No</th>
                            <th width="30%" class="text-center">Nama Legkap</th>
                            <!-- <th class="text-center">NIP</th> -->
                            <th class="text-center">Jabatan</th>
                            <!-- <th class="text-center">Instansi</th> -->
                            <th width="30%" class="text-center">Tutor</th>
                            <th class="text-center">Peserta Asing</th>
                            <th width="8%" class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $no=1;
                          foreach ($peserta as $data_row) { ?>
                          <tr>
                            <td class="text-center"><?php echo $no; ?></td>
                            <td class="text-center"><?php echo $data_row->gelar_depan.' '.$data_row->nama_lengkap.($data_row->gelar_belakang!="" ? ', '.$data_row->gelar_belakang : ''); ?></td>
                            <!-- <td class="text-center"><?php echo $data_row->nip; ?></td> -->
                            <td class="text-center"><?php echo $data_row->jabatan; ?></td>
                            <!-- <td class="text-center"><?php echo $data_row->instansi_negara; ?></td> -->
                            <td class="text-center"><?php echo $data_row->tutor_name; ?></td>
                            <td class="text-center">
                              <?php if($data_row->status == 1){ ?>
                              <button id="btn-status" class="btn btn-xs btn-success" data-id="<?php echo $data_row->id ?>" data-status="1"><span class="fa fa-check"></span></button>
                              <?php }else{ ?>
                              <button id="btn-status" class="btn btn-xs btn-warning" data-id="<?php echo $data_row->id ?>" data-status="0"><span class="fa fa-times"></span></button>
                              <?php } ?>
                            </td>
                            <td class="text-center">
                              <form action="pendidikan_peserta/pendidikan_peserta_form" method="post">
                                  <input type="hidden" name="id" value="<?php echo $data_row->id ?>">
                                  <button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Ubah" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                                  <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Hapus" data-placement="top" data-toggle="tooltip" data-target-id="<?php echo $data_row->id ?>"><i class="fa fa-trash-o"></i></button>
                              </form>
                          </td>
                          </tr>
                          <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-pendidikan_peserta-get_api-message-box" class="message-box message-box-primary animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/consume_api_insert_peserta'; ?>" method="post" id="px-pendidikan_peserta-get_api-message-form">
                <input type="hidden" name="table">
                <input type="hidden" name="select">
                <input type="hidden" name="where">
                <input type="hidden" name="limit">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content" style="margin-bottom: 20px">
                    <p>Anda yakin ingin mengambil data dari e-peserta?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Angkatan</label>
                    <div class="col-md-3 col-xs-12">
                        <input type="number" min="1" class="form-control" name="angkatan" required />
                    </div>
                </div>
                <div class="mb-footer">
                    <button class="btn btn-info btn-lg pull-right" type="submit">Ya</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="px-pendidikan_peserta-peserta_list-message-box" class="message-box message-box-warning animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller . '/' . $function_delete; ?>" method="post" id="px-pendidikan_peserta-peserta_list-message-form">
                <input type="hidden" name="id">
                <div class="mb-title"><span class="fa fa-warning"></span> Peringatan</div>
                <div class="mb-content">
                    <p>Anda yakin ingin menghapus data ini?</p>
                    <p class="msg-status"></p>                  
                </div>
                <div class="mb-footer">
                    <button class="btn btn-danger btn-lg pull-right" type="submit">Hapus</button>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- MODAL -->
<div id="modal_orders" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Urutan Peserta</h4>
      </div>
      <div class="modal-body" style="max-height: 400px;overflow-y: scroll;">
        <div class="row">
          <div class="col-md-2">
            <div class="panel-body">
              <ul class="list-group border-bottom">
                <?php $x=1; foreach ($peserta as $p_order) { ?>
                  <li class="list-group-item"><?php echo $x; ?></li>
                  <?php $x++; } ?>                          
                </ul>
              </div>
          </div>
          <div class="col-md-10" id="px-pendidikan_peserta-orders-dragable">
            <div class="panel-body panel-dragable">
              <ul class="list-group border-bottom">
                <?php $x=1; foreach ($peserta as $p_order) { ?>
                  <li class="list-group-item" id="item-<?php echo $p_order->id; ?>"><?php echo $p_order->gelar_depan.' '.$p_order->nama_lengkap.($p_order->gelar_belakang!="" ? ', '.$p_order->gelar_belakang : ''); ?></li>
                  <?php $x++; } ?>                          
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer" id="px-pendidikan_peserta-orders-dragable-footer">
          <div class="alert alert-success hidden"><strong>Berhasil! </strong><span></span></div>
          <div class="alert alert-warning hidden"><strong>Sedang diproses.. </strong><span>Mohon Tunggu...</span></div>
          <div class="alert alert-danger hidden"><strong>Gagal! </strong><span></span></div>
          <button type="button" class="btn btn-success urutan-save pull-right">Simpan</button>
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" onclick="$('.panel-dragable .list-group').sortable('cancel');">Batal</button>
        </div>
      </div>

    </div>
</div>
<!-- END MODAL -->

<style type="text/css">
  .panel-dragable > ul > li{
    cursor: pointer;
  }
</style>

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins/icheck/icheck.min.js"></script> 
<!-- END PAGE PLUGINS -->

<!-- datatables plugin -->
<link rel="stylesheet" type="text/css" href="assets/backend_assets/js/plugins/datatables/datatables.min.css"/>
<script type="text/javascript" src="assets/backend_assets/js/plugins/datatables/datatables.min.js"></script>

<script type="text/javascript" src="assets/backend_assets/page/pendidikan_peserta/pendidikan_peserta_list.js"></script>
<script type="text/javascript" src="assets/backend_assets/page/pendidikan_peserta/pendidikan_peserta_orders.js"></script>
<script type="text/javascript" src="assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="assets/backend_assets/js/actions.js"></script>