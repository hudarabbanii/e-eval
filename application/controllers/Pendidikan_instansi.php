<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_instansi extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_instansi', 'controller_name' => 'Admin Setting Penilai', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Setting Penilai', 'pendidikan_instansi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['submenu'] = $this->get_submenu($data['controller']);

        $data['content'] = $this->load->view('backend/pendidikan_instansi/index',$data,true);
        $this->load->view('backend/index',$data);
    }

    function instansi() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Instansi', 'instansi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['instansi'] = $this->model_basic->select_all($this->tbl_instansi);

        $data['content'] = $this->load->view('backend/pendidikan_instansi/instansi_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function instansi_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Instansi', 'instansi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $id = $this->input->post('id');
        if ($id) {
            $instansi = $this->model_basic->select_where($this->tbl_instansi, 'id', $id)->row();
            $data['data'] = $instansi;
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('backend/pendidikan_instansi/instansi_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function instansi_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Instansi', 'instansi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_instansi);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        if ($this->input->post('name')) {
            $do_insert = $this->model_basic->insert_all($this->tbl_instansi, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Instansi '.$insert['name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data berhasil','redirect' => $data['controller'].'/'.$data['function']));
            } else {
                $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
            }
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Cek kembali form Anda'));
    }

    function instansi_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Instansi', 'instansi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_instansi);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }

        $do_update = $this->model_basic->update($this->tbl_instansi, $update, 'id', $update['id']);

        if ($do_update) {
            $this->save_log_admin(ACT_UPDATE, 'Update Instansi '.$update['name']);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Edit data berhasil', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'error', 'msg' => 'Error'));
        }
    }

    function instansi_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Instansi', 'instansi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_instansi, 'id', $id)->row();
        $do_delete = $this->model_basic->delete($this->tbl_instansi, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Instansi '.$deleted_data->name);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Data berhasil dihapus', 'redirect' => $data['controller'].'/'.$data['function']));
        } else {
            $this->returnJson(array('status' => 'failed', 'msg' => 'Delete failed'));
        }
    }