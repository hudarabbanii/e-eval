<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class pendidikan_hasil extends PX_Controller {

    function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'pendidikan_hasil', 'controller_name' => 'Admin Pendidikan', 'controller_id' => 0);
    }
    
    public function index()
    {            
        $this->hasil();
    }

    function hasil() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil', 'hasil');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $pendidikan_id = $this->session->userdata('menu_pendidikan')['pendidikan_id'];
        $bagan_pendidikan_id = $this->session->userdata('menu_pendidikan')['bagan_pendidikan_id'];
        $pendidikan = $this->model_basic->select_where($this->tbl_pendidikan, 'id', $pendidikan_id);
        if($pendidikan->num_rows() != 0)
            $id = $pendidikan->row()->bagan_pendidikan_id;
        else
            redirect('pendidikan');

        $elemen_penilaian = $this->model_rev->get_data_skema_penilaian($pendidikan_id,$bagan_pendidikan_id,1);
        $data['elemen_penilaian'] = $elemen_penilaian;
        // echo "<pre>";
        // print_r($elemen_penilaian);
        // echo "</pre>";
        // die;

        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function hasil_bagan(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil', 'hasil');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $pendidikan_id = $this->input->get('pendidikan_id');
        $skema_penilaian_id = $this->input->get('skema_penilaian_id');

        $data['skema_penilaian'] = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $skema_penilaian_id)->row();
        //ambil dari tabel skema
        $ranking = $this->model_basic->select_where_array_order($this->tbl_nilai_akhir_peserta, [
            'pendidikan_id' => $pendidikan_id,
            'skema_penilaian_id' => $skema_penilaian_id
        ], 'npa', 'DESC')->result();
        foreach ($ranking as $data_row) {
            $peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $data_row->pendidikan_peserta_id);
            if($peserta->num_rows() > 0)
                $data_row->peserta_name = $peserta->row()->nama_lengkap;
            else
                $data_row->peserta_name = 'Data tidak ditemukan';
        }
        // echo "<pre>";
        // print_r($ranking);
        // echo "</pre>";
        // die;
        $data['ranking'] = $ranking;

        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_bagan_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    public function ajax_hasil_bagan_list()
    {
        $list = $this->model_hasil_bagan_list->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;

            $row = array();
            $row[] = $no;
            $row[] = $data_row->tanggal;
            $row[] = ($data_row->gelar_depan != "" ? $data_row->gelar_depan.' ' : '').$data_row->penilai.($data_row->gelar_belakang != "" ? ', '.$data_row->gelar_belakang : '');
            $row[] = $data_row->tim;
            $row[] = $data_row->bidang_studi;
            $row[] = '<a href="pendidikan_hasil/hasil_bagan_detail/'.$data_row->id.'" class="btn btn-success btn-xs" style="">Detail</a>';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_hasil_bagan_list->count_all(),
                        "recordsFiltered" => $this->model_hasil_bagan_list->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function hasil_bagan_detail($id){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil', 'hasil');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $elemen_penilaian = $this->model_basic->select_where($this->tbl_elemen_penilaian, 'id', $this->session->userdata('var')['elemen_penilaian_id']);
        if($elemen_penilaian->num_rows() == 1)
            $data['elemen_penilaian'] = $elemen_penilaian->row()->name;
        else
            $data['elemen_penilaian'] = 'Unknown';

        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $id)->row();

        //add var to session
        $var = array(
        'jadwal_id'        => $id,
        'penilai_id'        => $jadwal->penilai_id,
        'tim_id'            => $jadwal->tim_id,
        'bidang_studi_id'   => $jadwal->bidang_studi_id
        );
        $this->session->set_userdata('jadwal', $var);

        $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_title();
        $title = $get_elemen_penilaian->result();
        foreach ($title as $data_row) {
            $check_child = $this->model_hasil_bagan_detail->bagan_detail_child($data_row->elemen_penilaian_id);
            if($check_child->num_rows() == 0){
                $data_row->child = null;
            }else{
                $data_row->child = $check_child->result();
            }
        }
        $data['title'] = $title;
        
        $bidang_studi_status = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id'])->row()->status_asing;
        // get peserta list
        $row = array();
        $no = 0;
        $peserta = $this->model_hasil_bagan_detail->get_peserta_from_pendidikan_peserta($bidang_studi_status)->result();
        foreach ($peserta as $peserta_row) {
            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            foreach($get_elemen_penilaian->result() as $elemen_row){

                $check_child = $this->model_hasil_bagan_detail->bagan_detail_child($elemen_row->elemen_penilaian_id);
                if($check_child->num_rows() == 0){
                    $peserta_row->child = null;
                    $peserta_row->elemen_penilaian_id[] = $elemen_row->elemen_penilaian_id;

                    $nilai_non_akademis = $this->model_hasil_bagan_detail->get_nilai_non_akademis($peserta_row->id, $elemen_row->elemen_penilaian_id);
                    if($nilai_non_akademis->num_rows() == 0){
                            $peserta_row->nilai_non_akademis[] = 0;
                            $peserta_row->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + 0;
                    }else{
                            $peserta_row->nilai_non_akademis[] = $nilai_non_akademis->row()->nilai;
                            $peserta_row->keterangan[] = $nilai_non_akademis->row()->keterangan;
                            $peserta_row->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + $nilai_non_akademis->row()->nilai_bobot;
                            $nilai_akhir_non_akademis = $peserta_row->nilai_akhir_non_akademis;
                    }
                }else{
                    $peserta_row->child = $check_child->result();

                    foreach ($peserta_row->child as $child_row) {
                        $peserta_row->elemen_penilaian_id[] = $child_row->elemen_penilaian_id;
                        $nilai_akademis = $this->model_hasil_bagan_detail->get_nilai_akademis($peserta_row->id, $child_row->elemen_penilaian_id);
                        if($nilai_akademis->num_rows() == 0){
                                $peserta_row->nilai_akademis[] = 0;
                                $peserta_row->nilai_akhir_akademis = $nilai_akhir_akademis + 0;
                        }else{
                                $peserta_row->nilai_akademis[] = $nilai_akademis->row()->nilai;
                                $peserta_row->keterangan[] = $nilai_akademis->row()->keterangan;
                                $peserta_row->nilai_akhir_akademis = $nilai_akhir_akademis + $nilai_akademis->row()->nilai_bobot;
                                $nilai_akhir_akademis = $peserta_row->nilai_akhir_akademis;
                        }
                    }
                }
            }
        }
        $data['list'] = $peserta;

        //get header
        $jadwal = $this->model_basic->select_where($this->tbl_jadwal, 'id', $this->session->userdata('jadwal')['jadwal_id']);
        if($jadwal->num_rows() == 1)
            $data['jadwal'] = $jadwal->row()->tanggal;
        else
            $data['jadwal'] = 'Unknown';

        $bidang_studi = $this->model_basic->select_where($this->tbl_bidang_studi, 'id', $this->session->userdata('jadwal')['bidang_studi_id']);
        if($bidang_studi->num_rows() == 1)
            $data['bidang_studi'] = $bidang_studi->row()->name;
        else
            $data['bidang_studi'] = 'Unknown';

        $tim = $this->model_basic->select_where($this->tbl_tim, 'id', $this->session->userdata('jadwal')['tim_id']);
        if($tim->num_rows() == 1)
            $data['tim'] = $tim->row()->name;
        else
            $data['tim'] = 'Unknown';

        $penilai = $this->model_basic->select_where($this->tbl_penilai, 'id', $this->session->userdata('jadwal')['penilai_id']);
        if($penilai->num_rows() == 1)
            $data['penilai'] = ($penilai->row()->gelar_depan != "" ? $penilai->row()->gelar_depan.' ' : '').$penilai->row()->nama_lengkap.($penilai->row()->gelar_belakang != "" ? ', '.$penilai->row()->gelar_belakang : '');
        else
            $data['penilai'] = 'Unknown';
        
        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_bagan_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function hasil_peserta() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil Peserta', 'hasil_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['peserta'] = $this->model_basic->select_all($this->tbl_pendidikan_peserta);

        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_peserta_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function hasil_peserta_detail_list($id){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil Peserta', 'hasil_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['peserta'] = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $id)->row();

        $data['skema_penilaian'] = $this->model_basic->select_where($this->tbl_skema_penilaian, 'is_raport', 1)->result();

        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_peserta_detail_list', $data, true);
        $this->load->view('backend/index', $data);  
    }

    function hasil_peserta_detail_blok($peserta_id,$skema_penilaian_id){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Hasil Peserta', 'hasil_peserta');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['peserta_id'] = $peserta_id;
        $data['skema_penilaian_id'] = $skema_penilaian_id;

        $data['peserta'] = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $peserta_id)->row();
        if($data['peserta']->tutor_penilai_id != 0){
            $tutor = $this->model_basic->select_where($this->tbl_penilai, 'id', $data['peserta']->tutor_penilai_id);
            if($tutor->num_rows() == 1){
                $data['peserta']->tutor_name = $tutor->row()->gelar_depan.' '.$tutor->row()->nama_lengkap.($tutor->row()->gelar_belakang!="" ? ', '.$tutor->row()->gelar_belakang : '');
                $pangkat = $this->model_basic->select_where($this->tbl_pangkat, 'id', $tutor->row()->pangkat_id);
                $data['peserta']->tutor_pangkat = $pangkat->row()->name;
            }
            else
                $data['peserta']->tutor_name = 'Data tidak ditemukan.';
                $data['peserta']->tutor_pangkat = 'Data tidak ditemukan.';
        }else{
            $data['peserta']->tutor_name = 'Data tidak ditemukan.';
            $data['peserta']->tutor_pangkat = 'Data tidak ditemukan.';
        }

        $skema_penilaian_detail = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $skema_penilaian_id)->row();
        $data['detail'] = $skema_penilaian_detail;

        $sp_child = $this->model_basic->select_where_order($this->tbl_skema_penilaian, 'parent_id', $skema_penilaian_id, 'id', 'DESC')->result();

        $raport = array();
        $total_nilai_akhir_akademis = 0;
        $total_nilai_akhir_non_akademis = 0;
        foreach ($sp_child as $sp_child_row) {
            $data_akademik_child = array();
            $data_non_akademik_child = array();

            $var = array(
            'pendidikan_id'         => $this->session->userdata('menu_pendidikan')['pendidikan_id'],
            'bagan_pendidikan_id'   => $this->session->userdata('menu_pendidikan')['bagan_pendidikan_id'],
            'skema_penilaian_id'    => $sp_child_row->id,
            );
            $this->session->set_userdata('var', $var);

            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            $data_raport = new stdClass;
            $get_data_raport = $this->model_rev->get_data_skema_penilaian_detail_one_level($this->session->userdata('var')['pendidikan_id'],$this->session->userdata('var')['bagan_pendidikan_id'],$this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
            if($sp_child_row->is_raport == 1){
                $data_akademik_child = [];
                $data_non_akademik_child = [];
            }else{
                foreach($get_data_raport->tree as $elemen_row){
                    $data_raport->skema_penilaian_id[] = $elemen_row->id;
                    $data_raport->skema_penilaian_name[] = $elemen_row->name;

                    $data_raport->nilai_non_akademis[] = $elemen_row->nna;
                    $data_raport->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + ($elemen_row->bobot_nna*$elemen_row->nna/100);
                    $nilai_akhir_non_akademis = $data_raport->nilai_akhir_non_akademis;

                    $data_raport->nilai_akademis[] = $elemen_row->npa;
                    $data_raport->nilai_akhir_akademis = $nilai_akhir_akademis + ($elemen_row->bobot_npa*$elemen_row->npa/100);
                    $nilai_akhir_akademis = $data_raport->nilai_akhir_akademis;

                    $min_max_avg = $this->get_min_max_avg_skema($elemen_row->id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
                    $rank_npa = $this->get_rank_skema_npa_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
                    $rank_nna = $this->get_rank_skema_nna_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

                    $data_akademik_child[] = array(
                        'skema_penilaian_id' => $elemen_row->id, 
                        'skema_penilaian_name' => $elemen_row->name,
                        'nilai_akademis' => $elemen_row->npa, 
                        'rank_npa' => (isset($rank_npa->rank) ? $rank_npa->rank : 0),
                        'min_max_avg' => $min_max_avg
                    );

                    $data_non_akademik_child[] = array(
                        'skema_penilaian_id' => $elemen_row->id, 
                        'skema_penilaian_name' => $elemen_row->name,
                        'nilai_non_akademis' => $elemen_row->nna, 
                        'rank_nna' => (isset($rank_nna->rank) ? $rank_nna->rank : 0),
                        'min_max_avg' => $min_max_avg
                    );
                }
            }

            $parent_min_max_avg = $this->get_min_max_avg_skema($sp_child_row->id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
            $parent_rank_npa = $this->get_rank_skema_npa_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
            $parent_rank_nna = $this->get_rank_skema_nna_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

            $data_akademik_parent = array(
                'skema_penilaian_id' => $sp_child_row->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_akademis' => $get_data_raport->nilai_akademis, 
                'rank_npa' => (isset($parent_rank_npa->rank) ? $parent_rank_npa->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );
            $data_non_akademik_parent = array(
                'skema_penilaian_id' => $sp_child_row->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_non_akademis' => $get_data_raport->nilai_non_akademis, 
                'rank_nna' => (isset($parent_rank_nna->rank) ? $parent_rank_nna->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );

            $total_nilai_akhir_non_akademis = $total_nilai_akhir_non_akademis + ($sp_child_row->bobot_nna*$get_data_raport->nilai_non_akademis/100);
            $total_nilai_akhir_akademis = $total_nilai_akhir_akademis + ($sp_child_row->bobot_npa*$get_data_raport->nilai_akademis/100);

            $arr_data_ak_child[] = (Object) ['parent' => (Object) $data_akademik_parent, 'child' => $data_akademik_child];
            $arr_data_nak_child[] = (Object) ['parent' => (Object) $data_non_akademik_parent, 'child' => $data_non_akademik_child];
        }


        //update nilai akhir blok
        $this->add_edit_nilai_skema($peserta_id, $skema_penilaian_id, $total_nilai_akhir_non_akademis, $total_nilai_akhir_akademis);
        $nilai_akhir_min_max_avg = $this->get_min_max_avg_skema($skema_penilaian_id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
        $nilai_akhir_rank_npa = $this->get_rank_skema_npa_single($skema_penilaian_id,$peserta_id);
        $nilai_akhir_rank_nna = $this->get_rank_skema_nna_single($skema_penilaian_id,$peserta_id);

        $data_akhir_non_akademik = array(
            'skema_penilaian_id' => $skema_penilaian_detail->id, 
            'skema_penilaian_name' => $skema_penilaian_detail->name,
            'nilai_non_akademis' => $total_nilai_akhir_non_akademis, 
            'rank_nna' => $nilai_akhir_rank_nna->rank,
            'min_max_avg' => $nilai_akhir_min_max_avg
        );

        $data_akhir_akademik = array(
            'skema_penilaian_id' => $skema_penilaian_detail->id, 
            'skema_penilaian_name' => $skema_penilaian_detail->name,
            'nilai_akademis' => $total_nilai_akhir_akademis, 
            'rank_npa' => $nilai_akhir_rank_npa->rank,
            'min_max_avg' => $nilai_akhir_min_max_avg
        );

        $data_nilai_akhir_akademik = (Object )['parent' => (Object) $data_akhir_akademik, 'child' => [] ];
        $data_nilai_akhir_non_akademik = (Object )['parent' => (Object) $data_akhir_non_akademik, 'child' => [] ];

        array_push($arr_data_ak_child, $data_nilai_akhir_akademik);
        array_push($arr_data_nak_child, $data_nilai_akhir_non_akademik);

        $check_numpang_raport = $this->model_basic->select_where($this->tbl_skema_penilaian, 'is_other_raport', $skema_penilaian_id);
        if($check_numpang_raport->num_rows() > 0){
            $data_numpang = $check_numpang_raport->row();

            $data_akademik_child = array();
            $data_non_akademik_child = array();

            $var = array(
            'pendidikan_id'         => $this->session->userdata('menu_pendidikan')['pendidikan_id'],
            'bagan_pendidikan_id'   => $this->session->userdata('menu_pendidikan')['bagan_pendidikan_id'],
            'skema_penilaian_id'    => $data_numpang->id,
            );
            $this->session->set_userdata('var', $var);

            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            $data_raport = new stdClass;
            $get_data_raport = $this->model_rev->get_data_skema_penilaian_detail_self($this->session->userdata('var')['pendidikan_id'],$this->session->userdata('var')['bagan_pendidikan_id'],$this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

            $parent_min_max_avg = $this->get_min_max_avg_skema($data_numpang->id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
            $parent_rank_npa = $this->get_rank_skema_npa_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
            $parent_rank_nna = $this->get_rank_skema_nna_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

            $data_akademik_parent = array(
                'skema_penilaian_id' => $data_numpang->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_akademis' => $get_data_raport->nilai_akademis, 
                'rank_npa' => (isset($parent_rank_npa->rank) ? $parent_rank_npa->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );
            $data_non_akademik_parent = array(
                'skema_penilaian_id' => $data_numpang->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_non_akademis' => $get_data_raport->nilai_non_akademis, 
                'rank_nna' => (isset($parent_rank_nna->rank) ? $parent_rank_nna->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );

            $data_nilai_akhir_akademik = (Object )['parent' => (Object) $data_akademik_parent, 'child' => [] ];
            $data_nilai_akhir_non_akademik = (Object )['parent' => (Object) $data_non_akademik_parent, 'child' => [] ];

            array_push($arr_data_ak_child, $data_nilai_akhir_akademik);
            array_push($arr_data_nak_child, $data_nilai_akhir_non_akademik);
        }

        $data['raport'] = (Object) ['data_akademik' => $arr_data_ak_child, 'data_non_akademik' => $arr_data_nak_child];

        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_peserta_detail_blok', $data, true);
        $this->load->view('backend/index', $data);   
    }

    function print_raport_blok($peserta_id,$skema_penilaian_id){

        $data['peserta_id'] = $peserta_id;
        $data['skema_penilaian_id'] = $skema_penilaian_id;

        $data['peserta'] = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'id', $peserta_id)->row();
        if($data['peserta']->tutor_penilai_id != 0){
            $tutor = $this->model_basic->select_where($this->tbl_penilai, 'id', $data['peserta']->tutor_penilai_id);
            if($tutor->num_rows() == 1){
                $data['peserta']->tutor_name = $tutor->row()->gelar_depan.' '.$tutor->row()->nama_lengkap.($tutor->row()->gelar_belakang!="" ? ', '.$tutor->row()->gelar_belakang : '');
                $pangkat = $this->model_basic->select_where($this->tbl_pangkat, 'id', $tutor->row()->pangkat_id);
                $data['peserta']->tutor_pangkat = $pangkat->row()->name;
            }
            else
                $data['peserta']->tutor_name = 'Data tidak ditemukan.';
        }else{
            $data['peserta']->tutor_name = 'Data tidak ditemukan.';
        }

        $skema_penilaian_detail = $this->model_basic->select_where($this->tbl_skema_penilaian, 'id', $skema_penilaian_id)->row();
        $data['detail'] = $skema_penilaian_detail;

        $sp_child = $this->model_basic->select_where_order($this->tbl_skema_penilaian, 'parent_id', $skema_penilaian_id, 'id', 'DESC')->result();

        $raport = array();
        $total_nilai_akhir_akademis = 0;
        $total_nilai_akhir_non_akademis = 0;
        foreach ($sp_child as $sp_child_row) {
            $data_akademik_child = array();
            $data_non_akademik_child = array();

            $var = array(
            'pendidikan_id'         => $this->session->userdata('menu_pendidikan')['pendidikan_id'],
            'bagan_pendidikan_id'   => $this->session->userdata('menu_pendidikan')['bagan_pendidikan_id'],
            'skema_penilaian_id'    => $sp_child_row->id,
            );
            $this->session->set_userdata('var', $var);

            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            $data_raport = new stdClass;
            $get_data_raport = $this->model_rev->get_data_skema_penilaian_detail_one_level($this->session->userdata('var')['pendidikan_id'],$this->session->userdata('var')['bagan_pendidikan_id'],$this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
            if($sp_child_row->is_raport == 1){
                $data_akademik_child = [];
                $data_non_akademik_child = [];
            }else{
                foreach($get_data_raport->tree as $elemen_row){
                    $data_raport->skema_penilaian_id[] = $elemen_row->id;
                    $data_raport->skema_penilaian_name[] = $elemen_row->name;

                    $data_raport->nilai_non_akademis[] = $elemen_row->nna;
                    $data_raport->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + ($elemen_row->bobot_nna*$elemen_row->nna/100);
                    $nilai_akhir_non_akademis = $data_raport->nilai_akhir_non_akademis;

                    $data_raport->nilai_akademis[] = $elemen_row->npa;
                    $data_raport->nilai_akhir_akademis = $nilai_akhir_akademis + ($elemen_row->bobot_npa*$elemen_row->npa/100);
                    $nilai_akhir_akademis = $data_raport->nilai_akhir_akademis;

                    $min_max_avg = $this->get_min_max_avg_skema($elemen_row->id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
                    $rank_npa = $this->get_rank_skema_npa_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
                    $rank_nna = $this->get_rank_skema_nna_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

                    $data_akademik_child[] = array(
                        'skema_penilaian_id' => $elemen_row->id, 
                        'skema_penilaian_name' => $elemen_row->name,
                        'nilai_akademis' => $elemen_row->npa, 
                        'rank_npa' => (isset($rank_npa->rank) ? $rank_npa->rank : 0),
                        'min_max_avg' => $min_max_avg
                    );

                    $data_non_akademik_child[] = array(
                        'skema_penilaian_id' => $elemen_row->id, 
                        'skema_penilaian_name' => $elemen_row->name,
                        'nilai_non_akademis' => $elemen_row->nna, 
                        'rank_nna' => (isset($rank_nna->rank) ? $rank_nna->rank : 0),
                        'min_max_avg' => $min_max_avg
                    );
                }
            }

            $parent_min_max_avg = $this->get_min_max_avg_skema($sp_child_row->id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
            $parent_rank_npa = $this->get_rank_skema_npa_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
            $parent_rank_nna = $this->get_rank_skema_nna_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

            $data_akademik_parent = array(
                'skema_penilaian_id' => $sp_child_row->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_akademis' => $get_data_raport->nilai_akademis, 
                'rank_npa' => (isset($parent_rank_npa->rank) ? $parent_rank_npa->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );
            $data_non_akademik_parent = array(
                'skema_penilaian_id' => $sp_child_row->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_non_akademis' => $get_data_raport->nilai_non_akademis, 
                'rank_nna' => (isset($parent_rank_nna->rank) ? $parent_rank_nna->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );

            $total_nilai_akhir_non_akademis = $total_nilai_akhir_non_akademis + ($sp_child_row->bobot_nna*$get_data_raport->nilai_non_akademis/100);
            $total_nilai_akhir_akademis = $total_nilai_akhir_akademis + ($sp_child_row->bobot_npa*$get_data_raport->nilai_akademis/100);

            $arr_data_ak_child[] = (Object) ['parent' => (Object) $data_akademik_parent, 'child' => $data_akademik_child];
            $arr_data_nak_child[] = (Object) ['parent' => (Object) $data_non_akademik_parent, 'child' => $data_non_akademik_child];
        }


        //update nilai akhir blok
        $this->add_edit_nilai_skema($peserta_id, $skema_penilaian_id, $total_nilai_akhir_non_akademis, $total_nilai_akhir_akademis);
        $nilai_akhir_min_max_avg = $this->get_min_max_avg_skema($skema_penilaian_id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
        $nilai_akhir_rank_npa = $this->get_rank_skema_npa_single($skema_penilaian_id,$peserta_id);
        $nilai_akhir_rank_nna = $this->get_rank_skema_nna_single($skema_penilaian_id,$peserta_id);

        $data_akhir_non_akademik = array(
            'skema_penilaian_id' => $skema_penilaian_detail->id, 
            'skema_penilaian_name' => $skema_penilaian_detail->name,
            'nilai_non_akademis' => $total_nilai_akhir_non_akademis, 
            'rank_nna' => $nilai_akhir_rank_nna->rank,
            'min_max_avg' => $nilai_akhir_min_max_avg
        );

        $data_akhir_akademik = array(
            'skema_penilaian_id' => $skema_penilaian_detail->id, 
            'skema_penilaian_name' => $skema_penilaian_detail->name,
            'nilai_akademis' => $total_nilai_akhir_akademis, 
            'rank_npa' => $nilai_akhir_rank_npa->rank,
            'min_max_avg' => $nilai_akhir_min_max_avg
        );

        $data_nilai_akhir_akademik = (Object )['parent' => (Object) $data_akhir_akademik, 'child' => [] ];
        $data_nilai_akhir_non_akademik = (Object )['parent' => (Object) $data_akhir_non_akademik, 'child' => [] ];

        array_push($arr_data_ak_child, $data_nilai_akhir_akademik);
        array_push($arr_data_nak_child, $data_nilai_akhir_non_akademik);

        $check_numpang_raport = $this->model_basic->select_where($this->tbl_skema_penilaian, 'is_other_raport', $skema_penilaian_id);
        if($check_numpang_raport->num_rows() > 0){
            $data_numpang = $check_numpang_raport->row();

            $data_akademik_child = array();
            $data_non_akademik_child = array();

            $var = array(
            'pendidikan_id'         => $this->session->userdata('menu_pendidikan')['pendidikan_id'],
            'bagan_pendidikan_id'   => $this->session->userdata('menu_pendidikan')['bagan_pendidikan_id'],
            'skema_penilaian_id'    => $data_numpang->id,
            );
            $this->session->set_userdata('var', $var);

            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            $data_raport = new stdClass;
            $get_data_raport = $this->model_rev->get_data_skema_penilaian_detail_self($this->session->userdata('var')['pendidikan_id'],$this->session->userdata('var')['bagan_pendidikan_id'],$this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

            $parent_min_max_avg = $this->get_min_max_avg_skema($data_numpang->id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
            $parent_rank_npa = $this->get_rank_skema_npa_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);
            $parent_rank_nna = $this->get_rank_skema_nna_single($this->session->userdata('var')['skema_penilaian_id'],$data['peserta']->id);

            $data_akademik_parent = array(
                'skema_penilaian_id' => $data_numpang->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_akademis' => $get_data_raport->nilai_akademis, 
                'rank_npa' => (isset($parent_rank_npa->rank) ? $parent_rank_npa->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );
            $data_non_akademik_parent = array(
                'skema_penilaian_id' => $data_numpang->id, 
                'skema_penilaian_name' => $get_data_raport->name,
                'nilai_non_akademis' => $get_data_raport->nilai_non_akademis, 
                'rank_nna' => (isset($parent_rank_nna->rank) ? $parent_rank_nna->rank : 0),
                'min_max_avg' => $parent_min_max_avg
            );

            $data_nilai_akhir_akademik = (Object )['parent' => (Object) $data_akademik_parent, 'child' => [] ];
            $data_nilai_akhir_non_akademik = (Object )['parent' => (Object) $data_non_akademik_parent, 'child' => [] ];

            array_push($arr_data_ak_child, $data_nilai_akhir_akademik);
            array_push($arr_data_nak_child, $data_nilai_akhir_non_akademik);
        }

        $data['raport'] = (Object) ['data_akademik' => $arr_data_ak_child, 'data_non_akademik' => $arr_data_nak_child];

        $data['tanggal_cetak'] = $this->tanggal_indo(date('Y-m-d'));


        $html=$this->load->view('backend/print_document/raport_peserta', $data, true);

        ob_start();
        $pdfFilePath = "Form Raport.pdf";
        $this->load->library('m_pdf');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");

        $data['content'] = $this->load->view('backend/pendidikan_hasil/hasil_peserta_detail_blok', $data, true);
        $this->load->view('backend/index', $data);   
    }

    function refresh_ranking_blok1(){
        ini_set('max_execution_time', 120);

        $peserta = $this->model_basic->select_where($this->tbl_pendidikan_peserta, 'pendidikan_id', $this->session->userdata('var')['pendidikan_id'])->result();

        foreach ($peserta as $peserta_row) {
            $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_hasil_title_by_ep(1);

            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            $offcampus = new stdClass;
            //get data offcampus
            foreach($get_elemen_penilaian->result() as $elemen_row){
                
                $offcampus->elemen_penilaian_id_nna[] = $elemen_row->elemen_penilaian;
                $offcampus->elemen_penilaian_id_npa[] = $elemen_row->elemen_penilaian;

                $nilai_all_bs = $this->model_hasil_bagan_detail->get_hasil_nilai_all_bs($peserta_row->id, $elemen_row->elemen_penilaian_id);

                if($nilai_all_bs->num_rows() == 0){
                        $offcampus->nilai_non_akademis[] = 0;
                        $offcampus->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + 0;

                        $offcampus->nilai_akademis[] = 0;
                        $offcampus->nilai_akhir_akademis = $nilai_akhir_akademis + 0;
                }else{
                        $offcampus->nilai_non_akademis[] = $nilai_all_bs->row()->nna;
                        $offcampus->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + ($nilai_all_bs->row()->bobot_nna*$nilai_all_bs->row()->nna/100);
                        $nilai_akhir_non_akademis = $offcampus->nilai_akhir_non_akademis;

                        $offcampus->nilai_akademis[] = $nilai_all_bs->row()->npa;
                        $offcampus->nilai_akhir_akademis = $nilai_akhir_akademis + ($nilai_all_bs->row()->bobot_npa*$nilai_all_bs->row()->npa/100);
                        $nilai_akhir_akademis = $offcampus->nilai_akhir_akademis;
                }

                $offcampus->min_max_avg[] = $this->get_min_max_avg_elemen($elemen_row->elemen_penilaian_id,$this->session->userdata('menu_pendidikan')['pendidikan_id']);
            }

            $get_elemen_penilaian = $this->model_hasil_bagan_detail->get_hasil_title_by_ep(4);

            $nilai_akhir_akademis = 0;
            $nilai_akhir_non_akademis = 0;
            $blok1 = new stdClass;
            //get data blok 1
            foreach($get_elemen_penilaian->result() as $elemen_row){
                
                $blok1->elemen_penilaian_id_nna[] = $elemen_row->elemen_penilaian;
                $blok1->elemen_penilaian_id_npa[] = $elemen_row->elemen_penilaian;

                $nilai_all_bs = $this->model_hasil_bagan_detail->get_hasil_nilai_all_bs($peserta_row->id, $elemen_row->elemen_penilaian_id);

                if($nilai_all_bs->num_rows() == 0){
                        $blok1->nilai_non_akademis[] = 0;
                        $blok1->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + 0;

                        $blok1->nilai_akademis[] = 0;
                        $blok1->nilai_akhir_akademis = $nilai_akhir_akademis + 0;
                }else{
                        $blok1->nilai_non_akademis[] = $nilai_all_bs->row()->nna;
                        $blok1->nilai_akhir_non_akademis = $nilai_akhir_non_akademis + ($nilai_all_bs->row()->bobot_nna*$nilai_all_bs->row()->nna/100);
                        $nilai_akhir_non_akademis = $blok1->nilai_akhir_non_akademis;

                        $blok1->nilai_akademis[] = $nilai_all_bs->row()->npa;
                        $blok1->nilai_akhir_akademis = $nilai_akhir_akademis + ($nilai_all_bs->row()->bobot_npa*$nilai_all_bs->row()->npa/100);
                        $nilai_akhir_akademis = $blok1->nilai_akhir_akademis;
                }
            }

            $hasil = (Object) ['offcampus' => $offcampus, 'blok1' => $blok1];

            //nilai akhir blok 1
            $get_elemen_penilaian_akhir = $this->model_hasil_bagan_detail->get_hasil_akhir_title(7)->result();

            $naa_offcampus = $hasil->offcampus->nilai_akhir_akademis;
            $naa_blok1 = $hasil->blok1->nilai_akhir_akademis;

            $nnaa_offcampus = $hasil->offcampus->nilai_akhir_non_akademis;
            $nnaa_blok1 = $hasil->blok1->nilai_akhir_non_akademis;

            //update nilai offcampus
            $this->add_edit_nilai_skema($peserta_row->id, 9, $nnaa_offcampus, $naa_offcampus);
            //update nilai blok1
            $this->add_edit_nilai_skema($peserta_row->id, 8, $nnaa_blok1, $naa_blok1);

            $blok1->nilai_akhir_offcampus = ['npa' => $naa_offcampus, 'nna' => $nnaa_offcampus];
            $blok1->nilai_akhir_blok1 = ['npa' => $naa_blok1, 'nna' => $nnaa_blok1];

            $nilai_akhir_akademis = ($hasil->blok1->nilai_akhir_akademis * $get_elemen_penilaian_akhir[0]->bobot_npa / 100) + ($hasil->offcampus->nilai_akhir_akademis * $get_elemen_penilaian_akhir[1]->bobot_npa / 100);
            $nilai_akhir_non_akademis = ($hasil->blok1->nilai_akhir_non_akademis * $get_elemen_penilaian_akhir[0]->bobot_nna / 100) + ($hasil->offcampus->nilai_akhir_non_akademis * $get_elemen_penilaian_akhir[1]->bobot_nna / 100);
            
            $na_blok1 = (Object) ['hasil' => $hasil, 'nilai_akhir' => ['nna' => $nilai_akhir_non_akademis, 'npa' => $nilai_akhir_akademis]];

            //update nilai blok1
            $this->add_edit_nilai_skema($peserta_row->id, 7, $nilai_akhir_non_akademis, $nilai_akhir_akademis);

            echo $peserta_row->id.' done<br>';
        } //end foreach peserta
    }

    function add_edit_nilai_skema($peserta_id, $skema_penilaian_id, $nna, $npa){
        $check = $this->model_basic->select_where_array($this->tbl_nilai_akhir_peserta, 
            array(
                'pendidikan_id'=>$this->session->userdata('var')['pendidikan_id'],
                'pendidikan_peserta_id' => $peserta_id,
                'skema_penilaian_id' => $skema_penilaian_id
        ));
        if($check->num_rows() != 0){
            //update data
            $update['npa'] = $npa;
            $update['nna'] = $nna;
            $update['date_modified'] = date('Y-m-d H:i:s', now());

            $do_update = $this->model_basic->update($this->tbl_nilai_akhir_peserta, $update, 'id', $check->row()->id);
        }else{
            //insert data
            $insert['pendidikan_id'] = $this->session->userdata('var')['pendidikan_id'];
            $insert['pendidikan_peserta_id'] = $peserta_id;
            $insert['skema_penilaian_id'] = $skema_penilaian_id;
            $insert['npa'] = $npa;
            $insert['nna'] = $nna;
            $insert['date_created'] = date('Y-m-d H:i:s', now());
            $insert['date_modified'] = date('Y-m-d H:i:s', now());

            $do_insert = $this->model_basic->insert_all($this->tbl_nilai_akhir_peserta, $insert);
        }
    }

    function get_rank_skema_npa_single($skema_penilaian_id,$pendidikan_peserta_id){
        $rank = $this->model_ranking->get_rank_skema_npa($skema_penilaian_id,$pendidikan_peserta_id);
        return $rank;
    }
    function get_rank_skema_nna_single($skema_penilaian_id,$pendidikan_peserta_id){
        $rank = $this->model_ranking->get_rank_skema_nna($skema_penilaian_id,$pendidikan_peserta_id);
        return $rank;
    }

    function get_rank_skema_npa($pendidikan_peserta_id){
        $offcampus = $this->model_ranking->get_rank_skema_npa(9,$pendidikan_peserta_id);
        $blok1 = $this->model_ranking->get_rank_skema_npa(8,$pendidikan_peserta_id);
        $hasil_akhir_blok1 = $this->model_ranking->get_rank_skema_npa(7,$pendidikan_peserta_id);

        $kjj = $this->model_ranking->get_rank_skema_npa(10,$pendidikan_peserta_id);
        $bsi = $this->model_ranking->get_rank_skema_npa(11,$pendidikan_peserta_id);
        $lingstra = $this->model_ranking->get_rank_skema_npa(12,$pendidikan_peserta_id);

        $rank = (Object) ['offcampus' => $offcampus, 'blok1' => $blok1, 'hasil_akhir_blok1' => $hasil_akhir_blok1, 'kjj' => $kjj, 'bsi' => $bsi, 'lingstra' => $lingstra];
        return $rank;
    }

    function get_rank_skema_nna($pendidikan_peserta_id){
        $offcampus = $this->model_ranking->get_rank_skema_nna(9,$pendidikan_peserta_id);
        $blok1 = $this->model_ranking->get_rank_skema_nna(8,$pendidikan_peserta_id);
        $hasil_akhir_blok1 = $this->model_ranking->get_rank_skema_nna(7,$pendidikan_peserta_id);

        $hasil_akhir_kjj = $this->model_ranking->get_rank_skema_nna(10,$pendidikan_peserta_id);
        $hasil_akhir_bsi = $this->model_ranking->get_rank_skema_nna(11,$pendidikan_peserta_id);
        $hasil_akhir_lingstra = $this->model_ranking->get_rank_skema_nna(12,$pendidikan_peserta_id);

        $rank = (Object) ['offcampus' => $offcampus, 'blok1' => $blok1, 'hasil_akhir_blok1' => $hasil_akhir_blok1];
        return $rank;
    }

    function get_min_max_avg_skema_blok1($id, $pendidikan_id){
        $offcampus = $this->model_ranking->get_min_max_avg(9,$pendidikan_id);
        $blok1 = $this->model_ranking->get_min_max_avg(8,$pendidikan_id);
        $hasil_akhir_blok1 = $this->model_ranking->get_min_max_avg(7,$pendidikan_id);

        $min_max_avg_blok1 = (Object) ['offcampus' => $offcampus, 'blok1' => $blok1, 'hasil_akhir_blok1' => $hasil_akhir_blok1];
        return $min_max_avg_blok1;
    }

    function get_min_max_avg_elemen($elemen_penilaian_id,$pendidikan_id){
        $min_max_avg = $this->model_ranking->get_min_max_avg_elemen_penilaian($elemen_penilaian_id,$pendidikan_id);

        return $min_max_avg;
    }

    function get_min_max_avg_skema($skema_penilaian_id,$pendidikan_id){
        $min_max_avg = $this->model_ranking->get_min_max_avg_skema_penilaian($skema_penilaian_id,$pendidikan_id);

        return $min_max_avg;
    }

}