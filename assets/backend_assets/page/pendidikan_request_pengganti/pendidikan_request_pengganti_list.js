
function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}
//datatables function
var save_method; //for save method string
var table;

table = $('.datatable').DataTable({
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "bLengthChange": false,
  "dom": 'lBfrtip',
  "lengthMenu": [
      [ 10, 25, 50, -1 ],
      [ '10 rows', '25 rows', '50 rows', 'Show all' ]
    ],
  "buttons": [
  {
    extend: 'pageLength'
  }],
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "pendidikan_request_pengganti/ajax_pendidikan_request_pengganti_list",
      "type": "POST"
  },
  //Set column definition initialisation properties.
  "order": [[ 1, "desc" ]],
  "columnDefs": [
  { 
    "targets": [0,8,-1], //last column
    "orderable": false, //set not orderable
  },
  {
    "className": "text-center",
    "targets": [ 0,1,2,3,4,5,6,7,8,-1 ], //add class
  }
  ]

});

$('body').delegate('#btn-status','click',function(){
    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');
    var admin_id = $(this).attr('data-admin');
    var penilai_pengganti_id = $(this).attr('data-penilai');
    var jadwal_id = $(this).attr('data-jadwal');
    $.ajax({
      url : 'pendidikan_request_pengganti/ajax_status',
      type : 'POST',
      dataType : 'json',
      data : {
          id:id,
          status:status,
          admin_id: admin_id,
          penilai_pengganti_id: penilai_pengganti_id,
          jadwal_id: jadwal_id
        },
      success : function(response){
        if(response.status == 'ok'){
          $('#px-pendidikan_request_pengganti .alert-success').show().children('span').text(response.msg);
          setTimeout(function() {
                  $('#px-pendidikan_request_pengganti .alert-success').hide("slow");
              }, 1000);
          if(response.status_now == 1){
            $('#btn-status').prop('disabled',true);
            $('#status_text_0').addClass('hidden');
            $('#status_text_1').removeClass('hidden');
          }
        }
        else{
          $('#px-pendidikan_request_pengganti .alert-danger').show().children('span').text(response.msg);
          setTimeout(function() {
                  $('#px-pendidikan_request_pengganti .alert-danger').hide("slow");
              }, 1000);
          }
      },
      error : function(jqXHR, textStatus, errorThrown) {
        alert(textStatus, errorThrown);
      }
    });
  });