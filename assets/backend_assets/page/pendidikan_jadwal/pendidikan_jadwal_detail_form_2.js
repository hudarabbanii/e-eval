$(document).ready(function(){
    $('#px-pendidikan_jadwal-jadwal_detail-form').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    });
    
    $('#px-pendidikan_jadwal-jadwal_detail-form').validate({
        rules: {
            id: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            var judul_produk = $('#judul_produk').val();
            $('#px-pendidikan_jadwal-jadwal_detail-form .alert-warning').removeClass('hidden');
            $('#px-pendidikan_jadwal-jadwal_detail-form .alert-success').addClass('hidden');
            $('#px-pendidikan_jadwal-jadwal_detail-form .alert-danger').addClass('hidden');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize()+'&judul='+judul_produk,
                success : function(response){
                    $('#px-pendidikan_jadwal-jadwal_detail-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-pendidikan_jadwal-jadwal_detail-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-pendidikan_jadwal-jadwal_detail-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
})
